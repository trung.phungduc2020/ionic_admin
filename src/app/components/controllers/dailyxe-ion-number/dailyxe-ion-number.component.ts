import { Pipe, PipeTransform, NgZone, EventEmitter, Output } from '@angular/core';
const padding = "000000";

@Pipe({
  name: 'mycurrency'
})
export class MycurrencyPipe implements PipeTransform {
  private decimal_separator: string;
  private thousands_separator: string;

  constructor() {
    this.decimal_separator = '.';
    this.thousands_separator = ',';
  }
  transform(value: string, fractionSize: number = 0): string {

    if (parseFloat(value) % 1 != 0) {
      fractionSize = 2;
    }
    let [integer, fraction = ""] = (parseFloat(value).toString() || "").toString().split(".");

    fraction = fractionSize > 0
      ? this.decimal_separator + (fraction + padding).substring(0, fractionSize) : "";
    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.thousands_separator);
    if (isNaN(parseFloat(integer))) {
      integer = null;
    }
    return integer;

  }

  parse(value: string, fractionSize: number = 0): string {
    let [integer, fraction = ""] = (value || "").split(this.decimal_separator);

    integer = integer.replace(new RegExp(this.thousands_separator, "g"), "");

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this.decimal_separator + (fraction + padding).substring(0, fractionSize)
      : "";
    return integer + fraction;
  }

}

import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, ChangeDetectorRef, HostListener, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { CommonService } from 'src/app/forms/member/services/common.service';
import { isNumber } from 'util';
import { maxValueNumberDefault } from 'src/app/core/variables';


const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonNumberComponent),
  multi: true
};

@Component({
  selector: 'dailyxe-ion-number',
  templateUrl: './dailyxe-ion-number.component.html',
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, MycurrencyPipe]
})
export class DaiLyXeIonNumberComponent implements ControlValueAccessor, OnChanges {
  //#region ================= Variables ===============================
  @Input() placeholder: string;
  @Input() min = Number.MIN_SAFE_INTEGER;
  @Input() max = maxValueNumberDefault;
  @Output() onChange = new EventEmitter<any>();
  @Input() options: any = {};
  @Input() clearInput: boolean = true;
  public messageError:string;
  private _optionsDef: any ={
    hiddenSuggestions: false,  
    isRequired: false,
    title: ""
  };
  // Allow decimal numbers and negative values
  private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', '-', 'Home', 'ArrowLeft', 'ArrowRight'];
  //#endregion =============== End variablse ==========================
  constructor(
    public commonService: CommonService,
    public commonMethodsTs: CommonMethodsTs,
    public ref: ChangeDetectorRef,
    private formatcurrencypipe: MycurrencyPipe,
    private ngZone: NgZone,
    private _el: ElementRef
  ) {
  }
  private limitValue(value: number): number {
    if (isNumber(this.max) && value > this.max) {
      return this.max;
    }

    if (isNumber(this.min) && value < this.min) {
      return this.min;
    }

    return value;
  }

  async ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.options = Object.assign(this._optionsDef, this.options);

  }
  async ionViewWillEnter() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    //await this.init();
  }

  ngOnInit() {
    // this.innerValue = this.formatcurrencypipe.transform(this.value);
  }

  // onChange(data) {
  //   this.value = this.formatcurrencypipe.parse(data);
  // }
  //The internal data model
  public innerValue: any;

  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  //get accessor
  get value(): any {
    return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
    let number: any = this.formatcurrencypipe.parse(v);
    number = this.limitValue(number);
    let d = this.formatcurrencypipe.transform(number);
    if (v !== this.innerValue) {
      if (number == this.formatcurrencypipe.parse(this.innerValue)) {
        this.innerValue = null;
      }
      setTimeout(() => {
        this.innerValue = d;
      }, 0);
      
      this.onChangeCallback(number);
      this.checkError(number)
      this.onChange.emit(number);
    }
  }
  checkError(number)
  {
    this.messageError = "";

    if(+this.max > 0)
    {
      if(this.innerValue && number > this.max)
      {
        this.messageError = "Giá trị"+` ${this.options.title || 'dữ liệu'} tối đa ${this.max}`.toLowerCase();
      }
    }

    if(+this.min > 0 && number < this.min)
    {
      this.messageError = "Giá trị"+` ${this.options.title || 'dữ liệu'} tối thiểu ${this.min}`.toLowerCase();

    }
    if(!this.options.hiddenSuggestions && !this.innerValue && this.options.isRequired)
    {
      this.messageError = `${this.options.title || 'Dữ liệu'} không được bỏ trống`.toLowerCase();

    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.value = this.formatcurrencypipe.transform(value);
   
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }


}
