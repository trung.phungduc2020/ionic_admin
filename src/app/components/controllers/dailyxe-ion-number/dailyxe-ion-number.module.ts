import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaiLyXeIonNumberComponent, MycurrencyPipe } from './dailyxe-ion-number.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NumberOnlyLoadModule } from 'src/app/shared/directive/number-only/numbers-only.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NumberOnlyLoadModule
  ],
  declarations: [DaiLyXeIonNumberComponent, MycurrencyPipe],
  exports: [DaiLyXeIonNumberComponent, MycurrencyPipe],
})
export class DaiLyXeIonNumberModule { }
