import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { DaiLyXeIonViolationTinDangComponent } from "./dailyxe-ion-violation-tindang.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DaiLyXeIonViolationTinDangComponent],
  entryComponents: [
    DaiLyXeIonViolationTinDangComponent
  ]
})
export class DaiLyXeIonViolationTinDangModule {
  constructor() {
  }


}
