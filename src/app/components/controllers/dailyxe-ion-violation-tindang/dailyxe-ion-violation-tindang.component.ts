import { linkNoiDung } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Input, Component } from '@angular/core';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'dailyxe-ion-violation-tindang',
  templateUrl: './dailyxe-ion-violation-tindang.component.html',
})
export class DaiLyXeIonViolationTinDangComponent {
  //#region ================================== Variable =========================
  @Input() options: any = {};
  data: any = {
    content: ""
  };
  isShowTiepTuc: boolean = true;
  linkNoiDung = linkNoiDung;
  labelPages = labelPages;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController
    ) {
  }
  ionViewDidEnter() {
    
  }
  closeModal() {
    // this.selectedItem = item;
    this.dismiss()
  }

  send() {
    // this.selectedItem = item;
    this.dismiss(this.data);
  }

  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }
}
