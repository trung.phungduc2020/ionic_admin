import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaiLyXeIonImageComponent } from './dailyxe-ion-image.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'src/app/shared/directive/tooltip/tooltip';
import { CropImageModule } from './crop-image-modal/crop-image-modal.module';
import { DaiLyXeIonImageModal } from './dailyxe-ion-image.modal';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TooltipModule,
    CropImageModule
  ],
  providers:[DaiLyXeIonImageModal],
  declarations: [DaiLyXeIonImageComponent],
  exports: [DaiLyXeIonImageComponent],
})
export class DaiLyXeIonImageModule { }
