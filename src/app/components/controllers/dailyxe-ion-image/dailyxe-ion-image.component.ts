import { Variables, defaultWxh } from '../../../core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, ChangeDetectorRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { CropImageModal } from './crop-image-modal/crop-image-modal.page';
import { Files } from 'src/app/core/entity';
import { AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { CoreProcess } from 'src/app/core/core-process';
import { PictureMethodsTs } from 'src/app/core/picture-methods';
import { ModalController } from '@ionic/angular';
const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonImageComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-ion-image',
  templateUrl: './dailyxe-ion-image.component.html',
  styleUrls: ['./dailyxe-ion-image.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DaiLyXeIonImageComponent implements ControlValueAccessor, OnChanges {
  //#region ================= Variables ===============================
  @Input() placeholder: string;
  public srcImage: string;
  @Input() style: any = {
    'max-width': '500px',
    'height': '200px'
  };
  @Input() enabledUpload: any = true;
  @Input() options: any = {};
  private optionsDefault: any = {
    "rewriteUrl": "new-image",
    "enabledUpload": true
  };
  @Input() fileName: string = Variables.defaultFileName;

  @Output() onChange = new EventEmitter<any>();
  @Output() onChangeImage = new EventEmitter<any>();
  @Output() onRemoveImage = new EventEmitter<any>();
  // Upload Task 
  task: AngularFireUploadTask;
  // Progress in percentage
  percentage: Observable<number>;
  //Status check 
  isUploading: boolean;
  //#region Crop Image
  //#endregion =============== End variablse ==========================
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public pictureMethodsTs: PictureMethodsTs,
    public coreProcess: CoreProcess,
    public modalController: ModalController
  ) {
  }

  async chooseImage() {
    if (!this.enabledUpload) {
      return;
    }
    try {
      let base64Image: any = await this.pictureMethodsTs.actionChoosePictureSingal();
      if (base64Image) {
        // this.srcImage = base64Image;
        this.value = base64Image;
        // this.onChangeImage.emit(this.srcImage);
        return;
      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
  }
  async removeImage() {
    this.onRemoveImage.emit(this.value);
    this.srcImage = null;
    this.value = null;

  }
  async cropImage() {
    let imageBase64 = this.srcImage;
    let id = this.commonMethodsTs.getIdByUrlDailyXe(imageBase64);
    if (+id > 0) {
      imageBase64 = this.commonMethodsTs.formatUrlImage(imageBase64, defaultWxh.regular, true);
      imageBase64 = await this.commonMethodsTs.getBase64ImageFromURL(imageBase64);
    }
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: CropImageModal,
        componentProps: {
          imageBase64: imageBase64,
          options: {
          }
        }
      });

    modal.onDidDismiss().then(async (res: any) => {
      if (res !== null && res.data != null) {
        this.value = res.data;
      }
    });

    await modal.present();
  }
  async ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.options = Object.assign(this.options, this.optionsDefault);
  }
  async ionViewWillEnter() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    //await this.init();
    this.options = Object.assign(this.options, this.optionsDefault);
  }
  ngOnInit() {
    // this.innerValue = this.formatcurrencypipe.transform(this.value);
  }
  onIonBlur() {
  }
  //The internal data model
  public innerValue: any;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.srcImage = this.innerValue;
      this.onChangeCallback(this.innerValue);
      this.onChangeImage.emit(this.srcImage);
    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.value = value;
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
  async uploadImage(id?: any) {
    let f = new Files();
    let url = this.srcImage;
    let fileName = this.fileName;
    fileName = fileName.convertrUrlPrefix();
    let idByUrlDailyXe = this.commonMethodsTs.getIdByUrlDailyXe(url);
    id = +id > 0 ? id : (idByUrlDailyXe || -1);
    if (!url) {
      return id
    }

    if (!(id > 0)) {
      f.Id = 0;
      f.Name = fileName;
      /*tronghuu95 20190830162100 lý do là sẳn dùng để backup dữ liệu luôn */
      id = (await this.coreProcess.insertListImageToServerQuickByFiles([f]))[0];
    }
    this.isUploading = true;
    try {
      if (+id > 0) {
        url = await this.commonMethodsTs.convertImageResizer(url);
        let task = this.coreProcess.uploadFileForFireStore(url, fileName);
        this.percentage = task.percentageChanges();
        await task.snapshotChanges().toPromise();
        let filePath = await this.coreProcess.getDownloadURLForFireStore(fileName).toPromise();
        f.Id = id;
        f.ImageUrl = filePath;
        await this.coreProcess.updateListImageUrl([f]);

      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);

    }
    this.isUploading = false;
    return id;
  }
}
