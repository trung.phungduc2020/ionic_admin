import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import { CropImageModal } from './crop-image-modal.page';
import { ImageCropperModule } from 'ngx-image-cropper';
// The modal's component of the previous chapter
@NgModule({
     declarations: [
      CropImageModal,
      
     ],
     imports: [
       IonicModule,
       CommonModule,
       ImageCropperModule
     ],
     entryComponents: [
			CropImageModal
     ]
})
export class CropImageModule {}