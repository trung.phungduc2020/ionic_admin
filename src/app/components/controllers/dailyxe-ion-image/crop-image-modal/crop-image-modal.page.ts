import { Component, ViewChild } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ImageCropperComponent, ImageCroppedEvent } from 'ngx-image-cropper';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'app-crop-image-modal',
	templateUrl: './crop-image-modal.page.html',
	styleUrls: ['./crop-image-modal.page.scss'],
})
export class CropImageModal {

	// #region =============== Variables ====
	public lbl = labelPages;
	public imageBase64: any = ''; // iamge base 64
	public croppedImage: any = '';
	public showCropper = false;	
	public options:any = {};
	public idImage:any;
	public aspectRatio = 3/2;
	private optionsDefault: any = {
		'title': "Chỉnh sửa ảnh",
		'format': 'jpeg',
		'formatUrl': 'builLinkImage',
		'lAspectRatio': [{label: "3/2", value: 3/2}, {label: "4/3", value: 4/3}]
	};
	//#endregion ============ End variables ===
	constructor(
		public navParams: NavParams,
		public commonMethodsTs: CommonMethodsTs,
		public modalController: ModalController) {
	}



	@ViewChild(ImageCropperComponent, { static: true }) imageCropper: ImageCropperComponent;

	onChangeAspectRatio(item)
	{
		this.aspectRatio = item;
	}

	imageCropped(event: ImageCroppedEvent) {
		this.croppedImage = event.base64;
	}

	imageLoaded() {
		this.showCropper = true;
	}

	cropperReady() {
		console.log('Cropper ready')
	}

	loadImageFailed() {
		console.log('Load failed');
	}
	
	ionViewWillEnter() {
		this.imageBase64 = this.navParams.get('imageBase64');
		this.options = this.navParams.get('options');
		this.options = Object.assign(this.optionsDefault, this.options);
	}

	cropImage() {
		this.dismiss(this.croppedImage)
	}

	closeModal() {
		this.dismiss()
	}

	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}