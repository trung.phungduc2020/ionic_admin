import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { CommonService } from 'src/app/forms/member/services/common.service';
import { maxLengthTextDefault } from 'src/app/core/variables';
const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonTextComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-ion-text',
  templateUrl: './dailyxe-ion-text.component.html',
  styleUrls: ['./dailyxe-ion-text.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DaiLyXeIonTextComponent implements ControlValueAccessor, OnChanges {
  //#region ================= Variables ===============================
  @Input() label: string = "";
  @Input() placeholder: string;
  @Input() isRequired: boolean;
  @Input() disabled: boolean = false;
  @Input() maxlength: number = maxLengthTextDefault;
  @Input() minlength: number;
  @Input() options: any = {};
  @Input() clearInput: boolean = true;
  public messageError:string;
  private _optionsDef: any ={
    hiddenSuggestions: false,  
    isRequired: false,
    title: ""
  };

  //#endregion =============== End variablse ==========================
  constructor(
    public commonService: CommonService,
    public commonMethodsTs: CommonMethodsTs,
    public ref: ChangeDetectorRef,
  ) {
  }
  
  async ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.options = Object.assign(this._optionsDef, this.options);
  }
  async ionViewWillEnter() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    //await this.init();
  }
  ngOnInit() {
    // this.innerValue = this.formatcurrencypipe.transform(this.value);
  }
  

  //The internal data model
  public innerValue: any;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.checkError();
      this.onChangeCallback(this.innerValue);
    }
  }
  checkError()
  {
    this.messageError = "";

    if(+this.maxlength > 0)
    {
      if(this.innerValue && this.innerValue.length > this.maxlength)
      {
        this.messageError = "Chiều dài"+` ${this.options.title || 'chuỗi'} tối đa ${this.maxlength} ký tự`.toLowerCase();
      }
    }

    if(+this.minlength > 0 && this.innerValue.length < this.minlength)
    {
      this.messageError = "Chiều dài"+` ${this.options.title || 'chuỗi'} tối thiểu ${this.minlength} ký tự`.toLowerCase();

    }
    if(!this.options.hiddenSuggestions && !this.innerValue && this.options.isRequired)
    {
      this.messageError = `${this.options.title || 'Chuỗi'} không được bỏ trống`.toLowerCase();

    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.value = value;
    
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
