import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaiLyXeIonTextComponent } from './dailyxe-ion-text.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CustomInputDirectiveModule } from 'src/app/shared/directive/custom-input/custom-input.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomInputDirectiveModule
  ],
  declarations: [DaiLyXeIonTextComponent],
  exports: [DaiLyXeIonTextComponent],
})
export class DaiLyXeIonTextModule { }
