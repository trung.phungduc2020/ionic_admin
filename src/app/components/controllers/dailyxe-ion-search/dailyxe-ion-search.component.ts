import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, OnChanges, SimpleChanges, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { defaultPrice } from 'src/app/core/variables';
import { DaiLyXeIonFilterTinDangComponent } from '../dailyxe-ion-filter-tindang/dailyxe-ion-filter-tindang.component';
import { ModalController } from '@ionic/angular';
const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonSearchComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-ion-search',
  templateUrl: './dailyxe-ion-search.component.html',
  styleUrls: ['./dailyxe-ion-search.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DaiLyXeIonSearchComponent implements ControlValueAccessor, OnInit, OnChanges {
  //#region =========================== Variables =========================
  public defaultPrice: any = defaultPrice;
  public statusBoxSearch: boolean = false;
  // public clearTimeoutTuKhoaTimKiem: any;
  @Output("onChange") onChange = new EventEmitter();
  public modal: any;
  public search: any = {};
  @Input("type") type: any = "full";
  @Input("options") options: any = {
    trangThai: true,
    tinhTrang: false,
    tinhTrangDuyet: false,
    typeThongBao: false,
    trangThaiViPham: false,
    trangThaiBlackList: false,
    trangThaiIsBlock: false,
    thanhVien: false
  };

  //The internal data model
  // private innerValue: any = {};
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  //#endregion =========================== End Variables ===================
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController
    ) {
  }
  async ngOnChanges(changes: SimpleChanges) {
  }
  async ngOnInit() {
  }
  init() {
    this.search.idMenu = -1;
    this.search.idThuongHieu = -1;
    this.search.idTinhThanh = -1;
    this.search.idSortBy = -1;
    this.search.tinhTrang = -1;
    this.search.trangThai = -1;
    this.search.price = Object.assign({}, defaultPrice);
  }
  async openModalFilter() {
    this.modal = await this.modalController.create({
      component: DaiLyXeIonFilterTinDangComponent,
      componentProps: {
        data: this.search
      }
    });
    this.modal.onDidDismiss().then((res: any) => {
      if (res && res.data) {
        this.search = res.data;
        this.applySearch();
      }
    });
    await this.modal.present();
  }
  async applySearch() {
    this.value = this.commonMethodsTs.cloneObject(this.search);
    this.statusBoxSearch = false;
    this.onChange.emit(this.value);
  }
  updateFilter() {

    let numberBoxSearch = 0;
    try {
      if (this.search.idTinhThanh && this.search.idTinhThanh > -1) {
        numberBoxSearch++;
      }
      if (this.search.idSortBy && this.search.idSortBy > -1) {
        numberBoxSearch++;
      }
      if (this.search.tinhTrang && this.search.tinhTrang > -1) {
        numberBoxSearch++;
      }
      if (this.search.trangThai && this.search.trangThai > -1) {
        numberBoxSearch++;
      }
      if (this.search.idMenu && this.search.idMenu > -1) {
        numberBoxSearch++;
      }
      if (this.search.price && (this.search.price.lower != defaultPrice.lower || this.search.price.upper != defaultPrice.upper)) {
        numberBoxSearch++;
      }
    } catch (error) {

    }

    return numberBoxSearch;
  }

  onChangeTrangThai() {
    this.applySearch();
  }
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.search;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.search) {
      this.search = v || {};
      this.onChangeCallback(v);
    }

  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.search) {
      this.search = value || {};
    }
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
