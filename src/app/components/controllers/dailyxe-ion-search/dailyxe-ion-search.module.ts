import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DaiLyXeIonSearchComponent } from './dailyxe-ion-search.component';
import { CustomIonSelectModule } from '../custom-ion-select/custom-ion-select.module';
import { DaiLyXeIonFilterTinDangModule } from '../dailyxe-ion-filter-tindang/dailyxe-ion-filter-tindang.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomIonSelectModule,
    DaiLyXeIonFilterTinDangModule
  ],
  declarations: [DaiLyXeIonSearchComponent],
  exports: [DaiLyXeIonSearchComponent],
})
export class DaiLyXeIonSearchModule { }
