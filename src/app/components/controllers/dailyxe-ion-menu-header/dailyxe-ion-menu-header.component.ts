import { Variables } from './../../../core/variables';
import { Component, Input } from '@angular/core';
import { commonNavigate } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';

@Component({
  selector: 'dailyxe-ion-menu-header',
  templateUrl: './dailyxe-ion-menu-header.component.html',
})
export class DaiLyXeIonMenuHeaderComponent {
  //#region ================================== Variable =========================
  @Input() options: any = {};
  constructor(public commonMethodsTs: CommonMethodsTs) {
  }
  openDialogFavorite(favorite) {
    if (!Variables.tokenThanhVien) {
      favorite.openDialog();
    }
    else {
      this.commonMethodsTs.toPage_MemberTinDaLuu();
    }

  }
}
