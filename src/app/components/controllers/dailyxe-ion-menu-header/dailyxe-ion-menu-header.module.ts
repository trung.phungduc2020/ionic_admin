import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DaiLyXeIonMenuHeaderComponent } from './dailyxe-ion-menu-header.component';
import { IonicModule } from '@ionic/angular';
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewFavoriteLocalStoreModule } from '../../views';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageLazyLoadModule,
    ViewFavoriteLocalStoreModule
  ],
  declarations: [DaiLyXeIonMenuHeaderComponent],
  entryComponents: [
    DaiLyXeIonMenuHeaderComponent
  ]
})
export class DaiLyXeIonMenuHeaderModule {
  constructor() {
  }


}
