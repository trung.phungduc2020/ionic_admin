import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DaiLyXeFilterByCategoryComponent } from './dailyxe-filter-by-category.component';
import { IonicModule } from '@ionic/angular';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [DaiLyXeFilterByCategoryComponent],
  exports: [DaiLyXeFilterByCategoryComponent],

})
export class DaiLyXeFilterByCategoryModule { }
