import { NgModule } from '@angular/core';
import { CustomIonSelectModule } from './custom-ion-select/custom-ion-select.module';
import { DaiLyXeIonSearchModule } from './dailyxe-ion-search/dailyxe-ion-search.module';
import { DaiLyXeIonNumberModule } from './dailyxe-ion-number/dailyxe-ion-number.module';
import { DaiLyXeIonEmailModule } from './dailyxe-ion-email/dailyxe-ion-email.module';
import { DaiLyXeIonPasswordModule } from './dailyxe-ion-password/dailyxe-ion-password.module';
import { DaiLyXeIonImgModule } from './dailyxe-ion-img/dailyxe-ion-img.module';
import { DaiLyXeIonNumberPhoneModule } from './dailyxe-ion-numberphone/dailyxe-ion-numberphone.module';
import { DaiLyXeIonViolationTinDangModule } from './dailyxe-ion-violation-tindang/dailyxe-ion-violation-tindang.module';
import { DaiLyXeIonImageModule } from './dailyxe-ion-image/dailyxe-ion-image.module';
import { DaiLyXeIonListImageModule } from './dailyxe-ion-list-image/dailyxe-ion-list-image.component.module';
import { DaiLyXeFilterByCategoryModule } from './dailyxe-filter-by-category/dailyxe-filter-by-category.module';
import { DaiLyXeIonTextModule } from './dailyxe-ion-text/dailyxe-ion-text.module';

@NgModule({
  exports: [
    CustomIonSelectModule,
    DaiLyXeIonSearchModule,
    DaiLyXeIonNumberModule,
    DaiLyXeIonEmailModule,
    DaiLyXeIonPasswordModule,
    DaiLyXeIonImgModule,
    DaiLyXeIonNumberPhoneModule,
    DaiLyXeIonViolationTinDangModule,
    DaiLyXeIonImageModule,
    DaiLyXeIonListImageModule,
    DaiLyXeFilterByCategoryModule,
    DaiLyXeIonTextModule
    
  ],
})
export class ControllersModule { }
export { CustomIonSelectModule } from './custom-ion-select/custom-ion-select.module';
export { DaiLyXeIonSearchModule } from './dailyxe-ion-search/dailyxe-ion-search.module';
export { DaiLyXeIonNumberModule } from './dailyxe-ion-number/dailyxe-ion-number.module';
export { DaiLyXeIonPasswordModule } from './dailyxe-ion-password/dailyxe-ion-password.module';
export { DaiLyXeIonImgModule } from './dailyxe-ion-img/dailyxe-ion-img.module';
export { DaiLyXeIonNumberPhoneModule } from './dailyxe-ion-numberphone/dailyxe-ion-numberphone.module';
export { DaiLyXeIonViolationTinDangModule } from './dailyxe-ion-violation-tindang/dailyxe-ion-violation-tindang.module';
export { DaiLyXeIonImageModule } from './dailyxe-ion-image/dailyxe-ion-image.module';
export { DaiLyXeIonListImageModule } from './dailyxe-ion-list-image/dailyxe-ion-list-image.component.module';
export { DaiLyXeFilterByCategoryModule } from './dailyxe-filter-by-category/dailyxe-filter-by-category.module';
export { DaiLyXeIonTextModule } from './dailyxe-ion-text/dailyxe-ion-text.module';
