import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ImageLazyLoadModule } from "src/app/shared/directive/image-lazy-load/image-lazy-load.module";
import { DaiLyXeIonFilterTinDangComponent } from "./dailyxe-ion-filter-tindang.component";
import { CustomIonSelectModule } from "../custom-ion-select/custom-ion-select.module";
import { NgObjectPipesModule } from "src/app/shared/pipes";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageLazyLoadModule,
    CustomIonSelectModule,
    NgObjectPipesModule
  ],
  declarations: [DaiLyXeIonFilterTinDangComponent],
  exports: [DaiLyXeIonFilterTinDangComponent],
  entryComponents: [
    DaiLyXeIonFilterTinDangComponent
  ]

})
export class DaiLyXeIonFilterTinDangModule { }
