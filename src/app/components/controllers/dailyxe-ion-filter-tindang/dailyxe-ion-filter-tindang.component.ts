import { defaultPrice, listTinhTrangDangTin, sortByListTinDang } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
	selector: 'dailyxe-ion-filter-tindang',
	templateUrl: './dailyxe-ion-filter-tindang.component.html',
})
export class DaiLyXeIonFilterTinDangComponent {
	public search: any = {};
	public defaultPrice: any = defaultPrice;
	public listTinhTrangDangTin: any = listTinhTrangDangTin;
	public sortByListTinDang: any = sortByListTinDang;
	public numberBoxSearch: any = 0;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		private navParams: NavParams,
		public modalController: ModalController) {
	}

	ngOnInit() {
		this.init();
	}

	init() {
		this.refreshSearch();
	}
	ionViewWillEnter() {
		this.search = Object.assign(this.search, this.navParams.get('data'));
		this.checkNumberBoxSearch();
	}
	refreshSearch() {
		// this.search.idThuongHieu = -1;
		this.search.idTinhThanh = null;
		this.search.idSortBy = null;
		this.search.tinhTrang = null;
		this.search.idDongXe = null;
		this.search.idLoaiXe = null;
		this.search.isPinTop = false;
		this.search.price = Object.assign({}, this.defaultPrice);
		this.applySearch();
	}

	checkNumberBoxSearch() {
		let convertParamsSearch = this.commonMethodsTs.convertParamsSearch(this.search || {});
		this.search = convertParamsSearch.search;
		this.numberBoxSearch = convertParamsSearch.numberBoxSearch;
	}



	applySearch() {

		this.dismiss(this.search);
	}

	closeModal() {
		// this.selectedItem = item;
		this.dismiss()
	}

	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}
