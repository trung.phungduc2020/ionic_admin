import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaiLyXeIonEmailComponent } from './dailyxe-ion-email.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CustomInputDirectiveModule } from 'src/app/shared/directive/custom-input/custom-input.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomInputDirectiveModule
  ],
  declarations: [DaiLyXeIonEmailComponent],
  exports: [DaiLyXeIonEmailComponent],
})
export class DaiLyXeIonEmailModule { }
