import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ResponseBase } from 'src/app/core/entity';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { defaultWxh } from 'src/app/core/variables';
const noop = () => {
};

@Component({
	selector: 'dailyxe-ion-thuonghieu-tindang',
	templateUrl: './dailyxe-ion-thuonghieu-tindang.component.html',
})
export class DaiLyXeIonThuongHieuTinDangComponent {
	//#region ================================== Variable =========================
	// @Input() data: any;
	@Input() options: any = {};
	public listData: any = [];
	//The internal data model
	private innerValue: any = '';
	//Placeholders for the callbacks which are later provided
	//by the Control Value Accessor
	private onTouchedCallback: () => void = noop;
	private onChangeCallback: (_: any) => void = noop;
	@Output("onChange") onChange = new EventEmitter();
	public isLoaded = false;

	//#endregion =============================== End variable ====================

	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
		) {
		this.init();
	}

	async ngOnChanges(changes: SimpleChanges) {

		// await this.init();
	}

	async init() {
		this.isLoaded = false;
		this.listData = this.commonMethodsTs.formatUrlHinhDaiDienByListData(await this.loadThuongHieu(), defaultWxh.two);
		this.isLoaded = true;
	}

	async loadThuongHieu() {
		let params:any = {};
		params.displayItems = params.displayItems || -1;
		params.trangThai = params.trangThai || "1";
		let data: any = [];

		try {
			let result: ResponseBase = await this.adminDaiLyXeBLL_InterfaceData.getByParams(params, "SearchThuongHieu").toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				data = result.DataResult || [];
			}
			else {
				this.commonMethodsTs.createMessageError(result.getMessage());
			}

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return data;
	}

	public selected(id) {
		if (this.value == id) {
			this.value = null;
		}
		else {
			this.value = id;
		}
	}

	//get accessor
	get value(): any {
		return this.innerValue;
	};

	//set accessor including call the onchange callback
	set value(v: any) {
		if (v !== this.innerValue) {
			this.innerValue = v;
			this.onChangeCallback(v);
			this.onChange.emit(this.innerValue);
		}
	}

	//Set touched on blur
	onBlur() {
		this.onTouchedCallback();
	}

	//From ControlValueAccessor interface
	writeValue(value: any) {
		if (value !== this.innerValue) {
			this.innerValue = value;
		}
	}

	//From ControlValueAccessor interface
	registerOnChange(fn: any) {
		this.onChangeCallback = fn;
	}

	//From ControlValueAccessor interface
	registerOnTouched(fn: any) {
		this.onTouchedCallback = fn;
	}
}
