import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ImageLazyLoadModule } from "src/app/shared/directive/image-lazy-load/image-lazy-load.module";
import { CustomIonSelectModule } from "..";
import { NgObjectPipesModule } from "src/app/shared/pipes";
import { DaiLyXeIonThuongHieuTinDangComponent } from "./dailyxe-ion-thuonghieu-tindang.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageLazyLoadModule,
    CustomIonSelectModule,
    NgObjectPipesModule
  ],
  declarations: [DaiLyXeIonThuongHieuTinDangComponent],
  exports: [DaiLyXeIonThuongHieuTinDangComponent],
})
export class DaiLyXeIonThuongHieuTinDangModule { }
