import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import { SelectedStaticModal } from './selected-static-modal.page';
// The modal's component of the previous chapter
@NgModule({
     declarations: [
			SelectedStaticModal
     ],
     imports: [
       IonicModule,
       CommonModule
     ],
     entryComponents: [
			SelectedStaticModal
     ]
})
export class SelectedStaticModalModule {}