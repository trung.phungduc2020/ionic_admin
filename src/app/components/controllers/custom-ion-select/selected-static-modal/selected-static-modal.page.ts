import { Component } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { labelPages } from 'src/app/core/labelPages';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-selected-static-modal',
	templateUrl: './selected-static-modal.page.html',
	styleUrls: ['./selected-static-modal.page.scss'],
})
export class SelectedStaticModal {

	// #region =============== Variables ====
	public lbl = labelPages;
	public options: any = {};
	// public selectedItem: any = {};
	public value: any = {};
	public data: any = [];
	public dataTemp: any = [];
	public textSearch: any = "";
	private optionsDefault: any = {
		'title': "Chọn",
		'labelAttr': "Name",
		'valueAttr': "Id"
	};
	//#endregion ============ End variables ===
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public modalController: ModalController
		) {
	}

	ionViewWillEnter() {
		this.dataTemp = this.commonMethodsTs.cloneArray(this.data);
		this.options = Object.assign(this.optionsDefault, this.options);
	}

	selected(item) {
		this.dismiss(item)
	}

	closeModal() {
		this.dismiss()
	}

	ionInput(ev: any) {
		const val = ev.target.value;
		if (val && val.trim() != '') {
			let strSearch = val;
			if(val)
			{
				strSearch = val.toLowerCase();
				strSearch = this.commonMethodsTs.toASCII(strSearch);
			}
			
			this.data = this.dataTemp.filter((item: any) => {
				let tuKhoaTimKiem = item.TuKhoaTimKiem;
				if (!tuKhoaTimKiem) {
					tuKhoaTimKiem = item[this.options.labelAttr];
					if(tuKhoaTimKiem)
					{
						tuKhoaTimKiem = tuKhoaTimKiem.toLowerCase();
					tuKhoaTimKiem = this.commonMethodsTs.toASCII(tuKhoaTimKiem);
					}
					
				}
				return !tuKhoaTimKiem ? true: tuKhoaTimKiem.indexOf(strSearch) > -1;
			});
		}

	}

	ionClear() {
		this.data = this.dataTemp;
	}

	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}