import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CustomIonSelectComponent } from './custom-ion-select.component';
import { SelectedMasterModalModule } from './selected-master-modal/selected-master-modal.module';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { SelectedStaticModalModule } from './selected-static-modal/selected-static-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectedMasterModalModule,
    SelectedStaticModalModule
  ],
  declarations: [CustomIonSelectComponent],
  exports: [CustomIonSelectComponent],
  providers: [AdminRaoVatBLL_UserGroup]

})
export class CustomIonSelectModule { }
