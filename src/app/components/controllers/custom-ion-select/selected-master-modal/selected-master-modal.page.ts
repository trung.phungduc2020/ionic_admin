import { Component } from '@angular/core';
import { labelPages } from 'src/app/core/labelPages';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CustomIonSelectModal, TypeMaster } from '../custom-ion-select.modal';
import { ModalController } from '@ionic/angular';
import { currentPageDefault } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';

@Component({
	selector: 'app-selected-master-modal',
	templateUrl: './selected-master-modal.page.html',
	styleUrls: ['./selected-master-modal.page.scss'],
	providers: [CustomIonSelectModal]
})
export class SelectedMasterModal {

	// #region =============== Variables ====
	public lbl = labelPages;
	public options: any = {};
	public type: string;

	public value: any;
	public data: any;
	public showThumbnail: any = false;
	public search: any = {};
	public totalItems: number = 0;
	tuKhoaTimKiem: string;
	public itemAdminNotApprove = { Id: 0, HoTen: "Chưa ai đăng ký" };
	// main
	//#endregion ============ End variables ===
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public customIonSelectMasterModal: CustomIonSelectModal,
		public modalController: ModalController
	) {
	}

	ionViewWillEnter() {
		this.options = Object.assign({
			'title': "Chọn",
			'labelAttr': "Name",
		}, this.options);
		this.search = Object.assign({
			'displayPage': 1,
			'displayItems': 10,
			'tuKhoaTimKiem': "",
		}, this.search);
		this.tuKhoaTimKiem = this.search.tuKhoaTimKiem;
		this.init();
	}

	async loadData(displayPage: any = currentPageDefault) {
		if (displayPage == 1) {
			this.data = null;
		}
		let type = this.type;
		if (this.type == TypeMaster.AdminApprove) {
			type = TypeMaster.Admin;
		}
		let search = this.commonMethodsTs.cloneObject(this.search);
		search.displayPage = displayPage;
		let res: ResponseBase = await this.customIonSelectMasterModal.loadData("Search" + type, search);
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			this.search.displayPage = displayPage;
			if (displayPage == 1) {
				this.data = res.DataResult || [];
			}
			else {
				this.data = this.data.concat(res.DataResult);
			}
			this.totalItems = res.getTotalItems();
		}

	}
	async init() {

		if (!this.data || this.data.length == 0 || (this.totalItems < this.data.length)) {
			this.data = null;
			this.search.displayItems = 20;
			await this.loadData();
		}
	}

	ionSearch(tuKhoaTimKiem: any) {

		this.search.displayPage = 1;
		this.search.tuKhoaTimKiem = tuKhoaTimKiem;
		this.loadData();
	}



	public async doInfinite(infiniteScroll) {
		try {
			if ((this.data.length > 0 && this.data.length < this.totalItems)) {
				await this.loadData(this.search.displayPage + 1);
			}

		} catch (error) {
		}
		infiniteScroll.target.complete();

	}

	selected(item) {
		this.dismiss(item)
	}
	ionChangeSearch() {
		if (this.tuKhoaTimKiem != this.search.tuKhoaTimKiem) {
			this.ionSearch(this.tuKhoaTimKiem);

		}
	}
	closeModal() {
		this.dismiss()
	}
	async dismiss(item?: any) {

		await this.modalController.dismiss({ value: item ? item.Id : this.value, itemSelected: item, data: this.data, search: this.search, totalItems: this.totalItems });
	}
}