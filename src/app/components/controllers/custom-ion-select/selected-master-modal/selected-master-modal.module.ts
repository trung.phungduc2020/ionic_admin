import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import { SelectedMasterModal } from './selected-master-modal.page';
import { FormsModule } from '@angular/forms';
// The modal's component of the previ./selected-master-modal.page
@NgModule({
     declarations: [
			SelectedMasterModal
     ],
     imports: [
       IonicModule,
       CommonModule,
       FormsModule
     ],
     entryComponents: [
			SelectedMasterModal
     ],
     exports: [SelectedMasterModal],


})
export class SelectedMasterModalModule {}