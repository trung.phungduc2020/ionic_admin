import { ItemRecord } from '../../../core/entity';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { SelectedMasterModal } from './selected-master-modal/selected-master-modal.page';
import { CustomIonSelectModal, TypeMaster, TypeStatic } from './custom-ion-select.modal';
import { commonMessages, listTinhTrangDangTin, listTrangThai, sortByListTinDang, listTenNhienLieu, listIsDuyet, listTypeThongBao, listTrangThaiDangTinViolation, listTrangThaiUserBlackList, listTrangThaiIsBlock, listOsName } from 'src/app/core/variables';
import { SelectedStaticModal } from './selected-static-modal/selected-static-modal.page';
import { ModalController } from '@ionic/angular';

const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CustomIonSelectComponent),
  multi: true
};
@Component({
  selector: 'custom-ion-select',
  templateUrl: './custom-ion-select.component.html',
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, AdminRaoVatBLL_UserGroup, CustomIonSelectModal]
})


export class CustomIonSelectComponent implements ControlValueAccessor, OnChanges {
  @Input() type: string;
  public labelAttr: string;
  public valueAttr: string;
  @Input() placeholder: string = "Chọn";
  @Input() options: any = {};
  @Input() data: any;
  @Input() params: any = {};
  @Input() isDialog = true; // tronghuu95 20190791 114600 chuẩn hóa về một dạng theo ý A.Trung
  @Input() disabled = false;
  @Input() showThumbnail = false;
  @Output() onChange = new EventEmitter();
  @Output() onSelected = new EventEmitter();
  public _itemSelected;
  get itemSelected(): any {
    return this._itemSelected;
  };
  //set accessor including call the onchange callback
  set itemSelected(v: any) {
    this._itemSelected = v;
    this.onSelected.emit(this._itemSelected);
  }
  optionsDefault: any = {};
  public isLoaded: boolean = false;
  public isReady: boolean = false;
  public dataDismiss: any;
  private _listTypeMaster: string[] = Object.keys(TypeMaster);
  public isTypeMaster: boolean = false;

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public customIonSelectMasterModal: CustomIonSelectModal,
    public modalController: ModalController
  ) {
    // this.optionsDefault.isShowFirstRecord = true;
  }
  ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (this.isReady) this.init();
  }
  ngOnInit() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (!this.isReady) {
      setTimeout(() => {
        this.init();
      }, 500);
    }
  }
  async init() {

    //ưu tiên dùng data chuyền vào
    this.dataDismiss = null;
    this.isLoaded = false;
    this.options = Object.assign(this.optionsDefault, this.options);
    // gọi api để lấy dữ liệu
    if (this._listTypeMaster.indexOf(this.type) >= 0) {
      this.isTypeMaster = true;
      this.isDialog = true;
      this.data = null;

      if (this.type) {
        this.valueAttr = this.valueAttr || "Id";
        switch (this.type) {
          case TypeMaster.ThuongHieu:
            this.labelAttr = this.labelAttr || "TenThuongHieu";
            this.showThumbnail = true;
            break;
          case TypeMaster.DongXe:
            this.labelAttr = this.labelAttr || "VietTat";
            this.showThumbnail = true;
            break;
          case TypeMaster.TinhThanh:
            this.labelAttr = this.labelAttr || "TenTinhThanh";
            this.options.labelDefault = "Tỉnh thành";
            this.params.displayItems = "20";
            this.params.orderString = "ThuTu:asc";
            this.showThumbnail = false;
            break;
          case TypeMaster.QuanHuyen:
            this.labelAttr = this.labelAttr || "TenQuanHuyen";
            this.params.displayItems = "20";
            this.showThumbnail = false;
            break;
          case TypeMaster.PhuongXa:
            this.labelAttr = this.labelAttr || "TenPhuongXa";
            this.showThumbnail = false;
            break;
          case TypeMaster.Menu:
            this.labelAttr = this.labelAttr || "TenMenu";
            this.options.labelDefault = "Danh mục";
            this.showThumbnail = false;
            break;
          case TypeMaster.LoaiXe:
            this.labelAttr = this.labelAttr || "TenLoaiXe";
            this.showThumbnail = false;
            break;
          case TypeMaster.MauSac:
            this.labelAttr = this.labelAttr || "TenMauSac";
            this.showThumbnail = false;
            break;
          case TypeMaster.ThanhVien:
            this.labelAttr = this.labelAttr || "HoTen";
            this.showThumbnail = false;
            break;
          case TypeMaster.Admin:
            this.labelAttr = this.labelAttr || "HoTen";
            this.showThumbnail = false;
            break;
          case TypeMaster.AdminApprove:
            this.labelAttr = this.labelAttr || "HoTen";
            this.showThumbnail = false;
            break;

          default:
            break;
        }
        await this.loadData();

      }
    }
    else {
      this.isTypeMaster = false;
      this.labelAttr = this.labelAttr || "label";
      this.valueAttr = this.valueAttr || "value";
      if (!this.data || this.data.length <= 0 && this.type) {

        switch (this.type) {
          case TypeStatic.TinhTrangDangTin:
            this.options.labelDefault = "Tình trạng";
            this.data = listTinhTrangDangTin;
            break;
          case TypeStatic.TrangThai:
            this.options.labelDefault = "Trạng thái";
            this.data = listTrangThai;
            break;
          case TypeStatic.SortTinDang:

            this.data = sortByListTinDang;
            break;
          case TypeStatic.NhienLieu:

            this.data = listTenNhienLieu;
            break;
          case TypeStatic.TinhTrangDuyet:

            this.data = listIsDuyet;
            break;
          case TypeStatic.TypeThongBao:

            this.data = listTypeThongBao;
            break;
          case TypeStatic.TrangThaiViPham:

            this.data = listTrangThaiDangTinViolation;
            break;
          case TypeStatic.TrangThaiBlackList:

            this.data = listTrangThaiUserBlackList;
            break;
          case TypeStatic.TrangThaiIsBlock:

            this.data = listTrangThaiIsBlock;
            break;
          case TypeStatic.OsName:

            this.data = listOsName;
            break;
         
          case TypeStatic.UserGroup:
            this.labelAttr = "Title";
            this.valueAttr = "Id";
            this.data = await this.customIonSelectMasterModal.getUserGroup(this.params);
            break;
          default:
            break;
        }
      }
      this.getItemSelected();
    }
    this.isReady = true;
    this.isLoaded = true;
  }
  //#region ======================== load Data =========================

  removeItemSelected() {
    if (this.value != null) {
      this.value = null;
      this.itemSelected = null;
      this.ionChange(this.value)
    }

  }
  public getItemSelected() {
    let index = this.data.findIndex(res => res[this.valueAttr] == this.value);
    this.itemSelected = this.data[index];
  }
  showClear() {
    if (typeof this.value === "number") {
      return +this.value >= 0
    }
    if (!this.value) return false;

    return true;
  }

  async loadData() {
    try {
      let params = this.commonMethodsTs.cloneObject(this.params || {});
      if (!(+this.value > 0)) {
        this.itemSelected = null;
      }
      else {
        if (this.type == TypeMaster.AdminApprove && this.value == 0) {
          this.itemSelected = { Id: 0, HoTen: "Chưa ai đăng ký" }
        }
        else {
          params.id = this.value;
          params.displayItems = this.params.displayItems || 1;
          params.trangThai = this.params.trangThai || "1";
          let data = await this.customIonSelectMasterModal.getData(this.type, params);
          this.itemSelected = data[0];
        }

      }
    } catch (error) {

    }
  }

  openModal() {
    if (this.isTypeMaster) {
      this.openModalForMaster();
    }
    else {
      this.openModalForStatic();
    }
  }

  async openModalForMaster() {
    let componentProps = {};
    if (!this.dataDismiss) {
      componentProps = {
        value: this.value,
        type: this.type,
        search: this.params,
        options: {
          'labelAttr': this.labelAttr,
          'title': this.placeholder
        },
        showThumbnail: this.showThumbnail
      }
    }
    else {
      componentProps = {
        value: this.value,
        type: this.type,
        itemSelected: this.dataDismiss.itemSelected,
        data: this.dataDismiss.data,
        search: this.dataDismiss.search,
        totalItems: this.dataDismiss.totalItems,
        options: {
          'labelAttr': this.labelAttr,
          'title': this.placeholder
        },
        showThumbnail: this.showThumbnail
      }
    }
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: SelectedMasterModal,
        componentProps: componentProps
      });
    modal.onDidDismiss().then((res: any) => {
      //this.dismiss({value: item.Id, itemSelected: item, data: this.data, search: this.search, totalItems: this.totalItems })
      if (res && res.data) {
        this.dataDismiss = res.data;
        if (this.value != this.dataDismiss.value) {
          this.itemSelected = this.dataDismiss.itemSelected;
          this.value = this.dataDismiss.value;
          this.ionChange(this.value);
        }

      }
    });
    await modal.present();
  }

  async openModalForStatic() {
    if (!this.data || this.data.length == 0) {
      this.commonMethodsTs.createMessageWarning(commonMessages.M001);
      return;
    }
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: SelectedStaticModal,
        componentProps: {
          data: this.data,
          value: this.value,
          options: {
            'labelAttr': this.labelAttr,
            'valueAttr': this.valueAttr,
            'title': this.placeholder
          },
          showThumbnail: this.showThumbnail
        }
      });
    modal.onDidDismiss().then((res: any) => {
      if (res && res.data != null) {
        this._itemSelected = res.data;
        this.onSelected.emit(this._itemSelected);
        this.value = this._itemSelected[this.valueAttr];
        this.ionChange(this.value);
      }
    });
    await modal.present();
  }
  //#endregion  ===================== End Load Data ===================
  //Set touched on blur
  ionChange($event) {
    this.onChange.emit(this.value);

  }
  //The internal data model
  public innerValue: any = -1;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);

    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.value = value;
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.id = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};