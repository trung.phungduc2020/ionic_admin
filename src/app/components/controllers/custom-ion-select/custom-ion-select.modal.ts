import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
import { defaultWxh, commonParams } from 'src/app/core/variables';

@Injectable()
export class CustomIonSelectModal {
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
    public adminRaoVatBLL_UserGroup: AdminRaoVatBLL_UserGroup,

  ) { }

  async getData(type: string, params: any) {
    if (!type) return [];
    if(type == TypeMaster.AdminApprove)
      {
        type = TypeMaster.Admin;
      }
    type = "Search" + type;
    let data = [];
    try {
      let result = await this.loadData(type, params)
      if (result) {
        data = result.DataResult || [];
      }

    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return data;
  }

  async loadData(type: string, params: any) {
    try {
      
      params[commonParams.trangThai] = params[commonParams.trangThai] || "1";
      let result: ResponseBase = await this.adminDaiLyXeBLL_InterfaceData.getByParams(params, type).toPromise();
      result = new ResponseBase(result);
      if (result.isSuccess()) {
        result.DataResult  = this.commonMethodsTs.formatUrlHinhDaiDienByListData(result.DataResult || [], defaultWxh.two)
        return result;
      }
      else {
        this.commonMethodsTs.createMessageError(result.getMessage());
      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return new ResponseBase();;
  }

  async getUserGroup(params:any) {
    params = params || {}
    params.displayItems = params.displayItems || "-1";
    params.trangThai = params.trangThai || "1";
    let result = await this.adminRaoVatBLL_UserGroup.getByParams(params).toPromise();
    if (result.IntResult == 1) {
      return result.DataResult;
    }
    return [];
  }
  
}

export const TypeMaster = {
  ThuongHieu: "ThuongHieu",
  DongXe: "DongXe",
  LoaiXe: "LoaiXe",
  TinhThanh: "TinhThanh",
  QuanHuyen: "QuanHuyen",
  PhuongXa: "PhuongXa",
  Menu: "Menu",
  ThanhVien: "ThanhVien",
  Admin: "Admin",
  AdminApprove: "AdminApprove",
  MauSac: "MauSac"
}
export const TypeStatic = {
  TinhTrangDangTin: "TinhTrangDangTin", 
  TrangThai: "TrangThai", 
  SortTinDang: "TrangThai", 
  NhienLieu:"NhienLieu", 
  TinhTrangDuyet:"TinhTrangDuyet", 
  TypeThongBao:"TypeThongBao", 
  TrangThaiViPham:"TrangThaiViPham", 
  TrangThaiBlackList:"TrangThaiBlackList",  
  TrangThaiIsBlock:"TrangThaiIsBlock", 
  OsName:"OsName", 
  UserGroup:"UserGroup",

}