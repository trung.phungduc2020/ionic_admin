import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { DaiLyXeIonShareTinDangComponent } from "./dailyxe-ion-share-tindang.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DaiLyXeIonShareTinDangComponent],
  entryComponents: [
    DaiLyXeIonShareTinDangComponent
  ]
})
export class DaiLyXeIonShareTinDangModule {
  constructor() {
  }


}
