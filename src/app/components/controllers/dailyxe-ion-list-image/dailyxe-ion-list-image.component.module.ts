import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'src/app/shared/directive/tooltip/tooltip';
import { DaiLyXeIonListImageComponent } from './dailyxe-ion-list-image.component';
import { DaiLyXeIonImageModule } from '../dailyxe-ion-image/dailyxe-ion-image.module';
import { DaiLyXeIonListImageModal } from './dailyxe-ion-list-image.modal';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TooltipModule,
    DaiLyXeIonImageModule
  ],
  providers:[DaiLyXeIonListImageModal],
  declarations: [DaiLyXeIonListImageComponent],
  exports: [DaiLyXeIonListImageComponent],
})
export class DaiLyXeIonListImageModule { }
