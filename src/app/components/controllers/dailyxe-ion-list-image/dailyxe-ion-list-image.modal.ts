import { CoreProcess } from 'src/app/core/core-process';
import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ThanhVienRaoVatBLL_InterfaceData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { ResponseBase, LayoutAlbum } from 'src/app/core/entity';
import { defaultWxh } from 'src/app/core/variables';

@Injectable()
export class DaiLyXeIonListImageModal {
    constructor(
      public commonMethodsTs: CommonMethodsTs,
      public coreProcess: CoreProcess,
      public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData

    ) { }
    async getLayoutAlbumByMa(ma:string)
    {
      let data: LayoutAlbum = new LayoutAlbum();
      try {
        let result = await this.thanhVienRaoVatBLL_InterfaceData.getLayoutAlbumByMa(ma).toPromise();
        result = new ResponseBase(result);
        if (result.isSuccess()) {
          // update lại dữ liệu
          result.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(result.DataResult, defaultWxh.two);
          data = result.DataResult as LayoutAlbum;
        }
      } catch (error) {
        this.commonMethodsTs.createMessageErrorByTryCatch(error);
      }
      return data;
    }
}
