import { Variables, defaultValue } from '../../../core/variables'
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, ViewChildren, QueryList } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Files, LayoutAlbum } from 'src/app/core/entity';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { DaiLyXeIonListImageModal } from './dailyxe-ion-list-image.modal';
import { DaiLyXeIonImageComponent } from '../dailyxe-ion-image/dailyxe-ion-image.component';
import { TrurthifyPipe } from 'src/app/shared/pipes';
import { CoreProcess } from 'src/app/core/core-process';
import { ThanhVienRaoVatBLL_InterfaceData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { PictureMethodsTs } from 'src/app/core/picture-methods';
const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonListImageComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-ion-list-image',
  templateUrl: './dailyxe-ion-list-image.component.html',
  styleUrls: ['./dailyxe-ion-list-image.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, ThanhVienRaoVatBLL_InterfaceData]
})
export class DaiLyXeIonListImageComponent implements ControlValueAccessor, OnChanges {
  //#region ================= Variables ===============================
  @Input() placeholder: string;
  @Input() options: any = {};
  @Input() maximumImagesCount: any = defaultValue.maxImageDescription;
  @Input() style: any = { 'width': '148px', 'height': '148px' };
  @Input() fileName: string = Variables.defaultFileName;
  @Input() fixedButton: boolean = false;
  @Input() hiddenButton: boolean = false;
  private optionsDefault: any = {
    "rewriteUrl": "new-image",
  };
  @ViewChildren('img') listImageComponent: QueryList<DaiLyXeIonImageComponent>;
  public layoutAlbum: LayoutAlbum = new LayoutAlbum();
  //#region Crop Image
  //#endregion =============== End variablse ==========================
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public pictureMethodsTs: PictureMethodsTs,
    public imagePicker: ImagePicker,
    public daiLyXeIonListImageModal: DaiLyXeIonListImageModal,
    public coreProcess: CoreProcess,
  ) {
  }
  async ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

  }
  async ionViewWillEnter() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    //await this.init();
    this.options = Object.assign(this.optionsDefault, this.options);

    
  }

  checkMaximumImagesCount() {
    this.value = this.value || [];
    if (this.value && this.value.length >= this.maximumImagesCount) {
      this.commonMethodsTs.createMessageWarning("Bạn chỉ đã vượt quá tối đa " + this.maximumImagesCount + " items");
      return false;
    }
    return true;
  }
  async chooseListHinhAnh() {
    if (!this.checkMaximumImagesCount()) {
      return;
    }
    let maximumImagesCount = this.maximumImagesCount - this.value.length;
    try {
      let listImages: any = await this.pictureMethodsTs.actionChoosePictureMutiple(maximumImagesCount);
      if (listImages instanceof Array) {
        let value: any = this.commonMethodsTs.cloneArray(this.value || []);
        for (let i = 0; i < listImages.length; i++) {
          let srcImage = listImages[i];
          let v = srcImage;
          value.push(v);
        }
        this.value = value;
      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
      return;
    }
  }
  ngOnInit() {
    // this.innerValue = this.formatcurrencypipe.transform(this.value);
  }
  onChange(i, data) {
    if (data) {
      this.value[i] = data;
    }
  }
  onRemoveImage(index) {
    let value = this.commonMethodsTs.cloneArray(this.value);
    value.splice(index, 1);
    this.value = value;
  }

  async clearAll()
	{
		let resAlert = await this.commonMethodsTs.alertConfirm("Bạn có chắc chắn muốn <strong>xóa hết ảnh</strong>");
		if(resAlert)
		{
			this.value = [];
		}
  }
  
  onIonBlur() {
  }
  //The internal data model
  public innerValue: any;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = this.commonMethodsTs.cloneArray(v || []);
      this.onChangeCallback(this.innerValue);
      this.typeLayout()
    }
  }
  async typeLayout() {

    let ma = await this.commonMethodsTs.getMaLayoutAlbum(this.value);
    this.layoutAlbum = await this.daiLyXeIonListImageModal.getLayoutAlbumByMa(ma);
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.value = value;
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  async uploadImage() {
    try {
      /*ds ids hình*/
      let ids = [];
      /*ds vị trí uri in ids */
      let lNotNumberOfIds = [];
      /*tương ứng từng file => sẽ được convert ra id bằng insertListImageToServerQuick*/
      let lFiles: Files[] = [];
      this.innerValue.forEach((element, index) => {
        let f = new Files();
        let id = this.commonMethodsTs.getIdByUrlDailyXe(element);
        if (id > 0) {
          ids.push(id)
        }
        else {
          ids.push(element);
          lNotNumberOfIds.push(index);
          f.Id = 0;
          f.Name = this.fileName;
          lFiles.push(f);
        }

      });
      /*Thưc hiện convert lFiles => list ids*/
      let idFiles = [];
      if (lFiles && lFiles.length > 0) {
        idFiles = await this.coreProcess.insertListImageToServerQuickByFiles(lFiles);

      }
      /*Mapping lại các idFiles vào ids*/
      lNotNumberOfIds.forEach((element, index) => {
        ids[element] = idFiles[index]
      });

      /*tương ứng listImageComponent === ids nên ta có*/
      // await Promise.all(this.listImageComponent.map((item, index) => item.uploadImage(ids[index])));
      //tạm chuyển qua chạy trình tự
      for (let index = 0; index < ids.length; index++) {
        let listImageComponent = this.listImageComponent["_results"][index];
        await listImageComponent.uploadImage(ids[index]);
      }
      /*Loại bỏ các id không hợp lệ*/
      ids = new TrurthifyPipe().transform(ids);
      return ids;
    } catch (error) {
      return [];
    }

  }
}
