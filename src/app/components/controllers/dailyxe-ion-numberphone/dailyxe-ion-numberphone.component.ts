import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, forwardRef, Input, OnChanges, SimpleChanges, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { CommonService } from 'src/app/forms/member/services/common.service';
const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeIonNumberPhoneComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-ion-numberphone',
  templateUrl: './dailyxe-ion-numberphone.component.html',
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class DaiLyXeIonNumberPhoneComponent implements ControlValueAccessor, OnChanges {
  //#region ================= Variables ===============================
  imgSrc: string;
  @Input() placeholder: string;
  @Input() disabled: boolean = false;
  
  // Allow decimal numbers and negative values
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  //#endregion =============== End variablse ==========================
  constructor(
    public commonService: CommonService,
    public commonMethodsTs: CommonMethodsTs,
    public ref: ChangeDetectorRef,
  ) {
  }
  
  async ngOnChanges(changes: SimpleChanges) {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }
  async ionViewWillEnter() {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    //await this.init();
  }
  ngOnInit() {
    // this.innerValue = this.formatcurrencypipe.transform(this.value);
  }
  //The internal data model
  public innerValue: any;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    this.imgSrc = this.commonMethodsTs.builLinkImage("image-edit", v || -1);
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    // value = this.formatcurrencypipe.transform(value);
    this.value = value;
    // if (value !== this.innerValue) {
    //   this.innerValue = value;
    // }
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
