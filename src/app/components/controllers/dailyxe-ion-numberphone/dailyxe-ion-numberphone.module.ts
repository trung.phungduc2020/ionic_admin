import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DaiLyXeIonNumberPhoneComponent } from '../dailyxe-ion-numberphone/dailyxe-ion-numberphone.component';
import { NumberOnlyLoadModule } from 'src/app/shared/directive/number-only/numbers-only.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NumberOnlyLoadModule
  ],
  declarations: [DaiLyXeIonNumberPhoneComponent],
  exports: [DaiLyXeIonNumberPhoneComponent],
})
export class DaiLyXeIonNumberPhoneModule { }
