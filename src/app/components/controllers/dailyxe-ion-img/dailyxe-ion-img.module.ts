import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DaiLyXeIonImgComponent } from '../dailyxe-ion-img/dailyxe-ion-img.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DaiLyXeIonImgComponent],
  exports: [DaiLyXeIonImgComponent],
})
export class DaiLyXeIonImgModule { }
