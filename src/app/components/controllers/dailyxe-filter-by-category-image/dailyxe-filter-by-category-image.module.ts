import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DaiLyXeFilterByCategoryImageComponent } from './dailyxe-filter-by-category-image.component';
import { IonicModule } from '@ionic/angular';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [DaiLyXeFilterByCategoryImageComponent],
  exports: [DaiLyXeFilterByCategoryImageComponent],

})
export class DaiLyXeFilterByCategoryImageModule { }
