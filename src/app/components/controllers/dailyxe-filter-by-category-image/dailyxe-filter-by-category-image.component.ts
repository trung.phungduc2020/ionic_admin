import { CommonService } from 'src/app/forms/member/services/common.service';
import { Component, OnChanges, EventEmitter, SimpleChanges, Output, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from 'src/app/core/entity';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { defaultWxh, noImage } from 'src/app/core/variables';
const noop = () => {
};
export const CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DaiLyXeFilterByCategoryImageComponent),
  multi: true
};
@Component({
  selector: 'dailyxe-filter-by-category-image',
  templateUrl: './dailyxe-filter-by-category-image.component.html',
  providers: [CONTROL_VALUE_ACCESSOR, AdminDaiLyXeBLL_InterfaceData]
})
export class DaiLyXeFilterByCategoryImageComponent implements ControlValueAccessor, OnChanges {
  //#region ================================== Variable =========================
  // @Input() data: any;
  @Input() options: any = {};
  @Input() type: string;
  @Input() labelAttr: string;
  @Input() valueAttr: string;
  @Input() params: any = {};
  public data: any = [];
  //The internal data model
  private innerValue: any;
  //Placeholders for the callbacks which are later provided
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  @Output("onChange") onChange = new EventEmitter();
  @Output("onSelected") onSelected = new EventEmitter();

  //#endregion =============================== End variable ====================
  constructor(
    public commonService: CommonService,
    public commonMethodsTs: CommonMethodsTs,
    public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
    ) {
  }
  ngOnChanges(changes: SimpleChanges) {
    this.init();
  }
  async init() {
    let dataCache = null;
    if (this.type) {
      switch (this.type) {
        case "LoaiXe":
          this.labelAttr = this.labelAttr || "TenLoaiXe";
          this.valueAttr = this.valueAttr || "Id";
          dataCache = this.commonMethodsTs.formatUrlHinhDaiDienByListData(await this.loadInterfaceDataDaiLyXe(), defaultWxh.two);
          break;
        case "DongXe":
          this.labelAttr = this.labelAttr || "VietTat";
          this.valueAttr = this.valueAttr || "Id";
          dataCache = this.commonMethodsTs.formatUrlHinhDaiDienByListData(await this.loadInterfaceDataDaiLyXe(), defaultWxh.two);
          break;
        default:
          break;
      }
    }
    if (this.data != dataCache) {
      this.data = dataCache;
    }
  }
  async loadInterfaceDataDaiLyXe() {
    this.params = this.params || {};
    this.params.displayItems = this.params.displayItems || -1;
    this.params.trangThai = this.params.trangThai || "1";
    let data: any = [];
    try {
      let result: ResponseBase = await this.adminDaiLyXeBLL_InterfaceData.getByParams(this.params, "Search" + this.type).toPromise();
      result = new ResponseBase(result);
      if (result.isSuccess()) {
        data = result.DataResult || [];

        data.forEach(async (element: any) => {
          let idFileDaiDien = element.IdFileDaiDien ? element.IdFileDaiDien : "-1";
          element.UrlHinhDaiDien = +idFileDaiDien > 0 ? this.commonMethodsTs.builLinkImage(element.RewriteUrl, idFileDaiDien, defaultWxh.two) : noImage;
        });
      }
      else {

        this.commonMethodsTs.createMessageError(result.getMessage());
      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return data;
  }
  selected(value: any = "") {
    if (!this.options.notChange) {
      this.value = value;


    }
    this.onSelected.emit(value);


  }

  //get accessor
  get value(): any {
    return this.innerValue;
  };
  //set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
      this.onChange.emit(this.innerValue);
    }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
