import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DaiLyXeIonPasswordComponent } from '../dailyxe-ion-password/dailyxe-ion-password.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [DaiLyXeIonPasswordComponent],
  exports: [DaiLyXeIonPasswordComponent],
})
export class DaiLyXeIonPasswordModule { }
