import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonMessages, Variables, commonAttr } from 'src/app/core/variables';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
import { async } from '@angular/core/testing';
import { DaiLyXeIonViolationTinDangComponent } from '../dailyxe-ion-violation-tindang/dailyxe-ion-violation-tindang.component';
import { ModalController } from '@ionic/angular';
@Injectable()
export class MenuDotsCustoms {
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    public modalController: ModalController,
  ) {
  }
  /** openMenuDots nguyên bản của @Đức
   * 
   * @param data 
   * @param options 
   */
  async openMenuDots(data: any, options: any = {}) {
    let idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
    let idThanhVienTao = this.commonMethodsTs.getData(data.ThanhVien_Ext, "Id", null) || this.commonMethodsTs.getData(data, "IdThanhVienContact_Ext", null);
    return new Promise(async (resolve, reject) => {

      let item = this.commonMethodsTs.cloneObject(data);
      let buttons: any[] = [
        /** Call */
        {
          text: 'Báo cáo vi phạm',
          icon: 'call',
          cssClass: 'ban',
          role: "destructive",
          handler: async () => {
            if (!Variables.isLoginThanhVien) {
              this.commonMethodsTs.toPage_MemberLogin();
              return;
            }

            if (item && item.IdThanhVien == idThanhVien) {
              this.commonMethodsTs.createMessageWarning(commonMessages.M088);
              return;
            }
            const modal = await this.modalController.create({
              component: DaiLyXeIonViolationTinDangComponent,
            });
            modal.onDidDismiss().then((res: any) => {
              if (res.data != null && res.data.content) {
                //this.dangTinDetailModal.creatViolation(item, res.data.content)
              }
            });

            await modal.present();

          }
        },
        {
          text: 'Gọi',
          icon: 'call',
          cssClass: 'phone',
          role: "destructive",
          handler: () => {
            if (!Variables.isLoginThanhVien) {
              this.commonMethodsTs.alertNotification(commonMessages.M103);
              return;
            }
            let soDienThoai = item.DangTinContact_Ext ? this.commonMethodsTs.getData(item.DangTinContact_Ext, "DienThoai", null) : this.commonMethodsTs.getData(item, "DienThoaiContact_Ext", null);
            if (!soDienThoai) {
              this.commonMethodsTs.createMessageWarning("Số điện thoại không hợp lệ");
              return;
            }
            window.open(`tel:${soDienThoai}`, '_system');
            resolve("call");

          }
        },
        /** Share */
        {
          text: 'Chia sẻ',
          cssClass: 'share',
          icon: 'share',
          handler: async () => {
            if (item.Id && item.RewriteUrl) {
              let link = this.commonMethodsTs.buildDynamicLinks_TinRao(item.Id, item.RewriteUrl);
              resolve("share");

            }

          }
        },
        /** Lưu xem sau */
        {
          text: item.IsFavorite ? "Bỏ lưu xem sau" : "Lưu xem sau",
          cssClass: 'bookmark',
          icon: 'bookmark',
          handler: async () => {
            this.commonMethodsTs.startLoading();
            try {
              if (item.IsFavorite) {
                // await this.dangTinDetailModal.removeFavorite(item.Id);
              }
              else {
                // await this.dangTinDetailModal.addFavorite(item.Id);
              }
            } catch (error) {

            }
            this.commonMethodsTs.stopLoading();

            resolve("bookmark")
          }
        },
        /** Copy */
        {
          text: 'Sao chép liên kết',
          cssClass: 'copy',
          icon: 'copy',
          handler: async () => {
            if (item.Id && item.RewriteUrl) {
              let link = this.commonMethodsTs.buildDynamicLinks_TinRao(item.Id, item.RewriteUrl);
              this.commonMethodsTs.clipboard.copy(link);
              this.commonMethodsTs.createMessageSuccess(commonMessages.M075);
              resolve("copy");

            }
          }
        },
      ];

      if (options.isFull) {
        buttons.push(
          {
            text: 'Chỉnh sửa',
            cssClass: 'create',
            icon: 'create',
            handler: () => {
              if (item.Id) {
                this.commonMethodsTs.toPage_MemberTinDang(item.Id);
                resolve("create");
              }
            }

          })
      };
      buttons.push({
        text: 'Bỏ qua',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          resolve("cancel");

        }
      });
      /** Trọng Hữu tạm đóng lại việc kiểm tra chức năng tin theo người đăng */
      // if(idThanhVien && idThanhVienTao && idThanhVienTao == idThanhVien)
      // {
      //   buttons= buttons.filter(item => item.icon != "call" && item.icon != "bookmark" );
      // }
      const actionSheet = await this.commonMethodsTs.actionSheetController.create({
        cssClass: 'action-sheets-basic',
        buttons: buttons
      });
      await actionSheet.present();
    })



  }

}