import { Component, Input, ElementRef, AfterViewInit, SimpleChanges } from '@angular/core';
import { typeNoiDung } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'view-baiviet',
  templateUrl: './view-baiviet.component.html'
})
export class ViewBaiVietComponent implements AfterViewInit {
  //#region ================================== Variable =========================
  // @Input() data: any;
  @Input() link: any = "";
  @Input() title: any = "";
  public typeNoiDung = typeNoiDung;
  constructor(
    public modalController: ModalController ) {}


  async ngOnChanges(changes: SimpleChanges) {

  }
  ngAfterViewInit() {
    // this.loadData();
  }
  cancel() {
    this.dismiss(null)
  }

  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }
}
