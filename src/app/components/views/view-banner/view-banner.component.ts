import { Component, Input, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SwiperComponent } from 'ngx-swiper-wrapper';
import { Variables } from 'src/app/core/variables';
@Component({
  selector: 'view-banner',
  templateUrl: './view-banner.component.html',
  styleUrls: ['./view-banner.scss']
})
export class ViewBannerComponent  {
  //#region ================================== Variable =========================
  // @Input() data: any;
  @Input() options: any = {};
 
  @ViewChild('usefulSwiper', { static: true }) usefulSwiper: any;
  isReady: boolean = false;
  listData: any[];
  public index: any = 0;
  public slideOpts: any = {
    initialSlide: 0,
    spaceBetween: 10,
    pagination: true,
    speed: 400,
  };
  //#endregion =============================== End variable ====================
  constructor(private _elementRef: ElementRef, ) {
  }
 
  getElement() {
    return this._elementRef.nativeElement;
  }
  indexChange(event: any) {

  }

  getData() {
    let data = [];

    try {

      if (Variables.listImageBanner && Variables.listImageBanner.length > 0) {
        Variables.listImageBanner.forEach(item => {
          data.push(`https://cdn.dailyxe.com.vn/image/banner-${item}j6.jpg`);
        })
  
        return data;
      }
     
  
    } catch (error) {
      
    }
    return data;

  }
}
