
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewBannerComponent } from './view-banner.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { PhotoGalleryModule } from '@twogate/ngx-photo-gallery'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageLazyLoadModule,
    SwiperModule,
    PhotoGalleryModule

  ],
  declarations: [ViewBannerComponent],
  exports: [ViewBannerComponent],

})
export class ViewBannerModule { }
