import { NgModule } from '@angular/core';
import { ViewBannerModule } from './view-banner/view-banner.module';
import { ViewDanhGiaModule } from './view-danhgia/view-danhgia.module';
import { ViewMemoryCacheModule } from './view-memory-cache/view-memory-cache.module';
import { ViewFavoriteLocalStoreModule } from './view-favorite-localstore/view-favorite-localstore.module';
import { ViewGalleryModule } from './view-gallery/view-gallery.module';
import { ViewRankModule } from './view-rank/view-rank.module';

@NgModule({
  exports: [
    ViewBannerModule,
    ViewDanhGiaModule,
    ViewMemoryCacheModule,
    ViewFavoriteLocalStoreModule,
    ViewGalleryModule,
    ViewRankModule,
  ],
})
export class ViewsModule { }

export { ViewBannerModule } from './view-banner/view-banner.module';
export { ViewDanhGiaModule } from './view-danhgia/view-danhgia.module';
export { ViewMemoryCacheModule } from './view-memory-cache/view-memory-cache.module';
export { ViewFavoriteLocalStoreModule } from './view-favorite-localstore/view-favorite-localstore.module';
export { ViewGalleryModule } from './view-gallery/view-gallery.module';
export { ViewRankModule } from './view-rank/view-rank.module';
