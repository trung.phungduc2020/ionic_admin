import { Input, Component } from '@angular/core';
import { DaiLyXeFavoriteLocalStore } from './dailyxe-favorite-localstore/dailyxe-favorite-localstore.page';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'view-favorite-localstore',
  templateUrl: './view-favorite-localstore.component.html',
})
export class ViewFavoriteLocalStoreComponent {
  @Input() isDialog: boolean = true;
  private modal: any;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController) {
  }
  async openDialog() {
    this.modal = await this.modalController.create({
      component: DaiLyXeFavoriteLocalStore,
      componentProps: {

      }
    });
    this.modal.onDidDismiss().then((res: any) => {
      if (res && res.data) {
        //xử lý khi load tắt dialog
      }
    });
    await this.modal.present();
  }


}