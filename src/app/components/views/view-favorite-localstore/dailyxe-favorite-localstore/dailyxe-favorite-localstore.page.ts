import { Variables, localStorageData, commonAttr } from 'src/app/core/variables';
import { Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { animationList } from 'src/app/shared/animations/animations';
import { ResponseBase } from 'src/app/core/entity';
import { DaiLyXeFavoriteLocalStoreModal } from './dailyxe-favorite-localstore.modal';
import { PluckPipe } from 'src/app/shared/pipes';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'dailyxe-favorite-localstore',
	templateUrl: './dailyxe-favorite-localstore.page.html',
	styleUrls: ['./dailyxe-favorite-localstore.page.scss'],
	animations: [animationList],

})
export class DaiLyXeFavoriteLocalStore {
	public data: any = [];
	public dataSelected: any = [];
	public lbl: any = labelPages;
	public totalItems = -1;
	constructor(
		public daiLyXeFavoriteLocalStoreModal: DaiLyXeFavoriteLocalStoreModal,
		public commonMethodsTs: CommonMethodsTs,
		public modalController: ModalController

	) {
		this.loadData();
	}
	public async loadData() {
		let params: any = {}
		if (!Variables.tokenThanhVien) {
			let data = [];


			let idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
			idThanhVien = idThanhVien ? idThanhVien : "0";
			// Lấy listLike từ localStore.
			let dataLS = this.commonMethodsTs.getDataPersonalizeLS();
			let listFavorite = dataLS[localStorageData.tinDangYeuThich + idThanhVien];

			// Lấy danh sách Id đã lưu từ localStore

			params.id = (new PluckPipe().transform(listFavorite, "value")).toString();
			if (!params.id) {
				this.totalItems = 0;
				this.data = [];
				return
			}
			params.displayItems = -1;
			let res: ResponseBase = await this.daiLyXeFavoriteLocalStoreModal.getData(params);
			res = new ResponseBase(res);
			if (res.isSuccess()) {
				this.totalItems = res.getTotalItems();
				this.data = data.concat(res.DataResult);
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		}

	}
	selected(id: any) {
		localStorage.setItem('saveLinkList', window.location.pathname);
		this.commonMethodsTs.toPage_HomeListTinDang(id);
		this.dismiss(id);
	}
	closeModal() {
		this.dismiss();
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}