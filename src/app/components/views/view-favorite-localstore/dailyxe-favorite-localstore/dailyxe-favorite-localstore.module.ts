import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { IonicRatingModule } from 'ionic-rating';
import { NgObjectPipesModule, NgDatePipesModule } from 'src/app/shared/pipes';
import { DaiLyXeFavoriteLocalStoreModal } from './dailyxe-favorite-localstore.modal';
import { DaiLyXeFavoriteLocalStore } from './dailyxe-favorite-localstore.page';
// The modal's component of the previous chapter
@NgModule({
  declarations: [
    DaiLyXeFavoriteLocalStore
  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ImageLazyLoadModule,
    IonicRatingModule,
    NgObjectPipesModule,
    NgDatePipesModule
  ],
  entryComponents: [
    DaiLyXeFavoriteLocalStore
  ],
  exports: [
    DaiLyXeFavoriteLocalStore
  ],
  providers: [DaiLyXeFavoriteLocalStoreModal]
})
export class DaiLyXeFavoriteLocalStoreModule { }