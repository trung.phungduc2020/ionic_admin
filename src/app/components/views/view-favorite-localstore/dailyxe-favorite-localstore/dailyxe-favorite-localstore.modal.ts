import { ThanhVienRaoVatBLL_InterfaceData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { commonParams, defaultWxh } from "src/app/core/variables";
import { ResponseBase } from "src/app/core/entity";

@Injectable()
export class DaiLyXeFavoriteLocalStoreModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData
    ) {
    }
    //GetData
    public async getData(search: any) {
        search = search || {};
        let res = new ResponseBase();
		try {
			let params: any = {};
			params[commonParams.displayItems] = search.displayItems;
			params[commonParams.displayPage] = search.displayPage;
			params[commonParams.id] = search.id;
			params[commonParams.trangThai] = 1;
			params[commonParams.orderString] = "NgayUp:desc";
			if (search.tinhTrang == 0 || search.tinhTrang == null || search.tinhTrang == '') {
				delete params[commonParams.tinhTrang];
			}
			res = await this.thanhVienRaoVatBLL_InterfaceData.searchDangTinByParams(params).toPromise();
			res = new ResponseBase(res);
			if (res.isSuccess()) {
				res.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);
				
				return res;
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return res;
    }
  
}
