import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ViewFavoriteLocalStoreComponent } from './view-favorite-localstore.component';
import { DaiLyXeFavoriteLocalStoreModule } from './dailyxe-favorite-localstore/dailyxe-favorite-localstore.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaiLyXeFavoriteLocalStoreModule,
  ],
  declarations: [ViewFavoriteLocalStoreComponent],
  exports: [ViewFavoriteLocalStoreComponent],

})
export class ViewFavoriteLocalStoreModule { }
