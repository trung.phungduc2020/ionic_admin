import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ViewMemoryCacheComponent } from './view-memory-cache.component';
import { DaiLyXeMemoryCacheModule } from './dailyxe-memory-cache/dailyxe-memory-cache.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaiLyXeMemoryCacheModule
  ],
  declarations: [ViewMemoryCacheComponent],
  exports: [ViewMemoryCacheComponent],

})
export class ViewMemoryCacheModule { }
