import { PluckPipe } from './../../../../shared/pipes/array/pluck';
import { FilterByPipe } from 'src/app/shared/pipes';
import { Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { DaiLyXeMemoryCacheModal } from './dailyxe-memory-cache.modal';
import { CauHinhHeThongService } from 'src/app/forms/member/services/cauHinhHeThong.service';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'dailyxe-memory-cache',
	templateUrl: './dailyxe-memory-cache.page.html',
	styleUrls: ['./dailyxe-memory-cache.page.scss'],
})
export class DaiLyXeMemoryCache {
	public data: any = [];
	public dataSelected: any = [];

	constructor(
		public daiLyXeMemoryCacheModal: DaiLyXeMemoryCacheModal,
		public commonMethodsTs: CommonMethodsTs,
		public cauHinhHeThongService: CauHinhHeThongService,
		public modalController: ModalController
	) {
		this.loadData();
	}
	public async loadData() {
		let params = {}
		this.data = await this.daiLyXeMemoryCacheModal.getData(params);
	}
	public async clearAll() {
		let res = await this.commonMethodsTs.alertConfirm('Bạn có chắc clear all?');
		if (res) {
			await this.daiLyXeMemoryCacheModal.clearAll();

		}
	}
	public async clear() {
		let res = await this.commonMethodsTs.alertConfirm('Bạn có chắc clear những cache bạn đã chọn!');
		if (res) {
			await this.daiLyXeMemoryCacheModal.clear(this.dataSelected);
		}
	}

	public async syncCauHinhHeThong() {
		await this.cauHinhHeThongService.syncDataFromServerToCache();
	}

	public async forcedUpdateConfigAllDevice(){

		let data = {
			Type: 1,
			TieuDe: "",
			NoiDung: "",
		}

		this.commonMethodsTs.storeMessageSendNotification(data);
	}

	ionChange() {
		this.dataSelected = new PluckPipe().transform(new FilterByPipe().transform(this.data, ["IsChecked"], true), "name");
	}
	closeModal() {
		this.dismiss();
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}