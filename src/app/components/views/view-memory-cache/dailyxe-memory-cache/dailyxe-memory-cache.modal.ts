import { PickPipe } from '../../../../shared/pipes/object/pick';
import { PluckPipe } from '../../../../shared/pipes/array/pluck';
import { GroupByPipe, SumPipe, TrurthifyPipe } from 'src/app/shared/pipes';
import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { AdminRaoVatBLL_OnlineStatistics, AdminRaoVatBLL_MemoryCache } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { commonMessages } from 'src/app/core/variables';
@Injectable()
export class DaiLyXeMemoryCacheModal {
    constructor(
        public adminRaoVatBLL_MemoryCache: AdminRaoVatBLL_MemoryCache,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(params: any) {
        //Gọi api
        return [{name: "dangtin"},{name: "dangtinrating"}];
    }
    public async clearAll() {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_MemoryCache.clearAllCache().toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M054)
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

    public async clear(listKey) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_MemoryCache.clearCache(listKey.toString()).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M054)
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
}
