import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { DaiLyXeMemoryCache } from './dailyxe-memory-cache.page';
import { AdminRaoVatBLL_MemoryCache } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { DaiLyXeMemoryCacheModal } from './dailyxe-memory-cache.modal';
import { FormsModule } from '@angular/forms';
// The modal's component of the previous chapter
@NgModule({
  declarations: [
    DaiLyXeMemoryCache
  ],
  imports: [
    FormsModule,
    IonicModule,
    CommonModule
  ],
  entryComponents: [
    DaiLyXeMemoryCache
  ],
  exports:[
    DaiLyXeMemoryCache
  ],
  providers: [AdminRaoVatBLL_MemoryCache, DaiLyXeMemoryCacheModal]
})
export class DaiLyXeMemoryCacheModule { }