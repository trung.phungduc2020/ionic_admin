import { DaiLyXeMemoryCache } from './dailyxe-memory-cache/dailyxe-memory-cache.page';
import { Input, Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'view-memory-cache',
  templateUrl: './view-memory-cache.component.html',
})
export class ViewMemoryCacheComponent {
  @Input() isDialog:boolean = true;
  private modal:any;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController)
  {
  }
  async openCache()
  {
    this.modal = await this.modalController.create({
      component: DaiLyXeMemoryCache,
      componentProps: {
        
      }
    });
    this.modal.onDidDismiss().then((res: any) => {
      if (res && res.data) {
        //xử lý khi load tắt dialog
      }
    });
    await this.modal.present();
  }


}