
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewDanhGiaDialogComponent } from './view-danhgia-dialog.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PhotoGalleryModule } from '@twogate/ngx-photo-gallery'
import { NgObjectPipesModule } from 'src/app/shared/pipes';
import { LongPressModule } from 'src/app/shared/directive/long-press';
import { IonicRatingModule } from 'ionic-rating';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageLazyLoadModule,
    NgObjectPipesModule,
    PhotoGalleryModule,
    LongPressModule,
    IonicRatingModule,
  ],
  declarations: [ViewDanhGiaDialogComponent],
  exports: [ViewDanhGiaDialogComponent],
  entryComponents: [ViewDanhGiaDialogComponent],
})
export class ViewDanhGiaDialogModule { }
