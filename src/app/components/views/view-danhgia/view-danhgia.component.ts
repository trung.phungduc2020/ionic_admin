import { Component, OnInit, OnChanges, Input } from "@angular/core";
import { currentPageDefault, Variables, commonMessages, defaultValue, commonAttr, noImageUser, defaultWxh } from "src/app/core/variables";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ViewDanhGiaModal } from "./view-danhgia.modal";
import { RaoVatDialogRatingTinDangService } from "src/app/shared/raovat-dialog/raovat-dialog-rating-tindang/raovat-dialog-rating-tindang.service";
import { ViewDanhGiaDialogComponent } from './view-danhgia-dialog/view-danhgia-dialog.component';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'view-danhgia-component',
	templateUrl: './view-danhgia.component.html',
	styleUrls: ['./view-danhgia.component.scss'],
})
export class ViewDanhGiaComponent implements OnInit, OnChanges {
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public viewDanhGiaModal: ViewDanhGiaModal,
		public raoVatDialogRatingTinDangService: RaoVatDialogRatingTinDangService,
		public modalController: ModalController
	) {
	}
	public modal: any;
	public variables = Variables;
	public defaultValue = defaultValue;
	public totalItems = 0;
	public currentPage = currentPageDefault;
	public data: any;
	public idThanhVien: any;
	@Input() idDangTin: any;
	@Input() dataDangTin: any;

	// @Input() averageRating: any = 0;
	//#endregion ============================= End variables ========================

	//#endregion
	async ngOnInit() {
		this.idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
		this.commonMethodsTs.platform.resume.subscribe(async () => {
			this.loadData();
		});
	}
	ngOnChanges() {
		if (this.dataDangTin) {
			this.idDangTin = this.dataDangTin.Id;
		}
		this.loadData();
	}
	ionViewDidEnter() {
		this.loadData();
	}

	public async doInfinite(infiniteScroll) {
		try {
			if ((this.data.length > 0 && this.data.length < this.totalItems)) {
				await this.loadData(this.currentPage + 1);
			}

		} catch (error) {
		}
		infiniteScroll.target.complete();

	}
	//#region private
	public async loadData(currentPage = currentPageDefault) {
		this.totalItems = 0;
		let params = {
			idTinDangs: this.idDangTin,
			displayItems: defaultValue.maxDanhGiaDefault,
			currentPage: currentPage
		}
		try {
			let lApi = await Promise.all([this.viewDanhGiaModal.getDataDangTinById(this.idDangTin, this.dataDangTin), this.viewDanhGiaModal.getData(params)]);
			this.currentPage = currentPage;
			if (this.currentPage == currentPageDefault) {
				this.data = [];
			}
			if (!(+this.idDangTin > 0)) {
				this.data = [];
				return;
			}
			this.dataDangTin = lApi[0];

			let resDanhGia: any = lApi[1];
			resDanhGia.forEach(async (element: any) => {
				let idFileDaiDienThanhVien = element && element.IdFileDaiDien ? element.IdFileDaiDien : "-1";
				element.UrlHinhDaiDienThanhVien = +idFileDaiDienThanhVien > 0 ? this.commonMethodsTs.builLinkImage(element.RewriteUrl, idFileDaiDienThanhVien, defaultWxh.two) : noImageUser;
			});
			this.data = resDanhGia;

		} catch (error) {

		}

	}
	async openModalRatingTinDang() {
		let idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);

		if (this.dataDangTin && this.dataDangTin.IdThanhVien == idThanhVien) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M087);
			return;
		}



		let res: any = await this.raoVatDialogRatingTinDangService.show(this.dataDangTin);
		if (res != null && res.content && res.rate > 0 && res.tieuDe) {
			await this.viewDanhGiaModal.insertRating(res.rate, res.content, this.idDangTin, res.tieuDe);
			this.loadData();

		}
	}

	async openDialogDanhGia() {
		this.modal = await this.modalController.create({
			component: ViewDanhGiaDialogComponent,
			componentProps: { idDangTin: this.idDangTin }
		});
		this.modal.onDidDismiss().then((res: any) => {
		});
		await this.modal.present();

	}

}
