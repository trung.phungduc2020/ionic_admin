import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ControllersModule } from "../../controllers";
import { IonicRatingModule } from 'ionic-rating';
import { NgPipesModule } from "src/app/shared/pipes";
import { ThanhVienRaoVatBLL_InterfaceData } from "src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL";
import { ViewDanhGiaComponent } from "./view-danhgia.component";
import { ViewDanhGiaModal } from "./view-danhgia.modal";
import { RaoVatDialogRatingTinDangService } from "src/app/shared/raovat-dialog/raovat-dialog-rating-tindang/raovat-dialog-rating-tindang.service";
import { ViewDanhGiaDialogModule } from './view-danhgia-dialog/view-danhgia-dialog.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		// ImageLazyLoadModule,
		ControllersModule,
		IonicRatingModule,
		NgPipesModule,
		ViewDanhGiaDialogModule
	],

	declarations: [
		ViewDanhGiaComponent
	],
	providers: [

		ThanhVienRaoVatBLL_InterfaceData,
		RaoVatDialogRatingTinDangService,
		ViewDanhGiaModal
	],
	exports: [ViewDanhGiaComponent],

})
export class ViewDanhGiaModule { }
