import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ThanhVienRaoVatBLL_InterfaceData, ThanhVienRaoVatBLL_InteractData } from "src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL";
import { ResponseBase } from "src/app/core/entity";
import { displayItemDefault, commonMessages, Variables, commonAttr } from "src/app/core/variables";
import * as _ from 'lodash';

@Injectable()
export class ViewDanhGiaModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData,
        public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    ) { }
    public async getDataDangTinById(idTinDangs, data) {
        if (!data) {
            data = {};
            try {
                let res: ResponseBase = await this.thanhVienRaoVatBLL_InterfaceData.searchDangTinById(idTinDangs).toPromise();
                res = new ResponseBase(res);
                if (res.isSuccess()) {
                    data = res.getFirstData();
                }
            } catch (error) {
                this.commonMethodsTs.createMessageErrorByTryCatch(error);
            }
        }

        data.TotalRate = data.TotalRate || 0;
        data.AvgRate = data.AvgRate || 0;
        data.Rate1 = data.Rate1 || 0;
        data.Rate2 = data.Rate2 || 0;
        data.Rate3 = data.Rate3 || 0;
        data.Rate4 = data.Rate4 || 0;
        data.Rate5 = data.Rate5 || 0;
        data.Rate1PT = _.round(+data.TotalRate > 0 ? data.Rate1 / data.TotalRate * 100 : 0, 1);
        data.Rate2PT = _.round(+data.TotalRate > 0 ? data.Rate2 / data.TotalRate * 100 : 0, 1);
        data.Rate3PT = _.round(+data.TotalRate > 0 ? data.Rate3 / data.TotalRate * 100 : 0, 1);
        data.Rate4PT = _.round(+data.TotalRate > 0 ? data.Rate4 / data.TotalRate * 100 : 0, 1);
        data.Rate5PT = _.round(+data.TotalRate > 0 ? data.Rate5 / data.TotalRate * 100 : 0, 1);

        return data;
    }
    //getData
    public async getData(params: any) {
        try {
            let data_params = {
                idTinDangs: params.idTinDangs,
                idUsers: params.idUsers,
                displayItems: params.displayItems || displayItemDefault,
                displayPage: params.currentPage,
                isCache: true
            }
            let res: ResponseBase = await this.thanhVienRaoVatBLL_InterfaceData.searchDangTinRating(data_params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return res.DataResult;

            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return {};
    }
    //insertRating
    public async insertRating(rate, content, idDangTin, tieuDe) {
        let info = this.commonMethodsTs.getInfoThanhVien();
        try {
            let dataRating = {
                Rating: rate,
                IdDangTin: idDangTin,
                IdUser: info.Id,
                IdFileDaiDien: info.IdFileDaiDien,
                HoTenThanhVien: info.HoTen,
                TieuDeDangTin_Ext: tieuDe,
                Description: content,
                TrangThai: 2
            }
       
            let result = await this.thanhVienRaoVatBLL_InteractData.insertDangTinRating(dataRating).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M039);
                return true
            }
            else {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

}
