
import { ViewRankComponent } from './view-rank.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewRankModal } from './view-rank.modal';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,

  ],
  declarations: [ViewRankComponent],
  exports: [ViewRankComponent],
  providers: [ViewRankModal]

})
export class ViewRankModule { }
