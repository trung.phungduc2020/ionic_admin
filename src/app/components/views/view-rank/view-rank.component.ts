import { Component, Input, ElementRef, AfterViewInit, ViewChild, NgZone } from '@angular/core';
import { ViewRankModal } from './view-rank.modal';
@Component({
  selector: 'view-rank',
  templateUrl: './view-rank.component.html',
  styleUrls: ['./view-rank.scss']
})
export class ViewRankComponent {
  //#region ================================== Variable =========================
  // @Input() data: any;
  @Input() options: any = {};
  @Input() idSelected = -1;
  @ViewChild("segment", { static: false }) segment: any;
  public index: any = 0;
  public data;

  //#endregion =============================== End variable ====================
  constructor(private ngZone: NgZone, public viewRankModal: ViewRankModal) {
    this.loadData();

  }

  loadData() {
    this.ngZone.run(() => {
      this.viewRankModal.getData().then(res => {
        this.data = res;
      });

    })
  }
}
