import { CommonMethodsTs } from "src/app/core/common-methods";
import { Injectable } from "@angular/core";
import { Variables} from "src/app/core/variables";
import { ResponseBase } from "src/app/core/entity";
import { ThanhVienRaoVatBLL_InteractData } from "src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL";
@Injectable()
export class ViewRankModal {
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData
	) {
	}
	
	public async getData() {
		
		if(!Variables.listUserRank || Variables.listUserRank.length == 0)
		{
			try {
				
				let usr = await this.thanhVienRaoVatBLL_InteractData.searchUserRank().toPromise();
				usr = new ResponseBase(usr);
				if (usr.isSuccess()) {
					Variables.listUserRank = (usr.DataResult || []);
				}
			} catch (error) {
				this.commonMethodsTs.createMessageErrorByTryCatch(error);
			}
		}
		return Variables.listUserRank;
	}
}
