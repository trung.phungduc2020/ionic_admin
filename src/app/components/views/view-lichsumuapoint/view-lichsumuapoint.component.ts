import { Component, Input, ElementRef, AfterViewInit, SimpleChanges } from '@angular/core';
import { typeNoiDung, commonParams, currentPageDefault, typeHistoryPoint, displayItemDefault } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ModalController } from '@ionic/angular';
import { AdminRaoVatBLL_HistoryPoint } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
@Component({
  selector: 'view-lichsumuapoint',
  templateUrl: './view-lichsumuapoint.component.html',
  providers: [AdminRaoVatBLL_HistoryPoint],
})
export class ViewLichSuMuaPointComponent implements AfterViewInit {
  //#region ================================== Variable =========================
  @Input() idThanhVien: any;
  public currentPage: any;
  public data: any;
  public totalItems: any;
  public typeHistoryPoint:any = typeHistoryPoint;

  constructor(
    public modalController: ModalController,
    public commonMethodsTs: CommonMethodsTs,
    public adminRaoVatBLL_HistoryPoint: AdminRaoVatBLL_HistoryPoint) { }


  async ngOnChanges(changes: SimpleChanges) {

  }
  async ngAfterViewInit() {
    if (!this.data) {
      await this.loadData();

    }
  }
  cancel() {
    this.dismiss(null)
  }

  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }

  async doRefresh(event) {
    this.init();
    event.target.complete();
  }
  async init() {
    let data = this.commonMethodsTs.cloneArray(this.data || []);
    this.data = null;
    await this.loadData();
    if (!this.data) {
      this.data = data;
    }
  }
  async loadData(currentPage = currentPageDefault) {
    try {
      // gọi api lấy list lịch sử
      let params: any = {};
      params[commonParams.displayItems] = displayItemDefault;
      params[commonParams.displayPage] = currentPage;
      params[commonParams.idsThanhVien] = this.idThanhVien;
      let res: ResponseBase = await this.adminRaoVatBLL_HistoryPoint.searchByParams(params).toPromise();
      res = new ResponseBase(res);
      if (res.isSuccess()) {
        this.currentPage = currentPage;
        if (currentPage == currentPageDefault) {
          this.data = [];
        }
        this.data = this.data.concat(res.DataResult);
        this.totalItems = res.getTotalItems();
      }
      else {
        this.commonMethodsTs.createMessageError(res.getMessage());
      }
    } catch (err) {
      this.commonMethodsTs.createMessageError(err);
    }
  }

  public async doInfinite(infiniteScroll) {
    try {
      if (this.data.length > 0 && this.data.length < this.totalItems) {
        await this.loadData(this.currentPage + 1);
      }
    } catch (error) {
    }
    infiniteScroll.target.complete();
  }
}
