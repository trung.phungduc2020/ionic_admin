
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewLichSuMuaPointComponent } from './view-lichsumuapoint.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgObjectPipesModule } from 'src/app/shared/pipes';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageLazyLoadModule,
    NgObjectPipesModule
  ],
  declarations: [ViewLichSuMuaPointComponent],
  entryComponents:[ViewLichSuMuaPointComponent],
  exports: [ViewLichSuMuaPointComponent]
})
export class ViewLichSuMuaPointModule { }
