import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, Input, AfterViewInit, SimpleChanges } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'view-gallery-dialog',
  templateUrl: './view-gallery-dialog.component.html',
})
export class ViewGalleryDialogComponent implements AfterViewInit {
  //#region ================================== Variable =========================
  // @Input() data: any;
  public prefixIdGallery: any = "gallery-";
  public currentUrl: any = "";
  @Input() data: any;
  @Input() index: any = 0;
  @Input() title: any = "";
  @Input() description: any = "";
  @Input() price: any = "";

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController) { }


  async ngOnChanges(changes: SimpleChanges) {

  }

  ngAfterViewInit() {
    this.scrollIntoView(this.index);
  }

  scrollIntoView(index: string) {
    let element = this.getContent("#" + this.prefixIdGallery + index);
    setTimeout(() => {
      element.scrollIntoView({ behavior: "smooth", block: "start", inline: "start" });
    },200);
  }

  getContent(selector) {
    return document.querySelector(selector);
  }

  cancel() {
    this.dismiss(null)
  }

  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }
}
