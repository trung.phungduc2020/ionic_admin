
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewGalleryDialogComponent } from './view-gallery-dialog.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PhotoGalleryModule } from '@twogate/ngx-photo-gallery'
import { NgObjectPipesModule } from 'src/app/shared/pipes';
import { LongPressModule } from 'src/app/shared/directive/long-press';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageLazyLoadModule,
    NgObjectPipesModule,
    PhotoGalleryModule,
    LongPressModule
  ],
  declarations: [ViewGalleryDialogComponent],
  exports: [ViewGalleryDialogComponent],
  entryComponents: [ViewGalleryDialogComponent],
})
export class ViewGalleryDialogModule { }
