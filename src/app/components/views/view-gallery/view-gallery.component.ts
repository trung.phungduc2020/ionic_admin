import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component, Input, AfterViewInit, SimpleChanges } from '@angular/core';
import { TrurthifyPipe } from 'src/app/shared/pipes';
import { defaultValue } from 'src/app/core/variables';
import { ViewGalleryDialogComponent } from './view-gallery-dialog/view-gallery-dialog.component';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'view-gallery',
  templateUrl: './view-gallery.component.html',
  styleUrls: ['./view-gallery.scss']
})
export class ViewGalleryComponent implements AfterViewInit {
  //#region ================================== Variable =========================
  // @Input() data: any;
  @Input() options: any = {};
  @Input() ids: any = {};
  @Input() data: any = [];
  @Input() maxImageForLayout: any = defaultValue.maxImageForLayout;
  @Input() hasSlider: boolean = true;
  @Input() className: any = "";
  @Input() title: any = "";
  @Input() description: any = "";
  @Input() price: any = "";

  
  public _className: any = "";
  private modal: any;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController) {
  }
  async ngOnChanges(changes: SimpleChanges) {
    if (changes["ids"]) {
      let data = [];

      try {
        if (!(this.ids instanceof Array)) {
          this.ids = JSON.parse(this.ids.toString());
        }
        this.ids = new TrurthifyPipe().transform(this.ids.map(Number));
        this.ids.forEach(id => {
          data.push(this.commonMethodsTs.builLinkImage(this.options.RewriteUrl || "image", id))
        });
      } catch (error) {

      }

      this.data = data;
    }
    this._className = this.className || await this.commonMethodsTs.getMaLayoutAlbum(this.data);
  }
  ngAfterViewInit() {
    // this.loadData();
  }

  async openDialogImages(data, index){
    this.modal = await this.modalController.create({
      component: ViewGalleryDialogComponent,
      cssClass:" view-gallery",
      componentProps: { data: data, index: index, title: this.title, description: this.description, price: this.price }
    });
    this.modal.onDidDismiss().then((res: any) => {
    });
    await this.modal.present();
  
  }

  
}
