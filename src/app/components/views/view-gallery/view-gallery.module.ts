
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ViewGalleryComponent } from './view-gallery.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { PhotoGalleryModule } from '@twogate/ngx-photo-gallery'
import { ViewGalleryDialogModule } from './view-gallery-dialog/view-gallery-dialog.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageLazyLoadModule,
    SwiperModule,
    PhotoGalleryModule,
    ViewGalleryDialogModule
  ],
  declarations: [ViewGalleryComponent],
  exports: [ViewGalleryComponent],

})
export class ViewGalleryModule { }
