import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: 'admin',
    loadChildren: './forms/raovat/admin/admin.page.module#AdminPageModule'

  },
  {
    path: '',
    loadChildren: './forms/raovat/admin/admin.page.module#AdminPageModule'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
