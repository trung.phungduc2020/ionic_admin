import { CoreProcess } from 'src/app/core/core-process';
import { Component, OnInit } from '@angular/core';
import { defaultWxh, Variables, commonParams, noImageUser, commonMessages } from "src/app/core/variables";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from 'src/app/core/entity';
import { AdminDaiLyXeBLL_Admin } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_DangTin, AdminRaoVatBLL_UserPoint } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { HttpParams } from '@angular/common/http';
import { CoreVariables } from 'src/app/core/core-variables';
import { CMSUserPointListModal } from '../cms-userpoint/cms-userpoint-list/cms-userpoint-list.modal';
import { AuthService } from 'src/app/core/http-interceptor/auth/auth.service';
import { AuthAdminService } from 'src/app/core/http-interceptor/auth/auth-admin.service';
@Component({
	selector: 'cms-dashboard',
	templateUrl: './cms-dashboard.page.html',
	styleUrls: ['./cms-dashboard.page.scss'],
	providers: [AdminDaiLyXeBLL_Admin, AdminRaoVatBLL_DangTin, AdminRaoVatBLL_UserPoint, CMSUserPointListModal]
})
export class CMSDashboardPage implements OnInit {
	public data: any = {};
	imgSrc: any;
	onChangeImage: any;
	public variables = Variables;
	public coreVariables = CoreVariables
	public toggle: any = {
		ThongBao: false,
		ViPham: false,
		ThongKe: false,
	}
	public noImageUser: any = noImageUser;
	public numNotApproved: number = 0;
	public numThanhVienMuaPoint: number = 0;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public coreProcess: CoreProcess,
		public adminRaoVatBLL_DangTin: AdminRaoVatBLL_DangTin,
		public cmsUserPointListModal: CMSUserPointListModal,
		public authAdminService: AuthAdminService

	) {
		if (!Variables.infoAdmin || !(Variables.infoAdmin.Id > 0)) {
			this.commonMethodsTs.createMessageError("Tài khoản không hợp lệ");
			this.commonMethodsTs.goBack();
			return;
		}
		this.data = Variables.infoAdmin;

	}
	ngOnInit() { }
	async ionViewWillEnter() {
		this.getNotApproved();
		this.getCountThanhVienMuaPoint();
		this.commonMethodsTs.storeToken();
	}

	async getNotApproved() {
		// gọi api lấy dữ liệu tree men
		this.numNotApproved = 0
		try {
			let params: HttpParams = new HttpParams();
			params = params.set(commonParams.displayItems, "1");
			params = params.set(commonParams.trangThai, "1");
			params = params.set(commonParams.isDuyet, "false");

			let res: ResponseBase = await this.adminRaoVatBLL_DangTin.getByParams(params).toPromise();
			res = new ResponseBase(res);
			if (res.isSuccess()) {
				this.numNotApproved = res.Pagination!.TotalItems || 0
			}
			else {
			}
		} catch (error) {
		}
	}


	async getCountThanhVienMuaPoint() {
		// gọi api lấy dữ liệu tree men
		this.numThanhVienMuaPoint = 0
		try {
			let params: any = {};
			params.displayItems = 1;
			params.trangThai = "1,2";
			let res: ResponseBase = await this.cmsUserPointListModal.getData(params);
			res = new ResponseBase(res);
			if (res.isSuccess()) {
				this.numThanhVienMuaPoint = res.Pagination!.TotalItems || 0
			}
			else {
			}
		} catch (error) {
		}
	}

	async logout() {
		
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn <strong>thoát</strong>');
		if (resAlert) {
			let res = this.authAdminService.logout();
			if (res) {
				this.commonMethodsTs.toPage_AdminLogin();
			}
			else {
				this.commonMethodsTs.createMessageError("Đăng xuất thất bại");
			}
		}

	}
}
