import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CMSDashboardPage } from './cms-dashboard.page';
import { ViewMemoryCacheModule } from 'src/app/components/views';

const routes: Routes = [
  {
    path: '',
    component: CMSDashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewMemoryCacheModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CMSDashboardPage]
})
export class CMSDashboardPageModule {}
