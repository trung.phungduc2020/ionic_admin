import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSUserPointListPage } from './cms-userpoint-list/cms-userpoint-list.page';
import { CMSUserPointDetailPage } from './cms-userpoint-detail/cms-userpoint-detail.page';

const appRoutes: Routes = [
    {
        path: '',
        component: CMSUserPointListPage,
      },
      {
        path: ':id',
        component: CMSUserPointDetailPage,
      }
];

export const CMSUserPoint_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);