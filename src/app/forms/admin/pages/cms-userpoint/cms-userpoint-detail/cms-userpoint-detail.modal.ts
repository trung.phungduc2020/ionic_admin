import { commonMessages, Variables } from '../../../../../core/variables';
import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { AdminRaoVatBLL_UserPoint, AdminRaoVatBLL_UserRank, AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_MarketingPlan } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { commonParams } from 'src/app/core/variables';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
@Injectable()
export class CMSUserPointDetailModal {
	constructor(
		public adminRaoVatBLL_UserRank: AdminRaoVatBLL_UserRank,
		public adminRaoVatBLL_UserPoint: AdminRaoVatBLL_UserPoint,
		public adminRaoVatBLL_ThanhVien: AdminRaoVatBLL_ThanhVien,
		public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
		public adminRaoVatBLL_MarketingPlan: AdminRaoVatBLL_MarketingPlan,
		public commonMethodsTs: CommonMethodsTs,
	) {
	}
	public async getData(id) { //chủ yếu là thông point và có thêm một ít thông tin thành viên
		let lstRank = await this.getListRank();
		let result: ResponseBase = await this.adminRaoVatBLL_UserPoint.getIndex(id).toPromise();
		result = new ResponseBase(result);
		if (result.isSuccess()) {
			let data = result.getFirstData();
			let lapi = [];
			if (data.IdMarketingPlan > 0) {
				lapi.push(this.getMarketingPlan(data.IdMarketingPlan));
			}
			data.ThanhVien_Ext = data.ThanhVien_Ext.DataResult[0] || {}
			if (data.IdThanhVien > 0) {
				lapi.push(this.getThanhVienRaoVat(data.IdThanhVien));

			}
			let dataMarketingCode = {};
			let thanhVienRaoVat: any = {};
			if (lapi && lapi.length > 0) {
				let lres = await Promise.all(lapi);
				if (data.IdMarketingPlan > 0) {
					dataMarketingCode = lres[0];
					thanhVienRaoVat = lres[1]

				}
				else {
					thanhVienRaoVat = lres[0];
				}

			}
			data.MarketingPlan_Ext = dataMarketingCode;
			data.TongTien = +data.PointTmp * +Variables.ratePointToVND * 1000;
			data.DataRank = this.commonMethodsTs.searchItemById(lstRank, thanhVienRaoVat.IdRank) || Variables.listUserRank[0];
			thanhVienRaoVat.IdRank = data.DataRank.Id
			data.DataGiamUuDai = this.commonMethodsTs.calcValueUuDai(data.MarketingPlan_Ext, data.TongTien, data.PointTmp) || {};
			let tongTienConLai = data.TongTien - +data.DataGiamUuDai.tienGiamUuDai;
			data.DataRank.TienGiamThanhVien = ((+data.DataRank.PercentDiscount || 0) * tongTienConLai) / 100;
			return data;
		}

	}
	async getListRank() {

		if (!Variables.listUserRank || Variables.listUserRank.length == 0) {
			let usr = await this.adminRaoVatBLL_UserRank.searchUserRank().toPromise();
			usr = new ResponseBase(usr);
			if (usr.isSuccess()) {
				Variables.listUserRank = (usr.DataResult || []);
			}
		}
		return Variables.listUserRank;
	}
	public async getThanhVienRaoVat(idThanhVien) {
		let data = {};
		let res: ResponseBase = await this.adminRaoVatBLL_ThanhVien.getIndex(idThanhVien).toPromise();
		res = new ResponseBase(res);
		if (res.isSuccess()) {

			data = res.getFirstData();
		}
		else {
			throw res.getMessage();
		}
		return data
	}
	public async getMarketingPlan(idMarketingPlan) {
		let dataResult: any;
		let res: ResponseBase = await this.adminRaoVatBLL_MarketingPlan.getIndex(idMarketingPlan).toPromise();
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			let data = res.DataResult;
			if (data && data.length > 0) {
				dataResult = data[0];
			}
		}
		else {
			throw res.getMessage();
		}
		return dataResult;
	}
	async registAdminApproveUserPoint(idUserPoint) {
		try {

			let result = await this.adminRaoVatBLL_UserPoint.registAdminApproveUserPoint(idUserPoint).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				this.commonMethodsTs.createMessageSuccess(commonMessages.M067);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}
	async removeAdminApproveUserPoint(idUserPoint) {
		try {
			let result = await this.adminRaoVatBLL_UserPoint.removeAdminApproveUserPoint(idUserPoint).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				this.commonMethodsTs.createMessageSuccess(commonMessages.M081);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}
	async  xacNhanMuaPoint(idThanhVien, dataRank, userPoint, note) { // update thông tin
		try {
			// goi api update thong tin point
			let ids = idThanhVien;
			let idRank = dataRank.Id;
			let percentDiscount = dataRank.PercentDiscount;
			let result: ResponseBase = await this.adminRaoVatBLL_UserPoint.xacNhanMuaPoint(ids, idRank, percentDiscount, note).toPromise();
			result = new ResponseBase(result);
			if (!result.isSuccess()) {
				this.commonMethodsTs.createMessageError(result.getMessage());
				return false;
			}
			else {
				// Broadcast signal => Mua Poin
				let dataNotification = {
					Type: 2,
					TieuDe: commonMessages.N008,
					NoiDung: this.commonMethodsTs.stringFormat(commonMessages.N009, userPoint.PointTmp),
					IdThanhVien: idThanhVien
				}
				// this.commonMethodsTs.storeMessageSendNotification(dataNotification);
				userPoint.Point = +userPoint.Point + +userPoint.PointTmp;
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);

				userPoint.PointTmp = null;
				if (result.StrResult != '') {
					// Broadcast signal => Thăng hạng Ranking
					let dataNotification = {
						Type: 2,
						TieuDe: commonMessages.N001,
						NoiDung: this.commonMethodsTs.stringFormat(commonMessages.N002, result.StrResult),
						IdThanhVien: idThanhVien
					}
					// this.commonMethodsTs.storeMessageSendNotification(dataNotification);
				}

				return true;
			}
		} catch (err) {
			this.commonMethodsTs.createMessageError(err);
		}
		return false;
	}

}	
