import { Component } from '@angular/core';
import { CommonMethodsTs } from "src/app/core/common-methods";
import { commonParams, typeHistoryPoint, Variables } from 'src/app/core/variables';
//provides
import { CMSUserPointDetailModal } from './cms-userpoint-detail.modal';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ViewLichSuMuaPointComponent } from 'src/app/components/views/view-lichsumuapoint/view-lichsumuapoint.component';
@Component({
	selector: 'cms-userpoint-detail',
	templateUrl: './cms-userpoint-detail.page.html',
	styleUrls: ['./cms-userpoint-detail.page.scss'],
	providers: [CMSUserPointDetailModal]
})
export class CMSUserPointDetailPage {
	//#region variables
	static status = false;
	public data: any;
	public options: any =
		{
			rate: Variables.ratePointToVND,
			typeHistoryPoint: typeHistoryPoint
		}
	// //#endregion
	// //#region constructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsUserPointDetailModal: CMSUserPointDetailModal,
		public activatedRoute: ActivatedRoute,
		public modalController: ModalController
	) {
	}
	// //#endregion
	async ngOnInit() {
	}
	ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			let id = params[commonParams.id] || -1;
			if (+id <= 0) {
				this.commonMethodsTs.goBack();
				return;
			}
			await this.loadData(id);
		});
	}
	// //#region private
	async loadData(id?: any) {
		try {
			this.data = await this.cmsUserPointDetailModal.getData(id);
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
			this.commonMethodsTs.goBack();
		}
	}

	async registAdminApproveUserPoint() {
		this.commonMethodsTs.startLoading();
		await this.cmsUserPointDetailModal.registAdminApproveUserPoint(this.data.Id);
		await this.loadData();
		this.commonMethodsTs.stopLoading();


	}

	async removeAdminApproveUserPoint() {
		this.commonMethodsTs.startLoading();
		await this.cmsUserPointDetailModal.removeAdminApproveUserPoint(this.data.Id);
		await this.loadData();
		this.commonMethodsTs.stopLoading();

	}

	async savePoint() {
		const alert = await this.commonMethodsTs.alertController.create({

			message: 'Vui lòng <strong>nhập</strong> ghi chú',
			inputs: [
				{
					name: 'ghiChu',
					type: 'text',
					placeholder: 'Ghi chú ...',
				}
			],
			buttons:
				[
					{
						text: 'Hủy bỏ',
						role: 'cancel',
						cssClass: 'secondary',
						handler: () => {
						}
					},
					{
						text: 'Đồng ý',
						handler: async (event) => {
							if (!event.ghiChu) {
								this.commonMethodsTs.createMessageWarning("Vui lòng nhập ghi ghú");
								return;
							}
							if (+this.data.PointTmp > 0) {
								this.options.note = event.ghiChu;
								await this.xacNhanMuaPoint();

							}
						}
					}
				]
		});
		await alert.present();
	}

	private async  xacNhanMuaPoint() { // update thông tin
		this.commonMethodsTs.startLoading();
		try {
			// goi api update thong tin point
			let ids = this.data.IdThanhVien;
			let note = this.options.note;
			let res = await this.cmsUserPointDetailModal.xacNhanMuaPoint(ids, this.data.DataRank, this.data, note);
			if (res) {
				CMSUserPointDetailPage.status = true;
				this.commonMethodsTs.goBack();
			}
			else {
				this.loadData();
			}
		} catch (err) {
			this.commonMethodsTs.createMessageError(err);
		}
		this.commonMethodsTs.stopLoading();
	}


	async onLichSuMuaPoint() {
		const modal = await this.modalController.create({
			component: ViewLichSuMuaPointComponent,
			componentProps: { idThanhVien: this.data.IdThanhVien }
		});
		modal.onDidDismiss().then((res: any) => {
		});
		await modal.present();
	}
}
