import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { CMSUserPointDetailPage } from './cms-userpoint-detail/cms-userpoint-detail.page';
import { CMSUserPointListPage } from './cms-userpoint-list/cms-userpoint-list.page';
import { AdminRaoVatBLL_UserPoint, AdminRaoVatBLL_HistoryPoint, AdminRaoVatBLL_UserRank, AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_DaiLyXeData, AdminRaoVatBLL_HistoryProfile, AdminRaoVatBLL_MarketingPlan, } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { ViewGalleryModule, ViewRankModule } from 'src/app/components/views';
import { CMSUserPointDetailModal } from './cms-userpoint-detail/cms-userpoint-detail.modal';
import { NgDatePipesModule } from 'src/app/shared/pipes';
import { CMSUserPoint_RoutingModule } from './cms-userpoint.router';
import { ViewLichSuMuaPointModule } from 'src/app/components/views/view-lichsumuapoint/view-lichsumuapoint.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
		CMSUserPoint_RoutingModule,
		DaiLyXeIonNumberModule,
		ControllersModule,
		ViewGalleryModule,
		NgDatePipesModule,
		ViewRankModule,
		ViewLichSuMuaPointModule
	],
	declarations: [
		CMSUserPointListPage,
		CMSUserPointDetailPage,
	],

	providers: [
		AdminDaiLyXeBLL_ThanhVien,
		AdminRaoVatBLL_UserPoint,
		AdminRaoVatBLL_UserRank,
		AdminRaoVatBLL_ThanhVien,
		AdminRaoVatBLL_MarketingPlan
	]
})
export class CMSUserPointPageModule { }
