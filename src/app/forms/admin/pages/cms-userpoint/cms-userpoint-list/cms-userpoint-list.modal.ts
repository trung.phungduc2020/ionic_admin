import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { AdminRaoVatBLL_UserPoint } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { HttpParams } from '@angular/common/http';
import { commonParams, defaultWxh, commonAttr, commonMessages, Variables, noImage } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { PluckPipe } from 'src/app/shared/pipes';


@Injectable()
export class CMSUserPointListModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public adminRaoVatBLL_UserPoint: AdminRaoVatBLL_UserPoint
    ) {
    }
    //GetData
    public async getData(search: any) {

        // gọi api lấy dữ liệu tree men
        search = search || {};

        let data = null;
        try {
            // let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.displayItems, search.displayItems);
            params = params.set(commonParams.displayPage, search.displayPage);
            params = params.set(commonParams.tuKhoaTimKiem, search.tuKhoaTimKiem);
            params = params.set(commonParams.isViewType, "true");
            params = params.set(commonParams.orderString, "NgayCapNhat:desc");
            params = params.set(commonParams.allIncludingString, "true");
            let res: ResponseBase = await this.adminRaoVatBLL_UserPoint.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult.forEach(element => {
                    try {
                        let dataThanhVien = element.ThanhVien_Ext.DataResult[0];
                        if(dataThanhVien)
                        {
                            element.HoTenThanhVien = dataThanhVien.HoTen;
                            element.DienThoaiThanhVien = dataThanhVien.DienThoai;

                            let idFileDaiDien = this.commonMethodsTs.getData(dataThanhVien, "IdFileDaiDien", -1);
                            let rewriteUrl = this.commonMethodsTs.getData(dataThanhVien, "RewriteUrl", "hinh-dai-dien");
                            element.UrlHinhDaiDien = idFileDaiDien > 0 ? this.commonMethodsTs.builLinkImage(rewriteUrl, idFileDaiDien, defaultWxh.two) : noImage;
                        }
                    } catch (error) {}
                   
                    
                });
                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }

        return data;
    }

    
}
