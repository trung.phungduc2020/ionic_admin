import { Component, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
// provider
import { AdminRaoVatBLL_UserRank, AdminRaoVatBLL_ThanhVien } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams } from 'src/app/core/variables';
import { ResponseBase, ItemRecord } from 'src/app/core/entity';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
import { CMSUserPointDetailPage } from '../cms-userpoint-detail/cms-userpoint-detail.page';
import { CMSUserPointListModal } from './cms-userpoint-list.modal';
@Component({
	selector: 'cms-userpoint-list',
	templateUrl: './cms-userpoint-list.page.html',
	styleUrls: ['./cms-userpoint-list.page.scss'],
	providers: [CMSUserPointListModal],
	animations: [animationList]
})
export class CMSUserPointListPage {
	// //#region ================================= variable =====================
	public lbl = labelPages;
	public data: any;
	public search: any = {};
	public totalItems = 0;
	public countHeight = 0;
	public lstRank = [];
	public currentPage = 1;

	//#endregion ============================== and variable =================
	@ViewChild(IonContent, { static: true }) content: IonContent;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminRaoVatBLL_UserRank: AdminRaoVatBLL_UserRank,
		public adminRaoVatBLL_ThanhVien: AdminRaoVatBLL_ThanhVien,
		public cmsUserPointListModal: CMSUserPointListModal,
		public routerService: RouterExtService
	) {
	}
	async ngOnInit() {

	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSUserPointDetailPage.status) {

			CMSUserPointDetailPage.status = false;
			this.init();
		}
	}

	async init() {
		let data = this.commonMethodsTs.cloneArray(this.data || [] );
		this.data = null;
		await this.loadData();
		if(!this.data)
		{
			this.data = data;
		}
	}

	public async loadData(currentPage = currentPageDefault) {

		let params = this.commonMethodsTs.cloneObject(this.search || {});
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		let res: ResponseBase = await this.cmsUserPointListModal.getData(params);
		res = new ResponseBase(res);
		if (currentPage == currentPageDefault) {
			this.data = [];
		}
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			this.data = this.data.concat(res.DataResult || []);
			this.totalItems = res.getTotalItems();
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
			this.totalItems = 0;
		}

	}
	async doRefresh(event) {
		this.init();
		event.target.complete();
	}
	removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	async logScrolling() {
		let res = await this.content.getScrollElement();
		this.countHeight = res.scrollTop;
	}
	scrollToTop() {
		this.content.scrollToTop(500);
	}

	//#endregion
}
