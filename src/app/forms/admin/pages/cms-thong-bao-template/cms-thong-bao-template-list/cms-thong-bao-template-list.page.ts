import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams, defaultWxh } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { IonContent } from '@ionic/angular';
import { CMSThongBaoTemplateDetailPage } from '../cms-thong-bao-template-detail/cms-thong-bao-template-detail.page';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSThongBaoTemplateListModal } from './cms-thong-bao-template-list.modal';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-thong-bao-template-list',
	templateUrl: './cms-thong-bao-template-list.page.html',
	styleUrls: ['./cms-thong-bao-template-list.page.scss'],
	providers: [CMSThongBaoTemplateListModal],
	animations: [animationList]
})
export class CMSThongBaoTemplateListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	public data: any;
	public search: any = {};
	public totalItems = -1;
	//#endregion
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThongBaoTemplateListModal: CMSThongBaoTemplateListModal,
		public routerService: RouterExtService
	) {
	}
	ngOnInit() {
	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSThongBaoTemplateDetailPage.status)
		{
			CMSThongBaoTemplateDetailPage.status = false;
			this.init();
		} 
	}
	
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		let res: ResponseBase = await this.cmsThongBaoTemplateListModal.getData(params);
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			this.totalItems = res.getTotalItems();
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			let dataRes = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);
			this.data = this.data.concat(dataRes);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	public removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	public onSelect(id) {	
		this.commonMethodsTs.toPage_AdminThongBaoTemplate(id);
	}
	
	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	public scrollToTop() {
		this.content.scrollToTop(500);
	}
	
	//#endregion
}
