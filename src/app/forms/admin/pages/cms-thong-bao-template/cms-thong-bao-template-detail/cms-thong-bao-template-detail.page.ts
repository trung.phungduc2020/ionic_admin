import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { commonParams, commonMessages } from 'src/app/core/variables';
import { AdminRaoVatBLL_ThongBaoTemplate } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'cms-thong-bao-template-detail',
	templateUrl: './cms-thong-bao-template-detail.page.html',
	styleUrls: ['./cms-thong-bao-template-detail.page.scss'],
})
export class CMSThongBaoTemplateDetailPage implements OnInit {

	//#region variables
	static status = false;
	public displayData: any = {};
	//#endregion

	//#region contructor

	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminRaoVatBLL_ThongBaoTemplate: AdminRaoVatBLL_ThongBaoTemplate,
		public activatedRoute: ActivatedRoute

	) { }

	//#endregion

	ngOnInit() { }

	public ionViewWillEnter() {

		this.activatedRoute.params.subscribe(async (params) => {
			CMSThongBaoTemplateDetailPage.status = false;
			this.displayData.Id = params[commonParams.id] || -1;
			this.loadDataThongBao(this.displayData.Id);
		});
	}

	public async saveData() {

		if (!this.checkValue()) {
			this.commonMethodsTs.createMessageError(commonMessages.M041);
			return;
		}
		try {
			if (this.displayData.Id == -1) {
				let result = await this.adminRaoVatBLL_ThongBaoTemplate.insert(this.displayData).toPromise();
				result = new ResponseBase(result);
				CMSThongBaoTemplateDetailPage.status = true;
				if (result.isSuccess()) {
					this.commonMethodsTs.createMessageSuccess(commonMessages.M003);
					this.resetControls();
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage());
				}
			}
			else {
				let result = await this.adminRaoVatBLL_ThongBaoTemplate.update(this.displayData).toPromise();
				result = new ResponseBase(result);
				CMSThongBaoTemplateDetailPage.status = true;
				if (result.isSuccess()) {
					this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage());
				}
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	//#region private

	public async loadDataThongBao(id) {
		try {
			if (id == -1) {
				this.resetControls();
			} else {
				let paramsData = {
					ids: this.displayData.Id
				}
				let result = await this.adminRaoVatBLL_ThongBaoTemplate.search(paramsData).toPromise();
				result = new ResponseBase(result);
				if (result.isSuccess()) {
					this.displayData = result.DataResult[0];
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage())
				}
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	checkValue() {
		if (!this.displayData.Link || !this.displayData.NoiDung || !this.displayData.TieuDe || !this.displayData.GhiChu || !this.displayData.Ma) {

			return false;
		}
		return true;
	}

	public resetControls() {
		try {
			this.displayData.Id = -1;
			this.displayData.TieuDe = '';
			this.displayData.NoiDung = '';
			this.displayData.GhiChu = '';
			this.displayData.Ma = '';
			this.displayData.Link = '';
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	//#endregion
}
