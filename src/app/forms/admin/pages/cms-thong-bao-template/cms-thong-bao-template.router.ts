import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSThongBaoTemplateListPage } from './cms-thong-bao-template-list/cms-thong-bao-template-list.page';
import { CMSThongBaoTemplateDetailPage } from './cms-thong-bao-template-detail/cms-thong-bao-template-detail.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSThongBaoTemplateListPage,
	},
	{
		path: ':id',
		component: CMSThongBaoTemplateDetailPage,
	}
];

export const CMSThongBaoTemplate_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);