import { ControllersModule } from './../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule } from 'src/app/components/controllers';
import { CMSThongBaoTemplateDetailPage } from './cms-thong-bao-template-detail/cms-thong-bao-template-detail.page';
import { CMSThongBaoTemplateListPage } from './cms-thong-bao-template-list/cms-thong-bao-template-list.page';
import { CMSThongBaoTemplate_RoutingModule } from './cms-thong-bao-template.router';
import { AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_ThongBaoTemplate } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSThongBaoTemplate_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule
    ],
    declarations: [
		CMSThongBaoTemplateDetailPage,
		CMSThongBaoTemplateListPage,
	],
    providers: [
        AdminRaoVatBLL_ThongBaoTemplate,
        AdminRaoVatBLL_ThanhVien,
    ]
})
export class CMSThongBaoTemplatePageModule { }
