import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, listTrangThaiUserBlackList, listTrangThaiIsBlock, commonParams } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { IonContent } from '@ionic/angular';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSUserBlackListDetailPage } from '../cms-user-blacklist-detail/cms-user-blacklist-detail.page';
import { CMSUserBlackListListModal } from './cms-user-blacklist-list.modal';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-user-blacklist-list',
	templateUrl: './cms-user-blacklist-list.page.html',
	styleUrls: ['./cms-user-blacklist-list.page.scss'],
	providers: [CMSUserBlackListListModal],
	animations: [animationList]
})
export class CMSUserBlackListListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	public data: any;
	public search: any = {};
	public totalItems = -1;
	public lTrangThaiUserBlackList = listTrangThaiUserBlackList;
	public lTrangThaiIsBlock = listTrangThaiIsBlock;
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public routerService: RouterExtService,
		public cmsUserBlackListListModal: CMSUserBlackListListModal
	) {
	}
	ngOnInit() {
	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSUserBlackListDetailPage.status) {
			this.init();
		}
	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		params[commonParams.trangThai] = params.trangThaiBlackList;
		try {
			let res: ResponseBase = await this.cmsUserBlackListListModal.getData(params);
			res = new ResponseBase(res);
			if (res.isSuccess()) {
				this.currentPage = currentPage;
				if (currentPage == currentPageDefault) {
					this.data = [];
				}
				this.data = this.data.concat(res.DataResult);
				this.totalItems = res.getTotalItems();
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageError(error)
		}

	}
	public removeTuKhoaTimKiem() {
		this.loadData();
	}
	public onSelect(id) {
		this.commonMethodsTs.toPage_AdminUserBlackList(id);
	}

	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	public scrollToTop() {
		this.content.scrollToTop(500);
	}

	//#endregion
}
