import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_DangTinViolation, AdminRaoVatBLL_UserBlackList } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { HttpParams } from "@angular/common/http";
import { commonParams, dateFormat } from "src/app/core/variables";
import { ThanhVienRaoVatBLL_InterfaceData } from "src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL";
@Injectable()
export class CMSUserBlackListListModal {
    constructor(
        public adminRaoVatBLL_UserBlackList: AdminRaoVatBLL_UserBlackList,
        public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
        let params: any = {};
        params[commonParams.displayItems] = search.displayItems;
        params[commonParams.displayPage] = search.displayPage;
        params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
        params[commonParams.trangThai] = trangThai;
        params[commonParams.allIncludingString] = true;
        let res: ResponseBase = await this.adminRaoVatBLL_UserBlackList.search(params).toPromise();
        res = new ResponseBase(res);
        if (res.isSuccess()) {
            res.DataResult.forEach(element => {
                let compareDate = this.commonMethodsTs.compareDate(element.DenNgay, this.commonMethodsTs.getDateServe());
                if (element.TrangThai == 1 && compareDate == 1) {
                    element.IsBlock = 1;
                }

                else {
                    element.IsBlock = 2;
                }
            }
            )
        }
        
        return res;
    }
}
