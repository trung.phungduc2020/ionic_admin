import { Injectable } from '@angular/core';
import { ResponseBase, UserBlacklist } from 'src/app/core/entity';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonMessages } from 'src/app/core/variables';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_UserBlackList, AdminRaoVatBLL_DangTinViolation } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CoreProcess } from 'src/app/core/core-process';
import { CoreVariables } from 'src/app/core/core-variables';
@Injectable()
export class CmsUserBlacklistDetailModal {

    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public adminRaoVatBLL_UserBlackList: AdminRaoVatBLL_UserBlackList,
        public coreProcess: CoreProcess,
        public adminRaoVatBLL_DangTinViolation: AdminRaoVatBLL_DangTinViolation,
        public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien
    ) { }
    public maxDate;

    private async loadDataUserBlacklistById(id: any) {
        let paramsData = {
            id: id,
        }
        let res: ResponseBase = await this.adminRaoVatBLL_UserBlackList.search(paramsData).toPromise();
        res = new ResponseBase(res);
        let data = res.getFirstData() || {};

        // format hình ảnh
        data.ListImage = data.ListImage || "";
        let lstIdHinhAnh = this.commonMethodsTs.stringJsonToArray(data.ListImage || "");
        data.ListHinhAnh = [];
        if (lstIdHinhAnh && lstIdHinhAnh.length > 0) {
            for (let x = 0; x < lstIdHinhAnh.length; x++) {
                if (lstIdHinhAnh[x] > 0) {
                    data.ListHinhAnh[x] = this.commonMethodsTs.builLinkImage(data.RewriteUrl, lstIdHinhAnh[x])
                }
            }
        }

        this.checkTimeBlock(data);
        data.TuNgay = this.commonMethodsTs.toISOString(data.TuNgay);
        data.DenNgay = this.commonMethodsTs.toISOString(data.DenNgay);
        return data;
    }

    public checkTimeBlock(data: any) {
        data.ThoiGianHieuLuc = new Date(data.DenNgay).getTime() - new Date(data.TuNgay).getTime();
        return data;
    }

    //Hàm get by Id
    public async getIndex(id?: any) {
        let data: UserBlacklist = new UserBlacklist();
        if (+id > 0) {
            data = await this.loadDataUserBlacklistById(id);
        }
        else {
            let toDate = new Date(CoreVariables.timeDateServer);
            let toDatePlus = new Date(CoreVariables.timeDateServer);
            toDatePlus.setDate(+(toDatePlus.getDate()) + 1);
            data.TuNgay = toDate.toISOString();
            data.DenNgay = toDatePlus.toISOString();
            data.Id = -1;
            data.GhiChu = "";
            data.TrangThai = 1;
            data.ListImage = "";
            data.DienThoai = "";
            data.TenThanhVien = "";
            data.ListIdDangTin = "";
            data.IdThanhVien = "";

        }
        return data;
    }

    public async loadDataViPhamKhac(idThanhVien: any, id: any) {
        try {
            let paramsData = {
                idThanhVien: idThanhVien,
                displayItems: "-1",
                trangThai: "1"
            }
            let result = await this.adminRaoVatBLL_UserBlackList.search(paramsData).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                let data = result.DataResult;
                let arrayDate: any = [];
                let dataViPhamKhac = [];
                this.maxDate = this.commonMethodsTs.getDateServe();
                this.maxDate.setHours(+(this.maxDate.getHours()) - 7);

                data.forEach(element => {
                    if (element.Id != id) {
                        dataViPhamKhac.push(element);
                        arrayDate.push(new Date(element.TuNgay));
                        arrayDate.push(new Date(element.DenNgay));
                    }
                });

                // lấy đến ngày lớn nhất
                for (let i = 0; i <= arrayDate.length; i++) {
                    if (arrayDate[i] > this.maxDate) {
                        this.maxDate = arrayDate[i];
                    }
                    else {
                        this.maxDate = this.maxDate;
                    }
                }
                return dataViPhamKhac;

            } else {
                this.commonMethodsTs.createMessageError(result.getMessage())
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return {};
    }

    //saveUserBlacklist
    private async saveUserBlacklist(data: any) {

        if (data.ListHinhAnh && data.ListHinhAnh.length > 0) {
            data.ListImage = await this.coreProcess.insertByArray(data.ListHinhAnh, data.RewriteUrl);
            data.ListImage = JSON.stringify(data.ListImage);
        }
        data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
        data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);
        try {
            if (data.Id == -1) {
                delete data.Id;
                let result = await this.adminRaoVatBLL_UserBlackList.insert(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M003);
                    return true;
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                    return false;
                }
            }
            else {
                let result = await this.adminRaoVatBLL_UserBlackList.update(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                    return true;
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                    return false;
                }
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }
    //Update
    public async updateBlackList(data: any) {
        return await this.saveUserBlacklist(data);
    }
    //Delete
    public async deleteBlackList(data: any) {
        try {
            data.TrangThai = 3;
            let result: ResponseBase = await this.adminRaoVatBLL_UserBlackList.update(data).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(result.getMessage());
                return false;
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }

    public async loadDropdownThanhVien() {
        try {
            let params = {
                displayItems: "-1",
            }
            let res = await Promise.all([this.adminRaoVatBLL_DangTinViolation.searchUserBlacklist(params).toPromise(), this.adminDaiLyXeBLL_ThanhVien.searchByParams(params).toPromise()]);
            let userBlackList = res[0];
            let listThanhVien = res[1];
            userBlackList = new ResponseBase(userBlackList);
            if (userBlackList.isSuccess()) {
                let dataUserBlackList = userBlackList.DataResult;
                let dataListThanhVien = listThanhVien.DataResult;
                let lBlackList = [];
                dataListThanhVien.forEach(element => {
                    for (let i = 0; i < dataUserBlackList.length; i++) {
                        if (element.Id == +dataUserBlackList[i].IdThanhVienTao) {
                            lBlackList.push(element);
                        }
                    }
                });

                return lBlackList;
            } else {
                this.commonMethodsTs.createMessageError(userBlackList.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return [];
    }

    public async loadInfoThanhVienById(idThanhVien) {
        try {
            let params: any = {
                id: idThanhVien
            };
            let result: ResponseBase = await this.adminDaiLyXeBLL_ThanhVien.searchByParams(params).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                let dataRes = result.DataResult[0];
                return dataRes || {};
            }
            else {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return {};

    }

}
