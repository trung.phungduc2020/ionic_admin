import { Component, OnInit } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { commonParams, commonMessages, listThanhVienBlackList, listTrangThaiUserBlackList, dateFormat } from 'src/app/core/variables';
import { AdminRaoVatBLL_DangTinViolation } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { CmsUserBlacklistDetailModal } from './cms-user-blacklist-detail.modal';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'cms-user-blacklist-detail',
	templateUrl: './cms-user-blacklist-detail.page.html',
	styleUrls: ['./cms-user-blacklist-detail.page.scss'],
	providers: [CmsUserBlacklistDetailModal]
})
export class CMSUserBlackListDetailPage implements OnInit {
	//#region variables
	static status: any = false;
	public data: any = {};
	public lTrangThaiUserBlackList: any[] = listTrangThaiUserBlackList;
	public lThanhVienBlackList: any[] = listThanhVienBlackList;
	public lBlackList: any = [];
	public lTinDangViPham: any = [];
	public dataViPhamKhac: any = [];
	//public maxDate;

	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien,
		public adminRaoVatBLL_DangTinViolation: AdminRaoVatBLL_DangTinViolation,
		public cmsUserBlacklistDetailModal: CmsUserBlacklistDetailModal,
		public activatedRoute: ActivatedRoute
	) { }
	//#endregion
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSUserBlackListDetailPage.status = false;
			let id = params[commonParams.id] || -1;
			await this.init(id);

		});
	}

	public async init(id?: any) {
		try {
			let res = await Promise.all([this.cmsUserBlacklistDetailModal.getIndex(id), this.cmsUserBlacklistDetailModal.loadDropdownThanhVien(), this.cmsUserBlacklistDetailModal.loadDataViPhamKhac(this.data.IdThanhVien, this.data.Id)]);
			this.data = res[0];
			this.lBlackList = res[1];
			this.dataViPhamKhac = res[2];
			this.loadListTinDangViPham(this.data.IdThanhVien, this.data.Id);
			
		}
		catch (error) {
			this.commonMethodsTs.createMessageError(error);
		}
	}

	checkValue() {
		if (this.cmsUserBlacklistDetailModal.maxDate && ((new Date(this.data.TuNgay)) < this.cmsUserBlacklistDetailModal.maxDate)) {
			this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P010, this.commonMethodsTs.dateToDayMonthYear(this.cmsUserBlacklistDetailModal.maxDate)));
			return false;
		}
		if (!this.data.IdThanhVien || this.data.IdThanhVien == -1) {
			this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P007, labelPages.thanhVien));
			return false;
		}
		if (!this.data.TuNgay) {
			this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P005, labelPages.tuNgay));
			return false;
		}
		if (!this.data.DenNgay) {
			this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P005, labelPages.denNgay));
			return false;
		}
		if (this.data.TuNgay >= this.data.DenNgay) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M053);
			return false;
		}
		if (!this.data.GhiChu) {
			this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P005, labelPages.ghiChu));
			return false;
		}
		else {
			return true;
		}
	}

	public async saveData() {
		let data = this.commonMethodsTs.cloneObject(this.data);
		if (!this.checkValue()) {
			return;
		}
		this.data.TuNgay = this.commonMethodsTs.convertDate(this.data.TuNgay);
		this.data.DenNgay = this.commonMethodsTs.convertDate(this.data.DenNgay);
		try {
			if (data.Id == -1) {
				let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn tạo?');
				if (resAlert) {
					let result = await this.cmsUserBlacklistDetailModal.updateBlackList(data);
					if (result) {
						// Mr Trung: xử lý trên server
						// Broadcast signal
						// let dataNotification = {
						// 	Type: 2,
						// 	TieuDe: this.commonMethodsTs.stringFormat(commonMessages.T002, data.DienThoai),
						// 	NoiDung: this.commonMethodsTs.stringFormat(commonMessages.N006, data.TuNgay, data.DenNgay, data.GhiChu),
						// 	IdThanhVien: data.IdThanhVien
						// }
						// this.commonMethodsTs.storeMessageSendNotification(dataNotification);
						CMSUserBlackListDetailPage.status = true;
						this.buildForm();
					}
					else {
					}
				}
				else {
					return;

				}
			}
			else {

				let result = await this.cmsUserBlacklistDetailModal.updateBlackList(data);
				if (result) {
					CMSUserBlackListDetailPage.status = true;
					await this.init(data.Id);

				} else {

				}
			}
		} catch (error) {

			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}

	}

	public async xoaRecord() {
		let data = this.commonMethodsTs.cloneObject(this.data);
		try {
			let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn xóa?');
			if (resAlert) {
				let result = await this.cmsUserBlacklistDetailModal.updateBlackList(data);
				if (result) {
					if (data.ListImage && data.ListImage.length > 0) {
						data.ListImage = JSON.stringify(data.ListImage);
					}
					else {
						data.ListImage = "";
					}
					let result = await this.cmsUserBlacklistDetailModal.deleteBlackList(data);
					if (result) {
						CMSUserBlackListDetailPage.status = true;
						this.commonMethodsTs.toPage_AdminUserBlackList();
					}
				}
				else {
				}
			}
			else {
				return;

			}


		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	public async loadListTinDangViPham(idThanhVien, id) {
		this.lTinDangViPham = [];
		if (!idThanhVien) {
			return;
		}
		this.dataViPhamKhac = await this.cmsUserBlacklistDetailModal.loadDataViPhamKhac(idThanhVien, id);
		let dataThanhVien = await this.cmsUserBlacklistDetailModal.loadInfoThanhVienById(idThanhVien);
		this.data.TenThanhVien = dataThanhVien.HoTen;
		this.data.DienThoai = dataThanhVien.DienThoai;
		try {
			let paramsData = {
				allIncludingString: true,
				idThanhVien: idThanhVien
			}
			let result = await this.adminRaoVatBLL_DangTinViolation.search(paramsData).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess() && result.DataResult.length > 0) {
				this.lTinDangViPham = result.DataResult;
				this.data.ListIdDangTin = [];
				result.DataResult.forEach(element => {
					let tinDang = element.DangTin_Ext;
					if (tinDang) {
						this.data.ListIdDangTin.push(tinDang.Id);
					}
				});
				if (this.data.ListIdDangTin) {
					this.data.ListIdDangTin = JSON.stringify(this.data.ListIdDangTin);
				}
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	public checkTimeBlock() {
		this.data.ThoiGianHieuLuc = new Date(this.data.DenNgay).getTime() - new Date(this.data.TuNgay).getTime();
	}
	public async buildForm() {
		try {
			let newDate = this.commonMethodsTs.getDateServe();
			this.data.TuNgay = [{ value: this.commonMethodsTs.toISOString(this.data.TuNgay, newDate) }];
			this.data.DenNgay = [{ value: this.commonMethodsTs.toISOString(this.data.DenNgay, new Date(newDate.setDate(newDate.getDate() + 1))) }];
			this.data.Id = -1;
			this.data.GhiChu = "";
			this.data.TrangThai = 1;
			this.data.ListImage = "";
			this.data.DienThoai = "";
			this.data.TenThanhVien = "";
			this.data.ListIdDangTin = "";
			this.data.IdThanhVien = "";
			this.dataViPhamKhac = [];
			this.lTinDangViPham = [];

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	public routeToLinkViPham(id) {
		this.commonMethodsTs.toPage_AdminDangTinViolation(id);
	}

	public routeToLinkDangTin(id) {
		this.commonMethodsTs.toPage_AdminDangTin(id);
	}

	public routeToBlackList(id) {
		this.commonMethodsTs.toPage_AdminUserBlackList(id);
	}

	showClear() {
		if (typeof this.data.IdThanhVien === "number") {
			return +this.data.IdThanhVien >= 0
		}
		if (!this.data.IdThanhVien || this.data.Id > 0) return false;

		return true;
	}

	removeIdThanhVienSelected() {
		this.data.IdThanhVien = "";
	}

	changeTypeDanhSach() {
		this.data.IdThanhVien = "";
		this.lTinDangViPham = [];
		this.dataViPhamKhac = [];
	}

	//#endregion
}
