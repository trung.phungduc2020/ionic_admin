import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSUserBlackListDetailPage } from './cms-user-blacklist-detail/cms-user-blacklist-detail.page';
import { CMSUserBlackListListPage } from './cms-user-blacklist-list/cms-user-blacklist-list.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSUserBlackListListPage,
	},
	{
		path: ':id',
		component: CMSUserBlackListDetailPage,
	}
];

export const CMSUserBlackList_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);