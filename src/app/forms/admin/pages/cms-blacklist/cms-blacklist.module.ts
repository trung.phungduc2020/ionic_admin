import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_ThongBao, AdminRaoVatBLL_UserBlackList, AdminRaoVatBLL_DangTinViolation } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CMSUserBlackList_RoutingModule } from './cms-blacklist.router';
import { CMSUserBlackListDetailPage } from './cms-user-blacklist-detail/cms-user-blacklist-detail.page';
import { CMSUserBlackListListPage } from './cms-user-blacklist-list/cms-user-blacklist-list.page';
import { NgDatePipesModule } from 'src/app/shared/pipes';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSUserBlackList_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule,
        NgDatePipesModule
    ],
    declarations: [
		CMSUserBlackListDetailPage,
		CMSUserBlackListListPage,
	],
    providers: [
        AdminRaoVatBLL_ThongBao,
        AdminRaoVatBLL_ThanhVien,
        AdminRaoVatBLL_UserBlackList,
        AdminRaoVatBLL_DangTinViolation
    ]
})
export class CMSBlackListPageModule { }
