import { Injectable, ChangeDetectorRef } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { commonParams, defaultWxh, noImage } from "src/app/core/variables";
import { AdminRaoVatBLL_DangTinRating } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSDangTinRatingListModal {
    constructor(
        public adminRaoVatBLL_DangTinRating: AdminRaoVatBLL_DangTinRating,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params: any = {};
            params[commonParams.displayItems] = search.displayItems;
            params[commonParams.displayPage] = search.displayPage;
            params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
            params[commonParams.trangThai] = trangThai;
            let res: ResponseBase = await this.adminRaoVatBLL_DangTinRating.searchDangTinRating(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                let dataResult = [];
                let listHinhAnhJson:any[];
                res.DataResult.forEach((element, index) => {
                    if (element.DangTin_Ext) {
                        //element.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(element.DangTin_Ext.RewriteUrl, element.DangTin_Ext.IdFileDaiDien || -1, defaultWxh.two);

                        listHinhAnhJson = this.commonMethodsTs.stringJsonToArray(element.DangTin_Ext.ListHinhAnhJson);
                        if(listHinhAnhJson && listHinhAnhJson.length > 0)
                        {
                            element.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(element.DangTin_Ext.RewriteUrl, listHinhAnhJson[0], defaultWxh.two);
                        }
                        else
                        {
                            element.UrlHinhDaiDien = noImage;
                        }
                        dataResult.push(element);
                    }
                });
                res.DataResult = dataResult;
                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
}
