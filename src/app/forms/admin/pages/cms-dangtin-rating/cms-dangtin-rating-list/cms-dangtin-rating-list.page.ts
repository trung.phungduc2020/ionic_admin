import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams } from 'src/app/core/variables';
import { IonContent } from '@ionic/angular';
import { ResponseBase } from 'src/app/core/entity';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSDangTinRatingListModal } from './cms-dangtin-rating-list.modal';
import { CMSDangTinRatingDetailPage } from '../cms-dangtin-rating-detail/cms-dangtin-rating-detail.page';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';

@Component({
	selector: 'cms-dangtin-rating-list',
	templateUrl: './cms-dangtin-rating-list.page.html',
	styleUrls: ['./cms-dangtin-rating-list.page.scss'],
	providers: [CMSDangTinRatingListModal],
	animations: [animationList]
})
export class CMSDangTinRatingListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	public data: any;
	public search: any = {};
	public totalItems = -1;
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsDangTinRatingListModal: CMSDangTinRatingListModal,
		public routerService: RouterExtService,
	) {
	}
	ngOnInit() {
	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSDangTinRatingDetailPage.status) {
			CMSDangTinRatingDetailPage.status = false;
			this.init();
		}

	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});

		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		params[commonParams.trangThai] = params.tinhTrangDuyet >= 0 ? (params.tinhTrangDuyet == 1 ? 1 : 2) : 0;
		let res: ResponseBase = await this.cmsDangTinRatingListModal.getData(params);
		res = new ResponseBase(res);

		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.totalItems = res.getTotalItems();
			this.data = this.data.concat(res.DataResult || []);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	public removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	public scrollToTop() {
		this.content.scrollToTop(500);
	}
	public onSelect(id) {
		this.commonMethodsTs.toPage_AdminDangTinRating(id);
	}
	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	//#endregion
}
