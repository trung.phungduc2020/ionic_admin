import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonParams, commonMessages } from 'src/app/core/variables';
import { DangTinRating } from 'src/app/core/entity';
import { CMSDangTinRatingDetailModal } from './cms-dangtin-rating-detail.modal';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'cms-dangtin-rating-detail',
	templateUrl: './cms-dangtin-rating-detail.page.html',
	styleUrls: ['./cms-dangtin-rating-detail.page.scss'],
	providers: [CMSDangTinRatingDetailModal]
})
export class CMSDangTinRatingDetailPage implements OnInit {
	static status = false;
	//#region variables
	public label = labelPages.noneUpdate;
	public moTaNgan: any = '';
	public data: DangTinRating = new DangTinRating();
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsDangTinRatingDetailModal: CMSDangTinRatingDetailModal,
		public activatedRoute: ActivatedRoute
	) { }
	//#endregion
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSDangTinRatingDetailPage.status = false;
			this.data.Id = params[commonParams.id] || -1;
			this.data = await this.cmsDangTinRatingDetailModal.getData(this.data.Id);
			this.moTaNgan = this.data.DangTin_Ext.MoTaNgan;
		});
	}
	public async saveData(trangThai: any) {
		this.data.TrangThai = trangThai;
		let data = this.commonMethodsTs.cloneObject(this.data);
		try {

			let result = await this.cmsDangTinRatingDetailModal.updateRating(data);
			if (result && trangThai == 3) {
				CMSDangTinRatingDetailPage.status = true;
				this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
				this.commonMethodsTs.toPage_AdminDangTinRating();
			}
			if (result && trangThai != 3) {
				CMSDangTinRatingDetailPage.status = true;
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
			}
			else {
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		
	}

	async xoaRating(){
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn <strong>xóa</strong>');
		if (resAlert) {
			this.saveData(3);
		}
	}
	public xemTinLabelClick() {
		this.commonMethodsTs.toPage_MemberHomeDangTin(this.data.IdDangTin);
	}

	//#region private

	//#endregion
}
