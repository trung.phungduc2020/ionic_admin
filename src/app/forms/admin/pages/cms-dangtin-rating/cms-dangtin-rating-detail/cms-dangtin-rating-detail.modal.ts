import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase, DangTinRating } from "src/app/core/entity";
import { AdminRaoVatBLL_DangTinRating } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { NotificationSystemType, NotificationSystemTemplate, noImage, defaultWxh } from 'src/app/core/variables';
import { CommonService } from 'src/app/forms/member/services/common.service';
@Injectable()
export class CMSDangTinRatingDetailModal {
    constructor(
        public adminRaoVatBLL_DangTinRating: AdminRaoVatBLL_DangTinRating,
        public commonMethodsTs: CommonMethodsTs,
        public commonService: CommonService
    ) {
    }
    //GetData
    public async getData(id) {
        let data: DangTinRating = new DangTinRating();
        try {
            let param = {
                ids: id,
                displayItems: -1,
                displayPage: 1,
                allIncludingString: 'true'
            }
            let res: ResponseBase = await this.adminRaoVatBLL_DangTinRating.searchDangTinRating(param).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                let result = res.getFirstData();
                data.constructorFromObj(result);

                let listHinhAnhJson:any;
                //data.urlHinhDaiDien = this.commonMethodsTs.builLinkImage(result.DangTin_Ext.RewriteUrl, result.DangTin_Ext.IdFileDaiDien || -1);


                listHinhAnhJson = this.commonMethodsTs.stringJsonToArray(data.DangTin_Ext.ListHinhAnhJson);
                if(listHinhAnhJson && listHinhAnhJson.length > 0)
                {
                    data.urlHinhDaiDien = this.commonMethodsTs.builLinkImage(data.DangTin_Ext.RewriteUrl, listHinhAnhJson[0], defaultWxh.two);
                }
                else
                {
                    data.urlHinhDaiDien = noImage;
                }
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }

    //Update
    public async updateRating(data: any) {
        return await this.saveRating(data);
    }

    private async saveRating(data: any) {
        try {
            let result = await this.adminRaoVatBLL_DangTinRating.updateTrangThai(data.Id, data.TrangThai.toString()).toPromise()
            result = new ResponseBase(result);

            if (result.isSuccess()) {
                // Params
                let interfafceParams = {
                    typeNotification: NotificationSystemType.sendToUser,
                    ma: data.TrangThai == 1 ? NotificationSystemTemplate.F008 : NotificationSystemTemplate.F009,
                    tieuDe: data.DangTin_Ext.TieuDe,
                    idThanhVien: data.IdUser
                };

                this.commonService.adminInsertAndPushNotification(interfafceParams);
                
                return true;
            } else {
                this.commonMethodsTs.createMessageError(result.getMessage());
                return false;
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }
}
