import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSDangTinRatingListPage } from './cms-dangtin-rating-list/cms-dangtin-rating-list.page';
import { CMSDangTinRatingDetailPage } from './cms-dangtin-rating-detail/cms-dangtin-rating-detail.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSDangTinRatingListPage,
	},
	{
		path: ':id',
		component: CMSDangTinRatingDetailPage,
	}
];

export const CMSDangTinRating_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);