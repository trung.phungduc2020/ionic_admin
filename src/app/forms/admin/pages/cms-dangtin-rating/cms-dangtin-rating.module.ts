import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { CMSDangTinRating_RoutingModule } from './cms-dangtin-rating.router';
import { CMSDangTinRatingListPage } from './cms-dangtin-rating-list/cms-dangtin-rating-list.page';
import { CMSDangTinRatingDetailPage } from './cms-dangtin-rating-detail/cms-dangtin-rating-detail.page';
import { AdminRaoVatBLL_DangTinRating } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { IonicRatingModule } from 'ionic-rating';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
		IonicModule,
		CMSDangTinRating_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule,
        IonicRatingModule,
        
    ],
    declarations: [
		CMSDangTinRatingDetailPage,
		CMSDangTinRatingListPage
	],
    providers: [
        AdminRaoVatBLL_DangTinRating
    ]
})
export class CMSDangTinRatingPageModule { }
