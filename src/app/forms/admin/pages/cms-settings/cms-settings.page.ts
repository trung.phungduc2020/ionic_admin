import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { IonContent } from '@ionic/angular';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSSettingsModal } from './cms-settings.modal';
import { labelPages } from 'src/app/core/labelPages';
import { Variables, nameShortEnviroment, listTypeThongBao, listLinkNotify, listTrangThai } from 'src/app/core/variables';
@Component({
	selector: 'cms-settings',
	templateUrl: './cms-settings.page.html',
	styleUrls: ['./cms-settings.page.scss'],
	providers: [CMSSettingsModal],
})
export class CMSSettingsPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentVarriables:any;
	public currentSettings:any;	
	public isDev:any = Variables.isDev;
	public listDropdown = {
		lTypeThongBao: listTypeThongBao,
		lLinkNotify: listLinkNotify,
		lTrangThai: listTrangThai
	}
	public dataNotification:any = {
		Type:1
	};
	//#endregion
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public routerService: RouterExtService,
		public cMSSettingsModal: CMSSettingsModal,
	) {
	}
	ngOnInit() {
	}
	ionChangeVarriables(isShow:boolean)
	{
		this.currentVarriables =  isShow ? this.cMSSettingsModal.getVariables() : null;
	}

	async ionChangeSettings(isShow:boolean)
	{
		this.currentSettings =  isShow ? await this.cMSSettingsModal.getCurrentSettings() : null;
	}

    public updateSettingsByStage()
    {
		this.envTransOnDevice(nameShortEnviroment.productionstage);

    }

    public updateSettingsByProdLive()
    {
		this.envTransOnDevice(nameShortEnviroment.productionlive);


	}
	public updateSettingsByDev()
    {
		this.envTransOnDevice(nameShortEnviroment.development)

	}
	
	public async envTransOnDevice(enviroment:string)
	{
		await this.cMSSettingsModal.updateSettings(enviroment);
	}

	itemSelectedThanhVien(tv) {
		if (tv) {
			this.dataNotification.DienThoai = tv.DienThoai;

		}
		else
		{
			this.dataNotification.DienThoai = null
		}

	}
	itemSelectedUserGroup(usergroup) {
		if (usergroup && usergroup.LstThanhVien_Ext) {
			let lstThanhVien_Ext = [];
			let lstIdThanhVien_Ext = [];

			usergroup.LstThanhVien_Ext.forEach(element => {
				lstThanhVien_Ext.push(element.DataResult[0]);
				lstIdThanhVien_Ext.push(element.DataResult[0].Id)
			});
			this.dataNotification.LstThanhVien_Ext = lstThanhVien_Ext;
			this.dataNotification.GhiChu = lstIdThanhVien_Ext!.toString();

		}
		else
		{
			this.dataNotification.LstThanhVien_Ext = null;
			this.dataNotification.GhiChu = null;

		}

	}
	async pushSettingsByStage()
	{
		this.dataNotification.TieuDe = "Push load remote config Stage",
		this.dataNotification.NoiDung = "";
		let data:any = {};
		data.id = nameShortEnviroment.productionstage;
		this.dataNotification.Data = data;
		let res = await this.saveData();
		if(res)
		{
			this.commonMethodsTs.createMessageSuccess("Push load remote config Stage thành công");
		}
	}
	async pushSettingsByProdLive()
	{
		this.dataNotification.TieuDe = "Push load remote config Live",
		this.dataNotification.NoiDung = "";
		let data:any = {};
		data.id = nameShortEnviroment.productionlive;
		this.dataNotification.Data = data;
		let res = await this.saveData();
		if(res)
		{
			this.commonMethodsTs.createMessageSuccess("Push load remote config Live thành công");
		}
	}

	public async saveData() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			return false;
		}
		let newDate = this.commonMethodsTs.getDateServe();
		this.dataNotification.TuNgay = this.commonMethodsTs.convertDate(newDate);
		this.dataNotification.DenNgay = this.commonMethodsTs.convertDate(new Date(newDate.setMonth(newDate.getMonth() + 1)));
		
		return await this.cMSSettingsModal.saveNotications(this.dataNotification);
		
	}
	isCheckData()
	{
		return true
	}
	ionViewWillEnter() {
	}
	

	//#endregion
}
