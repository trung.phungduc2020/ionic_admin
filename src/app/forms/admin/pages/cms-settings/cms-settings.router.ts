import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSSettingsPage } from './cms-settings.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSSettingsPage,
	},
];

export const CMSSettings_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);