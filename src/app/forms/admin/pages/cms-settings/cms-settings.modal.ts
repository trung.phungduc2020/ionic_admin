import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { Variables, nameShortEnviroment, commonMessages, commonParams } from 'src/app/core/variables';
import { CoreProcess } from 'src/app/core/core-process';
import { AdminRaoVatBLL_ThongBao } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
@Injectable()
export class CMSSettingsModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public coreProcess: CoreProcess,
        public adminRaoVatBLL_ThongBao: AdminRaoVatBLL_ThongBao
    ) {
    }
    public getVariables() {
        let variables = {};
        Object.keys(Variables).forEach(key => {
            variables[key] = Variables[key];
        });
        return variables;
    }

    public async getCurrentSettings() {
        return await this.coreProcess.getRemoteCurrentConfig();
    }

    public async updateSettings(evn: string) {
        try {
            let config = await this.coreProcess.getRemoteConfig(evn);
            if (config) {
                this.coreProcess.setRemoteCurrentConfig(config);
                this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
            }
            else {
                this.commonMethodsTs.createMessageError(commonMessages.M006);

            }
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);

        }
    }

    public async saveNotications(data) {
        try {
            data.TenThanhVien_Ext = undefined;
            let resAlert = await this.commonMethodsTs.alertConfirm('Muốn push load setting này không?');
            if (resAlert) {
                let result = await this.adminRaoVatBLL_ThongBao.insert(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {
                    // Broadcast signal
                    this.commonMethodsTs.storeMessageSendNotification(data);
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                    return false;

                }
            }
            return true;
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

}   
