import { ControllersModule } from '../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSSettings_RoutingModule } from './cms-settings.router';
import { CMSSettingsPage } from './cms-settings.page';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AdminRaoVatBLL_ThongBao } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSSettings_RoutingModule,
        ControllersModule,
        NgxJsonViewerModule
    ],
    declarations: [
		CMSSettingsPage,
	],
    providers: [
        AdminRaoVatBLL_ThongBao
    ]
})
export class CMSSettingsPageModule { }
