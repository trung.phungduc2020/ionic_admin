import { commonMessages, commonParams } from '../../../../../core/variables';
import { Injectable } from '@angular/core';
import { AdminRaoVatBLL_UserPoint, AdminRaoVatBLL_HistoryPoint, AdminRaoVatBLL_UserRank, AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_DaiLyXeData, AdminRaoVatBLL_HistoryProfile } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
@Injectable()
export class CMSThanhVienDetailModal {
	constructor(
		public adminRaoVatBLL_HistoryPoint: AdminRaoVatBLL_HistoryPoint,
		public adminRaoVatBLL_UserRank: AdminRaoVatBLL_UserRank,
		public adminRaoVatBLL_UserPoint: AdminRaoVatBLL_UserPoint,
		public adminRaoVatBLL_ThanhVien: AdminRaoVatBLL_ThanhVien,
		public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien,
		public adminRaoVatBLL_DaiLyXeData: AdminRaoVatBLL_DaiLyXeData,
		public adminRaoVatBLL_HistoryProfile: AdminRaoVatBLL_HistoryProfile,
		public commonMethodsTs: CommonMethodsTs,
	) {
	}
	public async getInfoPoint(idThanhVien) { //chủ yếu là thông point và có thêm một ít thông tin thành viên
		try {
			let result: ResponseBase = await this.adminRaoVatBLL_UserPoint.search(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				return result.getFirstData();
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return null;
	}
	public async insertUserPoint(idThanhVien, rate) {
		try {
			let data = {
				"IdThanhVien": idThanhVien,
				"Point": 0,
				"PointTmp": 0,
				"PointUsed": 0,
				"Rate": rate
			};
			let res: ResponseBase = await this.adminRaoVatBLL_UserPoint.insert(data).toPromise();
			res = new ResponseBase(res);

			if (res.isSuccess()) {

				return true;
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}

	}

	public async getThanhVien(idThanhVien) {
		try {

			let res: ResponseBase = await this.adminDaiLyXeBLL_ThanhVien.getIndex(idThanhVien).toPromise();
			res = new ResponseBase(res);

			if (res.isSuccess()) {

				return res.getFirstData();
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return {};
	}

	public async getThanhVienRaoVat(idThanhVien) {
		try {

			let res: ResponseBase = await this.adminRaoVatBLL_ThanhVien.getIndex(idThanhVien).toPromise();
			res = new ResponseBase(res);

			if (res.isSuccess()) {

				return res.getFirstData();
			}
			else {
				this.commonMethodsTs.createMessageError(res.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return {};
	}

	async getHistoryPointByIdThanhVien(idThhanhVien) {
		try {
			// gọi api lấy list lịch sử
			let params: any = {};
			params[commonParams.displayItems] = 30;
			params[commonParams.displayPage] = 1;
			params[commonParams.idsThanhVien] = idThhanhVien;
			let result: ResponseBase = await this.adminRaoVatBLL_HistoryPoint.searchByParams(params).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				return result.DataResult;
			}
			else {
				this.commonMethodsTs.createMessageError(result.getMessage());
			}
		} catch (err) {
			this.commonMethodsTs.createMessageError(err);
		}
		return [];
	}
	async getListRank() {
		try {
			let dataParams = {
				displayPage: '1',
				displayItems: '-1',
				trangThai: '1',
			}
			let result = await this.adminRaoVatBLL_UserRank.search(dataParams).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				return result.DataResult;
			} else {
				this.commonMethodsTs.createMessageError(result.getMessage())
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return [];
	}

	async saveRank(idThanhVien, idRank) {
		try {
			let putData = {
				Id: idThanhVien,
				IdRank: idRank
			}
			let result: ResponseBase = await this.adminRaoVatBLL_ThanhVien.update(putData).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				// Broadcast signal
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true
			}
			else {
				this.commonMethodsTs.createMessageError(result.getMessage());

			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}

	async updateDaXacThucCmnd(idThanhVien) {
		try {
			let result = await this.adminRaoVatBLL_DaiLyXeData.updateDaXacThucCmnd(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				
				// Insert history
				var saveHistoryThanhVien = await this.getThanhVien(idThanhVien);
				this.adminRaoVatBLL_HistoryProfile.postHistoryProfile(saveHistoryThanhVien, { daXacThucCmnd: true, idThanhVien: idThanhVien }).subscribe();
                
				// Broadcast signal
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}

	async updateDaXacThucPassport(idThanhVien) {
		try {

			let result = await this.adminRaoVatBLL_DaiLyXeData.updateDaXacThucPassport(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {

				// Insert history
				var saveHistoryThanhVien = await this.getThanhVien(idThanhVien);
				this.adminRaoVatBLL_HistoryProfile.postHistoryProfile(saveHistoryThanhVien, { daXacThucPassport: true, idThanhVien: idThanhVien }).subscribe();
                
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}
}	
