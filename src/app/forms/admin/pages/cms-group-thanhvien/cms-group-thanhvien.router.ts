import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSGroupThanhVienListPage } from './cms-group-thanhvien-list/cms-group-thanhvien-list.page';
import { CMSGroupThanhVienDetailPage } from './cms-group-thanhvien-detail/cms-group-thanhvien-detail.page';

const appRoutes: Routes = [
    {
        path: '',
        component: CMSGroupThanhVienListPage,
      },
      {
        path: ':id',
        component: CMSGroupThanhVienDetailPage,
      }
];

export const CMSGroupThanhVien_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);