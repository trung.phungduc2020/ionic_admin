import { Injectable } from '@angular/core';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { HttpParams } from '@angular/common/http';
import { commonParams, defaultWxh } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';

@Injectable()
export class CMSGroupThanhVienListModal {
    constructor(
        public adminRaoVatBLL_UserGroup: AdminRaoVatBLL_UserGroup,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.displayItems, search.displayItems);
            params = params.set(commonParams.displayPage, search.displayPage);
            params = params.set(commonParams.tuKhoaTimKiem, search.tuKhoaTimKiem);
            params = params.set(commonParams.trangThai, trangThai);
            params = params.set(commonParams.allIncludingString, "fasle");

            let res: ResponseBase = await this.adminRaoVatBLL_UserGroup.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);

                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
}
