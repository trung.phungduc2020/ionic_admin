import { Component, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
// provider
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { CMSGroupThanhVienListModal } from './cms-group-thanhvien-list.modal';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { animationList } from 'src/app/shared/animations/animations';
import { CMSGroupThanhVienDetailPage } from '../cms-group-thanhvien-detail/cms-group-thanhvien-detail.page';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-group-thanhvien-list',
	templateUrl: './cms-group-thanhvien-list.page.html',
	styleUrls: ['./cms-group-thanhvien-list.page.scss'],
	providers: [CMSGroupThanhVienListModal],
	animations: [animationList]
})
export class CMSGroupThanhVienListPage implements OnInit {
	//#region ================================= variable =====================
	public lbl = labelPages;
	public data: any;
	public search: any = {};
	public totalItems = 0;
	public countHeight = 0;
	public currentPage = 1;
	//#endregion ============================== and variable =================
	@ViewChild(IonContent, { static: true }) content: IonContent;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThanhVienListModal: CMSGroupThanhVienListModal,
		public routerService: RouterExtService
	) {
	}
	async ngOnInit() {

	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSGroupThanhVienDetailPage.status)
		{
			CMSGroupThanhVienDetailPage.status = false;
			this.init();
		} 
		
	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});

		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		let res: ResponseBase = await this.cmsThanhVienListModal.getData(params);
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.totalItems = res.getTotalItems();
			this.data = this.data.concat(res.DataResult || []);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	doInfinite(infiniteScroll) {
		try {
			setTimeout(() => {
				if (this.data.length < this.totalItems) {
					this.currentPage++;
					this.loadData(this.currentPage);
					infiniteScroll.target.complete();
				}
			}, 0);
		} catch (error) { }
	}
	async logScrolling() {
		let res = await this.content.getScrollElement();
		this.countHeight = res.scrollTop;
	}
	scrollToTop() {
		this.content.scrollToTop(500);
	}
	select(id: any) {
		this.commonMethodsTs.toPage_AdminGroupThanhVien(id);
	}

	//#endregion
}
