import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { CMSGroupThanhVien_RoutingModule } from './cms-group-thanhvien.router';
import { CMSGroupThanhVienDetailPage } from './cms-group-thanhvien-detail/cms-group-thanhvien-detail.page';
import { CMSGroupThanhVienListPage } from './cms-group-thanhvien-list/cms-group-thanhvien-list.page';
import { ControllersModule } from 'src/app/components/controllers';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
		CMSGroupThanhVien_RoutingModule,
		ControllersModule,
	],
	declarations: [
		CMSGroupThanhVienListPage,
		CMSGroupThanhVienDetailPage,
	],

	providers: [
		AdminRaoVatBLL_UserGroup,
		AdminDaiLyXeBLL_ThanhVien

	]
})
export class CMSGroupThanhVienPageModule { }
