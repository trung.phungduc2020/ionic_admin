import { defaultWxh } from '../../../../../core/variables';
import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase, UserGroup } from 'src/app/core/entity';
import { commonParams } from 'src/app/core/variables';
import { AdminRaoVatBLL_UserGroup } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { HttpParams } from '@angular/common/http';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
@Injectable()
export class CMSGroupThanhVienDetailModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public adminRaoVatBLL_UserGroup: AdminRaoVatBLL_UserGroup,
        public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien
    ) {
    }
    // AdminDaiLyXeBLL_ThanhVien
    async getListThanhVien(ids) {
        // gọi api lấy dữ liệu tree men
        let data: any[] = [];
        try {
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.ids, ids);
            // params = params.set(commonParams.trangThai, "1,2");    
            // params = params.set(commonParams.allIncluding, "true");    

            let res: ResponseBase = await this.adminDaiLyXeBLL_ThanhVien.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                data = res.DataResult;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
    async getData(id) {
        // gọi api lấy dữ liệu tree men
        let data: UserGroup = new UserGroup();
        try {
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.id, id);
            params = params.set(commonParams.trangThai, "1,2");
            params = params.set(commonParams.allIncludingString, "true");

            let res: ResponseBase = await this.adminRaoVatBLL_UserGroup.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                data = res.getFirstData() as UserGroup;
                let lstIdUser_Ext = [];
                let lstThanhVien_Ext = [];
                data.LstThanhVien_Ext.forEach(element => {
                    lstIdUser_Ext.push(element.DataResult[0]!.Id)
                    lstThanhVien_Ext.push(element.DataResult[0]);
                });
                data.LstIdUser_Ext = lstIdUser_Ext.toString();
                data.LstThanhVien_Ext = lstThanhVien_Ext
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }

    async save(data) {
        // gọi api lấy dữ liệu tree men
        try {
            let params: HttpParams = new HttpParams();
            let res: ResponseBase
            if (+data!.Id > 0) {
                res = await this.adminRaoVatBLL_UserGroup.update(data, params).toPromise();
            }
            else {
                res = await this.adminRaoVatBLL_UserGroup.insert(data, params).toPromise();
            }
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

    async delete(id) {
        // gọi api lấy dữ liệu tree men
        try {
            let params: HttpParams = new HttpParams();
         
            let res: ResponseBase = await this.adminRaoVatBLL_UserGroup.updateTrangThai(id, "3").toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

    async addUserToGroup(idGroup:any, idThanhVien:any)
    {
        try {
            
            let res: ResponseBase = await this.adminRaoVatBLL_UserGroup.addUserToGroup(idGroup, idThanhVien).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
    async removeUserFromGroup(idGroup:any, idThanhVien:any)
    {
        try {
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.idGroup, idGroup);
            params = params.set(commonParams.idThanhVien, idThanhVien);
            let res: ResponseBase = await this.adminRaoVatBLL_UserGroup.removeUserFromGroup(idGroup, idThanhVien).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
}	
