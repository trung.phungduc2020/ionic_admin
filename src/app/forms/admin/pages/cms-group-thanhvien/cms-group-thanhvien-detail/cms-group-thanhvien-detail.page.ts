import { Component } from '@angular/core';
import { CommonMethodsTs } from "src/app/core/common-methods";
import { commonParams, commonMessages } from 'src/app/core/variables';
//provides
import { UserGroup } from 'src/app/core/entity';
import { CMSGroupThanhVienDetailModal } from './cms-group-thanhvien-detail.modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { animationList } from 'src/app/shared/animations/animations';
import { TrurthifyPipe } from 'src/app/shared/pipes';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'cms-group-thanhvien-detail',
	templateUrl: './cms-group-thanhvien-detail.page.html',
	styleUrls: ['./cms-group-thanhvien-detail.page.scss'],
	providers: [CMSGroupThanhVienDetailModal],
	animations: [animationList]

})
export class CMSGroupThanhVienDetailPage {
	//#region variables
	static status = false;
	public lbl = labelPages;
	public data: UserGroup;
	public myForm: FormGroup;
	public lThanhVien: any[] = [];
	//#endregion
	//#region constructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cMSGroupThanhVienDetailModal: CMSGroupThanhVienDetailModal,
		public fb: FormBuilder,
		public activatedRoute: ActivatedRoute

	) {
	}
	//#endregion
	async ngOnInit() {
	}
	ionViewWillEnter() {
		CMSGroupThanhVienDetailPage.status = false;
		this.activatedRoute.params.subscribe(async (params) => {
			let id = params[commonParams.id] || -1;
			this.loadData(id);
			this.data = new UserGroup();

		});
	}

	//#region private
	async loadData(id?: any) { //chủ yếu là thông point và có thêm một ít thông tin thành viên
		if (+id > 0) {
			try {
				this.data = await this.cMSGroupThanhVienDetailModal.getData(id);
			} catch (error) {
				this.commonMethodsTs.createMessageErrorByTryCatch(error);
			}
		}
		else {
			this.data = new UserGroup();
		}
		this.lThanhVien = this.data.LstThanhVien_Ext;
		this.buildForm();
	}

	async buildForm() {
		if (this.data) {
			try {
				this.myForm.reset();
			} catch (error) {
			}
			this.commonMethodsTs.ngZone.run(() => {
				let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
				group.Title = [this.data.Title, Validators.required];
				this.myForm = this.fb.group(group);
			})
		}
	}

	async getThanhVienById(ids) {
		if (ids) {
			return await this.cMSGroupThanhVienDetailModal.getListThanhVien(ids);
		}
		else {
			return []
		}
	}
	//saveData
	async saveData() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M018);
			
			return;
		}

		let myFormData = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSGroupThanhVienDetailModal.save(myFormData)
		if (res) {
			CMSGroupThanhVienDetailPage.status = true;
			if (+this.data!.Id > 0) {
				this.loadData(this.data!.Id);
			}
			else {
				this.commonMethodsTs.goBack();

			}
		}
	}
	isCheckData(): boolean {
		return !this.myForm.invalid;
	}
	async delete() {
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn xóa Group này?');
		if (resAlert) {
			if (+this.data!.Id > 0) {
				let res = await this.cMSGroupThanhVienDetailModal.delete(this.data.Id)
				if (res) {
					CMSGroupThanhVienDetailPage.status = true;
					this.commonMethodsTs.goBack();

				}
			}
			else {
				this.commonMethodsTs.createMessageWarning(commonMessages.M018);
				
				return;
			}
		}

	}

	async addThanhVienForGroup(comboxThanhVien: any) {

		let idThanhVienAdd = comboxThanhVien.value;
		let ids: any = [];
		let lstIdUser_Ext = this.myForm.value.LstIdUser_Ext;

		try {
			if (lstIdUser_Ext) {
				ids = lstIdUser_Ext.split(",");
			}
			ids.push(idThanhVienAdd);
			ids = new TrurthifyPipe().transform(ids);
		} catch (error) {

		}
		if (+this.data.Id > 0) {
			let res = await this.cMSGroupThanhVienDetailModal.addUserToGroup(this.data.Id, idThanhVienAdd);
			if (!res) {
				return
			}
		}
		this.myForm.controls.LstIdUser_Ext.setValue(ids.toString());
		let lTV = await this.getThanhVienById(idThanhVienAdd);
		this.lThanhVien = this.lThanhVien.concat(lTV);
		comboxThanhVien.value = null;
	}
	async removeThanhVienForGroup(idThanhVienRemove: any) {
		let ids
		let lstIdUser_Ext = this.myForm.value.LstIdUser_Ext;
		try {
			if (lstIdUser_Ext) {
				ids = lstIdUser_Ext.split(",");
			}
			ids = ids.filter(function (value, index, arr) {
				return value != idThanhVienRemove;
			});
		} catch (error) {

		}
		if (+this.data.Id > 0) {
			let res = await this.cMSGroupThanhVienDetailModal.removeUserFromGroup(this.data.Id, idThanhVienRemove);
			if (!res) {
				return
			}
		}
		this.myForm.controls.LstIdUser_Ext.setValue(ids.toString());
		this.lThanhVien = this.lThanhVien.filter(function (value, index, arr) {
			return value.Id != idThanhVienRemove;
		});
	}
	//#endregion
}
