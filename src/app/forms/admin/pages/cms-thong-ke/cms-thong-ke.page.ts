import { ScanPipe } from './../../../../shared/pipes/string/scan';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { IonContent, Events } from '@ionic/angular';
import { CMSThongKeModal } from './cms-thong-ke.modal';
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { SumPipe } from 'src/app/shared/pipes';
import { CoreVariables } from 'src/app/core/core-variables';
import { labelPages } from 'src/app/core/labelPages';
import { CoreProcess } from 'src/app/core/core-process';
@Component({
	selector: 'cms-thong-ke',
	templateUrl: './cms-thong-ke.page.html',
	styleUrls: ['./cms-thong-ke.page.scss'],
	providers: [CMSThongKeModal]
})
export class CMSThongKePage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public data: any;
	public search: any = {
		year: null,
		month: null,
		day: null
	};
	public lineChartData: ChartDataSets[] = [
		{ data: null, label: "Số lượt xem" },
	];
	countOnline;
	public lineChartLabels: Label[] = [];
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThongKeModal: CMSThongKeModal,
		public coreProcess: CoreProcess,

	) {
	}

	async ngOnInit() {
		if (!CoreVariables.timeDateServer) {
			CoreVariables.timeDateServer = await this.coreProcess.getTimeServer();
		}
		let now = this.commonMethodsTs.getDateServe();
		this.search.year = now.getFullYear();
		// this.search.month = now.getMonth();
		// this.search.day = now.getDate();

		this.data = {
			value: [],
			label: []
		};
		this.loadData();
	}

	async ionViewDidEnter() {
		if (!CoreVariables.timeDateServer) {
			CoreVariables.timeDateServer = await this.coreProcess.getTimeServer();
		}
		let now = this.commonMethodsTs.getDateServe();
		this.search.year = now.getFullYear();
		// this.search.month = now.getMonth();
		// this.search.day = now.getDate();

		
		if (!this.data || !this.data.value ||this.data.value.length == 0)
		{
			this.data = {
				value: [],
				label: []
			};
			this.loadData();
		} 
	}

	public async loadData() {
		let _search = this.commonMethodsTs.cloneObject(this.search);
		this.lineChartData[0].data = null;
		this.data = await this.cmsThongKeModal.getData(_search);
		this.data.total = new SumPipe().transform(this.data.value);
		this.lineChartData[0].data = this.data.value || [];
		let formatUnit = +_search.day > 0 ? "{0}h" : +_search.month > 0 ? "{0}d" : "T{0}"; // đơn vị cho label
		this.lineChartLabels = [];
		//2. Định dạng lại label cho hợp lý
		this.data.label.forEach((element, index) => {
			this.lineChartLabels[index] = new ScanPipe().transform(formatUnit, [element]);
		});
	}

	public scrollToTop() {
		this.content.scrollToTop(500);
	}
	customPickerOptions = {
		buttons: [{
			text: 'Xóa',
			handler: (data: any) =>
			{
				if(data.year)
				{
					this.search.year = null;

				}
				if(data.month)
				{
					this.search.month = null;

				}
				if(data.day)
				{
					this.search.day = null;

				}

			}

		},

		{
			text: 'Hủy',
			handler: (data: any) =>
			{
				console.log("Hủy")

			}

		},
		{
			text: 'Chọn',
			handler: (data: any) =>
			{
				if(data.year)
				{
					this.search.year = data.year.value;

				}
				if(data.month)
				{
					this.search.month = data.month.value;

				}
				if(data.day)
				{
					this.search.day = data.day.value;

				}


			}

		}
		],

	}
	
	//#endregion
}
