import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSThongKePage } from './cms-thong-ke.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSThongKePage,
	},
	
];

export const CMSThongKe_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);