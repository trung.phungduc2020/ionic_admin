import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { CMSThongKePage } from './cms-thong-ke.page';
import { CMSThongKe_RoutingModule } from './cms-thong-ke.router';
import { AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_OnlineStatistics } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSThongKe_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule,
        ChartsModule,
    ],
    declarations: [
		CMSThongKePage,
	],
    providers: [
        AdminRaoVatBLL_ThanhVien,
        AdminRaoVatBLL_OnlineStatistics
    ]
})
export class CMSThongKePageModule { }
