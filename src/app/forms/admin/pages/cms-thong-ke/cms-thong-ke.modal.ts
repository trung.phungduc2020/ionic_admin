import { PickPipe } from './../../../../shared/pipes/object/pick';
import { PluckPipe } from './../../../../shared/pipes/array/pluck';
import { GroupByPipe, SumPipe, TrurthifyPipe } from 'src/app/shared/pipes';
import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { AdminRaoVatBLL_OnlineStatistics } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSThongKeModal {
    constructor(
        public adminRaoVatBLL_OnlineStatistics: AdminRaoVatBLL_OnlineStatistics,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(params: any) {
        params = params || {};
        
        let data: any = {
            value: [],
            label: []
        };
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_OnlineStatistics.search(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                let resFirstData = res.getFirstData();
                // Đk phải có dữ liệu
                if (resFirstData) {
                    //1. Xử lý
                    if (+params.day > 0) {
                        //Xử lý chó lọc theo ngày
                        resFirstData.Value_Ext = resFirstData.Value_Ext.replace("},", '}'); // loại bỏ khoảng trống thừa
                        let obj = JSON.parse(resFirstData.Value_Ext) || {}; // convert dưc liệu thành dạng json
                        data.value = Object.values(obj);
                        data.label = Object.keys(obj);
                    }
                    else {
                        //Xử lý cho lọc theo năm, tháng
                        data.value = new TrurthifyPipe().transform(resFirstData.Value_Ext!.split(",").map(Number));
                        data.label = new TrurthifyPipe().transform(resFirstData.Label_Ext!.split(","));
                    }
                    data.value = data.value.map(Number);
                    data.label = data.label.map(Number);
                }
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        
        return data;
    }
    checkTypeDate(strDate: string) {
        let type = -1;
        try {
            type = strDate.toString().split("-").length;
        } catch (error) { }
        return type;
    }
}
