import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSUuDaiDetailPage } from './cms-uu-dai-detail/cms-uu-dai-detail.page';
import { CMSUuDaiListPage } from './cms-uu-dai-list/cms-uu-dai-list.page';

const appRoutes: Routes = [
    {
        path: '',
        component: CMSUuDaiListPage,
      },
      {
        path: ':id',
        component: CMSUuDaiDetailPage,
      }
];

export const CMSUuDai_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);