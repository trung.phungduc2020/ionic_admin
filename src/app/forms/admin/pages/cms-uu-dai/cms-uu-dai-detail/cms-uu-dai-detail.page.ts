import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { listMarketingPlanType, listMarketingPlanMaCauHinh, listMarketingPlanValueType, commonParams, commonMessages, commonNavigate } from 'src/app/core/variables';
import { CMSUuDaiDetailModal } from './cms-uu-dai-detail.modal';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { labelPages } from 'src/app/core/labelPages';
import { switchMap, map, filter } from 'rxjs/operators';
import { ReturnStatement } from '@angular/compiler';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';

@Component({
	selector: 'cms-uu-dai-detail',
	templateUrl: './cms-uu-dai-detail.page.html',
	styleUrls: ['./cms-uu-dai-detail.page.scss'],
})
export class CMSUuDaiDetailPage implements OnInit {
	//#region Variables
	static status: boolean = false;
	public data: any;
	public myForm: FormGroup;
	public urlHinhDaiDien: any = "";
	public configCkUuDai = {
		"height": 'auto',
		"resize_enabled": false,
		"toolbar": [],
		"toolbarStartupExpanded": false,
		"removePlugins": 'elementspath',
		"extraPlugins": 'divarea',
		"placeHolder": "",
		"readOnly": true
	};
	public listMarketingPlanType: any[] = listMarketingPlanType;
	public listMarketingPlanMaCauHinh: any[] = listMarketingPlanMaCauHinh;
	public listMarketingPlanValueType: any[] = listMarketingPlanValueType;
	public invalidData: boolean = false;
	public isDisabled: boolean = false;
	public nowDate: any = new Date();
	public maxYear: any = this.nowDate.getFullYear() + 1;
	public minYear: any = this.nowDate.getFullYear();

	state$: Observable<object>;
	//#endregion
	//#region contructor


	constructor(
		public cmsUuDaiDetailModal: CMSUuDaiDetailModal,
		public fb: FormBuilder,
		public commonMethodsTs: CommonMethodsTs,
		public activatedRoute: ActivatedRoute,
		public router: Router,
		private location: Location
	) { }
	//#endregion
	ngOnInit() { }



	ionViewWillEnter() {

		let dataRoute: any = this.location.getState();

		this.activatedRoute.params.subscribe(async (params) => {
			let id = params[commonParams.id] || -1;
			if (dataRoute && dataRoute.data) {
				this.data = dataRoute.data;
				this.buildForm();
			}
			else {
				this.loadData(id);
			}

		});
	}

	public async loadData(id) {
		try {
			if (id > 0) {
				this.data = await this.cmsUuDaiDetailModal.getDataById(id);
				this.isDisabled = true;
			}
			else {
				this.data = {};
			}
			this.buildForm();
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	private getRouteData(data: string): any {
		const root = this.commonMethodsTs.router.routerState.snapshot.root;
		return this.lastChild(root).data[0][data];
	}

	private lastChild(route: ActivatedRouteSnapshot): ActivatedRouteSnapshot {
		if (route.firstChild) {
			return this.lastChild(route.firstChild);
		} else {
			return route;
		}
	}

	buildForm() {
		if (this.data) {
			let newDate: any = new Date();
			this.commonMethodsTs.ngZone.run(() => {
				let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
				group.Id = [this.data.Id || -1];
				group.TieuDe = [this.data.TieuDe || null];
				group.Code_Ext = [this.data.LstMarketingCode_Ext && this.data.LstMarketingCode_Ext.length == 1 ? this.data.LstMarketingCode_Ext[0].Code : ""];
				group.MaCauHinh = [this.data.MaCauHinh || listMarketingPlanMaCauHinh[2].value]; // listMarketingPlanMaCauHinh[2] = SpecialDay
				group.Value = [this.data.Value || null];
				group.ValueType = [this.data.ValueType || -1];
				group.UnlimitedTime = [this.data.UnlimitedTime || false];
				group.UnlimitedUsed = [this.data.UnlimitedUsed || false];
				group.MoTaNgan = [this.data.MoTaNgan || null];
				group.TrangThai = [this.data.TrangThai || 1];
				group.GhiChu = [this.data.GhiChu || ""];
				group.TuNgay = [this.commonMethodsTs.toISOString(this.data.TuNgay, newDate)];
				group.DenNgay = [this.commonMethodsTs.toISOString(this.data.DenNgay, new Date(newDate.setMonth(newDate.getMonth() + 1)))];
				group.Type = [this.data.Type || 3]; // 3 = Hệ thống
				group.MaxValue = [this.data.MaxValue || null];
				group.MaxUsed = [this.data.MaxUsed || 1];
				group.TotalUsed = [this.data.TotalUsed || 0];
				group.IdFileDaiDien = [this.data.IdFileDaiDien || null];
				group.ThuTu = [this.data.ThuTu || 1];
				group.SendAll = [false];
				this.myForm = this.fb.group(group);
			})
		}
		this.ionChangeMaCauHinh();
	}
	async saveData() {
		try {
			// kiểm tra dữ liệu đầu vào
			// kiểm tra tên menu

			this.invalidData = this.checkErrorData();
			if (this.invalidData) {
				return;
			}


			if (!this.myForm.value.TieuDe) {
				this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P006, labelPages.title.toLocaleLowerCase()));
				return;
			}

			let data = this.commonMethodsTs.cloneObject(this.myForm.value);
			data.UrlHinhDaiDien = this.urlHinhDaiDien;

			if (data.UnlimitedTime) {
				data.TuNgay = data.DenNgay = null;

			} else {

				if (!data.TuNgay) {
					this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P007, labelPages.tuNgay.toLocaleLowerCase()));
					return;
				}
				if (!data.DenNgay) {
					this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P007, labelPages.denNgay.toLocaleLowerCase()));
					return;
				}
				data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
				data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);
			}

			let isSend: boolean = false;
			if (data.SendAll) {
				let resAlert = await this.commonMethodsTs.alertConfirm(`<p>Bạn có chắc chắn muốn gửi thông báo đến tất cả người dùng?</p> <p><i>- Tiêu đề:</i></p> <p>${data.TieuDe}</p><p><i>- Nội dung:</i> ${data.MoTaNgan}</p>`);
				if (resAlert) {
					isSend = true;
				}
			}

			let res = await this.cmsUuDaiDetailModal.saveData(data, isSend);
			if (res) {
				CMSUuDaiDetailPage.status = true;
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	checkErrorData() {
		if (this.myForm.value.TieuDe && this.myForm.value.TieuDe.length > 0 && this.myForm.value.MaCauHinh && this.myForm.value.MaCauHinh.length > 0 && +this.myForm.value.Type > 0 && +this.myForm.value.ValueType > 0 && +this.myForm.value.Value > 0 && +this.myForm.value.MaxValue > 0) {
			if (!this.myForm.value.UnlimitedTime) {
				if (this.myForm.value.TuNgay && this.myForm.value.DenNgay) {
					if (this.commonMethodsTs.compareTwoDateByMinutes(this.myForm.value.TuNgay, this.myForm.value.DenNgay) == 1) {
						this.commonMethodsTs.createMessageWarning(commonMessages.M072);
						return false;
					}
				}
			}
			return false;
		}
		return true;
	}

	setValueMoTaNgan(){
		if(this.myForm.value.TieuDe)
		{
			this.myForm.controls.MoTaNgan.setValue(this.myForm.value.TieuDe);
		}
	}


	async xoaUuDai() {
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn xoá?');
		if (resAlert) {
			let result = await this.cmsUuDaiDetailModal.xoaUuDai(this.data.Id);
			if (result) {
				CMSUuDaiDetailPage.status = true;
				this.commonMethodsTs.toPage_AdminUuDai();
			}
		}
	}

	duplicate() {
		let data = this.commonMethodsTs.cloneObject(this.data);
		data.Id = -1;
		this.commonMethodsTs.router.navigate([commonNavigate.cms_uuDai, -1], { state: { data: data } });
	}

	ionChangeMaCauHinh(){
		if(this.myForm.value.MaCauHinh == this.listMarketingPlanMaCauHinh[2].value)
		{
			this.randomCode();
		}
		else
		{
			this.myForm.controls.Code_Ext.setValue("");
		}
	}

	randomCode() {
		let maCauHinh = this.listMarketingPlanMaCauHinh.find(o => o.value === this.myForm.value.MaCauHinh);
		this.myForm.controls.Code_Ext.setValue(this.commonMethodsTs.randomCode(maCauHinh.key));
	}

	//#endregion
}
