import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_MarketingPlan } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CoreProcess } from "src/app/core/core-process";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase, MarketingPlan } from "src/app/core/entity";
import { commonMessages, commonNavigate } from "src/app/core/variables";

@Injectable()
export class CMSUuDaiDetailModal {

    constructor(
        public adminRaoVatBLL_MarketingPlan: AdminRaoVatBLL_MarketingPlan,
        public coreProcess: CoreProcess,
        public commonMethodsTs: CommonMethodsTs,
    ) { }
    public async loadData(id) {

        let data = {};
        try {
            data = await this.getDataById(id);
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
    public resetCurrentMenu(data) {
        data.TenMenu = '';
        data.MoTaTrang = '';
        data.IdMenuParent = 0;
        data.GhiChu = '';
        data.TrangThai = 1;
        return data;
    }
    async saveData(data, isSendAll:boolean = false) {
        try {
            this.commonMethodsTs.startLoading();
            try {
                if (data.UrlHinhDaiDien && data.UrlHinhDaiDien.length > 0) {
                    data.IdFileDaiDien = await this.coreProcess.insertByUrl(data.UrlHinhDaiDien, data.TenMenu || "new-file");                    
                }
            } catch (error) {
                this.commonMethodsTs.createMessageErrorByTryCatch(error);
                return;
            }
            let result: any;
            if (+data.Id > 0) {
                result = await this.adminRaoVatBLL_MarketingPlan.update(data).toPromise();
                result = new ResponseBase(result);
                // thông báo
                if (result.isSuccess()) {
                    
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }

            }
            else {
                result = await this.adminRaoVatBLL_MarketingPlan.insert(data).toPromise();
                result = new ResponseBase(result);
                // thông báo
                // Ko lưu notification
                if (result.isSuccess()) {
                    if(isSendAll)
                    {
                        let dataResult = result["DataResult"] ? result["DataResult"][0]: [];
                        let moTaNgan = dataResult&&dataResult.MoTaNgan?this.commonMethodsTs.removeHtml(dataResult.MoTaNgan):"";
                        let dataNotification = {
                            TieuDe: dataResult.TieuDe,
                            Type: 1,
                            NoiDung: moTaNgan,
                            Data: "",
                            Link: commonNavigate.f_uuDaiCuaBan
                        }
                        this.commonMethodsTs.storeMessageSendNotification(dataNotification);
                    }

                    this.commonMethodsTs.createMessageSuccess(commonMessages.M003)
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }
            }
            this.commonMethodsTs.stopLoading();
            return result.isSuccess()
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
    async xoaUuDai(id) {
        try {
            let result = await this.adminRaoVatBLL_MarketingPlan.updateTrangThai(id, '3').toPromise();
            result = new ResponseBase(result);
            let message = result.getMessage('');
            if (!message || message.length <= 0 || message == '') {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true
            } else {
                this.commonMethodsTs.createMessageError(message);
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

    //#region private methods
    // deleteElementAndChildren(list, id) {
    //     if (list == null || list.length <= 0) {
    //         return;
    //     }
    //     if (!id) {
    //         return;
    //     }
    //     this.commonMethodsTs.removeElementById(list, id);
    //     for (let i = 0; i < list.length; i++) {
    //         if (list[i].IdMenuParent == id) {
    //             this.deleteElementAndChildren(list, list[i].Id);
    //             i = -1;
    //         }
    //     }

    //     return list;
    // }
    async getDataById(id) {
        try {
            let params: any = {};
            params.id = id;
            let result: ResponseBase = await this.adminRaoVatBLL_MarketingPlan.search(params).toPromise();
            
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                return result.getFirstData();
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return new MarketingPlan();
    }
    // async getMenuByNotId(id?:any) {
    //     try {
    //         let params: any = {};
    //         params.notId = id
    //         let result: ResponseBase = await this.adminRaoVatBLL_Menu.search(params).toPromise();
    //         result = new ResponseBase(result);
    //         if (result.isSuccess()) {
    //             return result.DataResult;
    //         }
    //     } catch (error) {
    //         this.commonMethodsTs.createMessageErrorByTryCatch(error);
    //     }
    //     return []
    // }

}
