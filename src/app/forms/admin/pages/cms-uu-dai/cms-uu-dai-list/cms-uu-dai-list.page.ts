import { Component, OnInit } from '@angular/core';
import { labelPages } from 'src/app/core/labelPages';
import { displayItemDefault, currentPageDefault, commonParams } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CMSUuDaiListModal } from './cms-uu-dai-list.modal';
import { ResponseBase } from 'src/app/core/entity';
import { CMSUuDaiDetailPage } from '../cms-uu-dai-detail/cms-uu-dai-detail.page';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';

@Component({
    selector: 'cms-uu-dai',
    templateUrl: './cms-uu-dai-list.page.html',
    styleUrls: ['./cms-uu-dai-list.page.scss'],
})
export class CMSUuDaiListPage implements OnInit {
    //#region ================================= variable =====================
    public lbl = labelPages;
    public data: any;
    public search: any = {};
    public cols: any[];
    public tuKhoaTimKiem;
    public totalItems;
    public displayItems = displayItemDefault;
    public currentPage = currentPageDefault;
    //#endregion ============================== and variable =================
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public cmsUuDaiListModal: CMSUuDaiListModal,
        public routerService: RouterExtService

        ) {
    
    }
    public async ngOnInit() { }

    public async ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSUuDaiDetailPage.status)
		{
			CMSUuDaiDetailPage.status = false;
            this.loadData();
		} 
    }
   
    public async loadData(currentPage = currentPageDefault) {
        let params = this.commonMethodsTs.cloneObject(this.search || {});
        
        params[commonParams.displayItems] = displayItemDefault;
        params[commonParams.displayPage] = currentPage || currentPageDefault;
        params[commonParams.tuKhoaTimKiem] = this.tuKhoaTimKiem;
        params[commonParams.orderString] = "ThuTu,NgayTao:desc";
        params[commonParams.trangThai] = "1,2";
        let res: ResponseBase  = await this.cmsUuDaiListModal.getData(params);

        res = new ResponseBase(res);
        if (currentPage == currentPageDefault) {
            this.data = [];
        }
        
        if(res.isSuccess())
        {
            this.currentPage = currentPage;
            this.totalItems = res.getTotalItems();
            this.data = this.data.concat(res.DataResult);
        }
        else
        {
            this.data = [];
        }
    }

    public async doInfinite(infiniteScroll) {
        try {
          if (this.data.length > 0 && this.data.length < this.totalItems) {
            await this.loadData(this.currentPage + 1);
          }
        } catch (error) {
        }
        infiniteScroll.target.complete();
    
      }
    

    public removeTuKhoaTimKiem() {
        this.tuKhoaTimKiem = null;
        this.loadData();
    }

    public onSelect(id: any) {
        this.commonMethodsTs.toPage_AdminUuDai(id);
    }


    async doRefresh(event) {
		setTimeout(async () => {
            this.data = null;
			await this.loadData();
			event.target.complete();
		}, 100);
	}

}
