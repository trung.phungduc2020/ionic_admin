import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_Menu, AdminRaoVatBLL_MarketingPlan } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { noImageUuDai, defaultWxh, commonParams, commonMessages } from 'src/app/core/variables';
@Injectable()
export class CMSUuDaiListModal {
    constructor(
        public adminRaoVatBLL_MarketingPlan: AdminRaoVatBLL_MarketingPlan,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = [];

        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params = {};
            params[commonParams.displayItems] = search.displayItems;
            params[commonParams.displayPage] = search.displayPage;
            params[commonParams.trangThai] = search.trangThai;
            params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
            params[commonParams.orderString] = search.orderString;

            let res: ResponseBase = await this.adminRaoVatBLL_MarketingPlan.search(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                data = res.DataResult;
                let idFileDaiDien: any = -1;
                data.forEach(element => {
                    if (element.IdFileDaiDien) {
                        idFileDaiDien = element.IdFileDaiDien ? element.IdFileDaiDien : "-1";
                        element.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(element.RewriteUrl, idFileDaiDien, defaultWxh.two);
                    }
                    else {
                        element.UrlHinhDaiDien = noImageUuDai;
                    }
                });
                res.DataResult = data;
                return res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
            return null;
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return null;
    }

   
}
