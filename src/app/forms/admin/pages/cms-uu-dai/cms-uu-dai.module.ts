import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSUuDaiDetailPage } from './cms-uu-dai-detail/cms-uu-dai-detail.page';
import { CMSUuDai_RoutingModule } from './cms-uu-dai.router';
import { AdminRaoVatBLL_Menu, AdminRaoVatBLL_MarketingPlan } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { DaiLyXeIonImgModule, ControllersModule } from 'src/app/components/controllers';
import { TreeTableModule } from 'src/app/components/controllers/primeNg/treetable/treetable';
import { CMSUuDaiDetailModal } from './cms-uu-dai-detail/cms-uu-dai-detail.modal';
import { CMSUuDaiListPage } from './cms-uu-dai-list/cms-uu-dai-list.page';
import { CMSUuDaiListModal } from './cms-uu-dai-list/cms-uu-dai-list.modal';
import { CKEditorModule } from 'ngx-ckeditor';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSUuDai_RoutingModule,
        TreeTableModule,
        CKEditorModule,
        DaiLyXeIonImgModule,
        ControllersModule,
        ReactiveFormsModule,
        
    ],
    declarations: [CMSUuDaiListPage, CMSUuDaiDetailPage],
    providers: [
        AdminRaoVatBLL_MarketingPlan,
        CMSUuDaiListModal,
        CMSUuDaiDetailModal,
        AdminRaoVatBLL_Menu,
    ]
})
export class CMSUuDaiPageModule { }
