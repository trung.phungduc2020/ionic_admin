import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonPasswordModule } from 'src/app/components/controllers';
import { CMSLoginPage } from './cms-login.page';

const routes: Routes = [
    {
        path: '',
        component: CMSLoginPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        DaiLyXeIonPasswordModule
    ],
    declarations: [CMSLoginPage],
})
export class CMSLoginPageModule { }
