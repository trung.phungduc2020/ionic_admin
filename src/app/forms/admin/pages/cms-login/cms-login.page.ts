import { Component, OnInit } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonMessages } from "src/app/core/variables";
import { AuthAdminService } from 'src/app/core/http-interceptor/auth/auth-admin.service';

@Component({
    selector: 'cms-login',
    templateUrl: './cms-login.page.html',
    styleUrls: ['./cms-login.page.scss'],
})
export class CMSLoginPage implements OnInit {
    username: string;
    password: string;
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        private authAdminService: AuthAdminService,
    ) { }

    ngOnInit() {

    }
    async login() {
        if (!this.username || !this.password) {
            this.commonMethodsTs.createMessageWarning(commonMessages.M019);
            return;
        }
        let passwordMd5 = this.commonMethodsTs.convert_toMD5(this.password);

        this.commonMethodsTs.startLoading();
        let res = false;
        try {
            res = await this.authAdminService.login(this.username, passwordMd5).toPromise();
            if(res)
            {
                this.commonMethodsTs.toPage_AdminDashboard();
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        this.commonMethodsTs.stopLoading();
        return res!=null;

    }
}