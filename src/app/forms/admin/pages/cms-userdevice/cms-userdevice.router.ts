import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSUserDevicePage } from './cms-userdevice.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSUserDevicePage,
	},
];

export const CMSUserDevice_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);