import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { CMSUserDeviceModal } from './cms-userdevice.modal';
import { labelPages } from 'src/app/core/labelPages';
import { CommonMethodsTs } from 'src/app/core/common-methods';
@Component({
	selector: 'cms-userdevice',
	templateUrl: './cms-userdevice.page.html',
	styleUrls: ['./cms-userdevice.page.scss'],
	providers: [CMSUserDeviceModal],
})
export class CMSUserDevicePage implements OnInit {
	//#region variables
	public lbl = labelPages;
	public data: any;
	public defaultStatistic: any;
	public onlineStatistic: any;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	//#endregion
	constructor(
		public cMSUserDeviceModal: CMSUserDeviceModal,
		public commonMethodsTs: CommonMethodsTs
	) {

		this.onThongKe();
	}
	ngOnInit() {
	}

	async onThongKe(){
		this.defaultStatistic = await this.cMSUserDeviceModal.SearchUserDevice();
	}
	//#endregion
}
