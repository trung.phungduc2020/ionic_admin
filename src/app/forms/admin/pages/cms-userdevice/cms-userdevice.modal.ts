import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from 'src/app/core/entity';
import { AdminDaiLyXeBLL_CommonFile, AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_UserDevice } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
@Injectable()
export class CMSUserDeviceModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public adminDaiLyXeBLL_CommonFile: AdminDaiLyXeBLL_CommonFile,
        public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
        public adminRaoVatBLL_UserDevice: AdminRaoVatBLL_UserDevice
    ) {
    }

    public async SearchUserDevice(){
        try {
            let result = await this.adminRaoVatBLL_UserDevice.search().toPromise();
            result = new ResponseBase(result);
            console.log('result', result)
			return result;
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);
        }
    }
}