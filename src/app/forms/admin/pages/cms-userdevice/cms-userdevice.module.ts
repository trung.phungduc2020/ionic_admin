import { ControllersModule } from '../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSUserDevice_RoutingModule } from './cms-userdevice.router';
import { CMSUserDevicePage } from './cms-userdevice.page';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AdminRaoVatBLL_UserDevice } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSUserDevice_RoutingModule,
        ControllersModule,
        NgxJsonViewerModule
    ],
    declarations: [
		CMSUserDevicePage,
	],
    providers: [
        AdminRaoVatBLL_UserDevice
    ]
})
export class CMSUserDevicePageModule { }
