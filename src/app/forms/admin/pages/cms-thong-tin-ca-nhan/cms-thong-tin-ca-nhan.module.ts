import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CMSThongTinCaNhanPage } from './cms-thong-tin-ca-nhan.page';
import { DaiLyXeIonNumberPhoneModule } from 'src/app/components/controllers/dailyxe-ion-numberphone/dailyxe-ion-numberphone.module';
import { DaiLyXeIonEmailModule } from 'src/app/components/controllers/dailyxe-ion-email/dailyxe-ion-email.module';

const routes: Routes = [
    {
        path: '',
        component: CMSThongTinCaNhanPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        DaiLyXeIonNumberPhoneModule,
        DaiLyXeIonEmailModule
    ],
    declarations: [
        CMSThongTinCaNhanPage,
    ],
    
    providers: [
        
    ]
})
export class CMSThongTinCaNhanPageModule { }
