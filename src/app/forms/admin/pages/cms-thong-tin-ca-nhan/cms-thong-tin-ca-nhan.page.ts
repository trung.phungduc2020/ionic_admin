import { Component, OnInit, EventEmitter } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { defaultWxh, Variables } from 'src/app/core/variables';
import { AdminDaiLyXeBLL_Admin } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { CoreProcess } from 'src/app/core/core-process';
import { PictureMethodsTs } from 'src/app/core/picture-methods';
@Component({
	selector: 'cms-thong-tin-ca-nhan',
	templateUrl: './cms-thong-tin-ca-nhan.page.html',
	styleUrls: ['./cms-thong-tin-ca-nhan.page.scss'],
	providers: [AdminDaiLyXeBLL_Admin]
})
export class CMSThongTinCaNhanPage implements OnInit {
	//#region =============== Variables ================
	public dataRoot: any = {};
	public data: any = {};
	//#endregion ============ End variables ============
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public pictureMethodsTs: PictureMethodsTs,
		public adminDaiLyXeBLL_Admin: AdminDaiLyXeBLL_Admin,
		public coreProcess: CoreProcess
	) {
		
	 }
	ngOnInit() {
	}
	async ionViewDidEnter() {
		await this.loadData();
	}
	async loadData() {
		
		let id = Variables.infoAdmin.Id;
		if (+id > 0) {
			let res: ResponseBase = await this.adminDaiLyXeBLL_Admin.getIndex(id, Variables.infoAdmin.IsMain).toPromise();
			res = new ResponseBase(res);
			this.data = res.getFirstData();
			this.data.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(this.data.RewriteUrl, this.data.IdFileDaiDien, defaultWxh.two);
			// this.commonMethodsTs.setInfoAdminCommonByObject(JSON.stringify(this.data));
			this.dataRoot = this.commonMethodsTs.cloneObject(this.data);
		}
		
	}
	async updateData() {
		this.commonMethodsTs.createMessageWarning("Chức năng tạm thời đang được hoàn thiện");
		return;
		
	}
	async chooseImage() {
		try {
			let base64Image = await this.pictureMethodsTs.actionChoosePictureSingal();
			if (base64Image) {
				this.data.UrlHinhDaiDien = base64Image;
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error)
		}
	}
	
	checkData()
	{
		return true;
	}
}
