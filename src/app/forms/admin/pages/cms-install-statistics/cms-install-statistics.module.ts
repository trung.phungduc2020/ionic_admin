import { ControllersModule } from './../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule } from 'src/app/components/controllers';
import { AdminRaoVatBLL_InstallStatistics } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CMSInstallStatistics_RoutingModule } from './cms-install-statistics.router';
import { CMSInstallStatisticsListPage } from './cms-install-statistics-list/cms-install-statistics-list.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSInstallStatistics_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule
    ],
    declarations: [
		CMSInstallStatisticsListPage,
	],
    providers: [
        AdminRaoVatBLL_InstallStatistics,
    ]
})
export class CMSInstallStatisticsPageModule { }
