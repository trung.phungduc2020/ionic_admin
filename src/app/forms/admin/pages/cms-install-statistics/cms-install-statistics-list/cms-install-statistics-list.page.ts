import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams, defaultWxh, listOsName } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { IonContent } from '@ionic/angular';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { animationList } from 'src/app/shared/animations/animations';
import { CMSInstallStatisticsListModal } from './cms-install-statistics-list.modal';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-install-statistics-list',
	templateUrl: './cms-install-statistics-list.page.html',
	styleUrls: ['./cms-install-statistics-list.page.scss'],
	providers: [CMSInstallStatisticsListModal],
	animations: [animationList]
})
export class CMSInstallStatisticsListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	public data: any = [];
	public search: any = {};
	public totalItems = -1;
	//public lOsName:any = listOsName;
	//#endregion
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsInstallStatisticsListModal: CMSInstallStatisticsListModal,
		public routerService: RouterExtService
	) {
	}
	ngOnInit() {
	}
	ionViewDidEnter() {
		this.init();
	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});

		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		params[commonParams.osName] = params.osName;
		let res: ResponseBase = await this.cmsInstallStatisticsListModal.getData(params);
		res = new ResponseBase(res);

		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.totalItems = res.getTotalItems();
			let dataRes = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);
			this.data = this.data.concat(dataRes);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	public removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}

	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	public scrollToTop() {
		this.content.scrollToTop(500);
	}

	//#endregion
}
