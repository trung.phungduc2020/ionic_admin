import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSInstallStatisticsListPage } from './cms-install-statistics-list/cms-install-statistics-list.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSInstallStatisticsListPage,
	},
];

export const CMSInstallStatistics_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);