import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { CMSAttackModeModal } from './cms-attackmode.modal';
import { labelPages } from 'src/app/core/labelPages';
import { CommonMethodsTs } from 'src/app/core/common-methods';
@Component({
	selector: 'cms-attackmode',
	templateUrl: './cms-attackmode.page.html',
	styleUrls: ['./cms-attackmode.page.scss'],
	providers: [CMSAttackModeModal],
})
export class CMSAttackModePage implements OnInit {
	//#region variables
	public lbl = labelPages;
	public data: any;
	public defaultStatistic: any;
	public onlineStatistic: any;

	public cacheMaxAgeForUpdateTruyCap: any;
	public cacheMaxAgeForUpdateSoLuotXemTinTuc: any;
	public cacheMaxAgeForUpdateSoLuotXemDangTinRaoXe: any;
	public underAttackMode: any;
	public underAttackMode_IsTurnOnByAdminInCommonfileController: any;
	public underAttackModeAutoOnOrOff: any;
	public postThongKeTruyCap: any;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	//#endregion
	constructor(
		public cMSAttackModeModal: CMSAttackModeModal,
		public commonMethodsTs: CommonMethodsTs
	) {
		this.onThongKe();
	}
	ngOnInit() {
	}

	async onThongKe(){
		this.defaultStatistic = await this.cMSAttackModeModal.SearchThongKeTruyCapMacDinh();
		this.onlineStatistic = await this.cMSAttackModeModal.SearchThongKeUserOnline();
		if(this.onlineStatistic.StrResult) {
			var parJsonOnlineStatistic = this.onlineStatistic.StrResult ? JSON.parse(this.onlineStatistic.StrResult) : "";
			console.log('parJsonOnlineStatistic', parJsonOnlineStatistic)
			this.cacheMaxAgeForUpdateTruyCap = parJsonOnlineStatistic.CacheMaxAgeForUpdateTruyCap;
			this.cacheMaxAgeForUpdateSoLuotXemTinTuc = parJsonOnlineStatistic.CacheMaxAgeForUpdateSoLuotXemTinTuc;
			this.cacheMaxAgeForUpdateSoLuotXemDangTinRaoXe = parJsonOnlineStatistic.CacheMaxAgeForUpdateSoLuotXemDangTinRaoXe;
			this.underAttackMode = parJsonOnlineStatistic.UnderAttackMode;
			this.underAttackMode_IsTurnOnByAdminInCommonfileController = parJsonOnlineStatistic.UnderAttackMode_IsTurnOnByAdminInCommonfileController;
			this.underAttackModeAutoOnOrOff = parJsonOnlineStatistic.UnderAttackModeAutoOnOrOff;
			this.postThongKeTruyCap = parJsonOnlineStatistic.PostThongKeTruyCap;
		}
	}
	async onAttackMode(stn: boolean) {
		this.commonMethodsTs.startLoader("onAttackMode");
		try {
			let param: any = {};
			param.aw = 'R0h2bkAwNDA3MjAxMyQ=';
			param.stn = stn;
			this.data = await this.cMSAttackModeModal.AttackMode(param);
		} catch (error) {
			console.log('onAttackMode', error);
		}
		this.commonMethodsTs.stopLoader("onAttackMode");
	}
	onAttackOnMode()
	{
		this.onAttackMode(true);
	}
	onAttackOffMode()
	{
		this.onAttackMode(false);
	}

	

	//#endregion
}
