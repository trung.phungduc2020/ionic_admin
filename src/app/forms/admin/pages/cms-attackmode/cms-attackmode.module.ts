import { ControllersModule } from '../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSAttackMode_RoutingModule } from './cms-attackmode.router';
import { CMSAttackModePage } from './cms-attackmode.page';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AdminRaoVatBLL_ThongBao } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSAttackMode_RoutingModule,
        ControllersModule,
        NgxJsonViewerModule
    ],
    declarations: [
		CMSAttackModePage,
	],
    providers: [
        AdminRaoVatBLL_ThongBao
    ]
})
export class CMSAttackModePageModule { }
