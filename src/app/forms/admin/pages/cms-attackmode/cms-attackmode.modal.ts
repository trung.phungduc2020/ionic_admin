import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from 'src/app/core/entity';
import { AdminDaiLyXeBLL_CommonFile, AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
@Injectable()
export class CMSAttackModeModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public adminDaiLyXeBLL_CommonFile: AdminDaiLyXeBLL_CommonFile,
        public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData
    ) {
    }

    public async AttackMode(param) {
        try {
            let result = await this.adminDaiLyXeBLL_CommonFile.underAttackMode(param).toPromise();
            result = new ResponseBase(result);
			return result;
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);
        }
    }

    public async SearchThongKeTruyCapMacDinh(){
        try {
            let result = await this.adminDaiLyXeBLL_InterfaceData.searchThongKeTruyCapMacDinh().toPromise();
            result = new ResponseBase(result);
			return result;
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);
        }
    }

    public async SearchThongKeUserOnline(){
        try {
            let result = await this.adminDaiLyXeBLL_CommonFile.searchThongKeUserOnline().toPromise();
            result = new ResponseBase(result);
            console.log('SearchThongKeUserOnline:', result)
			return result;
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);
        }
    }
}