import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSAttackModePage } from './cms-attackmode.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSAttackModePage,
	},
];

export const CMSAttackMode_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);