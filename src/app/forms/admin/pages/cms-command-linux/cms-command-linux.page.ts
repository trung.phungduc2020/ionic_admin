import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSCommandLinuxModal } from './cms-command-linux.modal';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-command-linux',
	templateUrl: './cms-command-linux.page.html',
	styleUrls: ['./cms-command-linux.page.scss'],
	providers: [CMSCommandLinuxModal],
})
export class CMSCommandLinuxPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	public strCommand: string = ""; 
	public resResult: any = null; 
	//#endregion
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public routerService: RouterExtService,
		public cMSSettingsModal: CMSCommandLinuxModal,
	) {
		
	}
	ngOnInit() {
	}
	
	public async execCommand() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			this.commonMethodsTs.createMessageWarning("Chưa nhập command");
			return false;
		}		
		this.resResult =  await this.cMSSettingsModal.execCommand(this.strCommand);
		
	}

	isCheckData()
	{		
		return !!this.strCommand
	}
	getAllDefault()
	{		
		this.strCommand = "kubectl get all";
	}

	getAllRaoXe()
	{		
		this.strCommand = "kubectl get all -n prodraoxe";
	}

	restartApiRaoXe()
	{		
		this.strCommand = "kubectl rollout restart deployment apiraoxe -n prodraoxe";
	}

	restartPostgres() {
		this.strCommand = "kubectl exec -it pod/postgresql-postgresql-primary-0 -n prodraoxe bash -- pg_ctl reload";
	}

	logsRaoXe()
	{		
		this.strCommand = "kubectl logs service/apiraoxe -n prodraoxe";
	}

	logsPostgresRaoXe()
	{		
		this.strCommand = "kubectl logs pod/apiraoxe-postgresql-primary-0 -n prodraoxe";
	}

	ionViewWillEnter() {
	}
	

	//#endregion
}
