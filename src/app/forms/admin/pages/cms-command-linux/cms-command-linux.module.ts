import { ControllersModule } from '../../../../components/controllers/index';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSCommandLinux_RoutingModule } from './cms-command-linux.router';
import { CMSCommandLinuxPage } from './cms-command-linux.page';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { JSONEditorModule } from 'ngx-jsoneditor' 
 
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSCommandLinux_RoutingModule,
        ControllersModule,
        NgxJsonViewerModule,
        JSONEditorModule
    ],
    declarations: [
		CMSCommandLinuxPage,
	],
    providers: [
        AdminRaoVatBLL_InteractData
    ]
})
export class CMSCommandLunixPageModule { }
