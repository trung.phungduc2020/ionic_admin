import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSCommandLinuxPage } from './cms-command-linux.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSCommandLinuxPage,
	},
];

export const CMSCommandLinux_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);