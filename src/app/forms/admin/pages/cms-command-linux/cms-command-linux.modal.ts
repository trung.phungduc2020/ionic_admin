import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { CoreProcess } from 'src/app/core/core-process';
import { AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
@Injectable()
export class CMSCommandLinuxModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public coreProcess: CoreProcess,
        public adminRaoVatBLL_InteractData: AdminRaoVatBLL_InteractData
    ) {
    }
  
    public async execCommand(strCommand: string) {
        let kq: any = {};
        try {
            kq = await this.adminRaoVatBLL_InteractData.linuxExecCommand(strCommand).toPromise();
            
        } catch (error) {
            kq = JSON.parse(error);
        }
        return kq;
    }

}   
