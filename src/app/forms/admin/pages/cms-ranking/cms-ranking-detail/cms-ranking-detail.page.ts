import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { UserRank } from 'src/app/core/entity';
import { commonParams, commonMessages } from 'src/app/core/variables';
import { CMSRankingDetailModal } from './cms-ranking-detail.modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'cms-ranking-detail',
	templateUrl: './cms-ranking-detail.page.html',
	styleUrls: ['./cms-ranking-detail.page.scss'],
	providers: [CMSRankingDetailModal]
})
export class CMSRankingDetailPage implements OnInit {
	//#region variables
	static status = false;
	public lbl = labelPages;
	public data: UserRank = new UserRank();
	public myForm: FormGroup;

	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cMSRankingDetailModal: CMSRankingDetailModal,
		public fb: FormBuilder,
		public activatedRoute: ActivatedRoute


	) { }
	//#endregion
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSRankingDetailPage.status = false;
			let id = params[commonParams.id] || -1;
			this.loadData(id);
		});
	}
	public async loadData(id) {
		this.data = await this.cMSRankingDetailModal.getData(id);
		this.buildForm();
	}
	async buildForm() {
		if (this.data) {
			try {
				this.myForm.reset();
			} catch (error) {
			}
			this.commonMethodsTs.ngZone.run(() => {
				let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
				group.TieuDe = [this.data.TieuDe, Validators.required];
				group.MoTaNgan = [this.data.MoTaNgan];
				group.PercentDiscount = [this.data.PercentDiscount, Validators.required];
				group.PointRule = [this.data.PointRule, Validators.required];				
				this.myForm = this.fb.group(group);
			})
		}
	}
	public async saveData() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M018);
			return;
		}
		let data = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSRankingDetailModal.saveData(data);
		if (res) {
			CMSRankingDetailPage.status = true;
			if (!(this.data.Id > 0)) {
				this.commonMethodsTs.goBack();
				return;
			}
			// await this.loadData(this.data.Id);
		}
	}
	public async delete() {
		let data = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSRankingDetailModal.delete(data.Id);
		if (res) {
			CMSRankingDetailPage.status = true;
			this.commonMethodsTs.goBack();
		}

	}

	isCheckData(): boolean {
		return !this.myForm.invalid;
	}
	//#endregion
}
