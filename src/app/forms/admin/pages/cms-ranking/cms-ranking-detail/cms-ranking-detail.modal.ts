import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { Router } from "@angular/router";
import { ResponseBase, UserRank } from "src/app/core/entity";
import { commonParams, commonMessages } from "src/app/core/variables";
import { AdminRaoVatBLL_UserRank } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSRankingDetailModal {
    constructor(
        public adminRaoVatBLL_UserRank: AdminRaoVatBLL_UserRank,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(id) {
        let data:UserRank = new UserRank();
        if(!(id > 0))
        {
            return data;
        }
        try {
			let paramsData = {
                ids: id,
                trangThai: "1,2"
            }
            let result = await this.adminRaoVatBLL_UserRank.search(paramsData).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                data = result.DataResult[0] as UserRank;
            } else {
                this.commonMethodsTs.createMessageError(result.getMessage())
            }
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
    public async saveData(data) {
        try {

            //#endregion
            if (data.Id == -1) {
                let result = await this.adminRaoVatBLL_UserRank.insert(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M003);
                    return true;
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }
            }
            else {
                let result = await this.adminRaoVatBLL_UserRank.update(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {

                    this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                    return true;

                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
    public async delete(id) {
        try {
            let result = await this.adminRaoVatBLL_UserRank.updateTrangThai(id, '3').toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true;
            } else {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
}
