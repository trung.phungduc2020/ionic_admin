import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSRanking_RoutingModule } from './cms-ranking.router';
import { CMSRankingDetailPage } from './cms-ranking-detail/cms-ranking-detail.page';
import { CMSRankingListPage } from './cms-ranking-list/cms-ranking-list.page';
import { AdminRaoVatBLL_UserRank } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';

@NgModule({
    imports: [
        CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
		CMSRanking_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule
    ],
    declarations: [
		CMSRankingDetailPage,
		CMSRankingListPage,
	],
    providers: [
		AdminRaoVatBLL_UserRank
    ]
})
export class CMSRankingPageModule { }
