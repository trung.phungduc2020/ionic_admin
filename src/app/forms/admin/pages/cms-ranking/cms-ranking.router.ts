import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSRankingListPage } from './cms-ranking-list/cms-ranking-list.page';
import { CMSRankingDetailPage } from './cms-ranking-detail/cms-ranking-detail.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSRankingListPage,
	},
	{
		path: ':id',
		component: CMSRankingDetailPage,
	}
];

export const CMSRanking_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);