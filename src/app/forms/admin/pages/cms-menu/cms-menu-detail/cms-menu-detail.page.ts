import { CoreProcess } from 'src/app/core/core-process';
import { MenuRaoVat } from 'src/app/core/entity';
import { Component, OnInit } from '@angular/core';
//providers
import { commonMessages, commonParams } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CMSMenuDetailModal } from './cms-menu-detail.modal';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'cms-menu-detail',
	templateUrl: './cms-menu-detail.page.html',
	styleUrls: ['./cms-menu-detail.page.scss'],
})
export class CMSMenuDetailPage implements OnInit {
	//#region Variables
	public data: MenuRaoVat = new MenuRaoVat();
	public listMenu: any;
	//#endregion
	//#region contructor
	constructor(
		public cmsMenuDetailModal: CMSMenuDetailModal,
		public coreProcess: CoreProcess,
		public commonMethodsTs: CommonMethodsTs,
		public activatedRoute: ActivatedRoute,
		) { }
	//#endregion
	ngOnInit() { }
	ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			let id = params[commonParams.id] || -1;
			this.loadData(id);
		});
	}
	public async loadData(id) {
		try {
			if (id == -1) {
				this.listMenu = await this.cmsMenuDetailModal.getMenuByNotId();
				this.cmsMenuDetailModal.resetCurrentMenu(this.data);
			} else {
				this.data = await this.cmsMenuDetailModal.getMenuById(id);
				let lstAllMenu = await this.cmsMenuDetailModal.getMenuByNotId(id);
				this.listMenu = this.cmsMenuDetailModal.deleteElementAndChildren(lstAllMenu, id);
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	async saveData() {
		try {
			// kiểm tra dữ liệu đầu vào
			// kiểm tra tên menu
			if (!this.data.TenMenu) {
				this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P006, labelPages.nameMenu.toLocaleLowerCase()));
				return;
			}
			// kiểm tra vị trí menu
			if ((this.data.IdMenuParent < 0)) {
				this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P007, labelPages.positionMenu.toLocaleLowerCase()));
				return;
			}
			let data = this.commonMethodsTs.cloneObject(this.data);
			let res = this.cmsMenuDetailModal.saveData(data);
			if (res && !(+data.Id > 0)) {
				this.data = this.cmsMenuDetailModal.resetCurrentMenu(data);
			}

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	async xoaMenu() {
		let result = await this.cmsMenuDetailModal.xoaMenu(this.data.Id);
		if (result) {
			this.commonMethodsTs.toPage_AdminMenu();
		}
	}
	trangThaiChange(e) {
		this.data.TrangThai = e.detail.value;
	}



	//#endregion
}
