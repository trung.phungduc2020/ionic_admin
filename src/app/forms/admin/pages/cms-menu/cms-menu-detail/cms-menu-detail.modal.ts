import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_Menu } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CoreProcess } from "src/app/core/core-process";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase, MenuRaoVat } from "src/app/core/entity";
import { commonMessages } from "src/app/core/variables";

@Injectable()
export class CMSMenuDetailModal {

    constructor(
        public adminRaoVatBLL_Menu: AdminRaoVatBLL_Menu,
        public coreProcess: CoreProcess,
        public commonMethodsTs: CommonMethodsTs,
    ) { }
    public async loadData(id) {
        let data = {};
        try {
            data = await this.getMenuById(id);

        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
    public resetCurrentMenu(data) {
        data.TenMenu = '';
        data.MoTaTrang = '';
        data.IdMenuParent = 0;
        data.GhiChu = '';
        data.TrangThai = 1;
        return data;
    }
    async saveData(data) {
        try {

            try {
                if (data.UrlHinhDaiDien && data.UrlHinhDaiDien.length > 0) {
                    data.IdFileDaiDien = await this.coreProcess.insertByUrl(data.UrlHinhDaiDien, data.TenMenu || "new-file");
                }
            } catch (error) {
                this.commonMethodsTs.createMessageErrorByTryCatch(error);
                return;
            }
            let result: any;
            if (+data.Id > 0) {
                result = await this.adminRaoVatBLL_Menu.update(data).toPromise();
                result = new ResponseBase(result);
                // thông báo
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }

            }
            else {
                result = await this.adminRaoVatBLL_Menu.insert(data).toPromise();
                result = new ResponseBase(result);
                // thông báo
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M003)
                } else {
                    this.commonMethodsTs.createMessageError(commonMessages.M004);
                }
            }
            return result.isSuccess()
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
    async xoaMenu(id) {
        try {
            let result = await this.adminRaoVatBLL_Menu.updateTrangThai(id, '3').toPromise();
            result = new ResponseBase(result);
            let message = result.getMessage('');
            if (!message || message.length <= 0 || message == '') {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true
            } else {
                this.commonMethodsTs.createMessageError(message);
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }

    //#region private methods
    deleteElementAndChildren(list, id) {
        if (list == null || list.length <= 0) {
            return;
        }
        if (!id) {
            return;
        }
        this.commonMethodsTs.removeElementById(list, id);
        for (let i = 0; i < list.length; i++) {
            if (list[i].IdMenuParent == id) {
                this.deleteElementAndChildren(list, list[i].Id);
                i = -1;
            }
        }

        return list;
    }
    async getMenuById(id) {
        try {
            let params: any = {};
            params.id = id
            let result: ResponseBase = await this.adminRaoVatBLL_Menu.search(params).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                return result.getFirstData();
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return new MenuRaoVat();
    }
    async getMenuByNotId(id?: any) {
        try {
            let params: any = {};
            params.notId = id
            let result: ResponseBase = await this.adminRaoVatBLL_Menu.search(params).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                return result.DataResult;
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return []
    }

}
