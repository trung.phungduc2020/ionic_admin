import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSMenuListPage } from './cms-menu-list/cms-menu-list.page';
import { CMSMenuDetailPage } from './cms-menu-detail/cms-menu-detail.page';

const appRoutes: Routes = [
    {
        path: '',
        component: CMSMenuListPage,
      },
      {
        path: ':id',
        component: CMSMenuDetailPage,
      }
];

export const CMSMenu_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);