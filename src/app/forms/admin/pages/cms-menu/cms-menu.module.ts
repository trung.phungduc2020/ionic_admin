import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSMenuListPage } from './cms-menu-list/cms-menu-list.page';
import { CMSMenuDetailPage } from './cms-menu-detail/cms-menu-detail.page';
import { CMSMenu_RoutingModule } from './cms-menu.router';
import { AdminRaoVatBLL_Menu } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { DaiLyXeIonImgModule, ControllersModule } from 'src/app/components/controllers';
import { CMSMenuListModal } from './cms-menu-list/cms-menu-list.modal';
import { TreeTableModule } from 'src/app/components/controllers/primeNg/treetable/treetable';
import { CMSMenuDetailModal } from './cms-menu-detail/cms-menu-detail.modal';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSMenu_RoutingModule,
        TreeTableModule,
        DaiLyXeIonImgModule,
        ControllersModule
    ],
    declarations: [CMSMenuListPage, CMSMenuDetailPage],
    providers: [
        AdminRaoVatBLL_Menu,
        CMSMenuListModal,
        CMSMenuDetailModal
    ]
})
export class CMSMenuPageModule { }
