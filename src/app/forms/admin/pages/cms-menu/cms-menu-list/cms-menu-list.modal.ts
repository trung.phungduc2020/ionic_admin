import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_Menu } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
@Injectable()
export class CMSMenuListModal {
    constructor(
        public adminRaoVatBLL_Menu: AdminRaoVatBLL_Menu,
        public commonMethodsTs: CommonMethodsTs,
        ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = [];
        
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let paramsData = {
                trangThai: trangThai,
                tuKhoaTimKiem: search.tuKhoaTimKiem
            }
            let res: ResponseBase = await this.adminRaoVatBLL_Menu.searchMenuTree(paramsData).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                data = res.DataResult;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        
        return data;
    }
}
