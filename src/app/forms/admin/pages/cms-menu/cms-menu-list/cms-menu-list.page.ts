import { Component, OnInit } from '@angular/core';
import { labelPages } from 'src/app/core/labelPages';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CMSMenuListModal } from './cms-menu-list.modal';
import { currentPageDefault, commonParams, displayItemDefault } from 'src/app/core/variables';

@Component({
    selector: 'cms-menu-list',
    templateUrl: './cms-menu-list.page.html',
    styleUrls: ['./cms-menu-list.page.scss'],
})
export class CMSMenuListPage implements OnInit {
    //#region ================================= variable =====================
    public lbl = labelPages;
    public data: any;
    public search: any = {};
    public cols: any[];
    public tuKhoaTimKiem;
    //#endregion ============================== and variable =================
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public cmsMenuListModal: CMSMenuListModal) {
        
        this.cols = [
            { field: 'TenMenu', header: 'Tên menu' },
            { field: 'Edit', header: 'Sửa', style: { 'width': '45px', 'text-align': 'center' } }
        ];
    }
    public async ngOnInit() { }
    public async ionViewDidEnter() {
        await this.loadData();
    }
   
    public async loadData(currentPage = currentPageDefault) {
        let params = this.commonMethodsTs.cloneObject(this.search || {});
      
        params[commonParams.displayItems] = displayItemDefault;
        params[commonParams.displayPage] = currentPage || currentPageDefault;
        params[commonParams.tuKhoaTimKiem] = this.tuKhoaTimKiem;
        
        this.data = await this.cmsMenuListModal.getData(params);
    }
    public removeTuKhoaTimKiem() {
        this.tuKhoaTimKiem = null;
        this.loadData();
    }
    public onSelect(id: any) {
        this.commonMethodsTs.toPage_AdminMenu(id);
    }
   

}
