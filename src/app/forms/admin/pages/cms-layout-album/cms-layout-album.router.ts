import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSLayoutAlbumListPage } from './cms-layout-album-list/cms-layout-album-list.page';
import { CMSLayoutAlbumDetailPage } from './cms-layout-album-detail/cms-layout-album-detail.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSLayoutAlbumListPage,
	},
	{
		path: ':id',
		component: CMSLayoutAlbumDetailPage,
	}
];

export const CMSLayoutAlbum_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);