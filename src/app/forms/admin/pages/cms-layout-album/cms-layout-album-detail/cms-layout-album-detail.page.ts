import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { LayoutAlbum } from 'src/app/core/entity';
import { commonParams, commonMessages } from 'src/app/core/variables';
import { CMSLayoutAlbumDetailModal } from './cms-layout-album-detail.modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'cms-layout-album-detail',
	templateUrl: './cms-layout-album-detail.page.html',
	styleUrls: ['./cms-layout-album-detail.page.scss'],
	providers: [CMSLayoutAlbumDetailModal]
})
export class CMSLayoutAlbumDetailPage implements OnInit {
	//#region variables
	static status = false;
	public lbl = labelPages;
	public data: LayoutAlbum = new LayoutAlbum();
	public myForm: FormGroup;

	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cMSLayoutAlbumDetailModal: CMSLayoutAlbumDetailModal,
		public fb: FormBuilder,
		public activatedRoute: ActivatedRoute


	) { }
	//#endregion
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSLayoutAlbumDetailPage.status = false;
			let id = params[commonParams.id] || -1;
			this.loadData(id);
		});
	}
	public async loadData(id) {
		this.data = await this.cMSLayoutAlbumDetailModal.getData(id);
		this.buildForm();
	}
	async buildForm() {
		if (this.data) {
			try {
				this.myForm.reset();
			} catch (error) {
			}
			this.commonMethodsTs.ngZone.run(() => {
				let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
				group.Ma = [this.data.Ma, Validators.required];
				group.TongImage = [this.data.TongImage, Validators.required];
				group.UrlHinhDaiDien = [this.data.UrlHinhDaiDien];
				this.myForm = this.fb.group(group);
			})
		}
	}
	public async saveData() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M018);
			return;
		}
		let data = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSLayoutAlbumDetailModal.saveData(data);
		if (res) {
			CMSLayoutAlbumDetailPage.status = true;
			if (!(this.data.Id > 0)) {
				this.commonMethodsTs.goBack();
				return;
			}
			// await this.loadData(this.data.Id);
		}
	}
	public async delete() {
		let data = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSLayoutAlbumDetailModal.delete(data.Id);
		if (res) {
			CMSLayoutAlbumDetailPage.status = true;
			this.commonMethodsTs.goBack();
		}

	}

	isCheckData(): boolean {
		return !this.myForm.invalid;
	}
	//#endregion
}
