import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { Router } from "@angular/router";
import { ResponseBase, LayoutAlbum } from "src/app/core/entity";
import { commonParams, commonMessages, defaultWxh } from "src/app/core/variables";
import { AdminRaoVatBLL_LayoutAlbum } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CoreProcess } from 'src/app/core/core-process';
@Injectable()
export class CMSLayoutAlbumDetailModal {
    constructor(
        public adminRaoVatBLL_LayoutAlbum: AdminRaoVatBLL_LayoutAlbum,
        public commonMethodsTs: CommonMethodsTs,
        public coreProcess: CoreProcess
    ) {
    }
    //GetData
    public async getData(id) {
        let data: LayoutAlbum = new LayoutAlbum();
        if (!(id > 0)) {
            return data;
        }
        try {
            let paramsData = {
                ids: id,
                trangThai: "1,2"
            }
            let result = await this.adminRaoVatBLL_LayoutAlbum.search(paramsData).toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                result.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(result.DataResult || [], defaultWxh.two);
                data = result.DataResult[0] as LayoutAlbum;
            } else {
                this.commonMethodsTs.createMessageError(result.getMessage())
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
    public async saveData(data: LayoutAlbum) {
        try {
            //#endregion
            try {
                if (data.UrlHinhDaiDien && data.UrlHinhDaiDien.length > 0) {
                    data.IdFileDaiDien = await this.coreProcess.insertByUrl(data.UrlHinhDaiDien, data.Ma || "new-file");
                }
            } catch (error) {
                this.commonMethodsTs.createMessageErrorByTryCatch(error);
                return;
            }
            if (data.Id == -1) {
                let result = await this.adminRaoVatBLL_LayoutAlbum.insert(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {
                    this.commonMethodsTs.createMessageSuccess(commonMessages.M003);
                    return true;
                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }
            }
            else {
                let result = await this.adminRaoVatBLL_LayoutAlbum.update(data).toPromise();
                result = new ResponseBase(result);
                if (result.isSuccess()) {

                    this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                    return true;

                } else {
                    this.commonMethodsTs.createMessageError(result.getMessage());
                }
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
    public async delete(id) {
        try {
            let result = await this.adminRaoVatBLL_LayoutAlbum.updateTrangThai(id, '3').toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true;
            } else {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return false;
    }
}
