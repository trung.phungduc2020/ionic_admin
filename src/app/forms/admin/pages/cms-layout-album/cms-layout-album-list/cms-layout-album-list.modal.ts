import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { Router } from "@angular/router";
import { ResponseBase } from "src/app/core/entity";
import { commonParams, defaultWxh } from "src/app/core/variables";
import { AdminRaoVatBLL_LayoutAlbum } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSLayoutAlbumListModal {
    constructor(
		public adminRaoVatBLL_LayoutAlbum: AdminRaoVatBLL_LayoutAlbum,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params:any = {};
            params[commonParams.displayItems] = search.displayItems;
            params[commonParams.displayPage] = search.displayPage;
            params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
            params[commonParams.trangThai] = trangThai;           
            let res: ResponseBase = await this.adminRaoVatBLL_LayoutAlbum.search(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);
                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        
        return data;
    }
}
