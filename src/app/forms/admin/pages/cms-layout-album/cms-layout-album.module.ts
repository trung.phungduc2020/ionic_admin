import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSLayoutAlbum_RoutingModule } from './cms-layout-album.router';
import { AdminRaoVatBLL_LayoutAlbum } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { CMSLayoutAlbumDetailPage } from './cms-layout-album-detail/cms-layout-album-detail.page';
import { CMSLayoutAlbumListPage } from './cms-layout-album-list/cms-layout-album-list.page';

@NgModule({
    imports: [
        CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
		CMSLayoutAlbum_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule
    ],
    declarations: [
		CMSLayoutAlbumDetailPage,
		CMSLayoutAlbumListPage,
	],
    providers: [
		AdminRaoVatBLL_LayoutAlbum
    ]
})
export class CMSLayoutAlbumPageModule { }
