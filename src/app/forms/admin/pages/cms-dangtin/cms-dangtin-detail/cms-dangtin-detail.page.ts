import { Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonMessages, commonParams, listTinhTrangDangTinChiTiet, configCK, listTenNhienLieu, valueDefault, defaultValue, commonException, Variables } from 'src/app/core/variables';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DangTin } from 'src/app/core/entity';
import { CMSDangTinDetailModal } from './cms-dangtin-detail.modal';
import { CoreVariables } from 'src/app/core/core-variables';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'cms-dangtin-detail',
    templateUrl: './cms-dangtin-detail.page.html',
    styleUrls: ['./cms-dangtin-detail.page.scss'],
    providers: [CMSDangTinDetailModal]
})
export class CMSDangTinDetailPage {
    //#region ============================= Variables ========================
    static status = false;
    public lbl = labelPages;
    public valueDefault: any = valueDefault;
    public data: DangTin = new DangTin();
    public configCK = configCK;
    public listTinhTrangDangTin: any = listTinhTrangDangTinChiTiet;
    public lTenNhienLieu: any = listTenNhienLieu;
    public steps: any = [];
    public myForm: FormGroup;
    public defaultValue = defaultValue;
    public configCkTinDang = {
        "height": 'auto',
        "resize_enabled": false,
        "toolbar": [],
        "toolbarStartupExpanded": false,
        "removePlugins": 'elementspath',
        // "extraPlugins": 'divarea',
        "placeHolder": "123",
        "readOnly": true
    };
    //#endregion ============================= Variables ========================
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public fb: FormBuilder,
        public dangTinModal: CMSDangTinDetailModal,
        public activatedRoute: ActivatedRoute
    ) {
    }
    //#region ================== init
    async ionViewWillEnter() {
        this.activatedRoute.params.subscribe(async params => {
            CMSDangTinDetailPage.status = false;
            let id = params[commonParams.id] || -1;
            if (!(+id >= 0)) {
                this.commonMethodsTs.goBack();
                return;
            }
            await this.loadData(id)
        })
    }
    ionViewDidEnter() {
    }

    //#endregion ============== end int
    async loadData(id: any) {
        this.data = await this.dangTinModal.getIndex(id);
        await this.buildForm();
    }
    async buildForm() {
        if (this.data) {
            try {
                // this.myForm.reset();
            } catch (error) {
            }
            this.commonMethodsTs.ngZone.run(() => {
                let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
                group.IdTinhThanh = [this.data.IdTinhThanh || 31];
                group.IdMenu = [this.data.IdMenu || 1];
                group.IdMauNgoaiThat = [this.data.IdMauNgoaiThat || 1];
                group.IdMauNoiThat = [this.data.IdMauNoiThat || 1];
                group.MucTieuHaoNhienLieu100 = [this.data.MucTieuHaoNhienLieu100 || 10];
                group.MaLayoutAlbum = [this.data.MaLayoutAlbum.replace("_null", "_h")];
                let newDate = this.commonMethodsTs.getDateServe()
                group.TuNgay = [this.commonMethodsTs.toISOString(this.data.TuNgay)];
                group.DenNgay = [this.commonMethodsTs.toISOString(this.data.DenNgay, new Date(newDate.setMonth(newDate.getMonth() + 1)))];
                group.UrlHinhDaiDien = [this.data.UrlHinhDaiDien || null];
                group.MoTaChiTiet = [this.data.MoTaChiTiet || null];
                group.Type = [this.data.Type || 2];
                //update 
                group.ListHinhAnh = [this.data.ListHinhAnh || []];
                this.myForm = this.fb.group(group);
            })
        }
    }
    //saveData
    async saveData(data) {
        data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
        data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);
        if (await this.dangTinModal.saveDangTin(data)) {
            CMSDangTinDetailPage.status = true;
            return true
        }
        return false;
    }

    async savePinTopData(data) {
        if (await this.dangTinModal.savePinTopDangTin(data)) {
            CMSDangTinDetailPage.status = true;
            return true
        }
        return false;
    }
    // storeMessageDuyetTin(data: any) {
    //     data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
    //     data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);
    //     this.commonMethodsTs.storeMessageDuyetTin(data);
    // }
    //Xóa tin đăng
    public async remove() {

        let resAlert = await this.commonMethodsTs.alertConfirm('Bạn thật sự muốn xóa tin đăng này?');
        if (resAlert) {
            let res = await this.dangTinModal.deleteDangTin(this.myForm.value)
            if (res) {
                CMSDangTinDetailPage.status = true;
            }
        }
        else {
            return;

        }
    }

    //#region ======================================= Method
    isCheckData(data): boolean {
        if(data.IdAdminDangKyDuyet > 0 && data.IdAdminDangKyDuyet != Variables.infoAdmin.Id)
        {
            this.commonMethodsTs.createMessageWarning("Tin này đã có người khác đăng ký, bạn không có quyền chỉnh sửa")
            return false;
        }
        return true;
    }
    //Xử lý duyêt PinTop
    async ionChangePinTop() {
        let data = this.commonMethodsTs.cloneObject(this.data);
        data.IsPinTop = !data.IsPinTop;
        this.commonMethodsTs.startLoading();
        try {
            await this.savePinTopData(data);
            this.loadData(this.data.Id);
        } catch (error) {

        }
        this.commonMethodsTs.stopLoading();
    }

    // Xử lý duyêt
    async approval() {
        let data = this.commonMethodsTs.cloneObject(this.data);
        if(!this.isCheckData(data)) return;
        if (!(data.IdAdminDangKyDuyet > 0)) {
           
            let res = await this.commonMethodsTs.alertConfirm("Bạn có muốn <strong>đăng ký duyệt</strong> tin này?", { labelNo: "Không", labelYes: "Có" });
            if (res) {
                let res = await this.dangTinModal.registAdminApprove(data.Id);
                if (res) {
                    data.IdAdminDangKyDuyet = Variables.infoAdmin.Id;
                }
            }
            else
            {
               return;
            }
        }
        data.IsDuyet = true;
        data.GhiChu = '';
        this.commonMethodsTs.startLoading();
        try {
            let res = await this.saveData(data);
            //Chỉ gửi Message khi là duyệt
            // Mr Trung: xử lý server
            // if (res) {
            //     this.storeMessageDuyetTin(data);
            // }
            this.loadData(this.data.Id);
        } catch (error) {

        }

        this.commonMethodsTs.stopLoading();

    }
    //Từ chối duyệt
    async refuse() {
        let data = this.commonMethodsTs.cloneObject(this.data);
        data.IsDuyet = false;
        data.IdAdminDangKyDuyet = -1;
        data.TenAdminDangKyDuyet = '';

        let resAlert = await this.commonMethodsTs.alertInput('Bạn vui lòng <strong>nhập</strong> lý do từ chối duyệt!');
        if (resAlert != commonException.cancel) {
            if (!resAlert) {
                this.commonMethodsTs.createMessageWarning(this.commonMethodsTs.stringFormat(commonMessages.P005, labelPages.note));
                this.refuse();
                return;
            }
            data.GhiChu = resAlert;
        }
        this.commonMethodsTs.startLoading();
        try {
            let res = await this.saveData(data);
            // Mr Trung: Xử lý trên server
            // if (res) {
            //     this.commonMethodsTs.storeMessageDuyetTin(data);
            // }
            this.loadData(this.data.Id);

        } catch (error) {

        }
        this.commonMethodsTs.stopLoading();

    }
    //#endregion ==================================== End Method
    //#region ======================================= Destroy
    ionViewWillLeave() {
    }
    //#endregion ==================================== End Destroy

}
