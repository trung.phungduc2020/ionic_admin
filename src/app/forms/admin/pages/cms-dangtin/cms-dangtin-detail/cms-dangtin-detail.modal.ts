import { CoreProcess } from 'src/app/core/core-process';
import { Injectable } from '@angular/core';
import { ResponseBase, DangTin } from 'src/app/core/entity';
import { CommonMethodsTs } from 'src/app/core/common-methods';
// import { SqliteService } from 'src/app/shared/sqlite.service';
import { Variables, cacheData, commonMessages } from 'src/app/core/variables';
import { ThanhVienRaoVatBLL_Commonfile } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_DangTin } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { CommonService } from 'src/app/forms/member/services/common.service';
import { CoreVariables } from 'src/app/core/core-variables';
@Injectable()
export class CMSDangTinDetailModal {
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public thanhVienRaoVatBLL_Commonfile: ThanhVienRaoVatBLL_Commonfile,
        public adminRaoVatBLL_DangTin: AdminRaoVatBLL_DangTin,
        public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
        public commonService: CommonService,
        public coreProcess: CoreProcess
    ) { }
    //Hamf get DangTinById
    private async getDataDangTinbyId_Server(id: any) {
        let res: ResponseBase = await this.adminRaoVatBLL_DangTin.getIndex(id).toPromise();
        res = new ResponseBase(res);
        if (res.isSuccess()) {
            let data = res.getFirstData() || {};
            data.UrlHinhDaiDien = +data.IdFileDaiDien > 0 ? this.commonMethodsTs.builLinkImage(data.RewriteUrl, data.IdFileDaiDien) : null;
            data.ListHinhAnh = data.ListHinhAnh || [];
            let lstIdHinhAnh = this.commonMethodsTs.stringJsonToArray(data.ListHinhAnhJson || "");
            if (lstIdHinhAnh && lstIdHinhAnh.length > 0) {
                for (let x = 0; x < lstIdHinhAnh.length; x++) {

                    if (lstIdHinhAnh[x] > 0) {
                        data.ListHinhAnh[x] = this.commonMethodsTs.builLinkImage(data.RewriteUrl, lstIdHinhAnh[x])
                    }
                }
            }
            return data;
        }
        else {
            this.commonMethodsTs.createMessageError(res.getMessage());
            return {};
        }
    }

    public async getChiTietBaiViet(id: any) {
        if (+id <= 0) {
            return;
        }
        let res = await this.thanhVienRaoVatBLL_Commonfile.getChiTietDangTin(id).toPromise();
        if (res) {
            this.commonMethodsTs.storage.set(cacheData.DangTinDetail + id, res);
            return res;
        }
        if (!res) {
            return null;
        }
        return await this.commonMethodsTs.storage.get(cacheData.DangTinDetail + id);
    }
    //Hàm tạo chitietBaiViet
    public async createFileChiTietBaiViet(id: any, content: any) {
        if (+id <= 0) {
            this.commonMethodsTs.createMessageSuccess("Tin đăng không tồn tại");
            return false;
        }
        try {
            let res: ResponseBase = await this.thanhVienRaoVatBLL_Commonfile.createFileChiTietDangTin(id, content).toPromise();
                res = new ResponseBase(res);
                if (res.isSuccess()) {
                    return true;
                }
                else {
                    this.commonMethodsTs.createMessageSuccess(res.getMessage());
                    return false;
                }
        }
        catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }
    //Hàm get by Id
    public async getIndex(id?: any) {
        let data: DangTin = new DangTin();
        if (+id > 0) {
            data = await this.getDataDangTinbyId_Server(id);
            data.MoTaChiTiet = await this.getChiTietBaiViet(id);
        }

        return data;
    }

    //saveDangTin
    public async saveDangTin(data: any) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.update(data).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
                return false;
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }

    public async savePinTopDangTin(data: any) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.updatePinTop(data.Id, data.IsPinTop).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
                return false;
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
            return false;
        }
    }

    //Delete
    public async deleteDangTin(data: any) {
        try {
            let result: ResponseBase = await this.adminRaoVatBLL_DangTin.updateTrangThai(data.Id, "3").toPromise();
            result = new ResponseBase(result);
            if (result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M007);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);

        }

        return false;
    }
    async getDinhNghiaSanPhamByIdDongXe(idDongXe) {
        return await this.commonService.getFirstDinhNghiaSanPhamByIdDongXe(idDongXe);

    }

    //dangKyDuyet
    public async registAdminApprove(id: any) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.registAdminApprove(id).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);

        }
        return false
    }

}
