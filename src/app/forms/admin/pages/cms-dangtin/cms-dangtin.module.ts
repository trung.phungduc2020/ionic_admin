import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CMSDangTinListPage } from './cms-dangtin-list/cms-dangtin-list.page';
import { CMSDangTinDetailPage } from './cms-dangtin-detail/cms-dangtin-detail.page';
import { CMSDangTin_RoutingModule } from './cms-dangtin.router';
import { ReactiveFormsModule} from '@angular/forms';
import { AdminRaoVatBLL_DangTin, AdminRaoVatBLL_Menu, AdminRaoVatBLL_Commonfile, AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CKEditorModule } from 'ngx-ckeditor';
import { ImageLazyLoadModule } from 'src/app/shared/directive/image-lazy-load/image-lazy-load.module';
import { ControllersModule, DaiLyXeIonImageModule, DaiLyXeIonListImageModule } from 'src/app/components/controllers';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { NgPipesModule, NgDatePipesModule } from 'src/app/shared/pipes';
import { StepsModule } from 'src/app/components/controllers/primeNg/steps/steps';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ViewGalleryModule } from 'src/app/components/views';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CKEditorModule,
    CMSDangTin_RoutingModule,
    ImageLazyLoadModule,
    ControllersModule,
    NgPipesModule,
    ImageCropperModule,
    StepsModule,
    DaiLyXeIonImageModule,
    DaiLyXeIonListImageModule,
    NgDatePipesModule,
    ViewGalleryModule,
  ],
  declarations: [CMSDangTinListPage, CMSDangTinDetailPage],
  providers: [
    AdminRaoVatBLL_DangTin,
    AdminRaoVatBLL_Menu,
    AdminDaiLyXeBLL_InterfaceData,
    AdminRaoVatBLL_Commonfile,
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData
    
  ]
})
export class CMSDangTinPageModule { }
