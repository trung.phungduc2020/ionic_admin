import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSDangTinListPage } from './cms-dangtin-list/cms-dangtin-list.page';
import { CMSDangTinDetailPage } from './cms-dangtin-detail/cms-dangtin-detail.page';

const appRoutes: Routes = [
  {
    path: '',
    component: CMSDangTinListPage,
  }
  ,
  {
    path: ':id',
    component: CMSDangTinDetailPage,
  },
];

export const CMSDangTin_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);