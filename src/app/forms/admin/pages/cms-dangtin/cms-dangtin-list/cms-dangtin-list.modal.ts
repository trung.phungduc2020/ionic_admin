import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_DangTin } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { HttpParams } from "@angular/common/http";
import { commonParams, defaultWxh, commonMessages, commonAttr } from "src/app/core/variables";

@Injectable()
export class CMSDangTinListModal {
    constructor(
        public adminRaoVatBLL_DangTin: AdminRaoVatBLL_DangTin,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1";
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.displayItems, search.displayItems);
            params = params.set(commonParams.displayPage, search.displayPage);
            params = params.set(commonParams.tuKhoaTimKiem, search.tuKhoaTimKiem);
            params = params.set(commonParams.idThanhVien, search.idThanhVien);
            params = params.set(commonParams.idAdminDangKyDuyet, +search.idAdminApprove >= 0? search.idAdminApprove: -1);
            params = params.set(commonParams.trangThai, trangThai);
            params = params.set(commonParams.isDuyet, search.isDuyet);
            params = params.set(commonParams.orderString, "NgayCapNhat:desc");
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienTinRaoByListData(res.DataResult, defaultWxh.two);
                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }

        console.log('data', data)
        return data;
    }

    //dangKyDuyet
    public async registAdminApprove(id: any) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.registAdminApprove(id).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M067);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);

        }
        return false
    }

    //Hủy Đăng ký duyệt
    public async removeAdminApprove(id: any) {
        try {
            let res: ResponseBase = await this.adminRaoVatBLL_DangTin.removeAdminApprove(id).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(commonMessages.M081);
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageError(error);

        }
        return false
    }
}
