import { ResponseBase } from 'src/app/core/entity';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IonContent } from '@ionic/angular';
//providers
import { displayItemDefault, currentPageDefault, commonParams, commonAttr, Variables } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { AdminRaoVatBLL_DangTin } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSDangTinDetailPage } from '../cms-dangtin-detail/cms-dangtin-detail.page';
import { CMSDangTinListModal } from './cms-dangtin-list.modal';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-dangtin-list',
	templateUrl: './cms-dangtin-list.page.html',
	styleUrls: ['./cms-dangtin-list.page.scss'],
	providers: [CMSDangTinListModal],
	animations: [animationList]

})
export class CMSDangTinListPage implements OnInit, OnDestroy {
	//#region ================================= variable =====================
	public lbl = labelPages;
	public data: any;
	public search: any = {};
	public totalItems = 0;
	public countHeight = 0;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	//#endregion ============================== and variable =================
	//#region constructor
	constructor(
		public adminRaoVatBLL_DangTin: AdminRaoVatBLL_DangTin,
		public commonMethodsTs: CommonMethodsTs,
		public routerService: RouterExtService,
		public cmsDangTinListModal: CMSDangTinListModal) {
	}
	//#endregion
	ngOnInit() { }
	scrollToTop() {
		this.content.scrollToTop(500);
	}
	async ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSDangTinDetailPage.status) {
			CMSDangTinDetailPage.status = false;
			this.init();
		}
		
	}
	async logScrolling() {
		let res = await this.content.getScrollElement();
		this.countHeight = res.scrollTop;
	}
	async init() {
		await this.loadData();
	}
	async doRefresh(event) {
		this.loadData();
		event.target.complete();
	  }
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});		
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		if (params.tinhTrangDuyet!=null && params.tinhTrangDuyet!=undefined) {
			params[commonParams.isDuyet] = params.tinhTrangDuyet > 0;

		}
		let res: ResponseBase = await this.cmsDangTinListModal.getData(params);
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.data = this.data.concat(res.DataResult);
			this.totalItems = res.getTotalItems();
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	async searchData() {
		this.search.displayItems = displayItemDefault;
		this.search.displayPage = currentPageDefault;
		await this.loadData();
	}
	removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	async onSelect(item: any) {
		let id = item.Id;
		if (!(+item.IdAdminDangKyDuyet > 0)) {
			let res = await this.commonMethodsTs.alertConfirm("Bạn có muốn <strong>đăng ký duyệt</strong> tin này?", {labelNo: "Không", labelYes: "Có"});
			if (res) {
				let res = await this.cmsDangTinListModal.registAdminApprove(item.Id);
				if (res) {
					item.IdAdminDangKyDuyet = Variables.infoAdmin.Id;
					item.TenAdminDangKyDuyet = Variables.infoAdmin.HoTen;
				}
				
			}
		}
		this.commonMethodsTs.toPage_AdminDangTin(id);
	}
	async registAdminApprove(item, index, slidingItem) {
		slidingItem.close();
		let res = await this.cmsDangTinListModal.registAdminApprove(item.Id);
		if (res) {
			this.data[index].IdAdminDangKyDuyet = Variables.infoAdmin.Id;
			this.data[index].TenAdminDangKyDuyet = Variables.infoAdmin.HoTen;
		}
	}
	async removeAdminApprove(item, index, slidingItem) {
		slidingItem.close();
		let res = await this.cmsDangTinListModal.removeAdminApprove(item.Id);
		if (res) {
			this.data[index].IdAdminDangKyDuyet = null;
			this.data[index].TenAdminDangKyDuyet = '';
		}
	}
	ngOnDestroy() { }
}
