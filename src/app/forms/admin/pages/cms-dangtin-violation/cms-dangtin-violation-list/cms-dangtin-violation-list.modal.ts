import { commonAttr } from './../../../../../core/variables';
import { Injectable } from "@angular/core";
import { AdminRaoVatBLL_DangTinViolation } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { HttpParams } from "@angular/common/http";
import { commonParams, defaultWxh } from "src/app/core/variables";
@Injectable()
export class CMSDangTinViolationListModal {
    constructor(
        public adminRaoVatBLL_DangTinViolation: AdminRaoVatBLL_DangTinViolation,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params: any = {};
            params[commonParams.displayItems] = search.displayItems;
            params[commonParams.displayPage] = search.displayPage;
            params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
            params[commonParams.trangThai] = trangThai;
            params[commonParams.allIncludingString] = true;
            let res: ResponseBase = await this.adminRaoVatBLL_DangTinViolation.search(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult.forEach((element, index) => {
                    let idHinhDaiDien = this.commonMethodsTs.getData(element.DangTin_Ext, "IdFileDaiDien", -1);
                    let rewriteUrl = this.commonMethodsTs.getData(element.DangTin_Ext, "RewriteUrl", "hinh-dai-dien");
                    element.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(rewriteUrl, idHinhDaiDien, defaultWxh.two)
                });
                data = res;

            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return data;
    }
}
