import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams, listTrangThaiDangTinViolation } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { IonContent } from '@ionic/angular';
import { CMSDangTinViolationListModal } from './cms-dangtin-violation-list.modal';
import { CMSDangTinViolationDetailPage } from '../cms-dangtin-violation-detail/cms-dangtin-violation-detail.page';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-dangtin-violation-list',
	templateUrl: './cms-dangtin-violation-list.page.html',
	styleUrls: ['./cms-dangtin-violation-list.page.scss'],
	providers: [CMSDangTinViolationListModal],
	animations: [animationList]
})
export class CMSDangTinViolationListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	public currentPage = 1;
	public data: any;
	public search: any = {};
	public totalItems = -1;
	public lTrangThai = listTrangThaiDangTinViolation
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsDangTinViolationListModal: CMSDangTinViolationListModal,
		public routerService: RouterExtService
	) {
	}
	ngOnInit() {
	}
	ionViewWillEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSDangTinViolationDetailPage.status)
		{
			CMSDangTinViolationDetailPage.status = false;
			this.init();
		} 
		
	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});
		
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		params[commonParams.trangThai] = params.trangThaiViPham;
		let res: ResponseBase = await this.cmsDangTinViolationListModal.getData(params);
		res = new ResponseBase(res);
		
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.totalItems = res.getTotalItems();
			this.data = this.data.concat(res.DataResult);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}
	}
	public removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	
	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	  }
	public scrollToTop() {
		this.content.scrollToTop(500);
	}
	public onSelect(id) {
		this.commonMethodsTs.toPage_AdminDangTinViolation(id);
	}
	public goToLinkDangTin(id) {
		
		this.commonMethodsTs.toPage_HomeListTinDang(id);
	}
	//#endregion
}
