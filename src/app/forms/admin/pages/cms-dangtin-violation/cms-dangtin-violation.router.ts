import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSDangTinViolationListPage } from './cms-dangtin-violation-list/cms-dangtin-violation-list.page';
import { CMSDangTinViolationDetailPage } from './cms-dangtin-violation-detail/cms-dangtin-violation-detail.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSDangTinViolationListPage,
	},
	{
		path: ':id',
		component: CMSDangTinViolationDetailPage,
	}
];

export const CMSDangTinViolation_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);