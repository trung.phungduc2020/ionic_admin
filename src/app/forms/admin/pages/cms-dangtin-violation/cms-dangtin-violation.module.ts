import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_DangTinViolation } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CMSDangTinViolationDetailPage } from './cms-dangtin-violation-detail/cms-dangtin-violation-detail.page';
import { CMSDangTinViolationListPage } from './cms-dangtin-violation-list/cms-dangtin-violation-list.page';
import { CMSDangTinViolation_RoutingModule } from './cms-dangtin-violation.router';
import { NgDatePipesModule } from 'src/app/shared/pipes';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CMSDangTinViolation_RoutingModule,
        DaiLyXeIonNumberModule,
        ControllersModule,
        NgDatePipesModule
    ],
    declarations: [
        CMSDangTinViolationDetailPage,
        CMSDangTinViolationListPage,
    ],
    providers: [
        AdminRaoVatBLL_DangTinViolation,
        AdminRaoVatBLL_ThanhVien,
    ]
})
export class CMSDangTinViolationPageModule { }
