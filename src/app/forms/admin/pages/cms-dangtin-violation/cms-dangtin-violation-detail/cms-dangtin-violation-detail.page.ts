import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { commonParams, commonMessages, listTrangThaiDangTinViolation } from 'src/app/core/variables';
import { AdminRaoVatBLL_DangTinViolation } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'cms-dangtin-violation-detail',
	templateUrl: './cms-dangtin-violation-detail.page.html',
	styleUrls: ['./cms-dangtin-violation-detail.page.scss'],
})
export class CMSDangTinViolationDetailPage implements OnInit {
	//#region variables
	static status: any = false;
	public displayData: any = {};
	public lTrangThai: any = listTrangThaiDangTinViolation;
	public lbl: any = labelPages;
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminRaoVatBLL_DangTinViolation: AdminRaoVatBLL_DangTinViolation,
		public activatedRoute: ActivatedRoute
	) { }
	//#endregion
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSDangTinViolationDetailPage.status = false;
			this.displayData.Id = params[commonParams.id] || -1;
			this.loadDataViolation(this.displayData.Id);
		});
	}
	public async saveData() {
		this.checkValue();
		if (!this.checkValue()) {
			this.commonMethodsTs.createMessageError(commonMessages.M041);
			return;
		}
		try {
			let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn cập nhật vi phạm này không ?');
			if (resAlert) {
				let result = await this.adminRaoVatBLL_DangTinViolation.update(this.displayData).toPromise();
				result = new ResponseBase(result);
				CMSDangTinViolationDetailPage.status = true;
				if (result.isSuccess()) {
					this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage());
				}
			}
			else {
				return;

			}

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	public async xoaThongBao() {
		try {
			this.displayData.TrangThai = 3;
			let result = await this.adminRaoVatBLL_DangTinViolation.update(this.displayData).toPromise();
			result = new ResponseBase(result);
			CMSDangTinViolationDetailPage.status = true;
			if (result.isSuccess()) {
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);

				this.commonMethodsTs.toPage_AdminDangTinViolation();
			} else {
				this.commonMethodsTs.createMessageError(result.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	//#region private
	public async loadDataViolation(id) {
		try {
			if (id == -1) {
			} else {
				let paramsData = {
					Id: id,
					allIncludingString: true,
				}
				let result = await this.adminRaoVatBLL_DangTinViolation.search(paramsData).toPromise();
				result = new ResponseBase(result);
				if (result.isSuccess()) {
					this.displayData = result.DataResult[0];
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage())
				}
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}
	checkValue() {
		if (this.displayData.TrangThai == 1 && !this.displayData.NoiDungXuLy) {
			return false;
		}
		if (this.displayData.TrangThai == 2) {
			this.displayData.NoiDungXuLy = '';
		}
		return true;
	}
	public goToLinkDangTin(id) {
		this.commonMethodsTs.toPage_HomeListTinDang(id);
	}
	//#endregion
}
