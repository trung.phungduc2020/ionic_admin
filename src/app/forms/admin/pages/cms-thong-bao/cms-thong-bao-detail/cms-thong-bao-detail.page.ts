import { Component, OnInit } from '@angular/core';
//providers
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ThongBao } from 'src/app/core/entity';
import { commonParams, listTypeThongBao, listLinkNotify, listTrangThai, commonMessages } from 'src/app/core/variables';
import { CMSThongBaoDetailModal } from './cms-thong-bao-detail.modal';
import { FormBuilder, FormGroup } from '@angular/forms';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'cms-thong-bao-detail',
	templateUrl: './cms-thong-bao-detail.page.html',
	styleUrls: ['./cms-thong-bao-detail.page.scss'],
	providers: [CMSThongBaoDetailModal]
})
export class CMSThongBaoDetailPage implements OnInit {
	//#region variables
	static status = false;
	public lbl = labelPages;
	public data: ThongBao = new ThongBao();
	public myForm: FormGroup;
	public listDropdown = {
		lTypeThongBao: listTypeThongBao,
		lLinkNotify: listLinkNotify,
		lTrangThai: listTrangThai
	}
	// @ViewChild('comboxThanhVien', { static: false }) comboxThanhVien: any;
	//#endregion
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cMSThongBaoDetailModal: CMSThongBaoDetailModal,
		public fb: FormBuilder,
		public activatedRoute: ActivatedRoute

	) { }
	ngOnInit() { }
	public ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			CMSThongBaoDetailPage.status = false;
			let id = params[commonParams.id] || -1;
			this.loadData(id);
		});
	}
	public async loadData(id) {
		this.data = await this.cMSThongBaoDetailModal.getData(id);
		this.buildForm()
	}
	async buildForm() {
		if (this.data) {
			try {
				this.myForm.reset();
			} catch (error) {
			}
			let disabled = this.data.Id > 0;
			this.commonMethodsTs.ngZone.run(() => {
				let newDate = this.commonMethodsTs.getDateServe();
				let group: any = this.commonMethodsTs.createObjectForFormGroup(this.data);
				group.TieuDe = [{ value: this.data.TieuDe, disabled: disabled }];
				group.NoiDung = [{ value: this.data.NoiDung, disabled: disabled }];
				group.Type = [{ value: this.data.Type, disabled: disabled }];
				group.Link = [{ value: this.data.Link, disabled: disabled }];
				group.IdUserGroup = [this.data.IdUserGroup];
				group.IdThanhVien = [this.data.IdThanhVien];
				group.TrangThai = [this.data.TrangThai == 1];
				group.Data = [this.data.Data];
				group.DienThoai = [];
				group.LstThanhVien_Ext = [];
				group.GhiChu = [];
				group.TuNgay = [{ value: this.commonMethodsTs.toISOString(this.data.TuNgay, newDate), disabled: disabled }];
				group.DenNgay = [{ value: this.commonMethodsTs.toISOString(this.data.DenNgay, new Date(newDate.setMonth(newDate.getMonth() + 1))), disabled: disabled }];
				this.myForm = this.fb.group(group);
			})
		}
	}
	public async saveData() {
		// kiểm tra dữ liệu đầu vào
		if (!this.isCheckData()) {
			return;
		}
		let data:any = this.commonMethodsTs.cloneObject(this.data.Id > 0?  this.data : this.myForm.value);
		data.TrangThai = this.myForm.value.TrangThai? 1: 2;
		data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
		data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);

		let res = await this.cMSThongBaoDetailModal.save(data);
		if (res) {
			CMSThongBaoDetailPage.status = true;
			if (!(data.Id > 0)) {
				this.commonMethodsTs.toPage_AdminThongBao();
				return;
			}
			this.loadData(this.data.Id);

		}
	}
	public async xoaThongBao() {
		let data = this.commonMethodsTs.cloneObject(this.myForm.value);
		let res = await this.cMSThongBaoDetailModal.xoaThongBao(data.Id);
		if (res) {
			CMSThongBaoDetailPage.status = true;
			this.commonMethodsTs.toPage_AdminThongBao();
		}
	}

	itemSelectedThanhVien(tv) {
		if (tv) {
			this.myForm.controls.DienThoai.setValue(tv.DienThoai);

		}

	}
	itemSelectedUserGroup(usergroup) {
		if (usergroup && usergroup.LstThanhVien_Ext) {
			let lstThanhVien_Ext = [];
			let lstIdThanhVien_Ext = [];

			usergroup.LstThanhVien_Ext.forEach(element => {
				lstThanhVien_Ext.push(element.DataResult[0]);
				lstIdThanhVien_Ext.push(element.DataResult[0].Id)
			});
			this.myForm.controls.LstThanhVien_Ext.setValue(lstThanhVien_Ext);
			this.myForm.controls.GhiChu.setValue(lstIdThanhVien_Ext!.toString());

		}

	}
	isCheckData(): boolean {
		if (this.data.Id > 0 && this.myForm.value.TrangThai == (this.data.TrangThai == 1)) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M086);
			return;
		}
		if (this.commonMethodsTs.compareTwoDateByMinutes(this.myForm.value.TuNgay, this.myForm.value.DenNgay) == 1) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M072);
			return false;
		}

		if (this.myForm.invalid) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M018);
			return false;
		}
		return true;
	}
}
