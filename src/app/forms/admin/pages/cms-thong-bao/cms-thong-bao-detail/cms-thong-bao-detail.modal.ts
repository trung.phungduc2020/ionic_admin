import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase, ThongBao } from "src/app/core/entity";
import { commonMessages } from "src/app/core/variables";
import { AdminRaoVatBLL_ThongBao } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSThongBaoDetailModal {
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public adminRaoVatBLL_ThongBao: AdminRaoVatBLL_ThongBao,
	) {
	}
	//GetData
	public async getData(id) {
		let data: ThongBao = new ThongBao();
		if (!(id > 0)) {
			return data;
		}
		try {
			let paramsData = {
				ids: id,
				trangThai: "1,2"
			}
			let result = await this.adminRaoVatBLL_ThongBao.search(paramsData).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				
				data = result.DataResult[0] as ThongBao;
			} else {
				this.commonMethodsTs.createMessageError(result.getMessage())
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return data;
	}

	public async save(data) {
		try {
			data.TenThanhVien_Ext = undefined;
			if (!(data.Id > 0)) {
				let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc chắn muốn tạo thông báo này?');
				if (resAlert) {
					let result = await this.adminRaoVatBLL_ThongBao.insert(data).toPromise();
					result = new ResponseBase(result);
					if (result.isSuccess()) {
						// Broadcast signal
						this.commonMethodsTs.storeMessageSendNotification(data);
						this.commonMethodsTs.storeMessageSendNotificationFirestore(data);
						this.commonMethodsTs.createMessageSuccess(commonMessages.M003);
					} else {
						this.commonMethodsTs.createMessageError(result.getMessage());
					}
				}
				return resAlert;
			}
			else {
				let result = await this.adminRaoVatBLL_ThongBao.update(data).toPromise();
				result = new ResponseBase(result);
				if (result.isSuccess()) {
					this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
					return true;
				} else {
					this.commonMethodsTs.createMessageError(result.getMessage());
				}
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}
	public async xoaThongBao(id) {
		try {
			let result = await this.adminRaoVatBLL_ThongBao.updateTrangThai(id, "3").toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true;
			} else {
				this.commonMethodsTs.createMessageError(result.getMessage());
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;

	}
}
