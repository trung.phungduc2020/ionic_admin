import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSThongBaoDetailPage } from './cms-thong-bao-detail/cms-thong-bao-detail.page';
import { CMSThongBaoListPage } from './cms-thong-bao-list/cms-thong-bao-list.page';

const appRoutes: Routes = [
	{
		path: '',
		component: CMSThongBaoListPage,
	},
	{
		path: ':id',
		component: CMSThongBaoDetailPage,
	}
];

export const CMSThongBao_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);