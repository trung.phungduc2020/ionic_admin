import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { CMSThongBaoDetailPage } from './cms-thong-bao-detail/cms-thong-bao-detail.page';
import { CMSThongBaoListPage } from './cms-thong-bao-list/cms-thong-bao-list.page';
import { CMSThongBao_RoutingModule } from './cms-thong-bao.router';
import { AdminRaoVatBLL_ThanhVien, AdminRaoVatBLL_ThongBao } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';

@NgModule({
    imports: [
        CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
        CMSThongBao_RoutingModule,
		DaiLyXeIonNumberModule,
        ControllersModule,

    ],
    declarations: [
		CMSThongBaoDetailPage,
		CMSThongBaoListPage,
	],
    providers: [
        AdminRaoVatBLL_ThongBao,
        AdminRaoVatBLL_ThanhVien
        
    ]
})
export class CMSThongBaoPageModule { }
