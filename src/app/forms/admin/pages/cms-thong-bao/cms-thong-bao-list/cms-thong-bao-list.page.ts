import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams, listTypeThongBao, defaultWxh } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { IonContent } from '@ionic/angular';
import { CMSThongBaoListModal } from './cms-thong-bao-list.modal';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { CMSThongBaoDetailPage } from '../cms-thong-bao-detail/cms-thong-bao-detail.page';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
@Component({
	selector: 'cms-thong-bao-list',
	templateUrl: './cms-thong-bao-list.page.html',
	styleUrls: ['./cms-thong-bao-list.page.scss'],
	providers: [CMSThongBaoListModal],
	animations: [animationList]
})
export class CMSThongBaoListPage implements OnInit {
	//#region variables
	public lbl = labelPages;
	@ViewChild(IonContent, { static: true }) content: IonContent;
	@Input("options") options: any = {
		trangThai: true,
		typeThongBao: true,
		thanhVien: false,
	};
	public currentPage = 1;
	public data: any;
	public search: any = {};
	public totalItems = -1;
	public lTypeThongBao = listTypeThongBao;
	//#endregion
	//#region contructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThongBaoListModal: CMSThongBaoListModal,
		public routerService: RouterExtService
	) {
	}
	//#endregion
	ngOnInit() {
	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSThongBaoDetailPage.status)
		{
			CMSThongBaoDetailPage.status = false;
			this.init();
		}
	}
	async init() {
		await this.loadData();
	}
	public async loadData(currentPage = currentPageDefault) {
		let params = this.commonMethodsTs.cloneObject(this.search || {});
		if (params.typeThongBao == 2) {
			this.options = {
				trangThai: true,
				typeThongBao: true,
				thanhVien: true,
			}

			params[commonParams.idThanhVien] = params.idThanhVien;
		}
		if (params.typeThongBao != 2) {
			this.options = {
				trangThai: true,
				typeThongBao: true,
				thanhVien: false,
			}

			params[commonParams.idThanhVien] = '';
		}


		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		params[commonParams.type] = params.typeThongBao;
		let res: ResponseBase = await this.cmsThongBaoListModal.getData(params);
		res = new ResponseBase(res);
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			if (currentPage == currentPageDefault) {
				this.data = [];
			}
			this.totalItems = res.getTotalItems();
			let dataRes = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);
			this.data = this.data.concat(dataRes);
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
		}

	}
	public removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	public onSelect(id) {
		this.commonMethodsTs.toPage_AdminThongBao(id);
	}

	public async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	public scrollToTop() {
		this.content.scrollToTop(500);
	}
	//#endregion
}
