import { Injectable } from "@angular/core";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { ResponseBase } from "src/app/core/entity";
import { commonParams } from "src/app/core/variables";
import { AdminRaoVatBLL_ThongBao } from "src/app/core/api/admin/rao-vat/adminRaoVatBLL";
@Injectable()
export class CMSThongBaoListModal {
    constructor(
		public adminRaoVatBLL_ThongBao: AdminRaoVatBLL_ThongBao,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {
        // gọi api lấy dữ liệu tree men
        search = search || {};
        
        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params:any = {};
            params[commonParams.displayItems] = search.displayItems;
            params[commonParams.displayPage] = search.displayPage;
            params[commonParams.tuKhoaTimKiem] = search.tuKhoaTimKiem;
            params[commonParams.trangThai] = trangThai;  
            params[commonParams.type] = search.type;  
            params[commonParams.idThanhVien] = search.idThanhVien;           
            let res: ResponseBase = await this.adminRaoVatBLL_ThongBao.search(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                data = res;
            }
            else {
                throw res.getMessage();
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        
        return data;
    }
}
