import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { CMSThanhVien_RoutingModule } from './cms-thanhvien.router';
import { CMSThanhVienDetailPage } from './cms-thanhvien-detail/cms-thanhvien-detail.page';
import { CMSThanhVienListPage } from './cms-thanhvien-list/cms-thanhvien-list.page';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { DaiLyXeIonNumberModule, ControllersModule } from 'src/app/components/controllers';
import { ViewGalleryModule, ViewRankModule } from 'src/app/components/views';
import { NgDatePipesModule } from 'src/app/shared/pipes';
import { AdminRaoVatBLL_HistoryProfile, AdminRaoVatBLL_DaiLyXeData, AdminRaoVatBLL_ThanhVien } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ViewLichSuMuaPointModule } from 'src/app/components/views/view-lichsumuapoint/view-lichsumuapoint.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		ReactiveFormsModule,
		CMSThanhVien_RoutingModule,
		DaiLyXeIonNumberModule,
		ControllersModule,
		ViewGalleryModule,
		NgDatePipesModule,
		ViewRankModule,
		ViewLichSuMuaPointModule	],
	declarations: [
		CMSThanhVienListPage,
		CMSThanhVienDetailPage,
	],

	providers: [
		AdminDaiLyXeBLL_ThanhVien,
		AdminRaoVatBLL_DaiLyXeData,
		AdminRaoVatBLL_HistoryProfile,
		AdminRaoVatBLL_ThanhVien
	]
})
export class CMSThanhVienPageModule { }
