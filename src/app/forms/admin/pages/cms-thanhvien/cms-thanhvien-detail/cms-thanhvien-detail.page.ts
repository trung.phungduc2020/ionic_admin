import { Component } from '@angular/core';
import { CommonMethodsTs } from "src/app/core/common-methods";
import { commonParams, commonMessages} from 'src/app/core/variables';
//provides
import { CMSThanhVienDetailModal } from './cms-thanhvien-detail.modal';
import { labelPages } from 'src/app/core/labelPages';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ViewLichSuMuaPointComponent } from 'src/app/components/views/view-lichsumuapoint/view-lichsumuapoint.component';
import { AdminRaoVatBLL_UserRank, AdminRaoVatBLL_UserPoint } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
@Component({
	selector: 'cms-thanhvien-detail',
	templateUrl: './cms-thanhvien-detail.page.html',
	styleUrls: ['./cms-thanhvien-detail.page.scss'],
	providers:[AdminRaoVatBLL_UserRank, AdminRaoVatBLL_UserPoint, CMSThanhVienDetailModal]
})
export class CMSThanhVienDetailPage {
	//#region variables
	static status = false;
	public data: any;
	public lbl = labelPages;
	public dataRank:any = {};
	public userPoint:any= {};
	public lstRank = [];
	//#endregion
	//#region constructor
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThanhVienDetailModal: CMSThanhVienDetailModal,
		public activatedRoute: ActivatedRoute,
		public modalController: ModalController
	) {
	}
	//#endregion
	async ngOnInit() {
	}
	ionViewWillEnter() {
		this.activatedRoute.params.subscribe(async (params) => {
			let id = params[commonParams.id] || -1;
			if (+id <= 0) {
				this.commonMethodsTs.goBack();
				return;
			}
			await this.loadData(id);
		});
	}
	//#region private
	async loadData(id?: any) {
		try {
			this.lstRank = await this.cmsThanhVienDetailModal.getListRank();
			this.data = await this.cmsThanhVienDetailModal.getData(id);
			let lstRank = this.commonMethodsTs.cloneArray(this.lstRank);

			if (this.data.IdRank > 0) {
				this.dataRank = this.commonMethodsTs.searchItemById(lstRank, this.data.IdRank) 

			}
			else
			{
				this.dataRank = lstRank[0];
				this.data.IdRank = this.dataRank.Id;
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
	}

	async xacNhanCMND() {
		//Thêm đk xác định có hình ảnh CMND
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc đã kiểm tra CMND?');
		if (resAlert) {
			await this.cmsThanhVienDetailModal.updateDaXacThucCmnd(this.data.Id);
			await this.loadData(this.data.Id);
		}
	}

	async xacNhanPassport() {
		let resAlert = await this.commonMethodsTs.alertConfirm('Bạn có chắc đã kiểm tra passport?');
		if (resAlert) {
			await this.cmsThanhVienDetailModal.updateDaXacThucPassport(this.data.Id);
			await this.loadData(this.data.Id);
		}
	}

	async onLichSuMuaPoint() {
		const modal = await this.modalController.create({
			component: ViewLichSuMuaPointComponent,
			componentProps: { idThanhVien: this.data.Id }
		});
		modal.onDidDismiss().then((res: any) => {
		});
		await modal.present();
	}

	toPageListThanhVien() {
		let id = this.commonMethodsTs.getData(this.data, "Id", -1);
		id > 0 && localStorage.setItem('fromChiTiet', this.data.Id);
		this.commonMethodsTs.goBack();
	}

}
