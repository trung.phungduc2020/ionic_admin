import { Injectable } from '@angular/core';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_DaiLyXeData, AdminRaoVatBLL_HistoryProfile, AdminRaoVatBLL_UserRank, AdminRaoVatBLL_UserPoint, AdminRaoVatBLL_ThanhVien } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { ResponseBase } from 'src/app/core/entity';
import { commonMessages, Variables } from 'src/app/core/variables';

@Injectable()
export class CMSThanhVienDetailModal {
	constructor(
		public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien,
		public adminRaoVatBLL_DaiLyXeData: AdminRaoVatBLL_DaiLyXeData,
		public adminRaoVatBLL_HistoryProfile: AdminRaoVatBLL_HistoryProfile,
		public commonMethodsTs: CommonMethodsTs,
		public adminRaoVatBLL_UserRank: AdminRaoVatBLL_UserRank,
		public adminRaoVatBLL_UserPoint: AdminRaoVatBLL_UserPoint,
		public adminRaoVatBLL_ThanhVien: AdminRaoVatBLL_ThanhVien
	) {
	}
	public async getData(idThanhVien) {
		let data:any = {};
		let lapi = [this.adminDaiLyXeBLL_ThanhVien.getIndex(idThanhVien).toPromise(), this.adminRaoVatBLL_ThanhVien.getIndex(idThanhVien).toPromise(), this.adminRaoVatBLL_UserPoint.search(idThanhVien, true).toPromise()]
		// let res: ResponseBase = await this.adminDaiLyXeBLL_ThanhVien.getIndex(idThanhVien).toPromise();
		// let res: ResponseBase = await this.adminRaoVatBLL_ThanhVien.getIndex(idThanhVien).toPromise();
		let lres = await Promise.all(lapi);
		lres[0] = new ResponseBase(lres[0]);
		lres[1] = new ResponseBase(lres[1]);
		lres[2] = new ResponseBase(lres[2]);

		if (lres[0].isSuccess()) {

			data =  lres[0].getFirstData();
		}
		else {
			throw lres[0].getMessage();
		}
		if (lres[1].isSuccess()) {

			data.IdRank =  lres[1].getFirstData().IdRank;
		}
		else {
			throw lres[1].getMessage();
		}
		if (lres[2].isSuccess()) {

			data.UserPoint =  lres[2].getFirstData();
		}
		else {
			throw lres[2].getMessage();
		}
		return data;
	}

	async updateDaXacThucCmnd(idThanhVien) {
		try {
			let result = await this.adminRaoVatBLL_DaiLyXeData.updateDaXacThucCmnd(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				
				// Insert history
				var saveHistoryThanhVien = await this.getData(idThanhVien);
				this.adminRaoVatBLL_HistoryProfile.postHistoryProfile(saveHistoryThanhVien, { daXacThucCmnd: true, idThanhVien: idThanhVien }).subscribe();
                
				// Broadcast signal
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}

	async updateDaXacThucPassport(idThanhVien) {
		try {

			let result = await this.adminRaoVatBLL_DaiLyXeData.updateDaXacThucPassport(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {

				// Insert history
				var saveHistoryThanhVien = await this.getData(idThanhVien);
				this.adminRaoVatBLL_HistoryProfile.postHistoryProfile(saveHistoryThanhVien, { daXacThucPassport: true, idThanhVien: idThanhVien }).subscribe();
                
				this.commonMethodsTs.createMessageSuccess(commonMessages.M005);
				return true
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return false;
	}

	async getListRank() {
		
		if(!Variables.listUserRank || Variables.listUserRank.length == 0)
		{
			try {
				
				let usr = await this.adminRaoVatBLL_UserRank.searchUserRank().toPromise();
				usr = new ResponseBase(usr);
				if (usr.isSuccess()) {
					Variables.listUserRank = (usr.DataResult || []);
				}
			} catch (error) {
				this.commonMethodsTs.createMessageErrorByTryCatch(error);
			}
		}
		return Variables.listUserRank;
	}

	public async getInfoPoint(idThanhVien) { //chủ yếu là thông point và có thêm một ít thông tin thành viên
		try {
			let result: ResponseBase = await this.adminRaoVatBLL_UserPoint.search(idThanhVien, true).toPromise();
			result = new ResponseBase(result);
			if (result.isSuccess()) {
				return result.getFirstData();
			}
		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error);
		}
		return null;
	}
}	
