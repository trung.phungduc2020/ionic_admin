import { Component, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
// provider
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { displayItemDefault, currentPageDefault, commonParams } from 'src/app/core/variables';
import { ResponseBase, ItemRecord } from 'src/app/core/entity';
import { CMSThanhVienListModal } from './cms-thanhvien-list.modal';
import { RouterExtService } from 'src/app/shared/services/router-ext.service';
import { animationList } from 'src/app/shared/animations/animations';
import { labelPages } from 'src/app/core/labelPages';
import { CMSThanhVienDetailPage } from '../cms-thanhvien-detail/cms-thanhvien-detail.page';
@Component({
	selector: 'cms-thanhvien-list',
	templateUrl: './cms-thanhvien-list.page.html',
	styleUrls: ['./cms-thanhvien-list.page.scss'],
	providers: [CMSThanhVienListModal],
	animations: [animationList]
})
export class CMSThanhVienListPage implements OnInit {
	//#region ================================= variable =====================
	public lbl = labelPages;
	public data: any;
	public search: any = {
		orderString: 'HoTen'
	};
	public totalItems = 0;
	public countHeight = 0;
	public lstRank = [];
	public currentPage = 1;
	public optionsSearch = {
		"trangThai": true,
		"orderString": true,
		dataOrder:
			[
				new ItemRecord("Họ tên", "HoTen"),
				new ItemRecord("Ngày tham gia", "NgayTao")

			]
	}
	//#endregion ============================== and variable =================
	@ViewChild(IonContent, { static: true }) content: IonContent;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public cmsThanhVienListModal: CMSThanhVienListModal,
		public routerService: RouterExtService
	) {
	}
	async ngOnInit() {

	}
	ionViewDidEnter() {
		if (!(this.routerService.getIdPagePreviousUrlIsDetail() > 0) || CMSThanhVienDetailPage.status) {
			CMSThanhVienDetailPage.status = false;
			this.init();
		} 
	}
	
	async init() {
		let data = this.commonMethodsTs.cloneArray(this.data || []);
		this.data = null;
		await this.loadData();
		if(!this.data)
		{
			this.data = data;
		}
	}
	public async loadData(currentPage = currentPageDefault) {

		let params = this.commonMethodsTs.cloneObject(this.search || {});
		params[commonParams.displayItems] = displayItemDefault;
		params[commonParams.displayPage] = currentPage || currentPageDefault;
		let orderString;
		if(this.search.orderString) orderString = this.search.orderString;
		if(this.search.orderType) orderString = `${orderString}:${this.search.orderType}`;
		if(orderString)
		{
			params[commonParams.orderString] = orderString;

		}
		let res: ResponseBase = await this.cmsThanhVienListModal.getData(params);
		res = new ResponseBase(res);
		if (currentPage == currentPageDefault) {
			this.data = [];
		}
		if (res.isSuccess()) {
			this.currentPage = currentPage;
			this.data = this.data.concat(res.DataResult || []);
			this.totalItems = res.getTotalItems();
		}
		else {
			this.commonMethodsTs.createMessageError(res.getMessage());
			this.data = [];
			this.totalItems = 0;
		}

	}
	removeTuKhoaTimKiem() {
		this.search.tuKhoaTimKiem = '';
		this.loadData();
	}
	async doInfinite(infiniteScroll) {
		try {
			if (this.data.length > 0 && this.data.length < this.totalItems) {
				await this.loadData(this.currentPage + 1);
			}
		} catch (error) {
		}
		infiniteScroll.target.complete();
	}
	async logScrolling() {
		let res = await this.content.getScrollElement();
		this.countHeight = res.scrollTop;
	}
	scrollToTop() {
		this.content.scrollToTop(500);
	}
	selectThanhVien(id: any) {
		this.commonMethodsTs.toPage_AdminThanhVien(id);
	}
	async syncThanhVienByIds() {
		await this.cmsThanhVienListModal.syncThanhVienByIds();

	}
	//#endregion
}
