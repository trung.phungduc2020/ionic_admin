import { Injectable } from '@angular/core';
import { AdminDaiLyXeBLL_ThanhVien } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { AdminRaoVatBLL_ThanhVien } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { HttpParams } from '@angular/common/http';
import { commonParams, defaultWxh, commonAttr, commonMessages } from 'src/app/core/variables';
import { ResponseBase } from 'src/app/core/entity';
import { PluckPipe } from 'src/app/shared/pipes';

@Injectable()
export class CMSThanhVienListModal {
    constructor(
        public adminDaiLyXeBLL_ThanhVien: AdminDaiLyXeBLL_ThanhVien,
        public adminRaoVatBLL_ThanhVien: AdminRaoVatBLL_ThanhVien,
        public commonMethodsTs: CommonMethodsTs,
    ) {
    }
    //GetData
    public async getData(search: any) {

        // gọi api lấy dữ liệu tree men
        search = search || {};

        let data = null;
        try {
            let trangThai = +search.trangThai > 0 ? search.trangThai : "1,2";
            let params: HttpParams = new HttpParams();
            params = params.set(commonParams.displayItems, search.displayItems);
            params = params.set(commonParams.displayPage, search.displayPage);
            params = params.set(commonParams.tuKhoaTimKiem, search.tuKhoaTimKiem);
            params = params.set(commonParams.trangThai, trangThai);
            params = params.set(commonParams.orderString, search.orderString);

            let res: ResponseBase = await this.adminDaiLyXeBLL_ThanhVien.getByParams(params).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                res.DataResult = this.commonMethodsTs.formatUrlHinhDaiDienByListData(res.DataResult || [], defaultWxh.two);

                data = res;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }

        return data;
    }

    public async syncThanhVienByIds() {

        try {
            let lTV: ResponseBase = await this.getData({ trangThai: 1, displayItems: -1 });
            let ids = new PluckPipe().transform(lTV.DataResult, commonAttr.id);
            if (ids.length == 0) {
                this.commonMethodsTs.createMessageWarning(commonMessages.M006);
                return;
            }
            let res: ResponseBase = await this.adminRaoVatBLL_ThanhVien.syncThanhVienByIds(ids).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                if(res.IntResult > 0)
                {
                    this.commonMethodsTs.createMessageSuccess(`Đã đồng bộ dữ liệu của ${res.IntResult} thành viên`);

                }
                else
                {
                    this.commonMethodsTs.createMessageSuccess(`Dữ liệu đã được đồng bộ`);

                }
                
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }

        return false;
    }
}
