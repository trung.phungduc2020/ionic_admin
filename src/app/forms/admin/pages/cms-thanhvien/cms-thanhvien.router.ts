import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CMSThanhVienListPage } from './cms-thanhvien-list/cms-thanhvien-list.page';
import { CMSThanhVienDetailPage } from './cms-thanhvien-detail/cms-thanhvien-detail.page';

const appRoutes: Routes = [
    {
        path: '',
        component: CMSThanhVienListPage,
      },
      {
        path: ':id',
        component: CMSThanhVienDetailPage,
      }
];

export const CMSThanhVien_RoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);