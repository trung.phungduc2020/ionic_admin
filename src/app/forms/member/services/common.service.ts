import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable } from '@angular/core';
import { ThanhVienRaoVatBLL_InterfaceData, ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminDaiLyXeBLL_InterfaceData } from 'src/app/core/api/admin/dailyxe/adminDaiLyXeBLL';
import { CacheService } from 'ionic-cache';
import { commonMessages, dateFormat, commonParams, Variables, commonAttr } from 'src/app/core/variables';
import { AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
@Injectable({
    providedIn: 'root'
})
export class CommonService {
    //tronghuu95 20190717 Bổ sung cache cho datamater trong vòng 1 tiếng sẽ tự động mất cache và load dữ liệu mới => sau này các cache này sẽ có 1 lệnh xóa cache từ server mới xóa cache
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData,
        public adminDaiLyXeBLL_InterfaceData: AdminDaiLyXeBLL_InterfaceData,
        public cache: CacheService,
        private adminRaoVatBLL_InteractData: AdminRaoVatBLL_InteractData,
        private thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData
    ) {
    }
    //tronghuu95 20190717 Cache master tỉnh thành
    async getTinhThanh() {
        let params = {
            displayItems: "-1",
            trangThai: "1",
        }
        let cacheKey = "searchTinhThanhByParams" + JSON.stringify(params);
        let request = this.adminDaiLyXeBLL_InterfaceData.searchTinhThanhByParams(params);
        let result = await this.cache.loadFromObservable(cacheKey, request).toPromise();//tronghuu95 20190717 Cache tỉnh thành
        if (result.IntResult == 1) {
            return result.DataResult;
        }
        return [];
    }

    async getDongXeByIdThuongHieu(Id) {
        let params = {
            displayItems: "-1",
            IdThuongHieu: Id,
        }
        let result = await this.adminDaiLyXeBLL_InterfaceData.searchDongXeByParams(params).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult;
        }
        return [];
    }
    //tronghuu95 20190717 Cache master loại xe
    async getLoaiXe() {
        let params = {
            displayItems: "-1"
        }
        let cacheKey = "searchLoaiXeByParams" + JSON.stringify(params);
        let request = this.adminDaiLyXeBLL_InterfaceData.searchLoaiXeByParams(params);
        let result = await this.cache.loadFromObservable(cacheKey, request).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult;
        }
        return [];
    }
    async getFirstDinhNghiaSanPhamByIdDongXe(Id) {
        let params = {
            displayItems: "-1",
            IdDongXe: Id,
        }
        let result = await this.adminDaiLyXeBLL_InterfaceData.searchDinhNghiaSanPhamByParams(params).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult[0];
        }
        return [];
    }
    async getQuanHuyenById(Id) {
        let params = {
            displayItems: "-1",
            IdTinhThanh: Id
        }

        let result = await this.adminDaiLyXeBLL_InterfaceData.searchQuanHuyenByParams(params).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult;
        }
        return [];
    }
    //tronghuu95 20190717 Cache master màu sắc
    async getMauSac() {
        let params = {
            displayItems: "-1"
        }
        let cacheKey = "searchMauSacByParams" + JSON.stringify(params);
        let request = this.adminDaiLyXeBLL_InterfaceData.searchMauSacByParams(params);
        let result = await this.cache.loadFromObservable(cacheKey, request).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult;
        }
        return [];
    }
    //tronghuu95 20190717 Cache master menu
    async getMenu() {

        let param: any = {};
        param.displayItems = 100;
        param.displayPage = 0;
        param.trangThai = "1";
        param.capDo = "1";
        let request = this.thanhVienRaoVatBLL_InterfaceData.searchMenuByParams(param);
        let result = await request.toPromise();

        if (result.IntResult == 1) {
            return result.DataResult || [];
        }
        return [];
    }
    //tronghuu95 20190717 Cache master thương hiệu
    async getThuongHieu() {
        let params = {
            displayItems: "-1"
        }
        let cacheKey = "searchThuongHieu" + JSON.stringify(params);
        let request = this.adminDaiLyXeBLL_InterfaceData.searchThuongHieu(params.displayItems, null);
        let result = await this.cache.loadFromObservable(cacheKey, request).toPromise();
        if (result.IntResult == 1) {
            return result.DataResult || [];
        }
        return [];
    }
    
    async getCauHinhHeThong() {
        try {
            let params: any = {};
            let res = await this.thanhVienRaoVatBLL_InterfaceData.SearchCauHinhHeThong(params).toPromise();
            if (res.IntResult == 1) {
                return res.DataResult || [];
            }
            return [];
        }
        catch{
        }
    }

    // Đang sử dụng để SaveRating => xử lý notify riêng, tránh đụng vào Controller SaveRating
    async adminInsertAndPushNotification(interfafceParams) {
        // Insert To NotificationSystem
        await this.adminRaoVatBLL_InteractData.insertNotification(interfafceParams).toPromise();
    }
}
