import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable } from '@angular/core';
import { CacheService } from 'ionic-cache';
import { cacheData } from 'src/app/core/variables';
import * as firebase from 'firebase';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from './common.service';

interface CauHinhHeThong {
    Id: number,
    MaCauHinh: string,
    GiaTriCauHinh: string
}

@Injectable({
    providedIn: 'root'
})
export class CauHinhHeThongService {

    infoForm: FormGroup;

    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public cache: CacheService,
        private formBuilder: FormBuilder,
        private commonService: CommonService,
    ) {
        this.infoForm = this.formBuilder.group({
            'Id': [null, Validators.required],
            'MaCauHinh': [null, Validators.required],
            'GiaTriCauHinh': [null, Validators.required],
        });
    }

    async init() {
        return firebase.database().ref(cacheData.CauHinhHeThongFirebase);
    }

    async getDataFromCache() {
        return new Promise<any>(async (resolve) => {
            (await this.init()).on("value", snap => {
                return resolve(snapshotToArray(snap));
            });
        });
    }
    async getMaCauHinh(){
        var data = (await firebase.database().ref('/cauHinhHeThong').once('value')).val();
        return data|| {};
    }
    async getDataFromCacheByMaCauHinh(maCauHinh){
        var data = await this.getMaCauHinh();
        return data[maCauHinh].GiaTriCauHinh || '';
    }
    
    async add(cauHinhHeThong: CauHinhHeThong) {
        let newInfo = firebase.database().ref(cacheData.CauHinhHeThongFirebase).push();
        return newInfo.set(cauHinhHeThong);
    }

    async pushDataToCache() {
        try {
            var dataCache = await this.getDataFromCache();

            if (dataCache.length > 0) {
                return;
            }

            let res = await this.commonService.getCauHinhHeThong();
            if (res.length > 0) {
                await res.forEach(async elementServer => {
                    this.commonMethodsTs.angularFireDatabase.object('/cauHinhHeThong/' + elementServer.MaCauHinh).set({
                        GiaTriCauHinh: elementServer.GiaTriCauHinh
					}).then(() => {
					}).catch(() => {
                    })
                })
            }
        }
        catch{
        }
    }

    async syncDataFromServerToCache() {
        try {
            // remove first
            await this.removeAll();

            let res = await this.commonService.getCauHinhHeThong();
            if (res.length > 0) {
                await res.forEach(async elementServer => {
                    this.commonMethodsTs.angularFireDatabase.object('/cauHinhHeThong/' + elementServer.MaCauHinh).set({
                        GiaTriCauHinh: elementServer.GiaTriCauHinh
					}).then(() => {
					}).catch(() => {
                    })
                })
            }
        }
        catch{
        }
    }

    async removeAll() {
        await firebase.database().ref(cacheData.CauHinhHeThongFirebase).once('value')
            .then((res: any) => {
                res.ref.remove();
            });
    }
}

export const snapshotToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
        let item = childSnapshot.val();
        item.id = childSnapshot.key;
        returnArr.push(item);
    });

    return returnArr;
};