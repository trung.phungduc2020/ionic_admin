import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuardAdmin } from './guard-admin';
import { GuardLoginAdmin } from './guard-login-admin';
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'cms-dashboard',
    pathMatch: 'full',
    canActivate: [GuardAdmin]

  },
  {
    path: 'cms-dashboard',
    loadChildren: '../../admin/pages/cms-dashboard/cms-dashboard.module#CMSDashboardPageModule',
    canActivate: [GuardAdmin]

  },
  {
    path: 'cms-dangtin',
    loadChildren: '../../admin/pages/cms-dangtin/cms-dangtin.module#CMSDangTinPageModule',
    canActivate: [GuardAdmin]
  },
  {
    path: 'cms-menu',
    loadChildren: '../../admin/pages/cms-menu/cms-menu.module#CMSMenuPageModule',
    canActivate: [GuardAdmin],

  },
  {
    path: 'cms-thanhvien',
    loadChildren: '../../admin/pages/cms-thanhvien/cms-thanhvien.module#CMSThanhVienPageModule',
    canActivate: [GuardAdmin],

  },
  {
    path: 'cms-userpoint',
    loadChildren: '../../admin/pages/cms-userpoint/cms-userpoint.module#CMSUserPointPageModule',
    canActivate: [GuardAdmin],

  },
  {
    path: 'cms-group-thanhvien',
    loadChildren: '../../admin/pages/cms-group-thanhvien/cms-group-thanhvien.module#CMSGroupThanhVienPageModule',
    canActivate: [GuardAdmin],

  },
  {
    path: 'cms-settings',
    loadChildren: '../../admin/pages/cms-settings/cms-settings.module#CMSSettingsPageModule',
    canActivate: [GuardAdmin]
  },

  {
    path: 'cms-command-linux',
    loadChildren: '../../admin/pages/cms-command-linux/cms-command-linux.module#CMSCommandLunixPageModule',
    canActivate: [GuardAdmin]
  },

  {
    path: 'cms-ranking',
    loadChildren: '../../admin/pages/cms-ranking/cms-ranking.module#CMSRankingPageModule',
    canActivate: [GuardAdmin]
  },
  {
    path: 'cms-thong-tin-ca-nhan',
    loadChildren: '../../admin/pages/cms-thong-tin-ca-nhan/cms-thong-tin-ca-nhan.module#CMSThongTinCaNhanPageModule',
    canActivate: [GuardAdmin],

  },
  {
    path: 'cms-dang-tin-rating',
    loadChildren: '../../admin/pages/cms-dangtin-rating/cms-dangtin-rating.module#CMSDangTinRatingPageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-login',
    loadChildren: '../../admin/pages/cms-login/cms-login.module#CMSLoginPageModule',
    canActivate: [GuardLoginAdmin]
  },
  {
    path: 'cms-thong-bao',
    loadChildren: '../../admin/pages/cms-thong-bao/cms-thong-bao.module#CMSThongBaoPageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-thong-bao-template',
    loadChildren: '../../admin/pages/cms-thong-bao-template/cms-thong-bao-template.module#CMSThongBaoTemplatePageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-dang-tin-violation',
    loadChildren: '../../admin/pages/cms-dangtin-violation/cms-dangtin-violation.module#CMSDangTinViolationPageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-user-blacklist',
    loadChildren: '../../admin/pages/cms-blacklist/cms-blacklist.module#CMSBlackListPageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-thong-ke',
    loadChildren: '../../admin/pages/cms-thong-ke/cms-thong-ke.module#CMSThongKePageModule',
    canActivate: [GuardAdmin]

  },
  {
    path: 'cms-install-statistics',
    loadChildren: '../../admin/pages/cms-install-statistics/cms-install-statistics.module#CMSInstallStatisticsPageModule',
    canActivate: [GuardAdmin],
  }
  ,
  {
    path: 'cms-layout-album',
    loadChildren: '../../admin/pages/cms-layout-album/cms-layout-album.module#CMSLayoutAlbumPageModule',
    canActivate: [GuardAdmin],
  }
  ,
  {
    path: 'cms-uu-dai',
    loadChildren: '../../admin/pages/cms-uu-dai/cms-uu-dai.module#CMSUuDaiPageModule',
    canActivate: [GuardAdmin],
  },
  {
    path: 'cms-attackmode',
    loadChildren: '../../admin/pages/cms-attackmode/cms-attackmode.module#CMSAttackModePageModule',
    canActivate: [GuardAdmin],
  }
  ,
  {
    path: 'cms-userdevice',
    loadChildren: '../../admin/pages/cms-userdevice/cms-userdevice.module#CMSUserDevicePageModule',
    canActivate: [GuardAdmin],
  }
  ,

];

export const AdminRoutingModule: ModuleWithProviders = RouterModule.forChild(appRoutes);