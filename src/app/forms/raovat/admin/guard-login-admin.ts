import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { commonParams, Variables } from 'src/app/core/variables';

//tronghuu95 20181017 dùng để chặn và tránh lỗi vòng lặp login/home khi không có token
@Injectable()
export class GuardLoginAdmin implements CanActivate {
    constructor(public commonMethodsTs: CommonMethodsTs) {}

    canActivate() {
        // let ct = Variables.token;
        // check if user can access route.
        if (!Variables.tokenAdmin) {
          // user can access route
          return true;
        }
        this.commonMethodsTs.toPage_AdminDashboard();
        return false;
    }
}
