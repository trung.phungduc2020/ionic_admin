import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AdminRoutingModule } from './admin.router';
import { GuardAdmin } from './guard-admin';
import { GuardLoginAdmin } from './guard-login-admin';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminRoutingModule,
  ],
  declarations: [],
  providers: [
    GuardAdmin,
    GuardLoginAdmin
  ]
})
export class AdminPageModule { }
