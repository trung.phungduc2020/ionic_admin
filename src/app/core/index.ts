import { NgModule } from "@angular/core";
import { CoreTemplate } from "./core-template";
import { CommonMethodsTs } from "./common-methods";
import { CoreProcess } from "./core-process";
import { CoreHttp } from "./core-http";
import { HttpServiceModule } from "./http-interceptor/http.service";
import { AdminRaoVatBLLModule } from "./api/admin/rao-vat";
import { AdminDaiLyXeBLLModule } from "./api/admin/dailyxe";
import { ThanhVienDaiLyXeBLLModule } from "./api/thanh-vien/dailyxe";
import { ThanhVienRaoVatBLLModule } from "./api/thanh-vien/rao-vat";
@NgModule({
  imports: [
    AdminDaiLyXeBLLModule,
    AdminRaoVatBLLModule,
    ThanhVienDaiLyXeBLLModule,
    ThanhVienRaoVatBLLModule,
    HttpServiceModule,
  ],
  providers: [
    CoreHttp,
    CoreProcess,
    CoreTemplate,
    CommonMethodsTs,
  ]
})
export class CoreModule { }
export { AdminRaoVatBLLModule } from "./api/admin/rao-vat";
export { AdminDaiLyXeBLLModule } from "./api/admin/dailyxe";
export { ThanhVienDaiLyXeBLLModule } from "./api/thanh-vien/dailyxe";
export { ThanhVienRaoVatBLLModule } from "./api/thanh-vien/rao-vat";
