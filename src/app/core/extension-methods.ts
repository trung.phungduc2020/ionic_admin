


//#region ================================ String =============================
interface String {
    format(...args: string[]): string;
    isUrl(): boolean;
    isEmail(): boolean;
    isEmpty(): boolean;
    convertrUrlPrefix(): string;
}

String.prototype.format = function (...args: string[]): string {
    var str = this;
    return str.replace(/{(\d+)}/g, function (match, number) {
        return (typeof args[number] != 'undefined') ? args[number] : match;
    });
};

String.prototype.isUrl = function (): boolean {
    var str = this;
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if (!regex.test(str)) {
        return false;
    } else {
        return true;
    }
};
String.prototype.isEmail = function (): boolean {
    var str = this;
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(str).toLowerCase());
};

String.prototype.isEmpty = function (): boolean {
    var str = this;
    return str == null || str == undefined || str == "";
};

String.prototype.convertrUrlPrefix = function (): string {
    var str = this;
    try {
        str = str
            .toLowerCase()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, '')
            .replace(/đ/g, 'd').replace(/Đ/g, 'D')
            .replace(/ +/g, "-");
    }
    catch{
        return '';
    }

    return str;
};

//#endregion ============================= End string ==============================

