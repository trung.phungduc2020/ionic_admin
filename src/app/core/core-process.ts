import { CoreHttp } from './core-http';
import { Injectable } from '@angular/core';
import { apiHost, commonParams, apiHost_Sufix_DB, Variables, apiDaiLyXe, apiDaiLyXe_Sufix_DB, commonMessages, nameDBImageFireStore, localStorageData, nameShortEnviroment, commonAttr } from "./variables";
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { CommonMethodsTs } from './common-methods';
import { optionsHttp, ResponseBase, Files } from './entity';
import { TrurthifyPipe, PluckPipe } from '../shared/pipes';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { checkAvailability } from '@ionic-native/core';
import { ThanhVienRaoVatBLL_InteractData, ThanhVienRaoVatBLL_InterfaceData } from './api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { CoreVariables } from './core-variables';
import { labelPages } from './labelPages';
import { RaoLocalStore } from './rao-localstore';
import { AdminRaoVatBLL_Commonfile } from './api/admin/rao-vat/adminRaoVatBLL';
declare var require: any;

const crypto: any = require("crypto-js");
@Injectable()
//Mục đích giành cho những api chạy độc lập như login và logout
export class CoreProcess {
    private imageCollection: AngularFirestoreCollection<any>;

    constructor(
        public coreHttp: CoreHttp,
        public commonMethodsTs: CommonMethodsTs,
        public angularFireStorage: AngularFireStorage,
        public database: AngularFirestore,
        public raoLocalStore: RaoLocalStore,
        public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
        public adminRaoVatBLL_Commonfile: AdminRaoVatBLL_Commonfile,
        public thanhVienRaoVatBLL_InterfaceData: ThanhVienRaoVatBLL_InterfaceData
    ) {
        this.imageCollection = database.collection<any>('Images');
    }
    public buildFullDBApiHostForCheckConfig(apiHost: string, controllerName: string, ) {
        return apiHost + apiHost_Sufix_DB + controllerName; //crm/
    }

    public buildFullDBApiHost(controllerName: string, addApiHost_Sufix_DB: boolean = true) {
        return apiHost + (addApiHost_Sufix_DB ? apiHost_Sufix_DB : "/") + controllerName; //crm/
    }
    public buildFullDBApiDaiLyXe(controllerName: string) {

        return apiDaiLyXe + apiDaiLyXe_Sufix_DB + controllerName;
    }
    //#region ============================================ Image ==================================================
    private upLoadFile(dataURI: string, uploadFile_FileName?: string): Observable<any> {
        let fullUrl = this.buildFullDBApiDaiLyXe('UploadFile/Stream');
        let options: any = new optionsHttp();
        let headers = new HttpHeaders();
        let token = Variables.isViewAdmin ? Variables.tokenAdmin : Variables.tokenThanhVien;
        if (token) {
            headers = headers.set("Authorization", "bearer " + token);

        }
        let params = new HttpParams();
        let f: File = this.commonMethodsTs.createFileByDataURI(dataURI, uploadFile_FileName || Variables.defaultFileName);
        params = params.set(commonParams.uploadFile_FileName, uploadFile_FileName || Variables.defaultFileName);
        /*params = params.set(commonParams.uploadFile_FolderId, uploadFile_FolderId || "-1");
         params = params.set(commonParams.uploadFile_FileUpdateId, uploadFile_FileUpdateId!); //rao vật không dùng */
        params = params.set(commonParams.isFromRaoVat, "true");
        options.params = params;
        options.headers = headers;
        try {
            return this.coreHttp.post(fullUrl, f, options);

        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);

        }
        return null;

    }
    //insert
    async insertByUrl(url: string, fileName: string, idDef: any = -1) {
        fileName = fileName.convertrUrlPrefix();
        try {
            let id = this.commonMethodsTs.getIdByUrlDailyXe(url);
            if (id > 0) {
                return id;
            }
            else {
                if (!Variables.isUploadForFireStore) {
                    return await this.insertByUrl_Server(url, fileName, idDef)
                }
                else {
                    return await this.insertByUrl_FireStore(url, fileName, idDef)
                }
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return idDef;
    }
    /*
        Dùng để insert hình ảnh chỉ qua server
        url: là đường dẫn hình ảnh
        fileName: là đường dẫn hình ảnh

     */
    private async insertByUrl_Server(url: string, fileName: string, idDef: any = -1) {
        try {
            let res: ResponseBase = await this.upLoadFile(url, fileName).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return res.getFirstDataId();
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return idDef;
    }
    /*
       Dùng để insert hình ảnh lên server sau đó là firestore
       url: là đường dẫn hình ảnh
       fileName: là đường dẫn hình ảnh

    */
    private async insertByUrl_FireStore(url: string, fileName: string, idDef: any = -1) {
        try {
            let f = new Files();
            f.Id = 0;
            f.Name = fileName.convertrUrlPrefix();
            /*tronghuu95 20190830162100 lý do là sẳn dùng để backup dữ liệu luôn */
            let id = (await this.insertListImageToServerQuickByFiles([f]))[0];
            if (+id > 0) {
                let _fileNameWithId = fileName + "_i" + id;
                url = await this.commonMethodsTs.convertImageResizer(url);
                await this.uploadFileForFireStore(url, _fileNameWithId).snapshotChanges().toPromise();
                let filePath = await this.getDownloadURLForFireStore(_fileNameWithId).toPromise();
                f.Id = id;
                f.ImageUrl = filePath;
                await this.updateListImageUrl([f]);
                return id;
            }

        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return idDef;
    }

    public async insertByUrl_FireStoreForFile(f: Files) {
        try {
            let nameFile: any = f.Name + "_i" + f.Id;
            let imageUrl = await this.commonMethodsTs.convertImageResizer(f.ImageUrl);
            await this.uploadFileForFireStore(imageUrl, nameFile).snapshotChanges().toPromise();
            let filePath = await this.getDownloadURLForFireStore(nameFile).toPromise();
            f.ImageUrl = filePath;
            await this.updateListImageUrl([f]);
            return f;
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return null;
    }

    public insertByUrl_FireStoreForFileWithTask(f: Files) {
        try {
            let nameFile: string = f.Name + "_i" + f.Id;
            nameFile = nameFile.convertrUrlPrefix();
            let task = this.uploadFileForFireStore(f.ImageUrl, nameFile);
            task["finish"] = new Observable((observer) => {
                try {
                    task.snapshotChanges().toPromise().then(async res => {
                        let filePath = await this.getDownloadURLForFireStore(nameFile).toPromise();
                        f.ImageUrl = filePath;
                        await this.updateListImageUrl([f]);
                        observer.next(true);
                        observer.complete();

                    });
                } catch (error) {
                    observer.next(false);
                    observer.complete();
                }

            });
            return task;
        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);
        }
        return null;
    }

    //clear cache ảnh
    async clearCacheImage(id: any) {
    }
    //list là dạng array hoặc json array
    async insertByArray(list: any, fileName: string) {
        fileName = fileName.convertrUrlPrefix();
        let listHinhAnh = [];
        try {
            if (!list) return [];
            if (!(list instanceof Array)) {
                if (list.startsWith("[")) {
                    list = JSON.parse(list);
                }
                else {
                    list = list.split(",");
                }
            }
            list = new TrurthifyPipe().transform(list);
            let idHinh: any;
            let promiseAll = [];/*Mãng chứa api sẽ lưu*/

            let postionPromiseImage = [];/*Mãng chứa vị trí image sẽ được lưu*/


            let ids = [];
            if (!Variables.isUploadForFireStore) {
                for (let i = 0; i < list.length; i++) {
                    let element: any = list[i];
                    let id = this.commonMethodsTs.getIdByUrlDailyXe(element);
                    if (!(id > 0)) {
                        promiseAll.push(this.insertByUrl(element, fileName));
                        postionPromiseImage.push(i);

                    }
                    listHinhAnh.push(this.commonMethodsTs.getIdByUrlDailyXe(element) || -1)

                }
                ids = await Promise.all(promiseAll);

            }
            else {
                let lFiles: Files[] = []
                let f: any = {};
                let promiseUploadFireBase = [];/*Mãng chứa api sẽ lưu*/
                let lItem = [];/*Mãng chứa api sẽ lưu*/

                for (let i = 0; i < list.length; i++) {
                    let element: any = list[i];
                    let id = this.commonMethodsTs.getIdByUrlDailyXe(element);
                    if (!(id > 0)) {
                        lItem.push(element);
                        f.Id = 0;
                        f.Name = fileName;
                        lFiles.push(f)
                        postionPromiseImage.push(i);
                    }
                    listHinhAnh.push(this.commonMethodsTs.getIdByUrlDailyXe(element) || -1)

                }
                if (lFiles && lFiles.length > 0) {
                    ids = await this.insertListImageToServerQuickByFiles(lFiles);
                }

                for (let index = 0; index < lItem.length; index++) {
                    let element = lItem[index];
                    element = await this.commonMethodsTs.convertImageResizer(element);
                    await this.uploadFileForFireStore(element, fileName, index).snapshotChanges().toPromise();
                    promiseAll.push(this.getDownloadURLForFireStore(fileName, index).toPromise());
                }
                // let lUploadFireBase= await Promise.all(promiseUploadFireBase);
                let listpath = await Promise.all(promiseAll);
                lFiles = [];
                for (let index = 0; index < listpath.length; index++) {
                    let element = listpath[index];
                    let item: any = {}
                    item.Id = ids[index];
                    item.ImageUrl = element;
                    lFiles.push(item);
                }

                await this.updateListImageUrl(lFiles);

            }
            ids.forEach((element, index) => {
                listHinhAnh[postionPromiseImage[index]] = element;
            });

        } catch (error) {
        }
        return listHinhAnh;
    }
    /*tronghuu95 20190829112700 dùng để lưu nhanh list image
        numImage: số lượng image được lưu
        fileName: tên file hình ảnh
    */
    async insertListImageToServerQuickByFiles(data: Files[]) {
        let fullUrl = this.buildFullDBApiDaiLyXe('Files/Insertlist');
        let body: any = {};
        body[commonParams.isFromRaoVat] = 1;
        body[commonParams.data] = data;
        let headers = new HttpHeaders();
        let params = new HttpParams();
        let options: any = new optionsHttp();
        let token = Variables.isViewAdmin ? Variables.tokenAdmin : Variables.tokenThanhVien;
        if (token) {
            headers = headers.set("Authorization", "bearer " + token);

        } options.params = params;
        options.headers = headers;
        try {
            let res: ResponseBase = await this.coreHttp.post(fullUrl, body, options).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return new PluckPipe().transform(res.DataResult, "Id");
            }
            else {

                this.commonMethodsTs.createMessageError(res.getMessage());
            }

        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);

        }

        return []
    }

    async insertListImageToServerQuick(data: string[], nameFile: string = "New file") {
        let listImageUploads: any[] = [];
        let lIndexFiles: any[] = [];
        let ids: any[] = [];
        // phân tách 2 dạng 
        data.forEach((element, index) => {
            let idFile = this.commonMethodsTs.getIdByUrlDailyXe(element);
            if (!(idFile > 0)) {
                let f = new Files();
                f.Id = 0;
                f.Name = nameFile.convertrUrlPrefix();
                listImageUploads.push(f);
                lIndexFiles.push(index);
            }

            ids.push(idFile);
        });
        let fileUploads = [];
        if (listImageUploads && listImageUploads.length > 0) {
            let idUploads = await this.insertListImageToServerQuickByFiles(listImageUploads);
            if (idUploads.length == lIndexFiles.length) {
                lIndexFiles.forEach((i, index) => {
                    ids[i] = idUploads[index];
                    let f = new Files();
                    f.Id = idUploads[index];
                    f.ImageUrl = data[i];
                    f.Name = listImageUploads[index].Name
                    fileUploads.push(f)
                });

            }
            else {
                return null;
            }

        }

        return { ids: ids, fileUploads: fileUploads }; //ids là ds id của files và filesUploads là ds id của file được upload
    }


    /*tronghuu95 20190829112700 dùng cập nhật hình ảnh lên server sau khi đã upload lên firebase
            numImage: số lượng image được lưu
            fileName: tên file hình ảnh
        */
    async updateListImageUrl(data: Files[]) {
        try {
            let fullUrl = this.buildFullDBApiDaiLyXe('Files/UpdateListImageUrl');
            let body: any = {};
            body[commonParams.isFromRaoVat] = 1;
            body[commonParams.data] = data;
            let headers = new HttpHeaders();
            let params = new HttpParams();
            let options: any = new optionsHttp();
            let token = Variables.isViewAdmin ? Variables.tokenAdmin : Variables.tokenThanhVien;
            if (token) {
                headers = headers.set("Authorization", "bearer " + token);

            } options.params = params;
            options.headers = headers;
            let res: ResponseBase = await this.coreHttp.put(fullUrl, body, options).toPromise();
            res = new ResponseBase(res);
            if (res.isSuccess()) {
                return true;
            }
            else {
                this.commonMethodsTs.createMessageError(res.getMessage());
            }

        } catch (error) {
            this.commonMethodsTs.createMessageErrorByTryCatch(error);

        }
        return false;


    }
    /*tronghuu95 20190829112700 dùng upload hình ảnh lên server
        dataURI: Uri hình ảnh
        fileName: Nên dùng id làm hình ảnh
    */
    uploadFileForFireStore(dataURI: string, fileName: string, index = 1) {
        /*tronghuu95 idFolder dùng để xử lý lưu trên server */
        // let file: File = this.commonMethodsTs.createFileByDataURI(dataURI, fileName);
        let _fileName = this.commonMethodsTs.buildFileNameForFile(fileName, index);
        let imgBlob = this.commonMethodsTs.dataURItoBlob(dataURI);
        // Upload Task 
        let task: AngularFireUploadTask;
        // The angularFireStorage path
        const path = `${nameDBImageFireStore}/${_fileName}`;
        // Totally optional metadata
        // const customMetadata = { app: 'DaiLyXe Image Upload' };
        // The main task
        let fileRef = this.angularFireStorage.ref(path);
        task = fileRef.put(imgBlob);
        // Get file progress percentage
        return task;

    }
    /*tronghuu95 20190829112700 tải hình ảnh từ firestore về 
        fileName: Cái này là file lưu hình ảnh trên firestore
    */
    getDownloadURLForFireStore(fileName: string, index = 1) {
        let _fileName = this.commonMethodsTs.buildFileNameForFile(fileName, index);
        // The angularFireStorage path
        const path = `${nameDBImageFireStore}/${_fileName}`;
        //File reference
        const fileRef = this.angularFireStorage.ref(path);
        return fileRef.getDownloadURL();
    }

    addImagetoDB(image: any) {
        //Create an ID for document
        const id = this.database.createId();

        //Set document id with value in database
        this.imageCollection.doc(id).set(image).then(resp => {
        }).catch(error => {
            console.log("error " + error);
        });
    }
    //#endregion ========================================= End Image =========================================
    //#region ============================================ Config settings ========================================
    private checkRemoteConfig(domain, evn, ngayCapNhat) {
        let body = {};
        body[commonParams.id] = evn;
        body[commonParams.ngayCapNhat] = ngayCapNhat;
        let fullUrl = this.buildFullDBApiHostForCheckConfig(domain, "Commonfile/CheckRemoteConfig");
        let options: any = new optionsHttp();
        return this.coreHttp.post(fullUrl, body, options);


    }
    private async getRemoteConfigServer(nameEnviroment: string, ngayCapNhat: string) {
        //dùng vòng lặp 
        let listDomainCheckConfig = Variables.listDomainCheckConfig;
        for (let index = 0; index < listDomainCheckConfig.length; index++) {
            const element = listDomainCheckConfig[index];
            try {

                let response = await this.checkRemoteConfig(element, nameEnviroment, ngayCapNhat).toPromise();
                if (response) {
                    if (response == "uptodate") {
                        return null;
                    }
                    var uk: any = this.commonMethodsTs.decodeRSA(response[commonParams.cuk]);
                    var at: any = this.commonMethodsTs.decodeDES(response[commonParams.cat], uk);
                    let jsonToken = JSON.parse(at);
                    return jsonToken;
                }


            } catch (error) {
            }
        }
        return null;
    }
    /**
     * getRemoteConfig: lấy cấu hình remote theo các giá trị môi trường và ngày cập nhật
     * @param nameEnviroment : tên rút gọn của biến môi trường
     */
    public async getRemoteConfig(nameEnviroment: string) {
        let ngayCapNhat: any;
        //B1 Lấy dữ liệu local theo biến môi trường lên
        let dataConfig = this.raoLocalStore.getLocalStoreConfig(nameEnviroment);
        //B2 Kt dữ liệu nêu có thì gán ngày cập nhật bằng dataConfig.ngayCapNhat
        if (dataConfig) {
            ngayCapNhat = dataConfig.ngayCapNhat;
        }
        //B3 Gọi lên serve để kt và lấy giá trị config trên serve
        let dataConfigServer = await this.getRemoteConfigServer(nameEnviroment, ngayCapNhat);
        if (dataConfigServer) {
            // Lưu data lại local
            this.raoLocalStore.setLocalStoreConfig(dataConfigServer, nameEnviroment);
            //get lên
            dataConfig = this.raoLocalStore.getLocalStoreConfig(nameEnviroment);
        }
        //B4: return dataConfig
        return dataConfig;
    }
    public async getRemoteCurrentConfig() {
        let ngayCapNhat: any;
        let dataConfig = this.raoLocalStore.getLocalStoreConfig(nameShortEnviroment.current);
        let evn = Variables.isDev ? nameShortEnviroment.development : nameShortEnviroment.productionlive;
        //-logic 1: nếu current ko có thì await checkremote rl
        if (!dataConfig) {
            let dataConfigServer = await this.getRemoteConfigServer(evn, ngayCapNhat);
            if (dataConfigServer) {
                // Lưu data lại local
                this.raoLocalStore.setLocalStoreConfig(dataConfigServer, evn);
                //get lên
                dataConfig = this.raoLocalStore.getLocalStoreConfig(evn);
            }
        }
        else {
            // -logic 2: nếu current == rl thì checkremote nhưng ko cần await
            if (dataConfig.enviroment == nameShortEnviroment.productionlive) {
                this.getRemoteConfigServer(evn, ngayCapNhat).then(dataConfigServer => {
                    if (dataConfigServer) {
                        // Lưu data lại local
                        this.raoLocalStore.setLocalStoreConfig(dataConfigServer, evn);
                        //get lên
                        dataConfig = this.raoLocalStore.getLocalStoreConfig(evn);
                    }
                }
                );
            }


        }

        //B4: return dataConfig
        return dataConfig;
    }

    public setRemoteCurrentConfig(config: any) {
        try {
            this.raoLocalStore.setLocalStoreConfig(config, nameShortEnviroment.current);
            let dataConfig = this.raoLocalStore.getLocalStoreConfig(nameShortEnviroment.current);
            if (dataConfig) {
                let connectionStrings = dataConfig.connectionStrings;
                if (connectionStrings) {
                    connectionStrings = JSON.parse(connectionStrings);
                    this.commonMethodsTs.updateVariables(connectionStrings);
                }
            }
            return true
        } catch (error) {

        }
        return false;
    }
    //#endregion ========================================= End config settings ====================================
    //#region ============================================ ES =====================================================
    public searchESDangTin(tuKhoaTimKiem: string, displayItems: any, displayPage: any) {
        let params = new HttpParams();
        params = params.append(commonParams.tuKhoaTimKiem, tuKhoaTimKiem);
        params = params.append(commonParams.displayItems, displayItems);
        params = params.append(commonParams.displayPage, displayPage);
        let options: any = new optionsHttp();
        options.params = params;

        return this.coreHttp.get(`${Variables.domainES}/api/elasticsearch/SearchRaoXeDangTin`, options);
    }

    public searchESGoiY(tuKhoaTimKiem: string) {
        let params = new HttpParams();
        params = params.append(commonParams.tuKhoaTimKiem, tuKhoaTimKiem);
        params = params.append(commonParams.orderString, "Count1:desc");
        params = params.append(commonParams.displayItems, "10");
        let options: any = new optionsHttp();
        options.params = params;
        return this.coreHttp.get(`${Variables.domainES}/api/elasticsearch/SearchRaoXeTuKhoa`, options);
    }
    //#endregion ============================================ END ES =====================================================
    //#region =============================================== DangTin ====================================================
    //add vào yêu thích
    async addFavorite(idDangTin) {
        try {

            let result = await this.thanhVienRaoVatBLL_InterfaceData.addFavorite(idDangTin).toPromise();
            result = new ResponseBase(result);
            if (!result.isSuccess()) {
                this.commonMethodsTs.createMessageError(result.getMessage());
                return false;
            }
            let idThanhVien = "0";
            if (Variables.tokenThanhVien) {
                idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
            }
            return this.commonMethodsTs.addDataPersonalizeLS(idThanhVien, localStorageData.tinDangYeuThich, idDangTin);
        }
        catch (e) {
            this.commonMethodsTs.createMessageErrorByTryCatch(e);
        }
        return false;
    }


    //remove vào yêu thích
    async removeFavorite(idDangTin) {
        try {
            let result = await this.thanhVienRaoVatBLL_InterfaceData.removeFavorite(idDangTin).toPromise();
            result = new ResponseBase(result);
            if (!result.isSuccess()) {
                this.commonMethodsTs.createMessageSuccess(result.getMessage());
                return false;
            }
            let idThanhVien = "0";
            if (Variables.tokenThanhVien) {
                idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
            }
            return this.commonMethodsTs.removeDataPersonalizeLS(idThanhVien, localStorageData.tinDangYeuThich, idDangTin);
        }
        catch (e) {
            this.commonMethodsTs.createMessageErrorByTryCatch(e);
        }
        return false;
    }

    // add tin like
    async addLike(idDangTin) {
        try {
            let result = await this.thanhVienRaoVatBLL_InterfaceData.addLike(idDangTin).toPromise();
            result = new ResponseBase(result);
            if (!result.isSuccess()) {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
            let idThanhVien = "0";
            if (Variables.tokenThanhVien) {
                idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
            }
            return this.commonMethodsTs.addDataPersonalizeLS(idThanhVien, localStorageData.tinDangLike, idDangTin);
        }
        catch (e) {
            this.commonMethodsTs.createMessageErrorByTryCatch(e);
        }
        return false;
    }

    //remove tin like
    async removeLike(idDangTin) {
        try {
            let result = await this.thanhVienRaoVatBLL_InterfaceData.removeLike(idDangTin).toPromise();
            result = new ResponseBase(result);
            if (!result.isSuccess()) {
                this.commonMethodsTs.createMessageError(result.getMessage());
            }
            let idThanhVien = "0";
            if (Variables.tokenThanhVien) {
                idThanhVien = this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
            }
            return this.commonMethodsTs.removeDataPersonalizeLS(idThanhVien, localStorageData.tinDangLike, idDangTin);
        }
        catch (e) {
            this.commonMethodsTs.createMessageErrorByTryCatch(e);
        }
        return false;
    }
    async getTimeServer() {
        let timeDateServer = CoreVariables.timeDateServer;
        try {
            let currentServerTime = await this.thanhVienRaoVatBLL_InterfaceData.getServerTime().toPromise();
            timeDateServer = this.commonMethodsTs.newDate(currentServerTime).getTime();
            CoreVariables.timeDateServer = timeDateServer;
        } catch (error) {

        }
        return timeDateServer;

    }
    //#endregion ============================================ End Dang Tin ===============================================

}