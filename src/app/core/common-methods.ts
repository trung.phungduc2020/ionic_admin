import { TrimPipe } from "./../shared/pipes/string/trim";
import { FilterByPipe } from "./../shared/pipes/array/filter-by";
import { Injectable, NgZone } from "@angular/core";
import {
  ToastController,
  ActionSheetController,
  LoadingController,
  AlertController,
  ModalController,
  Events,
  Platform,
  PopoverController,
} from "@ionic/angular";
import {
  configImageExtension,
  configIconExtension,
  configFlashExtension,
  configMediaExtension,
  configDocumentExtension,
  configIsAllowExtension,
  apiHost,
  commonParams,
  fileTypeWordExtension,
  fileTypeExcelExtension,
  fileTypePowerpointExtension,
  fileTypePdfExtension,
  fileTypeSoundExtension,
  fileTypeVideoExtension,
  Variables,
  defaultWxh,
  commonNavigate,
  shortDateTimeForSql,
  backgroundColorForMessage,
  apiCdn,
  durationForMessage,
  commonMessages,
  localStorageData,
  apiHostDailyXe,
  TypeLayoutAlbum,
  defaultValue,
  keyPersonalizeLS,
  versionStructorPersonalizeLS,
  countItemRemoveInArrayPersonalize,
  maxItemInArrayPersonalize,
  linkNoiDung,
  commonException,
  defaultPrice,
  nameShortEnviroment,
  TypeSmsValidation,
  MoneyDiscountType,
  linkFirebaseToPage,
  commonAttr,
} from "./variables";
import { Md5 } from "ts-md5/dist/md5";
import { NavigationExtras, Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { MainServices, FcmService } from "../shared/services";
import {
  ScanPipe,
  LatinisePipe,
  MatchPipe,
  ReplacePipe,
  TrurthifyPipe,
  DiffObjPipe,
  DaiLyXeTimeAgoPipe,
} from "../shared/pipes";
import { NgxSpinnerService } from "ngx-spinner";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Device } from "@ionic-native/device/ngx";
import * as _ from "lodash";
import { environment } from "src/environments/environment";
import * as firebase from "firebase";
import { AngularFireDatabase } from "@angular/fire/database";
import { DangTinContact, DangTin, ErrorApi } from "./entity";
import { OpenNativeSettings } from "@ionic-native/open-native-settings/ngx";
import { Clipboard } from "@ionic-native/clipboard/ngx";
import { CoreVariables } from "./core-variables";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { ViewBaiVietComponent } from "../components/views/view-baiviet/view-baiviet.component";
import { labelPages } from "./labelPages";
import { NgxImageCompressService } from "ngx-image-compress";
import { Storage } from "@ionic/storage";
import { UniqueDeviceID } from "@ionic-native/unique-device-id/ngx";
import { checkAvailability } from "@ionic-native/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";

declare var require: any;
declare var escape: any;
declare var window: any;
declare var google: any;
@Injectable()
export class CommonMethodsTs {
  private _datePipe: DatePipe;
  private listApiCalling: any[] = [];
  private defaultNoImageSrc: any;
  firemsg = firebase.database().ref("/messages");
  private messagesCollection: AngularFirestoreCollection<any>;

  private modal: any;
  constructor(
    public mainService: MainServices,
    public router: Router,
    public toastController: ToastController,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public ngZone: NgZone,
    public loadingController: LoadingController,
    public spinner: NgxSpinnerService,
    public geolocation: Geolocation,
    public device: Device,
    public fcm: FcmService,
    public angularFireDatabase: AngularFireDatabase,
    public openNativeSettings: OpenNativeSettings,
    public clipboard: Clipboard,
    public callNumber: CallNumber,
    public imageCompress: NgxImageCompressService,
    public storage: Storage,
    public events: Events,
    public uniqueDeviceID: UniqueDeviceID,
    public platform: Platform,
    public modalController: ModalController,
    public database: AngularFirestore
  ) {
    // this.keychainTouchId.setLocale("vi"); // set ngôn ngũ tiếng việt
    this._datePipe = new DatePipe("en-US");
    this.defaultNoImageSrc = this.builLinkImage("no-image", -1);
    this.messagesCollection = database.collection<any>("messages");
  }
  public isImage(extension: string): boolean {
    return !extension
      ? false
      : configImageExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isFlash(extension: string): boolean {
    return !extension
      ? false
      : configFlashExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isMedia(extension: string): boolean {
    return !extension
      ? false
      : configMediaExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isIcon(extension: string): boolean {
    return !extension
      ? false
      : configIconExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isDocument(extension: string): boolean {
    return !extension
      ? false
      : configDocumentExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isImageOrMedia(extension: string): boolean {
    return !extension
      ? false
      : this.isImage(extension) || this.isMedia(extension);
  }
  public isAllowExtension(extension: string): boolean {
    return !extension
      ? false
      : configIsAllowExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isWord(extension: string): boolean {
    return !extension
      ? false
      : fileTypeWordExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isExcel(extension: string): boolean {
    return !extension
      ? false
      : fileTypeExcelExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isPowerpoint(extension: string): boolean {
    return !extension
      ? false
      : fileTypePowerpointExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isPdf(extension: string): boolean {
    return !extension
      ? false
      : fileTypePdfExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isSound(extension: string): boolean {
    return !extension
      ? false
      : fileTypeSoundExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public isVideo(extension: string): boolean {
    return !extension
      ? false
      : fileTypeVideoExtension.indexOf(extension.toLowerCase()) > -1;
  }
  public toASCII(value: string) {
    if (value) {
      var str = value;
      str = str.toLowerCase();
      str = str.replace(/!|@|%|\\/g, " ");
      str = str.replace(/ + /g, " ");
      str = str.trim();
      return new LatinisePipe().transform(str);
    } else {
      return null!;
    }
    return null!;
  }
  /* end api host */
  /* api drive */
  public addTimeVariable(res: string) {
    if (!res) {
      return "";
    }
    res = res.split("?")[0];
    let newUrl =
      res + (res.indexOf("?") > -1 ? "&" : "?") + this.getDateServe().getTime();
    return newUrl;
  }
  public formatUrlImageAddTime(url: string) {
    if (!url && !this.isURl(url)) {
      return "";
    }
    let newUrl =
      url + (url.indexOf("?") > -1 ? "&" : "?") + this.getDateServe().getTime();
    return newUrl;
  }
  public formatUrlWithapiCdn(url: string, wxh?: string) {
    if (!url) {
      return this.defaultNoImageSrc;
    }
    url = this.replace_wxh(url, wxh);
    if (url.indexOf("/") == 0) {
      return apiHost + url;
    } else {
      return apiHost + "/" + url;
    }
  }
  public formatUrlWithapiHost(Url: string, wxh?: string) {
    if (!Url) {
      return this.defaultNoImageSrc;
    }
    let newUrl = this.addTimeVariable(this.replace_wxh(Url, wxh));
    if (!newUrl) {
      return this.defaultNoImageSrc;
    }
    return apiHost + "/" + newUrl;
  }
  public replace_wxh(url: string, wxh?: string): string {
    url = new ReplacePipe().transform(url, "j([1-9]|)\\.", `j${wxh || ""}.`);
    return url;
  }
  public formatUrlImage(
    url: string,
    wxh: string = defaultWxh.regular,
    addTime: boolean = true
  ) {
    if (!url) {
      return url;
    }
    if (addTime) {
      url = url.split("?")[0];
      url = this.addTimeVariable(url);
    }
    return this.isURl(url)
      ? this.replace_wxh(url, wxh)
      : this.formatUrlWithapiCdn(url, wxh);
  }
  public regularUrlImage(url: string, addTime: boolean = false) {
    if (!url && !this.isURl(url)) {
      return null;
    }
    url = url.split("?")[0];
    return this.formatUrlImage(url, defaultWxh.regular, addTime);
  }
  public isURl(str: string) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if (!regex.test(str)) {
      return false;
    } else {
      return true;
    }
  }
  public convert_toMD5(password: string): string {
    if (password) {
      let res = Md5.hashStr(password) || "";
      return res.toString().toLocaleUpperCase();
    }
    return "";
  }
  //Dịch mã token
  decodeToken(token: string) {
    var parts = token.split(".");
    if (parts.length !== 3) {
      throw new Error("JWT must have 3 parts");
    }
    var decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error("Cannot decode the token");
    }
    return JSON.parse(decoded);
  }
  private urlBase64Decode(str: string) {
    var output = str.replace(/-/g, "+").replace(/_/g, "/");
    switch (output.length % 4) {
      case 0: {
        break;
      }
      case 2: {
        output += "==";
        break;
      }
      case 3: {
        output += "=";
        break;
      }
      default: {
        throw "Illegal base64url string!";
      }
    }
    return decodeURIComponent(escape(window.atob(output)));
  }
  public navigate(commands: any[], extras?: NavigationExtras) {
    if (!Variables.isShownConfirmDialog) {
      this.router.navigate(commands, extras);
    }
  }
  public reloadPage() {
    window.location.reload();
  }
  public openUrl(url?: string, style: string = "_blank") {
    var win = window.open(url, style);
    // if (style == '_blank') {
    // 	win.focus();
    // }
  }
  public toPage(link: string, id?: any, extras?: any) {
    extras = extras || {};
    if (+id == -1 || +id > 0) {
      this.navigate([link, id], extras), { skipLocationChange: false };
    } else {
      this.navigate([link], extras), { skipLocationChange: false };
    }
  }
  public toPage_AdminThongTinCaNhan(id?: any) {
    this.toPage(commonNavigate.cms_thongTinCaNhan, id);
  }
  public toPage_AdminLogin(id?: any) {
    this.toPage(commonNavigate.cms_login, id);
  }
  public toPage_AdminDangTin(id?: any) {
    this.toPage(commonNavigate.cms_dangTin, id);
  }
  public toPage_Test() {
    this.toPage(commonNavigate.test);
  }
  public toPage_AdminDashboard(id?: any) {
    this.toPage(commonNavigate.cms_dashboard, id);
  }
  public toPage_AdminUserBlackList(id?: any) {
    this.toPage(commonNavigate.cms_userBlackList, id);
  }
  public toPage_AdminDangTinRating(id?: any) {
    this.toPage(commonNavigate.cms_dangTinRating, id);
  }
  public toPage_AdminSettings(id?: any) {
    this.toPage(commonNavigate.cms_settings, id);
  }
  public toPage_AdminLunix(id?: any) {
    this.toPage(commonNavigate.cms_command_linux, id);
  }
  public toPage_AdminDangTinViolation(id?: any) {
    this.toPage(commonNavigate.cms_dangTinViolation, id);
  }
  public toPage_AdminGiaoDichXe(id?: any) {
    this.toPage(commonNavigate.cms_giaoDichXe, id);
  }
  public toPage_AdminGiaoDichXeCreate(id?: any) {
    this.toPage(commonNavigate.cms_giaoDichXeCreate, id);
  }
  public toPage_AdminMenu(id?: any) {
    this.toPage(commonNavigate.cms_menu, id);
  }
  public toPage_AdminRanking(id?: any) {
    this.toPage(commonNavigate.cms_ranking, id);
  }
  public toPage_AdminThanhVien(id?: any) {
    this.toPage(commonNavigate.cms_thanhVien, id);
  }
  public toPage_AdminUserPoint(id?: any) {
    this.toPage(commonNavigate.cms_userpoint, id);
  }
  public toPage_AdminGroupThanhVien(id?: any) {
    this.toPage(commonNavigate.cms_groupThanhVien, id);
  }
  public toPage_AdminThongBao(id?: any) {
    this.toPage(commonNavigate.cms_thongBao, id);
  }
  public toPage_AdminThongBaoTemplate(id?: any) {
    this.toPage(commonNavigate.cms_thongBaoTemplate, id);
  }
  public toPage_AdminThongKe(id?: any) {
    this.toPage(commonNavigate.cms_thongKe, id);
  }
  public toPage_AdminInstallStatistics(id?: any) {
    this.toPage(commonNavigate.cms_installStatistics, id);
  }
  public toPage_AdminUserDevice(id?: any) {
    this.toPage(commonNavigate.cms_userDevice, id);
  }
  public toPage_AdminLayoutAlbum(id?: any) {
    this.toPage(commonNavigate.cms_layoutAlbum, id);
  }
  public toPage_AdminUuDai(id?: any) {
    this.toPage(commonNavigate.cms_uuDai, id);
  }
  public toPage_Member(id?: any) {
    this.toPage(commonNavigate.member, id);
  }
  public toPage_MemberLogin(id?: any) {
    this.toPage(commonNavigate.f_login, id);
  }
  public toPage_MemberDashboard(id?: any) {
    this.toPage(commonNavigate.f_dashboard, id);
  }
  public toPage_MemberHomeDangTin(id?: any) {
    this.toPage(commonNavigate.f_homeTinDang, id);
  }
  public toPage_TimKiem(id?: any) {
    this.toPage(commonNavigate.timKiem, id);
  }
  public toPage_MemberEditProfile(id?: any) {
    this.toPage(commonNavigate.f_infoUser, id);
  }
  public toPage_HomePage(id?: any) {
    this.toPage(commonNavigate.homePage, id);
  }
  public toPage_MemberTransactionCarManage(id?: any) {
    this.toPage(commonNavigate.f_transactionCarManage, id);
  }
  public toPage_MemberTransactionCarManageCreate(id?: any) {
    this.toPage(commonNavigate.f_transactionCarManageCreate, id);
  }
  public toPage_MemberDangTinManage(id?: any) {
    this.toPage(commonNavigate.f_tinDang, id);
  }
  public toPage_MemberDangTinRating(id?: any) {
    this.toPage(commonNavigate.f_tinDangRating, id);
  }
  public toPage_MemberThongTinPoint(id?: any) {
    this.toPage(commonNavigate.f_thongTinPoint, id);
  }
  public toPage_MemberTinDaLuu(id?: any) {
    this.toPage(commonNavigate.f_tinDaLuu, id);
  }
  public toPage_MemberTinDangRating(id?: any) {
    this.toPage(commonNavigate.f_tinDangRating, id);
  }
  public toPage_MemberForgotPassword(id?: any) {
    this.toPage(commonNavigate.f_forgotPassword, id);
  }
  public toPage_MemberRegister(id?: any) {
    this.toPage(commonNavigate.f_register, id);
  }
  public toPage_MemberDangTinCreate(id?: any) {
    this.toPage(commonNavigate.f_dangTinCreate, id);
  }
  public toPage_Notifications(id?: any) {
    this.toPage(commonNavigate.notifications, id);
  }
  public toPage_HomeListTinDang(id?: any, extras?: any) {
    this.toPage(commonNavigate.homeListTinDang, id, extras);
  }
  public toPage_MemberTinDang(id?: any) {
    this.toPage(commonNavigate.f_tinDang, id);
  }
  public toPage_TinTuc(id?: any) {
    this.toPage(commonNavigate.tinTuc, id);
  }
  public toPage_MemberPinTopHistory(id?: any) {
    this.toPage(commonNavigate.f_pinTopHistory, id);
  }
  public toPage_ShareApp() {
    this.toPage(commonNavigate.f_chiaSeApp);
  }
  public toPage_DanhSachUuDai() {
    this.toPage(commonNavigate.f_uuDaiCuaBan);
  }
  public toPage_ContactDailyxe(id?) {
    this.toPage(commonNavigate.f_contactDailyxe, id);
  }
  public toPage_AdvertisementDailyxe(id?) {
    this.toPage(commonNavigate.f_advertisementDailyxe, id);
  }
  public toPage_ChiTietUuDai(id?: any, extras?: any) {
    this.toPage(commonNavigate.f_uuDaiCuaBan, id, extras);
  }
  public toPage_AdminAttackMode(id?) {
    this.toPage(commonNavigate.cms_attackMode, id);
  }
  // public toPage_MemberContactDailyxe(id?: any) {
  // 	this.toPage(commonNavigate.f_contactDailyxe, id);
  // }
  public buildLinkTinTucInAppBrowser(id?: any, rewriteUrl?: string) {
    let link: string = "";
    link = apiHostDailyXe + "/tin-tuc/" + rewriteUrl + "-" + id + "d.html";
    return link;
  }
  public checkFileIsFromWebUri(file: any) {
    if (!file) {
      return false;
    }
    return (
      file.size! == 0 &&
      file.type! == "" &&
      file.name!.toLowerCase().startsWith("http")
    );
  }
  //#endregion
  // - `format` indicates which date/time components to include. The format can be predefined as
  // *   shown below or custom as shown in the table.
  // *   - `'medium'`: equivalent to `'yMMMdjms'` (e.g. `Sep 3, 2010, 12:05:08 PM` for `en-US`)
  // *   - `'short'`: equivalent to `'yMdjm'` (e.g. `9/3/2010, 12:05 PM` for `en-US`)
  // *   - `'fullDate'`: equivalent to `'yMMMMEEEEd'` (e.g. `Friday, September 3, 2010` for `en-US`)
  // *   - `'longDate'`: equivalent to `'yMMMMd'` (e.g. `September 3, 2010` for `en-US`)
  // *   - `'mediumDate'`: equivalent to `'yMMMd'` (e.g. `Sep 3, 2010` for `en-US`)
  // *   - `'shortDate'`: equivalent to `'yMd'` (e.g. `9/3/2010` for `en-US`)
  // *   - `'mediumTime'`: equivalent to `'jms'` (e.g. `12:05:08 PM` for `en-US`)
  // *   - `'shortTime'`: equivalent to `'jm'` (e.g. `12:05 PM` for `en-US`)
  //'yyyy-MM-dd HH:mm:ss'
  public convertDate(date: Date, format?: string): any {
    if (!(date instanceof Date)) {
      date = this.newDate(date);
    }
    date = date || this.getDateServe();
    // format = format || 'short';
    format = format || shortDateTimeForSql || "short";
    return this._datePipe.transform(date, format);
  }
  //Nguyen Trong Huu 17/03/2018 sao chép Array
  cloneArray(list: any[]) {
    try {
      return [].concat(list);
    } catch {
      return [];
    }
  }
  //Nguyen Trong Huu 17/03/2018 sao chép object
  cloneObject(obj: any) {
    try {
      return Object.assign({}, obj);
    } catch {
      return {};
    }
  }
  //Nguyen Trong Huu 17/03/2018 sao chép string
  cloneString(str: string) {
    try {
      return new TrimPipe().transform(str);
    } catch {
      return "";
    }
  }
  copyObjectForDuplicate(obj: any) {
    let data: any = this.cloneObject(obj);
    try {
      data.IdSeo = undefined;
      data.Seo = undefined;
      return data;
    } catch {
      return {};
    }
  }
  //tronghuu95
  setReadTB1() {
    if (!localStorage.getItem(commonParams.readTB1)) {
      localStorage.setItem(commonParams.readTB1, "false");
    }
  }
  checkReadTB1() {
    if (
      !localStorage.getItem(commonParams.readTB1) ||
      localStorage.getItem(commonParams.readTB1) == "false"
    ) {
      localStorage.setItem(commonParams.readTB1, "true");
      return true;
    } else {
      return false;
    }
  }
  //tronghuu95 20302018check nhóm quyền của Admin
  goBack() {
    //C1
    // this._location.back();
    //C2
    setTimeout(() => {
      window.history.back();
    }, 500);
    //C3
    //this.navCtrl.goBack();
  }
  /* Encrypt/Decrypt base64 */
  /* nguyencuongcs 20180422 */
  /*
   * Function to convert from UTF8 to Base64 solving the Unicode Problem
   * Requires: window.btoa and window.encodeURIComponent functions
   * More info: http://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
   * Samples:
   *      b64EncodeUnicode('✓ à la mode'); // "4pyTIMOgIGxhIG1vZGU="
   *      b64EncodeUnicode('\n'); // "Cg=="
   */
  public b64EncodeUnicode(str: string): string {
    if (window && "btoa" in window && "encodeURIComponent" in window) {
      return btoa(
        encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => {
          return String.fromCharCode(("0x" + p1) as any);
        })
      );
    } else {
      console.warn(
        "b64EncodeUnicode requirements: window.btoa and window.encodeURIComponent functions"
      );
      return "";
    }
  }
  /*
   * Function to convert from Base64 to UTF8 solving the Unicode Problem
   * Requires window.atob and window.decodeURIComponent functions
   * More info: http://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
   * Samples:
   *      b64DecodeUnicode('4pyTIMOgIGxhIG1vZGU='); // "✓ à la mode"
   *      b64DecodeUnicode('Cg=='); // "\n"
   */
  public b64DecodeUnicode(str: string): string {
    if (window && "atob" in window && "decodeURIComponent" in window) {
      return decodeURIComponent(
        Array.prototype.map
          .call(atob(str), (c: string) => {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join("")
      );
    } else {
      console.warn(
        "b64DecodeUnicode requirements: window.atob and window.decodeURIComponent functions"
      );
      return "";
    }
  }
  public Base64Encode(plainText: string): string {
    return btoa(encodeURIComponent(plainText));
  }
  public Base64Decode(base64EncodedData: string): string {
    return decodeURIComponent(atob(base64EncodedData));
  }
  public buildStringJsonFromList(list: any, proValue: any = "Content") {
    try {
      let mappingObject: any = {};
      list.forEach((obj: any) => {
        mappingObject[obj.Key] = obj[proValue];
      });
      return JSON.stringify(mappingObject);
    } catch {
      return "";
    }
  }
  public getWeekOfYear = function (date: Date) {
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    // January 4 is always in week 1.
    var week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return (
      1 +
      Math.round(
        ((date.getTime() - week1.getTime()) / 86400000 -
          3 +
          ((week1.getDay() + 6) % 7)) /
          7
      )
    );
  };
  public getQuarterOfYear(date: Date) {
    var month = date.getMonth() + 1;
    return Math.ceil(month / 3);
  }
  public findValueCauHinh_ToInt(
    data: any[],
    macauhinh: string,
    defaultValue: number = -1
  ): number {
    try {
      let kq = data.find((x: any) => x.MaCauHinh == macauhinh).GiaTriCauHinh;
      return !kq ? defaultValue : +kq;
    } catch {
      return defaultValue;
    }
  }
  public findValueCauHinh_ToString(
    data: any[],
    macauhinh: string,
    defaultValue: string = null!
  ): string {
    try {
      let kq = data.find((x: any) => x.MaCauHinh == macauhinh).GiaTriCauHinh;
      return !kq ? defaultValue : kq;
    } catch {
      return defaultValue;
    }
  }
  public addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }
  public minusDays(date: Date, days: number): Date {
    // date.setDate(date.getDate() - days);
    return date;
  }
  public findValue_ToInt(
    data: any,
    thuoctinh: string,
    defaultValue: number = -1
  ): number {
    try {
      let kq = data[thuoctinh];
      return !kq ? defaultValue : +kq;
    } catch {
      return defaultValue;
    }
  }
  public findValue_ToString(
    data: any,
    thuoctinh: string,
    defaultValue: string = "Không có thông tin"
  ): string {
    try {
      let kq = data[thuoctinh];
      return !kq ? defaultValue : kq;
    } catch {
      return defaultValue;
    }
  }
  public startLoading() {
    this.startLoader("loading-data");
  }
  public stopLoading() {
    this.stopLoader("loading-data");
  }
  private lockScreen() {
    try {
      this.presentLoadingWithOptions();
    } catch {}
  }
  private unlockScreen() {
    this.dismissPresentLoadingWithOptions();
  }
  public startLoader(req: any) {
    if (this.listApiCalling && this.listApiCalling.length == 0) {
      this.lockScreen();
    }
    this.listApiCalling.push(req);
  }
  public stopLoader(req: any) {
    try {
      let index: any = this.listApiCalling.indexOf(req);
      if (index !== -1) {
        this.listApiCalling.splice(index, 1);
      }
      if (this.listApiCalling.length == 0) {
        this.unlockScreen();
      }
    } catch {
      this.unlockScreen();
    }
  }
  //Hàm lấy data tránh lỗi null
  // dùng để lấy data
  private getValueBylistProperties(
    value: any,
    listProperties: any,
    defaultValue: any = null
  ) {
    try {
      let data = Object.assign({}, value);
      listProperties.forEach((pro: any) => {
        var regex = /(.*)\[(.*)\]/;
        var matches = regex.exec(pro);
        if (!matches) {
          data = data[pro];
        } else {
          data = data[matches[1]][+matches[2]];
        }
      });
      return data || defaultValue;
    } catch {
      return defaultValue;
    }
  }
  // private
  public filterByAttr(list, attr, value) {
    try {
      return new FilterByPipe().transform(list, [attr], value);
    } catch {
      return {};
    }
  }
  public getPropertieFromList(
    list,
    attr,
    valueCondition,
    atrrCondition = "Id"
  ) {
    try {
      let item = this.filterByAttr(
        list,
        atrrCondition,
        valueCondition
      )[0] as any;
      return item[attr];
    } catch {
      return {};
    }
  }
  public getData(items: any, attr: string, defaultValue: any = null): any {
    let listProperties = attr.split(".");
    return this.getValueBylistProperties(items, listProperties, defaultValue);
  }
  public getData_Int(items: any, attr: string, defaultValue: any = -1): any {
    return +this.getData(items, attr, defaultValue);
  }
  public getData_String(items: any, attr: string, defaultValue: any = ""): any {
    return this.getData(items, attr, defaultValue);
  }
  public getData_Boolean(
    items: any,
    attr: string,
    defaultValue: any = false
  ): any {
    let data = this.getData(items, attr, defaultValue);
    return data.toString().toLowerCase() == "true"
      ? true
      : defaultValue || false;
  }
  public parseToJson(data: any) {
    try {
      return JSON.parse(data);
    } catch {
      return {};
    }
  }
  //tronghuu95 xử lý cắt ảnh
  public dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    var byteString = atob(dataURI.split(",")[1]);
    // separate out the mime component
    var mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    // write the bytes of the string to an ArrayBuffer
    var arrayBuffer = new ArrayBuffer(byteString.length);
    var _ia = new Uint8Array(arrayBuffer);
    for (var i = 0; i < byteString.length; i++) {
      _ia[i] = byteString.charCodeAt(i);
    }
    var dataView = new DataView(arrayBuffer);
    var blob = new Blob([dataView], { type: mimeString });
    return blob;
  }
  public toDataURI(url: any, callback: any) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.onerror = function () {
      console.log("Lỗi kết nối server");
      callback("");
    };
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.send();
  }
  public createFileByDataURI(dataURI: string, nameFile: string): File {
    let blob = this.dataURItoBlob(dataURI);
    let f = new File([blob], nameFile, { type: "image/jpg" });
    return f;
  }
  public createObjectForFormGroup(d: any) {
    let o: any = {};
    Object.keys(d).forEach((p) => {
      o[p] = [d[p]];
    });
    return o;
  }
  // thư viện và mã hoá
  public encodeDES(decrypted: string, keyBase?: string) {
    try {
      return this.mainService.tripleDESService.encrypt(decrypted, keyBase);
    } catch {
      return null;
    }
  }
  public getKeyDesByRsa() {
    try {
      return this.mainService.rsaService.getKeyDes();
    } catch {
      return "";
    }
  }
  public decodeRSA(str: string, prk?: string) {
    try {
      return this.mainService.rsaService.decrypt(str, prk);
    } catch {
      return null;
    }
  }
  public decodeDES(encrypted: string, keyBase?: string) {
    try {
      return this.mainService.tripleDESService.decrypt(encrypted, keyBase);
    } catch {
      return null;
    }
  }
  //Kiểm tra time token
  //offsetSeconds: time cộng thêm
  public isTokenExpired(token: string, offsetSeconds: number = 0) {
    try {
      if (!token) {
        return true;
      }
      var date = this.getTokenExpirationDate(token);
      offsetSeconds = offsetSeconds || 0;
      if (date === null) {
        return true;
      }
      return date.valueOf() < new Date().getTime() + offsetSeconds * 1000;
    } catch {}
    return true;
  }
  private getTokenExpirationDate(token: string) {
    var decoded: any;
    decoded = this.decodeToken(token);
    if (typeof decoded.exp === "undefined") {
      return null;
    }
    var date = new Date(0); // The 0 here is the key, which sets the date to the epoch
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  async presentLoadingWithOptions(opts?: any) {
    let optsDef = {
      spinner: null,
      duration: 5000,
      message: "Xin chờ...",
      translucent: true,
      cssClass: "custom-class custom-loading",
    };
    opts = Object.assign(optsDef, opts || {});
    Variables.presentLoadingWithOptions = await this.loadingController.create(
      opts
    );
    return await Variables.presentLoadingWithOptions.present();
  }
  async dismissPresentLoadingWithOptions() {
    try {
      Variables.presentLoadingWithOptions.dismiss();
    } catch (error) {}
  }
  private convertToString(data, def = "") {
    try {
      if (!data || data.length <= 0) {
        return def;
      }
      return typeof data === "string" || data instanceof String
        ? data
        : JSON.stringify(data);
    } catch {
      return def;
    }
  }
  private convertArrayToArrayString(data) {
    try {
      if (!data || data.length <= 0) {
        return [];
      }
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        data[index] = this.convertToString(element);
      }
      return data;
    } catch {
      return [];
    }
  }
  async createMessageInfo(...message) {
    message = this.convertArrayToArrayString(message);
    await this.presentToastWithOptions(
      message.join(" - "),
      "top",
      true,
      backgroundColorForMessage.info
    );
  }
  async createMessageSuccess(...message) {
    message = this.convertArrayToArrayString(message);
    await this.presentToastWithOptions(
      message.join(" - "),
      "top",
      true,
      backgroundColorForMessage.success
    );
  }
  async createMessageError(...message) {
    message = this.convertArrayToArrayString(message);
    await this.presentToastWithOptions(
      message.join(" - "),
      "top",
      true,
      backgroundColorForMessage.error
    );
  }

  async createMessageErrorByTryCatch(error, messDef?: any) {
    if (Variables.isDev) {
      let strError = JSON.stringify(error);
      if (strError == "{}") strError = error;
      //gọi api để tổng hợp lỗi xuất hiện do try catch
      console.log(strError);
    }
    try {
      let obj: ErrorApi = JSON.parse(error) as ErrorApi;
      messDef = obj.Message;
    } catch (error) {}

    await this.createMessageError(messDef || error);
  }
  async alert(mess) {
    if (environment.production == false) {
      alert(mess);
    } else {
      console.log(mess);
    }
  }
  async createMessageWarning(...message) {
    message = this.convertArrayToArrayString(message);
    await this.presentToastWithOptions(
      message.join(" - "),
      "top",
      true,
      backgroundColorForMessage.warning
    );
  }
  async presentToastWithOptions(
    message: string,
    positionParam: any = "top",
    autoDismiss = true,
    backgroundColor: string = backgroundColorForMessage.error
  ) {
    var toast: any;
    if (autoDismiss) {
      toast = await this.toastController.create({
        message: message,
        showCloseButton: false,
        duration: durationForMessage,
        position: positionParam,
        buttons: [
          {
            icon: "close",
            role: "cancel",
          },
        ],
        color: backgroundColor,
      });
    } else {
      toast = await this.toastController.create({
        message: message,
        showCloseButton: false,
        position: positionParam,
        buttons: [
          {
            icon: "close",
            role: "cancel",
          },
        ],
        color: backgroundColor,
      });
    }
    toast.present();
  }
  /**
   *
   * nhut Array
   * <summary>xoá một phần tử khỏi mảng</sumary>
   * <param name="array"> mảng có phần tử cần xoá</param>
   *  <param name="id">id của phần tử cần xoá (element của array có thuộc tính id)</param>
   * <result></result>
   */
  removeElementById(array: any, id: any) {
    try {
      for (let i = 0; i < array.length; i++) {
        if (array[i].Id == id) {
          var index = array.indexOf(array[i]);
          if (index > -1) {
            array.splice(index, 1);
            return;
          }
        }
      }
    } catch (error) {
      return undefined;
    }
  }
  searchItemById(array: any[], id: any, attrId = "Id") {
    try {
      let filter = new FilterByPipe().transform(array, [attrId], id);
      return filter[0];
    } catch (error) {
      return null;
    }
  }
  searchItemByIdReturnPointer(array: any, id: any) {
    try {
      return array.filter(function (entry) {
        return entry.id == id;
      });
    } catch (error) {
      return undefined;
    }
  }
  stringJsonToArray(json: string) {
    let result = [];
    try {
      result = JSON.parse(json);
    } catch {
      result = [];
    }
    return result;
  }
  firstOrDefault(array: any) {
    try {
      return array[0];
    } catch {
      return null;
    }
  }
  splitStringToArray(str: string, char) {
    try {
      return str.split(char);
    } catch {
      return [];
    }
  }
  // dateTime
  dateTimeTodd_MM_yyyy_hh_mm_ss_ml(day) {
    var ngay = day.getDate();
    var thang = day.getMonth();
    thang = thang + 1;
    var nam = day.getFullYear();
    var gio = day.getHours();
    var phut = day.getMinutes();
    var giay = day.getSeconds();
    var miliGiay = day.getMilliseconds();
    var ngayStr = ngay < 10 ? "0" + ngay : ngay;
    var thangStr = thang < 10 ? "0" + thang : thang;
    var gioStr = gio < 10 ? "0" + gio : gio;
    var phutStr = ngay < 10 ? "0" + phut : phut;
    var giayStr = giay < 10 ? "0" + giay : giay;
    var miliGiayStr = (miliGiay / 10).toFixed(0);
    return (
      ngayStr +
      "_" +
      thangStr +
      "_" +
      nam +
      "_" +
      gioStr +
      "_" +
      phutStr +
      "_" +
      giayStr +
      "_" +
      miliGiayStr
    );
  }
  /**
   *
   * @param DateA
   * @param DateB
   */
  compareDateTime(DateA, DateB) {
    var a = this.newDate(DateA);
    var b = this.newDate(DateB);
    var msDateA = Date.UTC(
      a.getFullYear(),
      a.getMonth() + 1,
      a.getDate(),
      a.getHours(),
      a.getMinutes(),
      a.getSeconds(),
      a.getMilliseconds()
    );
    var msDateB = Date.UTC(
      b.getFullYear(),
      b.getMonth() + 1,
      b.getDate(),
      b.getHours(),
      b.getMinutes(),
      b.getSeconds(),
      b.getMilliseconds()
    );
    if (msDateA < msDateB) return -1;
    // a < b
    else if (msDateA == msDateB) return 0;
    // a == b
    else if (msDateA > msDateB) return 1;
    // a > b
    else return null; // error
  }
  compareDate(DateA, DateB) {
    var a = this.newDate(DateA);
    var b = this.newDate(DateB);
    var msDateA = Date.UTC(a.getFullYear(), a.getMonth() + 1, a.getDate());
    var msDateB = Date.UTC(b.getFullYear(), b.getMonth() + 1, b.getDate());
    if (msDateA < msDateB) return -1;
    // a < b
    else if (msDateA == msDateB) return 0;
    // a == b
    else if (msDateA > msDateB) return 1;
    // a > b
    else return null; // error
  }
  //time
  toHHMMSS = function (second, autoHideIfEqualZero = true) {
    try {
      var sec_num = parseInt(second); // don't forget the second param
      var hours = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - hours * 3600) / 60);
      var seconds = sec_num - hours * 3600 - minutes * 60;
      var hoursString = hours < 10 ? (hoursString = "0" + hours) : hours;
      var minutesString = minutes < 10 ? "0" + minutes : minutes;
      var secondsString = seconds < 10 ? "0" + seconds : seconds;
      if (autoHideIfEqualZero) {
        if (hours == 0 && minutes == 0) {
          return secondsString;
        } else if (hours == 0) {
          return minutesString + ":" + secondsString;
        } else {
          return hoursString + ":" + minutesString + ":" + secondsString;
        }
      } else {
        return hoursString + ":" + minutesString + ":" + secondsString;
      }
    } catch {
      return 0;
    }
  };
  toHHMMFromMinutes(toTalMinutes, autoHideIfEqualZero = true) {
    try {
      var minutesInt = parseInt(toTalMinutes);
      var hours = Math.floor(minutesInt / 60);
      var minutes = Math.floor(minutesInt - hours * 60);
      var hoursString = hours < 10 ? "0" + hours : hours;
      var minutesString = minutes < 10 ? "0" + minutes : minutes;
      if (autoHideIfEqualZero) {
        if (hours == 0 && minutes == 0) {
          return 0;
        } else if (hours == 0) {
          return minutesString;
        } else {
          return hoursString + ":" + minutesString;
        }
      } else {
        return hoursString + ":" + minutesString;
      }
    } catch {
      return "error";
    }
  }
  //text
  removeComma(text) {
    try {
      return text.toString().replace(/,/g, "");
    } catch {
      return "";
    }
  }
  // device
  async getUUID() {
    let res: any = "";
    try {
      res = await this.uniqueDeviceID.get();
    } catch {}
    return res;
  }
  async getDeviceInfo() {
    // let uid = await this.uniqueDeviceID.get();
    var dua = [
      "", // [0] => Id
      this.device.model, // Iphone 5.1
      this.device.platform, // [2] IOS - Mac OS
      this.device.manufacturer,
    ];
    return dua;
  }
  //ip
  async getIpCurrentDevice() {
    try {
      if (Variables.isMobile) {
        let result = null; // await this.networkInterface.getCarrierIPAddress();
        return result.ip;
      }
    } catch (error) {
      return null;
    }
  }
  // validate
  // phone
  isPhone(phone) {
    var phoneNum = phone.replace(/[^\d]/g, "");
    if (phoneNum.length > 6 && phoneNum.length < 11) {
      return true;
    }
    return false;
  }
  // object
  isEquivalentValueOfObject(a, b) {
    return JSON.stringify(a) == JSON.stringify(b);
  }
  getValueFromObject(key, obj) {
    try {
      if (obj && obj != {}) {
        if (obj.hasOwnProperty(key)) {
          return obj[key];
        }
      }
      return undefined;
    } catch (error) {
      return undefined;
    }
  }
  public logoutThanhVien(): any {
    localStorage.removeItem(commonParams.roleThanhVien);
    Variables.tokenThanhVien = null;
    Variables.infoThanhVien = {};
    // TranXuanDuc 20200408 bổ sung Variables.isLoginThanhVien = false;
    Variables.isLoginThanhVien = false;
    Variables.countNotification = 0;
    this.mainService.raoService.onReloadNotification.next(true);
    this.events.publish("counNotification", Variables.countNotification);
  }
  public logoutAdmin(): any {
    localStorage.removeItem(commonParams.roleAdmin);
    Variables.tokenAdmin = null;
    Variables.infoAdmin = {};
  }
  //tronghuu95 20190409 044400 hàm build LinkImage by Id
  builLinkImage(textLink, fileId, prefixSize = "") {
    textLink = textLink || "no-image";
    fileId = fileId || -1;
    var arrRewriteUrl = textLink.split("/");
    textLink = arrRewriteUrl[arrRewriteUrl.length - 1];
    let apiCdnHost;
    if (apiCdn.endsWith("/")) {
      apiCdnHost = apiCdn + "image/";
    } else {
      apiCdnHost = apiCdn + "/image/";
    }
    if (fileId < 1) {
      // 20191127 TranXuanDuc bổ sung return this.getLinkImageNotExisted();
      return this.getLinkImageNotExisted();
    }
    return apiCdnHost + textLink + "-" + fileId + "j" + prefixSize + ".jpg";
  }
  // 20191127 TranXuanDuc bổ sung getLinkImageNotExisted
  getLinkImageNotExisted() {
    return "/assets/no-image.jpg";
  }
  getThisYear() {
    return this.getDateServe().getFullYear();
  }
  // định dạng lại số điện thoại chuẩn
  formatNumberPhoneSantandize(numberPhone) {
    if (!numberPhone) {
      return null;
    }
    numberPhone = numberPhone.replace(/\D/g, ""); // loại bỏ các ký tự không phải số điện thoại
    if (numberPhone[0] != "0") {
      numberPhone = "0" + numberPhone;
    }
    return numberPhone;
  }
  // [recheck] tronghuu20191016144500 hàm này có thể loại bỏ đc rồi
  stringFormat(text: string, ...args): string {
    try {
      return new ScanPipe().transform(text, args);
    } catch {
      return "";
    }
  }
  // dùng để bổ sung UrlHinhDaiDien cho array
  formatUrlHinhDaiDienByListData(
    data,
    size: any = 0,
    attr: any = "IdFileDaiDien",
    attrMapping: any = "UrlHinhDaiDien"
  ) {
    try {
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        let idFileDaiDien = this.getData(element, attr, -1);
        data[index][attrMapping] = this.builLinkImage(
          element.RewriteUrl || "no-image",
          idFileDaiDien,
          size
        );
      }
      return data;
    } catch {
      return [];
    }
  }
  formatUrlHinhDaiDienTinRaoByListData(
    data,
    size: any = 0,
    attr: any = "ListHinhAnhJson",
    attrMapping: any = "UrlHinhDaiDien"
  ) {
    try {
      data = data.map(element =>{
        try {
          if(element[attr])
          {
            var lstIdFileDaiDien = JSON.parse(element[attr]);
            if(lstIdFileDaiDien.length > 0) element[attrMapping] =this.builLinkImage(element.RewriteUrl || "no-image", lstIdFileDaiDien[0],size);
          }
        } catch (error) {
          
        }
        return element
      })
      return data;
    } catch {
      return [];
    }
  }
  getParamsUrl(srtUrl, key): string {
    try {
      var url = new URL(srtUrl);
      return url.searchParams.get(key);
    } catch {
      return null;
    }
  }
  deleteParamsUrl(srtUrl, key): string {
    try {
      var url = new URL(srtUrl);
      url.searchParams.delete(key);
      return url.href;
    } catch {
      return null;
    }
  }
  hasParamsUrl(srtUrl, key: any): boolean {
    if (key && srtUrl) {
      try {
        let lkey = this.splitStringToArray(key, ",");
        for (let index = 0; index < lkey.length; index++) {
          const key = lkey[index].toString().trim();
          let value = this.getParamsUrl(srtUrl, key);
          if (value) {
            return true;
          }
        }
      } catch (error) {}
    }
    return false;
  }
  setParamsUrl(srtUrl, key, value): string {
    try {
      var url = new URL(srtUrl);
      // If your expected result is "http://foo.bar/?x=42&y=2"
      url.searchParams.set(key, value);
      return url.href;
    } catch {
      return null;
    }
  }
  replaceSizeImagesByViewPort(str): string {
    if (!str) return "";
    let reg = /[0-9]+(j[\d]*)\.(?:jpg|gif|png)/;
    let n = Variables.nView || "";
    return str.replace(reg, function (match, g1) {
      return match.replace(`${g1}`, `j${n}`);
    });
  }
  fetchImageMeta(image: HTMLImageElement, src: string): Promise<any> {
    return new Promise((resolve, error) => {
      image.src = src;
      image.onload = resolve; // eslint-disable-line no-param-reassign
      image.onerror = error; // eslint-disable-line no-param-reassign
    });
  }
  async getImageFromURL(url: string) {
    let image = new Image();
    try {
      await this.fetchImageMeta(image, url);
    } catch (error) {}
    return image;
  }
  async getBase64ImageFromURL(url: string) {
    let image = new Image();
    image.crossOrigin = "Anonymous";
    try {
      await this.fetchImageMeta(image, url);
    } catch (error) {}
    return this.getBase64Image(image);
  }
  getBase64Image(img: HTMLImageElement) {
    // We create a HTML canvas object that will create a 2d image
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    // This will draw image
    ctx.drawImage(img, 0, 0);
    // Convert the drawn image to Data URL
    var dataURL = canvas.toDataURL("image/jpeg");
    return dataURL;
    //  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
  getMinYearDoiXe() {
    return "1990";
  }
  getMaxYearDoiXe() {
    return this.getThisYear().toString();
  }
  async getCurrentPosition() {
    let resp: any;
    try {
      resp = await this.geolocation.getCurrentPosition();
      return resp.coords;
    } catch (error) {}
    return {
      latitude: 0,
      longitude: 0,
    };
  }
  async getLocationUser() {
    try {
      let resp = await this.getCurrentPosition();
      var geocoder = new google.maps.Geocoder();
      var latlng = { lat: resp.latitude, lng: resp.longitude };
      geocoder.geocode({ location: latlng }, function (results, status) {
        if (status === "OK") {
          if (results[0]) {
            localStorage.setItem("userLocation", JSON.stringify(results));
          } else {
            console.log("No results found");
          }
        } else {
          console.log("Geocoder failed due to: " + status);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }
  toISOString(date: any, def?: Date) {
    try {
      return new Date(date).toISOString();
    } catch (error) {
      if (def) {
        return def.toISOString();
      }
      return this.newDate().toISOString();
    }
  }
  convertTimeRangeToString(pDate) {
    let currentServerDatetime = this.getDateServe();
    let pDatetime = this.newDate(pDate);
    let res = "";
    if (pDatetime >= currentServerDatetime) {
      res = "vài giây trước";
      return res;
    }
    try {
      let datediff =
        (currentServerDatetime.getTime() - pDatetime.getTime()) / 1000 / 60;
      if (datediff != null) {
        res = Math.round(datediff) + " phút trước";
        if (datediff >= 60) {
          // 60 mins
          datediff = datediff / 60;
          res = Math.round(datediff) + " giờ trước";
          if (datediff >= 24) {
            // 24 hours
            res = this._datePipe.transform(pDate, "dd-MM-yyyy");
          }
        }
      }
    } catch {}
    return res;
  }
  newDate(date?: any) {
    if (!date) return this.getDateServe();
    try {
      date = date.replace(/-/g, "/").replace("T", " ").replace(/\..+/, "");
    } catch (error) {}
    return new Date(date);
  }
  formatMinutesToDHM(data) {
    let seconds = data * 60;
    let days = Math.floor(seconds / 86400);
    seconds -= days * 86400;
    let hours = Math.floor(seconds / 3600) % 24;
    seconds -= hours * 3600;
    let minutes = Math.floor(seconds / 60) % 60;
    if (days && hours && minutes) {
      return days + " ngày " + hours + " tiếng " + minutes + " phút ";
    }
    if (!days && hours && minutes) {
      return hours + " tiếng " + minutes + " phút ";
    }
    if (days && !hours && minutes) {
      return days + " ngày " + minutes + " phút ";
    }
    if (days && hours && !minutes) {
      return days + " ngày " + hours + " tiếng ";
    }
    if (!days && hours && !minutes) {
      return hours + " tiếng ";
    }
    if (days && !hours && !minutes) {
      return days + " ngày ";
    }
    if (!days && !hours && minutes) {
      return minutes + " phút ";
    } else {
      return days + " ngày ";
    }
  }
  getDaysInMonth(month: number, year: number) {
    return new Date(year, month, 0).getDate();
  }
  sumByProInArray(arrayObject: any, pro: string) {
    return arrayObject.reduce((acc) => acc + arrayObject[pro], 0);
  }
  dateToDayMonthYear(date) {
    return this._datePipe.transform(date, "dd-MM-yyyy");
  }
  getWxhByUrl(url: string) {
    try {
      let match = new MatchPipe().transform(url, "j(\\d*)\\.");
      return +match[1];
    } catch (error) {}
    return -1;
  }
  getIdByUrlDailyXe(url: string) {
    try {
      let match = new MatchPipe().transform(url, "-(\\d*)j\\d*\\.");
      return +match[1];
    } catch (error) {}
    return -1;
  }
  diffObj(obj1, obj2) {
    try {
      return !_.isEqual(obj1 || {}, obj2 || {});
    } catch (error) {}
    return true; //mặt định là khác
  }
  // ==== Save Data Personalize ====
  getDataPersonalizeLS() {
    let dataLS = {};
    try {
      dataLS = JSON.parse(localStorage.getItem(keyPersonalizeLS)) || {};
    } catch (error) {}
    return dataLS;
  }
  setDataPersonalizeLS(dataLS) {
    if (dataLS) {
      localStorage.setItem(keyPersonalizeLS, JSON.stringify(dataLS));
    }
  }
  //  Math.ceil((Math.abs(dateNow.getTime() - (new Date(item["date"])).getTime()) / (1000 * 3600 * 24))) <= 10
  //Add vào tin like
  addDataPersonalizeLS(
    idMember: any = 0,
    key: any,
    value: any,
    replace: any = false
  ) {
    let dataLS = this.getDataPersonalizeLS() || {};
    let version = dataLS["versionStructor"];
    let dataMember = dataLS[idMember] || {};
    let dataByKey = dataMember[key] || [];
    let dateNow = new Date();
    if (version != versionStructorPersonalizeLS) {
      dataLS = {};
      dataLS["versionStructor"] = versionStructorPersonalizeLS;
      dataByKey = replace ? {} : [];
      dataLS[idMember] = {};
    }
    if (dataByKey && dataByKey.length == maxItemInArrayPersonalize) {
      this.removeFirstItemsInArray(
        dataByKey,
        countItemRemoveInArrayPersonalize
      );
    }

    let newObj = {};
    newObj["date"] = dateNow;
    newObj["value"] = value;
    if (!replace) {
      dataByKey = dataByKey.filter((item) => value != item["value"]);
      dataByKey.push(newObj);
    } else {
      dataByKey = newObj;
    }
    dataMember[key] = dataByKey;
    dataLS[idMember] = dataMember;
    this.setDataPersonalizeLS(dataLS);
    return true;
  }
  removeFirstItemsInArray(arr, count) {
    arr = arr.splice(0, count);
  }
  //Add vào tin like
  syncAllPersonalizeLS(idMember: any = 0, key: any, values: any) {
    let dataLS = this.getDataPersonalizeLS();
    let dataMember = dataLS[idMember] || {};
    let dataByKey = [];
    let dateNow = new Date();
    if (values && values.length > 0) {
      let arrValues = values.split(",");
      let newObj = {};
      arrValues.forEach((value) => {
        newObj = {};
        newObj["date"] = dateNow;
        newObj["value"] = value;
        dataByKey.push(newObj);
      });
    }
    dataMember[key] = dataByKey;
    dataLS[idMember] = dataMember;
    this.setDataPersonalizeLS(dataLS);
    return true;
  }
  removeDataPersonalizeLS(idMember: any = 0, key: any, value: any) {
    let dataLS = this.getDataPersonalizeLS();
    let version = dataLS["versionStructor"];
    let dataMember = dataLS[idMember] ? dataLS[idMember] : {};
    let dataByKey = dataMember[key] || [];
    if (version != versionStructorPersonalizeLS) {
      dataLS = {};
      dataLS["versionStructor"] = versionStructorPersonalizeLS;
      dataByKey = [];
      dataLS[idMember] = {};
    }
    dataByKey = dataByKey.filter((item) => value != item["value"]);
    dataLS[idMember][key] = dataByKey;
    this.setDataPersonalizeLS(dataLS);
    return true;
  }
  removeAllDataPersonalizeLS(idMember: any = 0, key: any) {
    let dataLS = this.getDataPersonalizeLS();
    dataLS[idMember] = dataLS[idMember] || {};
    let version = dataLS["versionStructor"];
    if (version != versionStructorPersonalizeLS) {
      dataLS = {};
      dataLS["versionStructor"] = versionStructorPersonalizeLS;
      dataLS[idMember] = {};
    }
    dataLS[idMember][key] = [];
    this.setDataPersonalizeLS(dataLS);
    return true;
  }
  async storeToken() {
    try {
      let isAdmin: boolean = false;
      var idAdmin = await this.getInfoAdmin(commonAttr.id);
      if (idAdmin) {
        isAdmin = true;
      }
      var tokenDevice = await this.fcm.getToken();
      var uuid = Variables.uniqueDeviceID;
      if (uuid && uuid != "" && uuid != "undefined") {
        await firebase
          .database()
          .ref("admindevices/" + uuid)
          .once("value")
          .then((res: any) => {
            let tokenDatabase = res.val();
            if (
              tokenDatabase &&
              tokenDatabase != "" &&
              tokenDatabase != null &&
              tokenDatabase.tokenThanhVien != tokenDevice
            ) {
              this.angularFireDatabase
                .object("/admindevices/" + uuid)
                .set({
                  uid: uuid,
                  tokenDevice: tokenDevice,
                  idAdmin: idAdmin,
                  isAdmin: isAdmin,
                })
                .then(() => {
                  console.log("Update Token Admin/Member:==", uuid);
                })
                .catch(() => {});
            } else {
              this.angularFireDatabase
                .object("/admindevices/" + uuid)
                .set({
                  uid: uuid,
                  tokenDevice: tokenDevice,
                  idAdmin: idAdmin,
                  isAdmin: isAdmin,
                })
                .then(() => {
                  console.log("Create Token:==", uuid);
                })
                .catch(() => {});
            }
          });
      }
    } catch (error) {
      console.log(error.toString());
    }
  }

  async removeAdminFromToken() {
    var uuid = Variables.uniqueDeviceID;
    var tokenDevice = await this.fcm.getToken();
    var idAdmin = await this.getInfoAdmin(commonAttr.id);
    console.log("Start removeAdminFromToken:==", uuid);
    this.angularFireDatabase
      .object("/admindevices/" + uuid)
      .set({
        uid: uuid,
        tokenDevice: tokenDevice,
        idAdmin: null,
        isAdmin: null,
      })
      .then(() => {
        console.log("removeAdminFromToken:==", uuid);
      })
      .catch(() => {});
  }

  // Chỗ sử dụng:
  // a. Ưu đãi -> send all
  // b. Upload Tin Rao -> send to admin
  // c. Thông báo -> Notification System
  // 1: Send All
  // 2: Send to Specific User
  // 3: Send to Login
  // 4: Send to Group
  // 5: Send to Admin - Post need to be approve
  async storeMessageSendNotification(data) {
    data.Data = data.Data ? JSON.stringify(data.Data) : "";
    var linkToRedirect =
      data.Link && data.Link.indexOf("raovat/") > -1
        ? linkFirebaseToPage.inApp + "_" + (data.Link || "")
        : linkFirebaseToPage.outApp + "_" + (data.Link || "");

    var currentDate = this.convertDate(new Date(), "dd/MM/yyyy HH:mm");

    if (data.Type == 1) {
      this.angularFireDatabase
        .list(this.firemsg)
        .push({
          From: data.TieuDe,
          Type: data.Type,
          message: data.NoiDung,
          data: data.Data,
          link: linkToRedirect,
          DateCreated: currentDate,
        })
        .then(() => {})
        .catch(() => {});
    }
    if (data.Type == 2) {
      this.angularFireDatabase
        .list(this.firemsg)
        .push({
          From: data.TieuDe,
          Type: data.Type,
          message: data.NoiDung,
          ToIdThanhVien: data.IdThanhVien || null,
          To: data.TenThanhVien || null,
          data: data.Data,
          link: linkToRedirect,
          DateCreated: currentDate,
        })
        .then(() => {})
        .catch(() => {});
    }
    if (data.Type == 3) {
      this.angularFireDatabase
        .list(this.firemsg)
        .push({
          From: data.TieuDe,
          Type: data.Type,
          message: data.NoiDung,
          data: data.Data,
          link: linkToRedirect,
          DateCreated: currentDate,
        })
        .then(() => {})
        .catch(() => {});
    }
    if (data.Type == 4) {
      this.angularFireDatabase
        .list(this.firemsg)
        .push({
          From: data.TieuDe,
          Type: data.Type,
          message: data.NoiDung,
          ToIdGroup: data.IdUserGroup,
          ToLstIdThanhVien: data.GhiChu,
          data: data.Data,
          link: linkToRedirect,
          DateCreated: currentDate,
        })
        .then(() => {})
        .catch(() => {});
    }
    if (data.Type == 5) {
      // Notify Admin when post news
      this.angularFireDatabase
        .list(this.firemsg)
        .push({
          From: data.TieuDe,
          FromIdThanhVien: data.FromIdThanhVien,
          FromTenThanhVien: data.FromTenThanhVien,
          Type: data.Type,
          message: data.NoiDung,
          data: data.Data,
          link: linkFirebaseToPage.inAppAdminDuyetTin + "_" + data.Id,
          DateCreated: currentDate,
        })
        .then(() => {})
        .catch(() => {});
    }
  }

  // Chỗ sử dụng:
  // a. Ưu đãi -> send all
  // b. Upload Tin Rao -> send to admin
  // c. Thông báo -> Notification System
  // 1: Send All
  // 2: Send to Specific User
  // 3: Send to Login
  // 4: Send to Group
  // 5: Send to Admin - Post need to be approve
  async storeMessageSendNotificationFirestore(data) {
    data.Data = data.Data ? JSON.stringify(data.Data) : "";
    var linkToRedirect =
      data.Link && data.Link.indexOf("raovat/") > -1
        ? linkFirebaseToPage.inApp + "_" + (data.Link || "")
        : linkFirebaseToPage.outApp + "_" + (data.Link || "");

    var currentDate = this.convertDate(new Date(), "dd/MM/yyyy HH:mm");
    const id = this.database.createId();

    let insertData = {
      From: data.TieuDe || "",
      Type: data.Type || "",
      message: data.NoiDung || "",
      ToIdThanhVien: data.IdThanhVien || null,
      To: data.TenThanhVien || null,
      data: data.Data || "",
      link: linkToRedirect || "",
      DateCreated: currentDate || "",
      ToIdGroup: data.IdUserGroup || "",
      ToLstIdThanhVien: data.GhiChu || "",
    };

    console.log("data", data);

    this.messagesCollection
      .doc(id)
      .set(insertData)
      .then(() => console.log("Success"))
      .catch((err) => console.log("Error:", err));
  }

  convertFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  }
  getFullAddressContact(data: any) {
    let dc = "";
    if (data) {
      if (data.DiaChi) {
        dc = `${data.DiaChi}`;
      }
      if (data.TenQuanHuyen) {
        dc = `${dc}, ${data.TenQuanHuyen}`;
      }
      if (data.TenTinhThanh) {
        dc = `${dc}, ${data.TenTinhThanh}`;
      }
    }
    if (dc) {
      return dc;
    }
    return labelPages.noneUpdate;
  }
  buildTitleDangTin(data: DangTin) {
    let title: string = "Bán/Mua";
    try {
      if (data.IdDongXe > 0 || data.IdLoaiXe > 0) {
        if (data.Type == 1) {
          if (data.IdDongXe > 0) {
            title = `Bán ${this.getData(data, "TenDongXe", "")}`;
            title =
              +data.NamSanXuat > 0 ? `${title} ${data.NamSanXuat}` : title;
          }
        } else {
          if (data.IdLoaiXe > 0 && data.IdDongXe > 0) {
            title = `Mua ${this.getData(data, "TenLoaiXe", "")}`;
            title =
              +data.IdDongXe > 0
                ? `${title} ${this.getData(data, "TenDongXe", "")}`
                : title;
          } else if (data.IdDongXe > 0) {
            title = `Mua ${this.getData(data, "TenDongXe", "")}`;
          } else if (data.IdLoaiXe > 0) {
            title = `Mua ${this.getData(data, "TenLoaiXe", "")}`;
          }
        }
        return title;
      }
    } catch (error) {}
    return title;
  }
  // tronghuu95 20190423 090800 dùng để định dạng numberphone theo dạng chuẩn
  formatNumberPhone(numberPhone, countryCode = "+84") {
    if (!numberPhone) {
      return null;
    }
    if (numberPhone[0] != "+" && numberPhone[0] == "0") {
      numberPhone = countryCode + numberPhone.substr(1, numberPhone.length);
    }
    return numberPhone;
  }
  formatNumberPhoneByCountryCode(
    phoneNumber: string,
    countryCode: string = "vn"
  ) {
    if (!phoneNumber) {
      return null;
    }
    switch (countryCode) {
      case "vn":
        phoneNumber = phoneNumber.replace("+84", "0");
        break;
      default:
        phoneNumber = phoneNumber.replace("+84", "0");
    }
    return phoneNumber;
  }
  /**
   * getMaLayoutAlbum: Get mã danh sách
   * @param listImage : danh sách hình ảnh
   * result: layout_${type}${length}
   */
  async getMaLayoutAlbum(listImage: string[]) {
    let ma = "layout_none";
    try {
      let mainUrlImage = listImage[0];
      let maxImageForLayout = defaultValue.maxImageForLayout;
      let length = listImage.length;
      length = length > maxImageForLayout ? maxImageForLayout : length;
      let mainImage = await this.getImageFromURL(mainUrlImage);
      let minus = mainImage.width - mainImage.height;
      let minusPercentAbs = Math.abs(
        (1 * mainImage.height - mainImage.width) / mainImage.height
      );
      let type = TypeLayoutAlbum.horizontal;
      if (minusPercentAbs <= defaultValue.percentImageForLayout) {
        type = TypeLayoutAlbum.square;
      } else {
        type =
          minus > 0 ? TypeLayoutAlbum.horizontal : TypeLayoutAlbum.vertical;
      }
      ma = `layout_${type}${length}`;
    } catch (error) {}
    return ma;
  }
  /**
   * getMaLayoutAlbumByIds => get mã Album từ mãng id
   * @param ids : mãng id
   * @param albumName: tên albumName
   */
  async getMaLayoutAlbumByIds(ids: any) {
    let listImage = [];
    try {
      if (!(ids instanceof Array)) {
        ids = JSON.parse(ids.toString());
      }
      ids = new TrurthifyPipe().transform(ids.map(Number));
      ids.forEach((id) => {
        listImage.push(this.builLinkImage("", id));
      });
    } catch (error) {}
    return await this.getMaLayoutAlbum(listImage);
  }
  checkPassForRaoVat(value: string) {
    return value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/);
  }
  /**
   * buildHoTen => build full name từ HoDem va Ten
   * @param data : info thông tin thành viên
   * result: `${data.HoDem} ${data.Ten}`
   */
  buildHoTen(data: any) {
    return `${data.HoDem} ${data.Ten}`;
  }
  /**
   * getDateServe: get time từ timeDateServer
   */
  getDateServe() {
    return new Date(CoreVariables.timeDateServer);
  }
  /**
   * compareTwoDateByMinutes so sánh Date tới đơn vị phút
   * @param date1
   * @param date2
   * result:
   * 0: date1 == date2;
   * 1: date1 > date2;
   * -1:  date1 < date2
   */
  compareTwoDateByMinutes(date1: Date, date2: Date) {
    let timeSpan1 = this.convertDatebyMinutes(date1).getTime();
    let timeSpan2 = this.convertDatebyMinutes(date2).getTime();
    return timeSpan1 == timeSpan2 ? 0 : timeSpan1 > timeSpan2 ? 1 : -1;
  }
  convertDatebyMinutes(date1: Date) {
    let timeSpan1: any;
    try {
      timeSpan1 = date1.setSeconds(0, 0);
    } catch (error) {
      timeSpan1 = this.newDate(date1).setSeconds(0, 0);
    }
    return new Date(timeSpan1);
  }
  /**
   * alertInput: alert dạng input
   * @param message
   * @param options
   */
  alertInput(message: string, options?: any): Promise<string> {
    options = options || {};
    return new Promise(async (resolve, reject) => {
      let alert = await this.alertController.create({
        header: options.header,
        message: message,
        inputs: [
          {
            name: "value",
            type: "text",
            placeholder: options.placeholder || "Lý do không duyệt",
          },
        ],
        buttons: [
          {
            text: options.labelNo || labelPages.cancel,
            handler: (event) => resolve(commonException.cancel),
          },
          {
            text: options.labelYes || labelPages.ok,
            handler: (event) => resolve(event.value),
          },
        ],
      });
      await alert.present();
    });
  }

  alertConfirm(message: string, options?: any): Promise<boolean> {
    options = options || {};
    return new Promise(async (resolve, reject) => {
      const alert = await this.alertController.create({
        header: options.header,
        message: message,
        buttons: [
          {
            text: options.labelNo || labelPages.cancel,
            handler: () => resolve(false),
          },
          {
            text: options.labelYes || labelPages.ok,
            handler: () => resolve(true),
          },
        ],
      });
      await alert.present();
    });
  }
  alertConfirmV1(message: string, options?: any): Promise<number> {
    options = options || {};
    return new Promise(async (resolve, reject) => {
      const alert = await this.alertController.create({
        header: options.header,
        message: message,
        buttons: [
          {
            text: options.labelCancel || labelPages.exit,
            handler: (_) => resolve(-1),
          },
          {
            text: options.labelIgnore || labelPages.ignore,
            handler: (_) => resolve(0),
          },
          {
            text: options.labelRetry || labelPages.retry,
            handler: (_) => resolve(1),
          },
        ],
      });
      await alert.present();
    });
  }
  alertNotification(message: string, options?: any): Promise<boolean> {
    options = options || {};
    return new Promise(async (resolve, reject) => {
      let alert = await this.alertController.create({
        header: options.header,
        message: message,
        buttons: [
          {
            text: options.labelYes || "Ok",
            handler: (_) => resolve(true),
          },
        ],
      });
      await alert.present();
    });
  }
  /**
   *
   * @param deepLink: link web
   * @param hasFl : == true là ưu tiên web, == false là ưu tiên store
   */
  buildDynamicLinks(deepLink: string, hasFl: boolean = true) {
    if (!deepLink) return deepLink;
    try {
      //link: deeplink
      //apn: The package name of the Android app to use to open the link.
      //ibi: The bundle ID of the iOS app to use to open the link.
      //afl == ifl: The link to open when the app isn't installed.
      //&isi=${Variables.appStoreID}
      if (hasFl) {
        return `${Variables.domainDynamicLinks}/?link=${deepLink}&apn=${Variables.packageIdAndroid}&ibi=${Variables.bundleId}&afl=${deepLink}&ifl=${deepLink}&isi=${Variables.appStoreID}&efr=1`;
      } else {
        return `${Variables.domainDynamicLinks}/?link=${deepLink}&apn=${Variables.packageIdAndroid}&ibi=${Variables.bundleId}&isi=${Variables.appStoreID}&efr=1`;
      }
    } catch (error) {}
    return "";
  }

  //action là actionPage.tinRao
  //typePage là ký tự viết tắt của page
  deepLinkTinTucWithDomain(id, rewriteUrl) {
    try {
      let rewriteLink = `${Variables.domainDeepLinks}/${Variables.actionDeepLink}/${rewriteUrl}-${id}${commonParams.tinRao}.html`;
      return rewriteLink;
    } catch (error) {}
    return "";
  }
  deepLinkInstallWithDomain() {
    try {
      let rewriteLink = `${Variables.domainDeepLinks}/${
        Variables.actionDeepLink
      }?appinstall=${this.convert_toMD5(
        "d@i" + this.getInfoThanhVien(commonAttr.id)
      )}`;
      return rewriteLink;
    } catch (error) {}
    return "";
  }
  buildDynamicLinks_TinRao(id: any, rewriteUrl: string) {
    let deepLink = this.deepLinkTinTucWithDomain(id, rewriteUrl);
    return this.buildDynamicLinks(deepLink);
  }
  buildDynamicLinks_Install() {
    let deepLink = this.deepLinkInstallWithDomain();
    return this.buildDynamicLinks(deepLink, false);
  }
  // {id: matchGroup[1], typePage: matchGroup[2]}
  getInfoRewriteLinkWithDomain(linkRewriteWithDomain: string) {
    let matchGroup = linkRewriteWithDomain.match(/-(\d+)(\w+)\.html/);
    return { id: matchGroup[1], typePage: matchGroup[2] };
  }
  addThanhVienLogined(infoThanhVien: any) {
    if (infoThanhVien && infoThanhVien.Id > 0 && infoThanhVien.DienThoai) {
      let lUserOnFinger = this.getThanhVienLogined() || [];
      let findIndex = lUserOnFinger.findIndex((x) => x.Id == infoThanhVien.Id);
      if (findIndex >= 0) {
        lUserOnFinger[findIndex].UrlHinhDaiDien = infoThanhVien.UrlHinhDaiDien;
        lUserOnFinger[findIndex].IdFileDaiDien = infoThanhVien.IdFileDaiDien;
        lUserOnFinger[findIndex].HoTen = infoThanhVien.HoTen;
      } else {
        lUserOnFinger.push({
          Id: infoThanhVien.Id,
          UrlHinhDaiDien: infoThanhVien.UrlHinhDaiDien,
          IdFileDaiDien: infoThanhVien.IdFileDaiDien,
          HoTen: infoThanhVien.HoTen,
          DienThoai: infoThanhVien.DienThoai,
          IsFinger: false,
        });
      }
      localStorage.setItem(
        localStorageData.lUserOnFinger,
        JSON.stringify(lUserOnFinger)
      );
    }
  }
  addThanhVienFinger(infoThanhVien: any) {
    if (infoThanhVien && infoThanhVien.Id > 0 && infoThanhVien.DienThoai) {
      let lUserOnFinger = this.getThanhVienLogined() || [];
      let findIndex = lUserOnFinger.findIndex((x) => x.Id == infoThanhVien.Id);
      if (findIndex >= 0) {
        lUserOnFinger[findIndex].UrlHinhDaiDien = infoThanhVien.UrlHinhDaiDien;
        lUserOnFinger[findIndex].IdFileDaiDien = infoThanhVien.IdFileDaiDien;
        lUserOnFinger[findIndex].HoTen = infoThanhVien.HoTen;
        lUserOnFinger[findIndex].IsFinger = true;
      } else {
        lUserOnFinger.push({
          Id: infoThanhVien.Id,
          UrlHinhDaiDien: infoThanhVien.UrlHinhDaiDien,
          IdFileDaiDien: infoThanhVien.IdFileDaiDien,
          HoTen: infoThanhVien.HoTen,
          DienThoai: infoThanhVien.DienThoai,
          IsFinger: true,
        });
      }
      localStorage.setItem(
        localStorageData.lUserOnFinger,
        JSON.stringify(lUserOnFinger)
      );
    }
  }
  removeAllThanhVienFinger() {
    let lUserOnFinger = this.getThanhVienLogined() || [];
    lUserOnFinger.forEach((element, index) => {
      if (lUserOnFinger[index].IsFinger) {
        this.keychainTouchIdDelete(lUserOnFinger[index].DienThoai);
        lUserOnFinger[index].IsFinger = false;
      }
    });
    localStorage.setItem(
      localStorageData.lUserOnFinger,
      JSON.stringify(lUserOnFinger)
    );
  }
  removeThanhVienFinger(infoThanhVien: any) {
    let lUserOnFinger = this.getThanhVienLogined() || [];
    let findIndex = lUserOnFinger.findIndex((x) => x.Id == infoThanhVien.Id);
    if (findIndex >= 0) {
      lUserOnFinger[findIndex].UrlHinhDaiDien = infoThanhVien.UrlHinhDaiDien;
      lUserOnFinger[findIndex].IdFileDaiDien = infoThanhVien.IdFileDaiDien;
      lUserOnFinger[findIndex].RewriteUrl =
        infoThanhVien.RewriteUrl || infoThanhVien.HoTen;
      lUserOnFinger[findIndex].HoTen = infoThanhVien.HoTen;
      lUserOnFinger[findIndex].IsFinger = false;
      this.keychainTouchIdDelete(infoThanhVien.DienThoai);
    }
    localStorage.setItem(
      localStorageData.lUserOnFinger,
      JSON.stringify(lUserOnFinger)
    );
  }
  getThanhVienLogined() {
    try {
      let data =
        JSON.parse(localStorage.getItem(localStorageData.lUserOnFinger)) || [];
      data = this.formatUrlHinhDaiDienByListData(data);
      return data;
    } catch (error) {
      return [];
    }
  }
  async isAvailableTouchId() {
    try {
      // let res = await this.faio.isAvailable();
      // if (res == 'finger' || res == 'face')
      return true;
    } catch (error) {
      return false;
    }
  }
  checkDeviceIOS() {
    if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
      return true;
    }
    return false;
  }

  async shareNative(link: string) {
    this.platform.ready().then(async () => {
      if (link && link.length > 0) {
        try {
          // await this.socialSharing.share(link);
        } catch (error) {
          alert(error);
        }
      }
    });
  }
  async hasTouchId(key: string) {
    try {
      let res = await this.keychainTouchIdHas(key);
      return res == "OK";
    } catch (error) {
      return false;
    }
  }
  async keychainTouchIdHas(numberPhone) {
    try {
      let res = await this.storage.get("Fingerprint-DailyXe" + numberPhone);
      return res ? "OK" : "NO";
    } catch (error) {
      this.createMessageError(error);
    }
    return "NO";
  }
  async keychainTouchIdSave(numberPhone, passtoMD5) {
    try {
      // let res = await this.faio.show({
      // 	clientId: 'Fingerprint-DaiLyXe', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      // 	clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      // 	disableBackup: true,  //Only for Android(optional)
      // 	localizedFallbackTitle: 'Use Pin', //Only for iOS
      // 	localizedReason: 'Vui lòng xác thực' //Only for iOS
      // })
      await this.storage.set("Fingerprint-DailyXe" + numberPhone, passtoMD5);
      return "success";
    } catch (error) {
      // this.createMessageError(error);
    }
    return "error";
  }
  async keychainTouchIdVerify(numberPhone, message) {
    try {
      // await this.faio.show({
      // 	clientId: 'Fingerprint-DaiLyXe', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      // 	clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      // 	disableBackup: true,  //Only for Android(optional)
      // 	localizedFallbackTitle: 'Use Pin', //Only for iOS
      // 	localizedReason: 'Vui lòng xác thực' //Only for iOS
      // })
      return await this.storage.get("Fingerprint-DailyXe" + numberPhone);
    } catch (error) {
      // this.createMessageError(error);y
    }
    return null;
  }
  async keychainTouchIdDelete(numberPhone) {
    try {
      await this.storage.remove("Fingerprint-DailyXe" + numberPhone);
      return "OK";
    } catch (error) {
      this.createMessageError(error);
    }
    return "NO";
  }
  // nên là password
  async saveTouchId(numberPhone: any, password: any) {
    if (!(await this.isAvailableTouchId())) return false;
    try {
      let passtoMD5 = this.convert_toMD5(password);
      let res = await this.keychainTouchIdSave(numberPhone, passtoMD5);
      if (res == "success") {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      if (
        error ==
        "Failed to encrypt the data with the generated key: IllegalBlockSizeException: null"
      ) {
        let resAlert = await this.alertConfirm(commonMessages.M061);
        if (resAlert) {
          this.openNativeSettings
            .open("application_details")
            .then((res: any) => console.log(res))
            .catch((error: any) => console.error(error));
        }
      } else {
        this.createMessageErrorByTryCatch(error);
      }
      return false;
    }
  }
  async deleteTouchId(numberPhone: any) {
    if (!(await this.isAvailableTouchId())) return false;
    try {
      let res = await this.keychainTouchIdDelete(numberPhone);
      if (res == "OK") {
        this.removeThanhVienFinger(Variables.infoThanhVien);
        return true;
      } else {
        return false;
      }
    } catch (error) {
      this.createMessageErrorByTryCatch(error);
      return false;
    }
  }
  async verifyTouchId(
    numberPhone: any,
    message: any = "Đăng nhập bằng Touch ID cho DaiLyXe"
  ) {
    if (!(await this.isAvailableTouchId())) return false;
    try {
      try {
        let res = await this.keychainTouchIdVerify(numberPhone, message);
        return res;
      } catch (error) {
        if (error == -1) {
          // Xác thực vân nhiều lần
          this.createMessageError("Vui lòng kiểm tra lại vân tay của bạn");
          return null;
        }
        if (error == -2) {
          // Hủy bỏ
          return null;
        }
        if (error == -3) {
          // Chọn nhập mật khẩu
          this.createMessageError(
            "Đăng nhập bằng tài khoản và mật khẩu hoặc thử lại sau!"
          );
          return null;
        }
        if (
          error ==
          "Failed to encrypt the data with the generated key: IllegalBlockSizeException: null"
        ) {
          let resAlert = await this.alertConfirm(commonMessages.M061);
          if (resAlert) {
            this.openNativeSettings
              .open("application_details")
              .then((res: any) => console.log(res))
              .catch((error: any) => console.error(error));
          }
        } else {
          this.createMessageError("Bật đăng nhập bằng vân tay thất bại!");
        }
        return null;
      }
    } catch (error) {
      this.createMessageErrorByTryCatch(error);
      return null;
    }
  }
  async openIframeByLink(link: any, title: any = "Nội dung") {
    this.modal = await this.modalController.create({
      component: ViewBaiVietComponent,
      componentProps: { link: link, title: title },
    });
    this.modal.onDidDismiss().then((res: any) => {});
    await this.modal.present();
  }
  async openIframeByLinkDieuKhoan(title: any = "Điều khoản & Điều kiện") {
    return this.openIframeByLink(linkNoiDung.dieuKhoan, title);
  }
  getRaoBackgroud(idTinRao: any) {
    try {
      let detailRao = localStorage.getItem(localStorageData.detailRao);
      detailRao = JSON.parse(detailRao)[idTinRao];
      return detailRao;
    } catch (error) {}
    return null;
  }
  setRaoBackgroud(idTinRao: any, value: any) {
    try {
      let detailRao: any = localStorage.getItem(localStorageData.detailRao);
      detailRao = JSON.parse(detailRao || "{}");
      detailRao[idTinRao] = value;
      localStorage.setItem(
        localStorageData.detailRao,
        JSON.stringify(detailRao)
      );
    } catch (error) {}
    return null;
  }
  removeRaoBackgroud(idTinRao: any) {
    try {
      let detailRao: any = localStorage.getItem(localStorageData.detailRao);
      detailRao = JSON.parse(detailRao || "{}");
      detailRao[idTinRao] = undefined;
      localStorage.setItem(
        localStorageData.detailRao,
        JSON.stringify(detailRao)
      );
    } catch (error) {}
    return null;
  }
  removeAllRaoBackgroud() {
    try {
      localStorage.removeItem(localStorageData.detailRao);
    } catch (error) {}
    return null;
  }
  buildFileNameForFile(fileName: string, index?: any) {
    try {
      fileName = fileName || "new-file";
      fileName = fileName.convertrUrlPrefix();
      let idThanhVien = this.getData(Variables.infoThanhVien, "Id", -1);
      let tvId = "_" + commonParams.tv + idThanhVien;
      fileName = fileName + tvId;
      if (index > 0) {
        fileName += "_" + index;
      }
      let md5FileName = this.convert_toMD5(fileName);
      let first6characters = md5FileName.substring(md5FileName.length - 6);
      fileName = `${fileName}-${first6characters}`;
      return fileName;
    } catch (error) {}
    return fileName;
  }
  async getTypeByBase64Image(base64Image: string) {
    let htmlImage = await this.getImageFromURL(base64Image);
    return await this.getTypeByHTMLImageElement(htmlImage);
  }
  async getTypeByHTMLImageElement(htmlImage: HTMLImageElement) {
    // let mainImage = await this.getImageFromURL(htmlImage);
    let minus = htmlImage.width - htmlImage.height;
    let minusPercentAbs = Math.abs(
      (1 * htmlImage.height - htmlImage.width) / htmlImage.height
    );
    let type = TypeLayoutAlbum.horizontal;
    if (minusPercentAbs <= defaultValue.percentImageForLayout) {
      type = TypeLayoutAlbum.square;
    } else {
      type = minus > 0 ? TypeLayoutAlbum.horizontal : TypeLayoutAlbum.vertical;
    }
    return type;
  }
  async getRatioImage(base64Image: string) {
    //Vuông maximun = 900x900;
    //Chữ nhật nằm maximun = 1200x900;
    //Chữ nhật đứng maximun = 900x1200;
    let x = Variables.compressedForImageX; //===900
    let y = Variables.compressedForImageY; //===1200
    let htmlImage = await this.getImageFromURL(base64Image);
    let type = await this.getTypeByHTMLImageElement(htmlImage);
    let ratio = 100;
    switch (type) {
      //ngang
      case TypeLayoutAlbum.horizontal:
        if (htmlImage.width > x) {
          ratio = Math.round((x / htmlImage.width) * 100);
        }
        break;
      //dọc
      case TypeLayoutAlbum.vertical:
        if (htmlImage.height > y) {
          ratio = (y / htmlImage.height) * 100;
        }
        break;
      //vuông
      default:
        if (htmlImage.height > x) {
          ratio = (x / htmlImage.height) * 100;
        }
        break;
    }
    return ratio;
  }
  async convertImageResizer(base64Image: string) {
    if (!base64Image) return base64Image;
    if (this.isURl(base64Image)) {
      base64Image = await this.getBase64ImageFromURL(base64Image);
    }
    let byteCount = this.imageCompress.byteCount(base64Image);
    if (byteCount > 0 && byteCount > Variables.limitByteForImage)
      try {
        let ratio = await this.getRatioImage(base64Image);
        if (ratio > 1 && ratio < 100) {
          let quality = Variables.limitQualityForImage; // chắc lượng hình ảnh
          let imageCompressFile = await this.imageCompress.compressFile(
            base64Image,
            1,
            ratio,
            quality
          );
          let byteCountCompressFile = this.imageCompress.byteCount(
            imageCompressFile
          );
          if (byteCountCompressFile > 1000000) {
            base64Image = imageCompressFile;
          }
        }
      } catch (error) {
        this.createMessageErrorByTryCatch(error);
      }
    return base64Image;
  }

  public getTokenAdmin() {
    return Variables.tokenAdmin;
  }
  public getTokenThanhVien() {
    return Variables.tokenThanhVien;
  }
  public getInfoThanhVien(getProperty?: string) {
    let info: any = Variables.infoThanhVien || {};
    try {
      info.UrlHinhDaiDien = this.builLinkImage(
        info.RewriteUrl,
        info.IdFileDaiDien
      );
      if (getProperty) {
        let dataProperty = info[getProperty];
        if (getProperty.toLowerCase() == "ismain") {
          dataProperty =
            dataProperty.toString().toLowerCase() == "true" ? true : false;
        }

        return dataProperty;
      }
    } catch (error) {}
    if (getProperty) return null;
    return info || {};
  }
  public getInfoAdmin(getProperty?: string) {
    let info: any = Variables.infoAdmin || {};
    try {
      info.UrlHinhDaiDien = this.builLinkImage(
        info.RewriteUrl,
        info.IdFileDaiDien
      );
      if (getProperty) {
        let dataProperty = info[getProperty];
        if (getProperty.toLowerCase() == "ismain") {
          dataProperty =
            dataProperty.toString().toLowerCase() == "true" ? true : false;
        }
        return dataProperty;
      }
    } catch (error) {}
    return info || {};
  }

  public diffObject(obj1: any, obj2: any) {
    try {
      let diffObj = new DiffObjPipe().transform(obj1, obj2);
      return !(Object.keys(diffObj).length > 0);
    } catch (error) {}
    return false;
  }
  public telByNumberPhone(number) {
    if (number) {
      window.open(`tel:${number}`, "_system");
    }
  }
  public convertParamsSearch(search: any) {
    let numberBoxSearch = 0;
    if (search.isPinTop) {
      numberBoxSearch++;
    }
    if (search.idTinhThanh && search.idTinhThanh > -1) {
      numberBoxSearch++;
    }
    if (search.idSortBy && search.idSortBy > 0) {
      numberBoxSearch++;
    }
    if (search.tinhTrang && search.tinhTrang > 0) {
      numberBoxSearch++;
    }
    if (search.idDongXe && search.idDongXe > -1) {
      numberBoxSearch++;
    }
    if (search.idLoaiXe && search.idLoaiXe > -1) {
      numberBoxSearch++;
    }
    if (search.price) {
      if (
        search.price.lower != defaultPrice.lower ||
        search.price.upper != defaultPrice.upper
      ) {
        numberBoxSearch++;
      }
      if (search.price.lower >= search.price.upper) {
        search.price = {};
        search.price.lower = defaultPrice.lower;
        search.price.upper = defaultPrice.upper;
      }
    }
    return { numberBoxSearch: numberBoxSearch, search: search };
  }
  public copyText(text: string) {
    try {
      this.clipboard.copy(text);
      this.createMessageSuccess(commonMessages.M091);
      return true;
    } catch (error) {}
    this.createMessageError(commonMessages.M092);
    return false;
  }
  public pasteText(text: string) {
    try {
      this.clipboard.paste();
      this.createMessageSuccess(commonMessages.M093);
      return true;
    } catch (error) {}
    this.createMessageError(commonMessages.M094);
    return false;
  }
  public updateVariables(config: any) {
    let vars = this.cloneObject(Variables);
    try {
      Variables.setDomainConfigDoamin(config);
      let keys = Object.keys(config) || [];
      keys.forEach((k) => {
        Variables[k] = config[k];
      });
      return true;
    } catch (error) {
      Object.keys(vars).forEach((k) => {
        Variables[k] = vars[k];
      });
    }
    return false;
  }
  public isCheckNewEmailDaXacThuc(email) {
    let obJNewEamil = this.getListNewEmailDaXacThuc();
    return obJNewEamil[email] == true;
  }
  public setNewEmailDaXacThuc(email) {
    try {
      let obJNewEamil = this.getListNewEmailDaXacThuc();
      obJNewEamil[email] == true;
      localStorage.setItem(
        localStorageData.lNewEmailDaXacThuc,
        JSON.stringify(obJNewEamil)
      );
      return true;
    } catch (error) {}
    return false;
  }
  public deleteNewEmailDaXacThuc(email) {
    try {
      let obJNewEamil = this.getListNewEmailDaXacThuc();
      obJNewEamil[email] == undefined;
      localStorage.setItem(
        localStorageData.lNewEmailDaXacThuc,
        JSON.stringify(obJNewEamil)
      );
    } catch (error) {}
  }
  public getListNewEmailDaXacThuc() {
    try {
      let strEmailDaXacThuc = localStorage.getItem(
        localStorageData.lNewEmailDaXacThuc
      );
      let obJNewEamil = JSON.parse(strEmailDaXacThuc);
      return obJNewEamil || {};
    } catch (error) {}
    return {};
  }
  public async removeObjTimeByEmail() {
    await this.storage.remove(localStorageData.verifyEmail);
  }
  public async removeObjTimeByNumberPhone() {
    await this.storage.remove(localStorageData.verifyNumberPhone);
  }
  //#region ================================================ verify code Email =============================
  public async getObjTimeEmail() {
    let data = await this.storage.get(localStorageData.verifyEmail);
    return data ? JSON.parse(data) : {};
  }
  public async getObjTimeByEmail(email) {
    let data = await this.getObjTimeEmail();
    return data[email] || {};
  }
  public async setObjTimeByEmail(email, obj: any) {
    try {
      let data = await this.getObjTimeEmail();
      data[email] = obj;
      let strObj = JSON.stringify(data || {});
      await this.storage.set(localStorageData.verifyEmail, strObj);
      return true;
    } catch (error) {}
    return false;
  }
  public async initTimeEmailVerify() {
    let dataObjTime = await this.getObjTimeEmail();
    let lEmail = Object.keys(dataObjTime);
    for (let index = 0; index < lEmail.length; index++) {
      const email = lEmail[index];
      await this.initTimeVerifyByEmail(email);
      await this.initTimeBlockByEmail(email);
    }
  }
  public async initTimeVerifyByEmail(email) {
    Variables.timeVerifyByEmail = Variables.timeVerifyByEmail || {};
    let objectEmail: any = await this.getObjTimeByEmail(email);
    objectEmail[TypeSmsValidation.sendEmail] =
      objectEmail[TypeSmsValidation.sendEmail] || {};
    let longTime = objectEmail[TypeSmsValidation.sendEmail].longTime;
    if (longTime > 0) {
      Variables.timeVerifyByEmail[email] =
        Variables.timeVerifyByEmail[email] || {};
      if (Variables.timeVerifyByEmail[email].setInterval) {
        clearInterval(Variables.timeVerifyByEmail[email].setInterval);
      }
      Variables.timeVerifyByEmail[email].value = longTime;
      Variables.timeVerifyByEmail[email].setInterval = setInterval(() => {
        Variables.timeVerifyByEmail[email].value -= 1;
        objectEmail[TypeSmsValidation.sendEmail] =
          objectEmail[TypeSmsValidation.sendEmail] || {};
        objectEmail[TypeSmsValidation.sendEmail].longTime =
          Variables.timeVerifyByEmail[email].value;
        this.setObjTimeByEmail(email, objectEmail);
        if (
          Variables.timeVerifyByEmail[email].setInterval &&
          Variables.timeVerifyByEmail[email].value <= 0
        ) {
          clearInterval(Variables.timeVerifyByEmail[email].setInterval);
        }
      }, 1000);
    }
  }
  public async initTimeBlockByEmail(email) {
    Variables.timeVerifyByEmail = Variables.timeVerifyByEmail || {};
    let objectEmail: any = await this.getObjTimeByEmail(email);
    objectEmail[localStorageData.timeBlock] =
      objectEmail[localStorageData.timeBlock] || {};
    let longTime = objectEmail[localStorageData.timeBlock].longTime;
    if (longTime > 0) {
      Variables.timeVerifyByEmail[email] =
        Variables.timeVerifyByEmail[email] || {};
      Variables.timeVerifyByEmail[email][localStorageData.timeBlock] =
        Variables.timeVerifyByEmail[email][localStorageData.timeBlock] || {};
      if (
        Variables.timeVerifyByEmail[email][localStorageData.timeBlock]
          .setInterval
      ) {
        clearInterval(
          Variables.timeVerifyByEmail[email][localStorageData.timeBlock]
            .setInterval
        );
      }
      Variables.timeVerifyByEmail[email][
        localStorageData.timeBlock
      ].value = longTime;
      Variables.timeVerifyByEmail[email][
        localStorageData.timeBlock
      ].setInterval = setInterval(() => {
        Variables.timeVerifyByEmail[email][
          localStorageData.timeBlock
        ].value -= 1;
        objectEmail[localStorageData.timeBlock] =
          objectEmail[localStorageData.timeBlock] || {};
        objectEmail[localStorageData.timeBlock].longTime =
          Variables.timeVerifyByEmail[email][localStorageData.timeBlock].value;
        if (
          Variables.timeVerifyByEmail[email][localStorageData.timeBlock]
            .setInterval &&
          Variables.timeVerifyByEmail[email][localStorageData.timeBlock]
            .value <= 0
        ) {
          objectEmail[localStorageData.historyResend] = undefined;
          this.setObjTimeByEmail(email, objectEmail);
          clearInterval(
            Variables.timeVerifyByEmail[email][localStorageData.timeBlock]
              .setInterval
          );
        } else {
          this.setObjTimeByEmail(email, objectEmail);
        }
      }, 1000);
    }
  }
  //#region =============================================== end verify code Email ==========================
  //#region ================================================ verify code NumberPhone =============================
  public async getObjTimeNumberPhone() {
    let data = await this.storage.get(localStorageData.verifyNumberPhone);
    return data ? JSON.parse(data) : {};
  }
  public async getObjTimeByNumberPhone(numberPhone) {
    let data = await this.getObjTimeNumberPhone();
    return data[numberPhone] || {};
  }
  public async setObjTimeByNumberPhone(numberPhone, obj: any) {
    try {
      let data = await this.getObjTimeNumberPhone();
      data[numberPhone] = obj;
      let strObj = JSON.stringify(data || {});
      await this.storage.set(localStorageData.verifyNumberPhone, strObj);
      return true;
    } catch (error) {}
    return false;
  }
  public async initTimeNumberPhoneVerify() {
    let dataObjTime = await this.getObjTimeNumberPhone();
    let lNumberPhone = Object.keys(dataObjTime);
    for (let index = 0; index < lNumberPhone.length; index++) {
      const numberPhone = lNumberPhone[index];
      await this.initTimeVerifyByNumberPhone(
        numberPhone,
        TypeSmsValidation.ForgetPassword
      );
      await this.initTimeVerifyByNumberPhone(
        numberPhone,
        TypeSmsValidation.Regist
      );
      await this.initTimeVerifyByNumberPhone(
        numberPhone,
        TypeSmsValidation.updateInfoPerson
      );
      await this.initTimeBlockByNumberPhone(numberPhone);
    }
  }
  public async initTimeVerifyByNumberPhone(numberPhone, typeSmsValidation) {
    Variables.timeVerifyByNumberPhone = Variables.timeVerifyByNumberPhone || {};
    let objectNumberPhone: any = await this.getObjTimeByNumberPhone(
      numberPhone
    );
    objectNumberPhone[typeSmsValidation] =
      objectNumberPhone[typeSmsValidation] || {};
    let longTime = objectNumberPhone[typeSmsValidation].longTime;
    if (longTime > 0) {
      Variables.timeVerifyByNumberPhone[numberPhone] =
        Variables.timeVerifyByNumberPhone[numberPhone] || {};
      if (Variables.timeVerifyByNumberPhone[numberPhone].setInterval) {
        clearInterval(
          Variables.timeVerifyByNumberPhone[numberPhone].setInterval
        );
      }
      Variables.timeVerifyByNumberPhone[numberPhone].value = longTime;
      Variables.timeVerifyByNumberPhone[numberPhone].setInterval = setInterval(
        () => {
          Variables.timeVerifyByNumberPhone[numberPhone].value -= 1;
          objectNumberPhone[typeSmsValidation] =
            objectNumberPhone[typeSmsValidation] || {};
          objectNumberPhone[typeSmsValidation].longTime =
            Variables.timeVerifyByNumberPhone[numberPhone].value;
          this.setObjTimeByNumberPhone(numberPhone, objectNumberPhone);
          if (
            Variables.timeVerifyByNumberPhone[numberPhone].setInterval &&
            Variables.timeVerifyByNumberPhone[numberPhone].value <= 0
          ) {
            clearInterval(
              Variables.timeVerifyByNumberPhone[numberPhone].setInterval
            );
          }
        },
        1000
      );
    }
  }
  public async initTimeBlockByNumberPhone(numberPhone) {
    Variables.timeVerifyByNumberPhone = Variables.timeVerifyByNumberPhone || {};
    let objectNumberPhone: any = await this.getObjTimeByNumberPhone(
      numberPhone
    );
    objectNumberPhone[localStorageData.timeBlock] =
      objectNumberPhone[localStorageData.timeBlock] || {};
    let longTime = objectNumberPhone[localStorageData.timeBlock].longTime;
    if (longTime > 0) {
      Variables.timeVerifyByNumberPhone[numberPhone] =
        Variables.timeVerifyByNumberPhone[numberPhone] || {};
      Variables.timeVerifyByNumberPhone[numberPhone][
        localStorageData.timeBlock
      ] =
        Variables.timeVerifyByNumberPhone[numberPhone][
          localStorageData.timeBlock
        ] || {};
      if (
        Variables.timeVerifyByNumberPhone[numberPhone][
          localStorageData.timeBlock
        ].setInterval
      ) {
        clearInterval(
          Variables.timeVerifyByNumberPhone[numberPhone][
            localStorageData.timeBlock
          ].setInterval
        );
      }
      Variables.timeVerifyByNumberPhone[numberPhone][
        localStorageData.timeBlock
      ].value = longTime;
      Variables.timeVerifyByNumberPhone[numberPhone][
        localStorageData.timeBlock
      ].setInterval = setInterval(() => {
        Variables.timeVerifyByNumberPhone[numberPhone][
          localStorageData.timeBlock
        ].value -= 1;
        objectNumberPhone[localStorageData.timeBlock] =
          objectNumberPhone[localStorageData.timeBlock] || {};
        objectNumberPhone[localStorageData.timeBlock].longTime =
          Variables.timeVerifyByNumberPhone[numberPhone][
            localStorageData.timeBlock
          ].value;
        if (
          Variables.timeVerifyByNumberPhone[numberPhone][
            localStorageData.timeBlock
          ].setInterval &&
          Variables.timeVerifyByNumberPhone[numberPhone][
            localStorageData.timeBlock
          ].value <= 0
        ) {
          objectNumberPhone[localStorageData.historyResend] = undefined;
          this.setObjTimeByNumberPhone(numberPhone, objectNumberPhone);
          clearInterval(
            Variables.timeVerifyByNumberPhone[numberPhone][
              localStorageData.timeBlock
            ].setInterval
          );
        } else {
          this.setObjTimeByNumberPhone(numberPhone, objectNumberPhone);
        }
      }, 1000);
    }
  }
  //#region =============================================== end verify code NumberPhone ==========================
  calcValueUuDai(dataUuDai, tongTien, tmpPoint: number) {
    let res: any = {
      pointUuDai: 0,
      percentUuDai: 0,
      tieuDeUuDai: 0,
      tienGiamUuDai: 0,
      isQualified: false,
    };

    try {
      let data = this.cloneObject(dataUuDai);
      if (!data) {
        return res;
      }

      //let ma = data.MarketingCode_Ext && data.MarketingCode_Ext.Code ? data.MarketingCode_Ext.Code : "";
      let valueType = +data.ValueType || -1;
      let maxValue = +data.MaxValue || 999999; // Check lại 999 999 => %
      let value = +data.Value || 0;
      res.tieuDeUuDai = data.TieuDe || "";
      switch (valueType) {
        case 1: {
          // Point: Check if qualified (>= MaxValue) => apply promotion
          maxValue = data.MaxValue || 0; // Chọn Point
          if (+tmpPoint >= maxValue) {
            res.pointUuDai = value;
          } else {
            res.isQualified = false;
            return res;
          }
          break;
        }
        case 2: {
          //  %
          res.percentUuDai = value;
          // Tính xem % là bao nhiều tiền
          let tongTienTemp = (value * tongTien) / 100;
          // Kiểm tra nếu số tiền vượt quá giới hạn thì tiền giảm tối đa là maxValue.
          res.tienGiamUuDai = tongTienTemp > maxValue ? maxValue : tongTienTemp;
          break;
        }
        case 3: {
          // Cash tiền
          res.tienGiamUuDai = value;
          break;
        }
        default: {
          //statements;
          break;
        }
      }
    } catch {}

    res.isQualified = true;
    return res;
  }
  buildNoiDungChuyenKhoan(value) {
    let infoThanhVien = Variables.infoThanhVien;
    let hoTen = infoThanhVien ? infoThanhVien.HoTen : infoThanhVien.DienThoai;
    hoTen = new LatinisePipe().transform(hoTen);
    return "[" + hoTen + " - mua " + value + " point]";
  }
  public transformDailyTimeAgo(inputDate: any) {
    if (!(inputDate instanceof Date)) {
      inputDate = this.newDate(inputDate);
    }
    if (!inputDate || (!inputDate.getTime && !inputDate.toDate)) {
      return "Ngày không hợp lệ";
    }
    const past = inputDate.toDate ? inputDate.toDate() : inputDate.getTime();
    const now = CoreVariables.timeDateServer;
    if (past > now) {
      return "Đang cập nhật";
    }
    for (
      let i = 0,
        l = DaiLyXeTimeAgoPipe.MAPPER.length,
        ms = now - past,
        div = DaiLyXeTimeAgoPipe.YEAR_MS;
      i < l;
      ++i
    ) {
      const elm = DaiLyXeTimeAgoPipe.MAPPER[i];
      const unit = Math.floor(ms / (div /= elm.div));
      if (unit >= 1) {
        return unit === 1 ? elm.single : `${unit} ${elm.many} trước`;
      }
    }
    return "Mới đây";
  }

  /**
   * formatTuKhoaTimKiem: định dạng lại trước khi gửi lên server elasticsearch
   */
  formatTuKhoaTimKiem(tuKhoaTimKiem: string) {
    //Loại bỏ dấu tiếng việt
    //Replace các ký tự không phỉ chữ cũng không phải số thành ""
    //Loại bỏ khoảng trắng thừa ở giữa 2 từ
    //Loại bỏ khoảng trắng thừa ở 2 đầu của chuỗi
    tuKhoaTimKiem = tuKhoaTimKiem
      .replace(
        /[^\wÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s ]/g,
        " "
      )
      .replace(/\W+/g, " ")
      .replace(/ +/g, " ")
      .trim();

    return tuKhoaTimKiem;
  }

  copyCode(code) {
    if (code) {
      this.clipboard.copy(code);
      this.createMessageSuccess(commonMessages.M100);
    }
  }

  buildContentShare(code, link) {
    let str = "";
    if (link && code) {
      str =
        'Dùng mã giới thiệu "' +
        code +
        '" để được tặng điểm khi tải ứng dụng RaoXe tại: ' +
        link;
    }
    return str;
  }

  randomCode(maCauHinh) {
    var strDate = this.convertDate(new Date(), "ddMMyyyy HHmmss");
    var encrypted = this.convert_toMD5(strDate);
    var res =
      maCauHinh +
      encrypted.substring(1, 2) +
      encrypted.substring(encrypted.length - 2) +
      strDate.substring(strDate.length - 2);
    return res;
  }

  removeHtml(str) {
    if (str) {
      return str.replace(/(<([^>]+)>)/gi, "");
    }
    return "";
  }

  checkPluginAvailability(namPlugin: string) {
    return checkAvailability("cordova.plugin." + namPlugin) === true;
  }

  checkMaXacThuc(code) {
    return code && code.length == 6;
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public buildMoTaNgan(moTaNgan = "") {
    let moTaNganTemp = this.removeHtml(moTaNgan);
    let str =
      moTaNganTemp &&
      moTaNganTemp.length > defaultValue.maxLengthMoTaNganTinDang
        ? '<span class="view-more"><span>...</span>Xem thêm</span>'
        : "";
    if (moTaNgan && moTaNgan.endsWith("</p>")) {
      moTaNgan = moTaNgan.replace(/<\/p>/g, str + "</p>");
    } else if (moTaNgan && moTaNgan.endsWith("</p")) {
      moTaNgan = moTaNgan.replace(/<\/p/g, str + "</p>");
    } else if (moTaNgan && moTaNgan.endsWith("</")) {
      moTaNgan = moTaNgan.replace(/<\//g, str + "</p>");
    } else if (moTaNgan && moTaNgan.endsWith("<")) {
      moTaNgan = moTaNgan.replace(/</g, str + "</p>");
    } else {
      moTaNgan = moTaNgan + str;
    }
    return moTaNgan;
  }
}
