import { Injectable, Type } from "@angular/core";
import { HttpHeaders, HttpParams } from "@angular/common/http";
import { CoreVariables } from './core-variables';
@Injectable()
export class optionsCms {
	public search: any = {};
	public haveCheckBox: boolean = false;
	public hideShowColumns: boolean = false;
	public saveLocalColumns: boolean = false;
	public detailIsDialog: boolean = true;
	constructor() {
	}
}
@Injectable()
export class Files {
	TenAdminTao?: string;
	TenAdminCapNhat?: string;
	TenAdminXoa?: string;
	CapDo?: number;
	Id: number;
	FolderId: number;
	Name?: string;
	FileSize?: number;
	CreatedDate?: string;
	DuplicateNumber?: number;
	ImageUrl?: any;
	Extension?: string;
	Width?: number;
	Height?: number;
	IdAdminTao?: number;
	NgayTao?: Date;
	IdAdminCapNhatnumber?: number;
	NgayCapNhat?: Date;
	FullName?: string;
	IdAdminXoanumber?: number;
	NgayXoa?: Date;
	TrangThai: number;
	GhiChu?: string;
	TieuDe?: string;
	TuKhoaTimKiem?: string;
	Folder?: number;
	UrlHinhAnh: string = null!;
	UrlFile?: string;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
		this.FolderId = -1;
	}
}
export class Folders {
	Id: number;
	ParentId?: number;
	Name?: string;
	FullName?: string;
	CreatedDate?: Date;
	IdUserTao?: number
	NgayTao?: Date;
	IdUserCapNhat?: number;
	NgayCapNhat?: Date;
	CapDo?: number;
	DuplicateNumber?: number;
	TextSort?: string;
	ThuTu?: number;
	IdFoldersParentList?: string;
	TrangThai?: number;
	constructor() {
		this.Id = -1;
	}
}
// GridBottomInstance
export interface GridBottomInstance {
	//số item hiển thị tối đa 1 lần
	displayItem?: number;
	// page đang chọn
	currentPage?: number;
	//Tổng số Item
	totalItems?: number;
	//chiều dài của navi
	paginationRange?: number;
	totalPages?: number;
}
export interface ImagesInstance {
	id: string,
	filename?: string,
	src: string
}
export class Admin {
	Id: number;
	DienThoai?: string;
	Email?: string;
	HoTen?: string;
	IdAdminCapNhat?: number;
	IdAdminTao?: number;
	IdNhomQuyen: number;
	IsMain: boolean;
	IdFileDaiDien?: number;
	FileDaiDien_FullName?: string;
	FileDaiDien_Ext?: string;
	MatKhau?: string;
	MoTaTrang?: string;
	NgayCapNhat?: Date;
	NgayTao?: Date;
	RewriteUrl?: string;
	TenAdminCapNhat?: string;
	TenAdminTao?: string;
	TenDangNhap?: string;
	ThuTu?: number;
	TieuDeTrang?: string;
	TrangThai: number;
	TuKhoaTimKiem?: string;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
		this.IsMain = false;
		this.IdNhomQuyen = -1;
	}
}
//Trọng Hữu 22/09/2017  label là ý nghĩa của chuỗi Rexge, value là chuỗi Rexge muốn định dạng 
export class Validation {
	label: string;
	value: RegExp;
	constructor(label: string, value: RegExp) {
		this.label = label;
		this.value = value;
	}
}
export class ComponentInstance {
	constructor(public component: Type<any>, public data?: any, public params?: any) { }
}
export class RedirectLink {
	Id: number;
	LinkCu?: string;
	LinkMoi?: string;
	IdAdminTao?: number;
	NgayTao?: Date;
	IdAdminCapNhat?: number;
	NgayCapNhat?: Date;
	constructor() {
		this.Id = -1;
	}
}
export class FileChiTietDangTin {
	id: string;
	noiDung: string;
	constructor(id: string, noiDung: string) {
		this.id = id;
		this.noiDung = noiDung;
	}
}
export class ItemRecord {
	label: string;
	value: any;
	searchString: string;
	public static constLabelName: string = "label";
	public static constValueName: string = "value";
	public static constSearchName: string = "searchString";
	constructor(label: string, value: any, searchString?: any) {
		this.label = label;
		this.value = value;
		this.searchString = searchString;
	}
}
export interface ModalBodyType {
	IntResult: any,
	StrResult: any,
	DataResult: any,
}
export class EntityListImageJson {
	IdFile: number;
	TieuDe: string;
	GhiChu: string;
	Link: string;
	ThuTu: string;
	constructor(
		IdFile: number,
		Link?: string,
		TieuDe?: string,
		GhiChu?: string,
		ThuTu?: string
	) {
		this.IdFile = IdFile;
		this.TieuDe = TieuDe || "";
		this.GhiChu = GhiChu || "";
		this.Link = Link || "";
		this.ThuTu = ThuTu || "";
	}
}
//tronghuu95 20180630
export class ResponseBase {
	IntResult: number = -1;
	StrResult: string = "";
	DataResult: any[] = null!;
	Pagination: any;
	Message: string = null;
	GotoLink?: string;
	PropertyNameList: any;
	constructor(obj?: any) {
		if (obj) Object.assign(this, obj);
	}
	public isSuccess() {
		return this.IntResult >= 0;
	}
	public getMessage(defaultNoMessage = "Vui lòng báo cáo với admin để khắc phục lỗi") {
		if (!this.Message || this.Message.length <= 0 || this.Message == "") {
			return defaultNoMessage;
		}
		return this.Message || defaultNoMessage;
	}
	public haveDataResult() {
		return this.DataResult && this.DataResult.length > 0 ? true : false;
	}
	public getFirstData() {
		try {
			return this.DataResult[0];
		} catch {
			return null;
		}
	}
	public getFirstDataId() {
		try {
			return this.DataResult[0].Id;
		} catch {
			return -1;
		}
	}
	public getTotalItems() {
		try {
			return this.Pagination.TotalItems;
		} catch {
			return -1;
		}
	}
}
export class optionsHttp {
	headers?: HttpHeaders | {
		[header: string]: string | string[];
	};
	observe?: 'body';
	params?: HttpParams | {
		[param: string]: string | string[];
	};
	reportProgress?: boolean;
	responseType!: 'arraybuffer' | string;
	withCredentials?: boolean;
	constructor() {
	}
}
export class MenuRaoVat {
	Id?: number;
	IdMenuParent?: number;
	IdFileDaiDien?: number;
	CapDo?: number;
	TenMenu?: string;
	RewriteUrl?: string;
	UrlPrexix?: string;
	TuKhoaTimKiem?: string;
	TieuDeTrang?: string;
	MoTaTrang?: string;
	GhiChu?: string;
	ThuTu?: number;
	TrangThai?: number;
	IdAdminTao?: number;
	NgayTao?: Date;
	IdAdminCapNhat?: number;
	NgayCapNhat?: Date;
	FileDaiDien_Ext: any;
	UrlHinhDaiDien: any;
	constructor() {
		this.Id = -1;
		this.ThuTu = 1;
	}

}
export class ThanhVien {
	Id: number;
	IdFileDaiDien: number = null!;
	HoTen: string = null!;
	HoDem: string = null!;
	Ten: string = null!;
	Email: string = null!;
	DienThoai: string = null!;
	DiaChi: string = null!;
	MatKhau: string = null!;
	GioiTinh: boolean = true;
	NgaySinh?: Date = null!;
	RewriteUrl: string = null!;
	FileDaiDien_Ext: any = null!;
	FileCMNDMatTruoc_Ext: any = null!;
	FileCMNDMatSau_Ext: any = null!;
	IdTinhThanh?: number = null!;
	AppIdFacebook: string = null!;
	AccessTokenFacebook: string = null!;
	LinkFacebook: string = null!;
	AppIdGoogle: string = null!;
	AccessTokenGoogle: string = null!;
	LinkGoogle: string = null!;
	TrangThai?: number = null!;
	MaCapLaiMatKhau: string = null!;
	DaXacNhanThanhVien: boolean = false;
	PartnerType?: number = null!;
	IdFileCmndTruoc?: number = null!;
	IdFileCmndSau?: number = null!;
	ListHinhAnhJson: string = null!;
	TuKhoaTimKiem: string = null!;
	GhiChu: string = null!;
	NgayTao?: Date = null!;
	NgayCapNhat?: Date = null!;
	IdAdminCapNhat: number = null!;
	AdminNgayCapNhat?: Date = null!;
	IdShowRoom: string = null!;
	ThoiGianDangTinDaMua?: number = null!;
	DisplayTotalTime?: number = null!;
	DisplayRemainingTime?: number = null!;
	GhiChuHistory: string = null!;
	DisplayGhiChuHistory: string = null!;
	Fax: string = null!;
	IdAdminTao: number = null!;
	IdMaHoa: number = null!;
	IdMatKhauMacDinh: number = null!;
	IdPhuongXa: number = null!;
	IdQuanHuyen: number = null!;
	TenDangNhap: string = null!;
	ThuTu: number = null!;
	Website: string = null!;
	DaXacNhanEmail: boolean = false;
	DaXacNhanDienThoai: boolean = false;
	IdRank: number = null!;
	MatKhauLai: string= null!;
	TenTinhThanh:string= null!;
	TenQuanHuyen:string= null!;
	FullDiaChi:string= null!;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
		this.GioiTinh = true;
	}
	constructorFromObj(obj?: any) {
		if (obj) Object.assign(this, obj);
	}
}
export class GiaoDichXe {
	Id: number;
	IdNguoiMua?: number = null!;
	IdNguoiBan?: number = null!;
	IdAdmin?: number = null!;
	TenXe: string = null!;
	GiaXe?: number = null!;
	NgayLienHe?: Date = null!;
	NgayMuaXe?: Date = null!;
	TuKhoaTimKiem: string = null!;
	Commission?: number = null!;
	CommissionStatus?: number = null!;
	TransactionStatus?: number = null!;
	TransactionMethod?: number = null!;
	NhuCauNguoiMua: string = null!;
	IdAdminTao?: number = null!;
	NgayTao?: Date;
	IdAdminCapNhat: number = null!;
	NgayCapNhat?: Date;
	ShowRoom: string = null!;
	Admin_Ext: any = null!;
	ThanhVienMua_Ext: ThanhVien = null!;
	ThanhVienBan_Ext: ThanhVien = null!;
	IsThanhVienMuaExist: boolean;
	TransactionStatus_Ext: string = null!;
	TransactionMethod_Ext: string = null!;
	CommissionStatus_Ext: string = null!;
	NoteMua: string = null!;
	Comment_Ext: string = null!;
	IdNguoiTao_Ext: string = null!;
	HoTenNguoiTao_Ext: string = null!;
	IdDinhNghiaSanPham?: number = null!;
	constructor() {
		this.Id = -1;
		this.IsThanhVienMuaExist = false;
		this.NgayLienHe = new Date(CoreVariables.timeDateServer);
	}
}
export class HistoryTransaction {
	Id: number;
	IdGiaoDichXe?: number = null!;
	IdNguoiTao: string = null!;
	HoTen: string = null!;
	Comment: string = null!;
	NgayTao?: Date;
	NgayTao_Ext: string = null!;
	constructor() {
		this.Id = -1;
	}
}
export class DangTin {
	Id: number = -1;
	IdMenu: number = null!;
	TenMenu: string = null!;
	IdThuongHieu: number = null!;
	TenThuongHieu: string = null!;
	IdThanhVien: number = null!;
	TenThanhVien: string = null!;
	IdFileDaiDien: number = null!;
	TieuDe: string = null!;
	MoTaNgan: string = null!;
	RewriteUrl: string = null!;
	TinhTrang: number = 1;
	Gia: number = null!;
	IdTinhThanh: number = null!;
	TenTinhThanh: string = null!;
	TuNgay: Date = new Date(CoreVariables.timeDateServer);;
	DenNgay: Date = new Date(new Date(CoreVariables.timeDateServer).setMonth(new Date(CoreVariables.timeDateServer).getMonth() + 1))
	TuKhoaTimKiem: string = null!;
	SoLuotXem: number = null!;
	IsDuyet: boolean = false;
	NgayDuyet: Date = null;
	IsPinTop: boolean = false;
	NgayPinTop: Date = null;
	GhiChu: string = null!;
	NgayUp: Date = null;
	TrangThai: number = 1;
	NgayTao: Date = null;
	NgayCapNhat: Date = null;
	IdAdminCapNhat: number = null!;
	AdminNgayCapNhat: Date = null;
	FromToPost_Ext: string = null!;
	FileDaiDien_Ext: any = null!;
	Type: number = 1;
	ListHinhAnhJson: any = null!;
	UrlHinhDaiDien: any = null!;
	ThanhVienDang: any = null!;
	IdQuanHuyen: number = null!;
	TenQuanHuyen: string = null!;
	IdMauNoiThat: number = null!;
	TenMauNoiThat: string = null!;
	UrlHinhDaiDienNoiThat: any = null!;
	IdMauNgoaiThat: number = null!;
	TenMauNgoaiThat: string = null!;
	UrlHinhDaiDienNgoaiThat: any = null!;
	IdDongXe: number = null!;
	TenDongXe: string = null!;
	IdLoaiXe: number = null!;
	TenLoaiXe: string = null!;
	DongCo?: string = null!;
	HopSo?: string = null!;
	SoChoNgoi: number = null!;
	SoCua: number = 4;
	NamSanXuat: number = null!;
	SoKmDaDi: number = null!;
	NamDangKyXe: number = null!;
	HoTenLienHe: string = null!;
	EmailLienHe: string = null!;
	DienThoaiLienHe: any = null!;
	DiaChiLienHe: string = null!;
	NhienLieu: number = 1;
	MucTieuHaoNhienLieu100: number = null!;
	ListHinhAnh: any = [];
	MoTaChiTiet: any;
	IdFileDaiDienThanhVien: number = null!;
	DangTinTraPhi_Ext: any;
	PinTopOnOff: any = false;
	IdDangTinContact: any;
	DangTinContact_Ext: any = null;
	MaLayoutAlbum: string = null;
	LayoutAlbum_Ext: any = null;
	TongImage: any = null;
	IdAdminDangKyDuyet: any = null;
	ListFilesUploaded: any = null;
	IdDaiLy: string = null;
	constructor() {

	}
}
export class UserDevice {
	Id: number = null!;
	UserId: number = null!;
	UserPhone: string = null!;
	UserEmail: string = null!;
	DeviceName: string = null!;
	DeviceId: string = null!;
	OsName: string = null!;
	IpAddress: string = null!;
	LoginDate: Date = null!;
	LogoutDate: Date = null!;
	LatitudeX: any;
	LatitudeY: any;
}
export class DangTinFavorite {
	Id: number = null!;
	IdUser: number = null!;
	IdDangTin: number = null!;
	Type: number = null!;
	NgayTao: Date = null!;
	NgayCapNhat: Date = null!;
}
export class DangTinRating {
	Id: number = null!;
	IdUser: number = null!;
	IdDangTin: number = null!;
	Rating: number = null!;
	NgayTao: Date = null!;
	NgayCapNhat: Date = null!;
	Description: string = null!;
	HoTenThanhVien: string = null!;
	TrangThai: number = null!;
	DangTin_Ext: DangTin = null!;
	urlHinhDaiDien: string = null!;
	constructor() {
		this.Rating = 0;
	}
	constructorFromObj(obj?: any) {
		if (obj) Object.assign(this, obj);
	}
}
export class UserRank {
	Id: number = null!;
	TieuDe: string = null!;
	MoTaNgan: string = null!;
	NgayTao: Date = null!;
	NgayCapNhat: Date = null!;
	IdAdminTao: number = null!;
	IdAdminCapNhat: number = null!;
	TrangThai: number = null!;
	PercentDiscount: number = null!;
	TuKhoaTimKiem: string = null!;
	PointRule: number = null!;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
	}
	// constructorFromObj(obj?: any) {
	// 	if (obj) Object.assign(this, obj);
	// }
}
export class UserPoint {
	Id: number = null!;
	IdThanhVien: number = null!;
	Point: number = null!;
	PointTmp = null!;
	NgayCapNhat: Date = null!;
	IdAdminTao: number = null!;
	IdAdminCapNhat: number = null!;
	TrangThai: number = null!;
	ThanhVien_Ext: ThanhVien = null!;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
	}
	constructorFromObj(obj?: any) {
		if (obj) Object.assign(this, obj);
	}
}

export class UserBlacklist {
	Id: number = null!;
	IdThanhVien: string = null!;
	TuKhoaTimKiem: string = null!;
	ListIdDangTin: string = null!;
	ListImage: string = null!;
	ListHinhAnh: any;
	TrangThai: number = null!;
	TuNgay: any = null!;
	DenNgay: any = null!;
	TenThanhVien: string = null!;
	DienThoai: string = null!;
	GhiChu: string = null!;
	ListHinhAnhJson: string = null!;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
	}
}

export class UserGroup {
	Id: number = null!;
	Title: string = null!;
	TrangThai: number = null!;
	LstIdUser_Ext: string = null!;
	LstIdUser: number[] = [];
	LstThanhVien_Ext: any[] = [];
	NgayTao: Date;
	NgayCapNhat: Date;
	IdAdminTao: number = null!;
	TuKhoaTimKiem: string = null!;
	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
	}
}

export class ThongBao {
	Id: number = null!;
	IdAdminTao: number = null!;
	NgayTao: Date;
	IdAdminCapNhat: number = null!;
	NgayCapNhat: Date;
	TuKhoaTimKiem: string = null!;
	TrangThai: number = null!;
	IsView: string = null!;
	TieuDe: string = null!;
	NoiDung: string = null!;
	Type: number = null!;
	Link: string = null!;
	IdThanhVien: number = null!;
	IdUserGroup: number = null!;
	TenThanhVien_Ext: any;
	GhiChu: string = null!;
	Data: any = null!;
	TuNgay: Date;
	DenNgay: Date;

	constructor() {
		this.Id = -1;
		this.TrangThai = 1;
	}
}

export class DangTinContact {
	Id: number = -1;
	IdThanhVien: number = null!;
	HoTen: string = null!;
	Email: string = null!;
	DienThoai: string = null!;
	IdTinhThanh: number = 31;
	TenTinhThanh: string = null;
	IdQuanHuyen: number = null!;
	TenQuanHuyen: string = null!;
	DiaChi: string;
	IsDefault: any;
	TrangThai: any = 1;
	FullDiaChi: any;
	constructor() {
		this.Id = this.Id || -1;
		this.TrangThai = this.TrangThai || 1;
	}


}

export class LayoutAlbum {
	Id: number = null!;
	IdFileDaiDien: number = null!;
	UrlHinhDaiDien: string = null!;
	NameClass: string = null!;
	TongImage: number = null!;
	TrangThai: number = null!;
	Ma: string = null!;

	constructor() {
		this.Id = this.Id || -1;
		this.TrangThai = this.TrangThai || 1;
	}
}
export class DangTinTraPhi {
	Id: number = null!;
	DangTin_Ext: any;
	IdLoaiTraPhi: number = null!;
	IdDangTin: number = null!;
	NgayBatDau: Date;
	NgayKetThuc: Date;
	ThoiGianSuDung: number = null!;
	IdThanhVienTao: number = null!;
	NgayTao: Date;
	IdThanhVienCapNhat: number = null!;
	NgayCapNhat: Date;
	NgayKetThucDuDinh: Date;
	ThoiGianSuDungDuDinh: number = null!;

	constructor() {
		this.Id = this.Id || -1;
	}
}


export class MarketingPlan {
	Id?: number;
	TieuDe?: string;
	MaCauHinh?: string;
	Value?: string;
	ValueType?: string;
	UnlimitedTime?: string;
	UnlimitedUsed?: string;
	MoTaNgan?: string;
	TieuDeTrang?: string;
	TrangThai: any = 1;
	GhiChu?: string;
	TuNgay?: Date;
	DenNgay?: Date;
	Type?: number;
	NgayTao?: Date;
	MaxValue?: string;
	MaxUsed?: string;
	TotalUsed?: number;
	constructor() {
		this.Id = -1;
	}

}

export class ErrorApi {
	Message: string = "";
	Error: any = {};
	Url: any = "";

	constructor() {
	}

}
