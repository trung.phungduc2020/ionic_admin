import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { Variables, commonParams, commonNavigate, commonMessages } from "./variables";
import { HttpClient, HttpParams } from '@angular/common/http';
import { CommonMethodsTs } from './common-methods';
import { optionsHttp } from './entity';
//tronghuu95 20190516 class này thay cho interceptor lý do là HTTP không qua lớp intercep nên sinh ra lớp này thay thế
@Injectable()
export class CoreHttp {
	// reg = /[\"]/g;
	// token: any;
	methodApi = {
		GET: "GET",
		DELETE: "DELETE",
		PUT: "PUT",
		POST: "POST",
	}
	// isAdmin = false;
	constructor(
		private httpClient: HttpClient,
		public commonMethodsTs: CommonMethodsTs) {
	}
	public get(requestUri: string, optionsHttp: optionsHttp): Observable<any> {
		if(!(requestUri.toLocaleLowerCase().indexOf("/elasticsearch") >= 0))
		{
			try {
				let params: any = optionsHttp.params || new HttpParams();
				let tuKhoaTimKiem = params.get(commonParams.tuKhoaTimKiem);
				if (tuKhoaTimKiem) {
					tuKhoaTimKiem = tuKhoaTimKiem ? this.commonMethodsTs.toASCII(tuKhoaTimKiem.toLocaleLowerCase()) : '';
					optionsHttp.params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem)
				}
			} catch (error) {
				let params: any = optionsHttp.params || {};
				let tuKhoaTimKiem = params[commonParams.tuKhoaTimKiem];
				if (tuKhoaTimKiem) {
					tuKhoaTimKiem = tuKhoaTimKiem ? this.commonMethodsTs.toASCII(tuKhoaTimKiem.toLocaleLowerCase()) : '';
					optionsHttp.params[commonParams.tuKhoaTimKiem] = tuKhoaTimKiem;
				}
			}
		}
		
		let options: any = optionsHttp;


		return this.httpClient.get(requestUri, options)
	}
	public del(requestUri: string, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.httpClient.delete(requestUri, options);

	}
	public put(requestUri: string, body: any, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.httpClient.put(requestUri, body, options);
	}
	public post(requestUri: string, body: any, optionsHttp: optionsHttp): Observable<any> {

		let options: any = optionsHttp;
		return this.httpClient.post(requestUri, body, options);
	}

}