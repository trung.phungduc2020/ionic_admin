
import { Injectable } from '@angular/core';
import { projectName, commonParams, Variables } from './variables';
import { TripleDESService } from '../shared/services/triple-des.service';

@Injectable()
export class RaoLocalStore {
    private tripleDes: TripleDESService = new TripleDESService();
    // private nameStoreAdmin = projectName + "." + commonParams.token_roleAdmin;
    // private nameStoreThanhVien = projectName + "." + commonParams.token_roleThanhVien;
    constructor(
    ) {

    }
   
    //#endregion ====================== Get Info ======================
    public setLocalStoreConfigBy3Des(data3Des: any, enviroment:string) {
        try {
            localStorage.setItem(`ec${enviroment}.json`, data3Des); // mã hóa dữ liệu 
            return true;
        } catch { }
        return false
    }
    public setLocalStoreConfig(data: any, enviroment:string) {
        try {
            let encryptData = this.tripleDes.encrypt(JSON.stringify(data));
            return this.setLocalStoreConfigBy3Des(encryptData, enviroment);
        } catch { }
        return false
    }
    
    public getLocalStoreConfig(enviroment:string) {
        try {
            let nameStore = `ec${enviroment}.json`;
            let data = localStorage.getItem(nameStore);
            let decryptData = JSON.parse(this.tripleDes.decrypt(data));
            return decryptData;
        } catch {
            return null;
        }
    }
    public removeLocalStoreConfig(enviroment:string) {
        try {
            let nameStore = `ec${enviroment}.json`;
            localStorage.removeItem(nameStore);
            return true;
        } catch {
            return false;
        }
    }
   
}