import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { apiHost, apiHost_Sufix_DB, apiHistoryHost_Sufix_DB, apiDaiLyXe, apiDaiLyXe_Sufix_DB, Variables, apiCompute, apiCompute_Sufix_DB, apiHostDailyXe, apiHostDailyXe_Sufix_DB } from "./variables";
import { CommonMethodsTs } from './common-methods';
import { optionsHttp } from './entity';
import { CoreHttp } from './core-http';
@Injectable()
export class CoreTemplate {
	constructor(private coreHttp: CoreHttp, public commonMethodsTs: CommonMethodsTs) {
	}
	// #region Build 
	//URL Raovat
	public buildFullDBApiHost(controllerName: string, addApiHost_Sufix_DB: boolean = true) {
		
		return apiHost + (addApiHost_Sufix_DB ? apiHost_Sufix_DB : "/") + controllerName; //crm/
	}
	public buildFullDBApiHistoryHost(controllerName: string, addApiHistoryHost_Sufix_DB: boolean = true) {
		return apiHost + (addApiHistoryHost_Sufix_DB ? apiHistoryHost_Sufix_DB : "/") + controllerName; //crm/
	}
	//URL DaiLyXe
	public buildFullDBApiDaiLyXe(controllerName: string) {
		return apiDaiLyXe + apiDaiLyXe_Sufix_DB + controllerName;
	}

	public buildFullDBApiCompute(controllerName: string) {
		return apiCompute + apiCompute_Sufix_DB + controllerName;
	}
	public buildFullDBApiHostDailyxe(controllerName: string) {
		return apiHostDailyXe + apiHostDailyXe_Sufix_DB + controllerName;
	}
	// #endregion 
	//#region HttpClient Basic
	public httpGet(requestUri: string, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.coreHttp.get(requestUri, options);
	}
	public httpDelete(requestUri: string, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.coreHttp.del(requestUri, options);
	}
	public httpUpdate(requestUri: string, body: any, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.coreHttp.put(requestUri, body, options);
	}
	public httpInsert(requestUri: string, body: any, optionsHttp: optionsHttp): Observable<any> {
		let options: any = optionsHttp;
		return this.coreHttp.post(requestUri, body, options!);

	}

	//#endregion
	//#region api ApiHost */
	public getDataDBFromApiHost(controllerName: string, optionsHttp: optionsHttp, buildFullDBApiHost?: boolean): Observable<any[]> {
		if (buildFullDBApiHost == undefined || buildFullDBApiHost == null) {
			buildFullDBApiHost = true;
		}
		let requestString = this.buildFullDBApiHost(controllerName, buildFullDBApiHost);
		return this.httpGetWithUrlSearchParams(requestString, optionsHttp);
	}
	public getIndexDataDBFromApiHost(controllerName: string, Id: number, optionsHttp: optionsHttp): Observable<any> {
		let fullUrl = this.buildFullDBApiHost(controllerName) + '/' + Id;
		return this.httpGetWithUrlSearchParams(fullUrl, optionsHttp);
	}
	public httpGetWithUrlSearchParams(requestUri: string, optionsHttp: optionsHttp): Observable<any[]> {
		return this.httpGet(requestUri, optionsHttp!);
	}
	public updateDBFromApiHost(controllerName: string, data: any, optionsHttp: optionsHttp) {
		let id = data ? data.Id : undefined;
		let fullUrl = this.buildFullDBApiHost(controllerName) + (id != undefined ? "/" + id : "");
		return this.httpUpdate(fullUrl, data, optionsHttp);
	}
	public insertDBFromApiHost(controllerName: string, data: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiHost(controllerName);
		return this.httpInsert(fullUrl, data, optionsHttp);
	}
	public deleteDBFromApiHost(controllerName: string, id: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiHost(controllerName) + (id != undefined ? "/" + id : "");
		return this.httpDelete(fullUrl, optionsHttp);
	}
	//#endregion
	public getDataDBFromApiDaiLyXe(controllerName: string, optionsHttp: optionsHttp, buildFullDBApiHost?: boolean): Observable<any[]> {
		if (buildFullDBApiHost == undefined || buildFullDBApiHost == null) {
			buildFullDBApiHost = true;
		}
		let requestString = this.buildFullDBApiDaiLyXe(controllerName);
		return this.httpGetWithUrlSearchParams(requestString, optionsHttp);
	}
	public updateDBFromApiDaiLyXe(controllerName: string, body: any, optionsHttp: optionsHttp): Observable<any[]> {
		let id = body ? body.Id : undefined;
		let requestString = this.buildFullDBApiDaiLyXe(controllerName) + (id != undefined ? "/" + id : "");
		return this.httpUpdate(requestString, body, optionsHttp);
	}
	public insertDBFromApiDaiLyXe(controllerName: string, data: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiDaiLyXe(controllerName);
		return this.httpInsert(fullUrl, data, optionsHttp);
	}
	public deleteDBFromApiDaiLyXe(controllerName: string, id: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiDaiLyXe(controllerName) + (id != undefined ? "/" + id : "");
		return this.httpDelete(fullUrl, optionsHttp);
	}

	// History
	public insertDBFromApiHistoryHost(controllerName: string, data: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiHistoryHost(controllerName);
		return this.httpInsert(fullUrl, data, optionsHttp);
	}
	// API Compute
	public insertDBFromApiCompute(controllerName: string, data: any, optionsHttp: optionsHttp) {
		let fullUrl = this.buildFullDBApiCompute(controllerName);
		return this.httpInsert(fullUrl, data, optionsHttp);
	}
	// API Host DailyXe
	// public getDataDBFromApiHostDailyxe(controllerName: string, data: any, optionsHttp: optionsHttp) {
	// 	let fullUrl = this.buildFullDBApiHostDailyxe(controllerName);
	// 	return this.httpInsert(fullUrl, data, optionsHttp);
	// }

	public getDataDBFromApiHostDailyxe(controllerName: string, optionsHttp: optionsHttp): Observable<any[]> {
		let requestString = this.buildFullDBApiHostDailyxe(controllerName);
		return this.httpGetWithUrlSearchParams(requestString, optionsHttp);
	}
}