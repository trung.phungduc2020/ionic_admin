import { environment } from 'src/environments/environment';

//fix lỗi WARNING in Circular dependency detected: src\app\core\entity.ts -> src\app\core\variables.ts -> src\app\core\entity.ts
export class CoreVariables {
    public static timeDateServer: number = 0;
    public static timeoutApi: number = (environment.timeoutApi || 3) * 60 * 1000  ;
    public static lHealthDomainApi:any = {};
}