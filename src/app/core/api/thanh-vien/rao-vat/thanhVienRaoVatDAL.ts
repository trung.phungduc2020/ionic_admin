import { Injectable } from "@angular/core";
import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Variables, commonParams } from "src/app/core/variables";
import { CoreTemplate } from "src/app/core/core-template";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { Observable } from "rxjs";
import { optionsHttp } from "src/app/core/entity";
@Injectable()
export class ThanhVienRaoVatDAL {
    public controllerName: string = "";
    private formatHeaders(headers?: HttpHeaders) {
        let token = Variables.tokenThanhVien;
        if (!headers) {
            headers = new HttpHeaders();
        }
        if(token)
        {
            headers = headers.set("Authorization", "bearer " + token);
        }
        return headers
    }
    constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
        
    }
    public getList(params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any> {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.getDataDBFromApiHost(actionName, options);
    }
    public updateTrangThai(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.updateDBFromApiHost(actionName, null, options);
    }
    public update(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
		options.headers = this.formatHeaders(headers);
        return this.coreTemplate.updateDBFromApiHost(actionName, data, options);
    }
    public insert(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.insertDBFromApiHost(actionName, data, options);
    }
    public delete(id: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.deleteDBFromApiHost(actionName, id, options);
    }

    public insertHistory(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.insertDBFromApiHistoryHost(actionName, data, options);
    }
}