import { ThanhVienRaoVatBLL_Basic, ThanhVienRaoVatBLL_InteractData, ThanhVienRaoVatBLL_Commonfile, ThanhVienRaoVatBLL_UserPoint, ThanhVienRaoVatBLL_InterfaceData, ThanhVienRaoVatBLL_DailyxeData, ThanhVienRaoVatBLL_MoMo } from './thanhVienRaoVatBLL';
import { NgModule } from "@angular/core";
import { ThanhVienRaoVatDAL } from "./thanhVienRaoVatDAL";

const providers: any = [
	ThanhVienRaoVatDAL,
	ThanhVienRaoVatBLL_Basic,
	ThanhVienRaoVatBLL_InteractData,
	ThanhVienRaoVatBLL_Commonfile,
	ThanhVienRaoVatBLL_UserPoint,
	ThanhVienRaoVatBLL_InterfaceData,
	ThanhVienRaoVatBLL_DailyxeData,
	ThanhVienRaoVatBLL_MoMo]
@NgModule({
	providers: providers,
})
export class ThanhVienRaoVatBLLModule { }