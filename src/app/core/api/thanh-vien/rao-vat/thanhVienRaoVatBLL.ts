import { Injectable } from "@angular/core";
import { ThanhVienRaoVatDAL } from "./thanhVienRaoVatDAL";
import { CoreTemplate } from "src/app/core/core-template";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { commonParams, displayItemDefault, currentPageDefault, TypeSmsValidation, TypeDangTinFavorite, Variables } from "src/app/core/variables";
import { FileChiTietDangTin } from "src/app/core/entity";
@Injectable()
export class ThanhVienRaoVatBLL_Basic {
	protected thanhVienRaoVatDAL: ThanhVienRaoVatDAL;
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		this.thanhVienRaoVatDAL = new ThanhVienRaoVatDAL(this.coreTemplate, commonMethodsTs);
	}
	// Use this function in custom-combo.basic1
	public getByParams(params: HttpParams): Observable<any> {

		return this.thanhVienRaoVatDAL.getList(params);
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.thanhVienRaoVatDAL.getList(params);
	}
	public getIndex(Id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, Id!);
		return this.thanhVienRaoVatDAL.getList(params);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
			// data.IdThanhVienTao = this.commonMethodsTs.getInfoThanhVien(infoUser.id);
		} catch { };
		return this.thanhVienRaoVatDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		// data.IdThanhVienCapNhat = this.commonMethodsTs.getInfoThanhVien(infoUser.id);
		return this.thanhVienRaoVatDAL.update(data, params!, undefined);
	}
	public updateTrangThai(ids: string, trangThai: string) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		return this.thanhVienRaoVatDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.thanhVienRaoVatDAL.delete(null, params!, undefined);
	}
}
@Injectable()
export class ThanhVienRaoVatBLL_InteractData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "InteractData";
	}
	public updateTrangThaiThanhVien(data: any) {
		return this.thanhVienRaoVatDAL.update(data, null, "UpdateTrangThaiThanhVien");
	}
	public insertDangTinInteract(data: any) {
		delete data.Id;
		return this.thanhVienRaoVatDAL.insert(data, null, "CreateDangTin");
	}
	public updateDangTinXeInteract(data: any) {
		return this.thanhVienRaoVatDAL.update(data, null, "UpdateDangTin");
	}

	public updateHinhAnhDangTin(data:any) {
		let body = {
			"Id": data.Id,
			"ListHinhAnhJson": data.ListHinhAnhJson,
			"ListFilesUploaded": data.ListFilesUploaded,
			"MaLayoutAlbum": data.MaLayoutAlbum,
			"TongImage": data.TongImage,
		}
		return this.thanhVienRaoVatDAL.update(body, null, "UpdateHinhAnhDangTin");
	}
	public updateTrangThaiDangTin(ids: any, trangThai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids);
		params = params.set(commonParams.trangThai, trangThai);
		return this.thanhVienRaoVatDAL.update(null, params, "UpdateTrangThaiDangTin");
	}

	public momoRequest(data: any) {
		if (!Variables.uniqueDeviceID) {
			throw "Chức năng thanh toán không hổ trợ trên thiết bị này"
		}
		data.Uuid = Variables.uniqueDeviceID;
		data.ReturnUrl_Ext = "xxxx";

		return this.thanhVienRaoVatDAL.insert(data, null, "MomoRequest");
	}

	public momoTransactionStatus(orderId: any) {
		if (!orderId) {
			throw "Đơn hàng không tồn tại"
		}
		let data: any = {};
		data.OrderId_Ext = orderId;
		data.RequestId_Ext = orderId;

		return this.thanhVienRaoVatDAL.insert(data, null, "MomoTransactionStatus");
	}

	public syncUpdateDangTinXeInteract(data: any) {
		return this.thanhVienRaoVatDAL.update(data, null, "SyncUpdateDangTin");
	}
	public updatePinTopDangTin(data: any) {
		return this.thanhVienRaoVatDAL.update(data, null, "UpdatePinTopDangTin");
	}
	//#endregion
	//#region "UserDevice"
	public logOutUserDevice(body?: any) {
		return this.thanhVienRaoVatDAL.update(body, null, "LogoutUserDevice");
	}
	public loginUserDevice(data?: any, params?: any) {
		return this.thanhVienRaoVatDAL.insert(data, params, "LoginUserDevice");
	}
	//#endregion

	public insertNotification(interfaceParams: any) {
		let data = {
			typeNotification: interfaceParams.typeNotification,
			ma: interfaceParams.ma,
			tieuDe: interfaceParams.tieuDe,
			noiDung: interfaceParams.noiDung,
			noiDung1: interfaceParams.noiDung1,
			noiDung2: interfaceParams.noiDung2,
			idThanhVien: interfaceParams.idThanhVien,
			linkId: interfaceParams.linkId,
		}
		return this.thanhVienRaoVatDAL.insert(data, null, "MemberPushNotification");
	}

	public searchDangTinFavoriteByParams(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.idUsers, paramsData.idUsers);
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.orderString, paramsData.orderString);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.allIncludingString, paramsData.allIncludingString);
		params = params.set(commonParams.isCache, paramsData.isCache);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchDangTinFavorite');
	}

	public searchUserPoint(idThanhVien: any) {
		let params = new HttpParams();
		params = params.set(commonParams.idThanhVien, idThanhVien);
		params = params.set(commonParams.allIncludingString, "true");
		return this.thanhVienRaoVatDAL.getList(params, 'SearchUserPoint');
	}
	//#endregion

	//#region "dangTinLike"
	public insertDangTinLike(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, 'PostDangTinLike');
	}

	public addLike(idDangTin: any) {
		let data = {
			Id: idDangTin,
			IsLike: true,
			TrangThai: 1
		}
		return this.insertDangTinLike(data);
	}
	public removeLike(idDangTin: any) {
		let data = {
			Id: idDangTin,
			IsLike: false,
			TrangThai: 1
		}
		return this.insertDangTinLike(data);
	}
	//#endregion


	//#region "dangTinRating"
	public insertDangTinRating(body?: any) {
		console.log("body", body);
		return this.thanhVienRaoVatDAL.insert(body, null, 'PostDangTinRating');
	}
	public searchDangTinRatingByParams(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idUsers, paramsData.idUsers);
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.orderString, paramsData.orderString);
		params = params.set(commonParams.allIncludingString, paramsData.allIncludingString);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchDangTinRating');
	}
	//#endregion
	//#region "HistoryProfile"
	// public insertHistoryProfile(data?: any, paramsData?: any) {
	// 	paramsData = paramsData || {};
	// 	let params = new HttpParams();
	// 	params = params.set(commonParams.changePass, paramsData.changePass);
	// 	params = params.set(commonParams.changePhone, paramsData.changePhone);
	// 	params = params.set(commonParams.changeEmail, paramsData.changeEmail);
	// 	params = params.set(commonParams.removeAccount, paramsData.removeAccount);
	// 	return this.thanhVienRaoVatDAL.insert(data, params, 'PostHistoryProfile');
	// }
	//#endregion
	//#region "MaXacThuc"
	public deleteMaXacThucByEmail(email: any) {
		let params = new HttpParams();
		params = params.set(commonParams.email, email);
		params = params.set(commonParams.loaiXacNhan, TypeSmsValidation.sendEmail.toString());
		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteMaXacThucByEmail");
	}
	public deleteMaXacThucByPhone(dienThoai: any, typeSmsValidation: string) {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, typeSmsValidation.toString());
		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteMaXacThucByPhone");
	}
	public deleteMaXacThucByPhoneForForgetPassword(dienThoai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, TypeSmsValidation.ForgetPassword.toString());

		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteMaXacThucByPhone");
	}
	public deleteMaXacThucByPhoneForRegist(dienThoai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, TypeSmsValidation.Regist.toString());
		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteMaXacThucByPhone");
	}
	public deleteMaXacThucByPhoneInfoPerson(dienThoai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, TypeSmsValidation.updateInfoPerson.toString());
		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteMaXacThucByPhone");
	}
	//#endregion
	//#region "ThanhVien"
	public searchThanhVienFromRaoVat(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2')
		return this.thanhVienRaoVatDAL.getList(params, "SearchThanhVien");
	}
	public searchThanhVienFromRaoVatById(id: any, allIncludingString: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id);
		params = params.set(commonParams.trangThai, '1,2');
		params = params.set(commonParams.allIncludingString, allIncludingString)
		return this.thanhVienRaoVatDAL.getList(params, "SearchThanhVien");
	}
	//#endregion
	//#region "UserRank"
	public searchUserRank(): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, "-1");
		params = params.set(commonParams.displayPage, "1");
		params = params.set(commonParams.trangThai, '1');
		params = params.set(commonParams.orderString, 'PointRule:asc');
		return this.thanhVienRaoVatDAL.getList(params, "SearchUserRank");
	}
	//#endregion
	//#region NotificationUser
	public postNotificationUser(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, "PostNotificationUser");
	}
	public updateNotificationUser(data: any) {
		return this.thanhVienRaoVatDAL.update(data, null, 'UpdateNotificationUser');
	}
	public deleteNotificationUser(id, isView) {
		let body = {
			IdNotificationSystem: id,
			TrangThai: 2,
			IsView: isView,
		}
		return this.thanhVienRaoVatDAL.insert(body, null, "PostNotificationUser");

	}
	//#endregion

	//#region DangTinViolation
	public postDangTinViolation(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, "PostDangTinViolation");
	}
	//#endregion

	// //#region DangTinViolation
	// public postDangTinView(body?: any) {
	// 	return this.thanhVienRaoVatDAL.insert(body, null, "PostDangTinView");
	// }

	//#endregion

	//#region "PinTopHistory" đang đợi cung cấp api
	public searchDangTinTraPhi(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc");
		return this.thanhVienRaoVatDAL.getList(params, 'SearchDangTinTraPhi');
	}

	//DangTinContact
	public searchDangTinContact(idThanhVien: any) {
		let params = new HttpParams();
		params = params.set(commonParams.idThanhVien, idThanhVien);
		params = params.set(commonParams.displayItems, "-1");
		params = params.set(commonParams.trangThai, "1");

		return this.thanhVienRaoVatDAL.getList(params, 'SearchDangTinContact');
	}
	public postDangTinContact(body: any) {
		body.Id = null;
		return this.thanhVienRaoVatDAL.insert(body, null, "PostDangTinContact");
	}
	public updateDangTinContact(body: any) {
		let params = new HttpParams();
		// params = params.set(commonParams.id, body.Id);
		return this.thanhVienRaoVatDAL.update(body, params, "UpdateDangTinContact");
	}

	public deleteDangTinContact(id: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, id);
		return this.thanhVienRaoVatDAL.delete(null, params, "DeleteDangTinContact");
	}
	public searchDangTinById(id: any): Observable<any> {
		let params = {};
		params[commonParams.id] = id;
		return this.searchDangTinByParams(params);
	}
	public searchDangTinByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		// params = params.set(commonParams.token, "bearer " + Variables.tokenThanhVien);
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id!);
		params = params.set(commonParams.notId, paramsData.notId!);
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		if (paramsData.idMenu > 0) params = params.set(commonParams.idMenu, paramsData.idMenu);
		if (paramsData.idTinhThanh > 0) params = params.set(commonParams.idTinhThanh, paramsData.idTinhThanh);
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.isDuyet, paramsData.isDuyet);
		params = params.set(commonParams.isPinTop, paramsData.isPinTop);
		params = params.set(commonParams.gia, paramsData.gia);
		if (paramsData.tinhTrang > 0) params = params.set(commonParams.tinhTrang, paramsData.tinhTrang);
		params = params.set(commonParams.giaTu, paramsData.giaTu);
		params = params.set(commonParams.giaDen, paramsData.giaDen);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.idThuongHieu, paramsData.idThuongHieu);
		params = params.set(commonParams.idLoaiXes, paramsData.idLoaiXe!);
		params = params.set(commonParams.idDongXes, paramsData.idDongXe!);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayUp:desc");
		params = params.set(commonParams.isViewType, paramsData.isViewType || false);
		params = params.set(commonParams.isFromAdmin, paramsData.isFromAdmin);
		params = params.set(commonParams.isCache, paramsData.isCache);
		params = params.set(commonParams.timeEffective, paramsData.timeEffective);
		params = params.set(commonParams.deviceId, paramsData.deviceId);
		params = params.set(commonParams.allIncludingString, "true");
		params = params.set(commonParams.isPlusChuaDuyet, paramsData.isPlusChuaDuyet);

		return this.thanhVienRaoVatDAL.getList(params, "SearchDangTin");
	}
	//#endregion

	public searchMarketingPlan(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.trangThai, paramsData.trangThai);
		params = params.set(commonParams.orderString, paramsData.orderString);
		params = params.set(commonParams.isUsed, paramsData.isUsed);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchMarketingPlan');
	}

	public searchMarketingPlanUsedByUser(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.trangThai, paramsData.trangThai);
		params = params.set(commonParams.orderString, paramsData.orderString);
		params = params.set(commonParams.isUsed, paramsData.isUsed);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchMarketingPlanUsedByUser');
	}

	public activeMarketingCode(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.ma, paramsData.Ma);
		params = params.set(commonParams.idMarketingPlan, paramsData.idMarketingPlan);
		params = params.set(commonParams.Uuid, paramsData.Uuid);
		return this.thanhVienRaoVatDAL.getList(params, 'ActiveMarketingCode');
	}



	public removeMarketingCode(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.ma, paramsData.Ma);
		return this.thanhVienRaoVatDAL.getList(params, 'RemoveMarketingCode');
	}


	public getMaGioiThieu() {
		return this.thanhVienRaoVatDAL.insert(null, null, "GetMaGioiThieu");
	}


	public kichHoatMaGioiThieu(ma) {
		let body = {};
		body["ma"] = ma;
		return this.thanhVienRaoVatDAL.insert(body, null, "KichHoatMaGioiThieu");
	}


}
@Injectable()
export class ThanhVienRaoVatBLL_InterfaceData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "InterfaceData";
	}
	public searchDangTinByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		// params = params.set(commonParams.token, "bearer " + Variables.tokenThanhVien);
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id!);
		params = params.set(commonParams.notId, paramsData.notId!);
		if (+paramsData.idThanhVien > 0) params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		if (paramsData.idLoaiXe > 0) params = params.set(commonParams.idLoaiXes, paramsData.idLoaiXe);
		if (paramsData.idDongXe > 0) params = params.set(commonParams.idDongXes, paramsData.idDongXe);
		if (paramsData.idMenu > 0) params = params.set(commonParams.idMenu, paramsData.idMenu);
		if (paramsData.idTinhThanh > 0) params = params.set(commonParams.idTinhThanh, paramsData.idTinhThanh);
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.isDuyet, paramsData.isDuyet);
		params = params.set(commonParams.isPinTop, paramsData.isPinTop);
		params = params.set(commonParams.gia, paramsData.gia);
		if (paramsData.tinhTrang > 0) params = params.set(commonParams.tinhTrang, paramsData.tinhTrang);
		params = params.set(commonParams.giaTu, paramsData.giaTu);
		params = params.set(commonParams.giaDen, paramsData.giaDen);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.idThuongHieu, paramsData.idThuongHieu);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayUp:desc");
		params = params.set(commonParams.isViewType, paramsData.isViewType || false);
		params = params.set(commonParams.isFromAdmin, paramsData.isFromAdmin);
		params = params.set(commonParams.isCache, paramsData.isCache);
		params = params.set(commonParams.timeEffective, paramsData.timeEffective);
		params = params.set(commonParams.deviceId, paramsData.deviceId);
		params = params.set(commonParams.allIncludingString, "true");
		params = params.set(commonParams.isPlusChuaDuyet, paramsData.isPlusChuaDuyet);

		return this.thanhVienRaoVatDAL.getList(params, "SearchDangTin");
	}
	public searchDangTinDaDuyetByIsPinTop(displayItems?: any, displayPage?: any, trangThai?: any) {
		let params: any = {};
		params[commonParams.displayItems] = displayItems || displayItemDefault;
		params[commonParams.displayPage] = displayPage || currentPageDefault;
		params[commonParams.isPinTop] = true;
		params[commonParams.isDuyet] = true;
		params[commonParams.trangThai] = trangThai || "1";
		params = params.set(commonParams.isCache, true);
		return this.searchDangTinByParams(params);
	}
	public searchDangTinById(id: any): Observable<any> {
		let params: any = {};
		params[commonParams.id] = id || undefined;
		// Get TinDang include IsPinTop
		// params[commonParams.isCache] = true;
		return this.searchDangTinByParams(params);
	}
	public searchMenuByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id || undefined);
		params = params.set(commonParams.notId, paramsData.notId || undefined);
		params = params.set(commonParams.capDo, paramsData.capDo || undefined);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1");
		params = params.set(commonParams.orderString, paramsData.orderString || "TenMenu:asc");
		return this.thanhVienRaoVatDAL.getList(params, "SearchMenuRaoVat");
	}
	public SearchCauHinhHeThong(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.maCauHinh, paramsData.maCauHinh!);
		return this.thanhVienRaoVatDAL.getList(params, "SearchCauHinhHeThong");
	}
	public searchDangTinRating(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idUsers, paramsData.idUsers);
		params = params.set(commonParams.idTinDangs, paramsData.idTinDangs);
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayCapNhat:desc,NgayTao:desc");
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1");
		params = params.set(commonParams.isCache, paramsData.isCache);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchDangTinRating');
	}
	//#region "NotificationSystem"
	public searchNotificationSystem(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.isView, paramsData.isView);
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc");
		params = params.set(commonParams.allIncludingString, paramsData.allIncludingString);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchNotificationSystem');
	}
	//#endregion

	public getServerTime() {
		return this.thanhVienRaoVatDAL.getList(null, 'GetServerTime');
	}

	public searchUserBlackList(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchUserBlackList');
	}

	//#region DangTinViolation
	public postInstallationStatistics(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, "PostInstallationStatistics");
	}

	// // #region PostDangTinView => For Show Description
	// public postDangTinView(body?: any) {
	// 	return this.thanhVienRaoVatDAL.insert(body, null, "PostDangTinView");
	// }

	public searchLayoutAlbum(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.ma, paramsData.ma);
		params = params.set(commonParams.trangThai, paramsData.trangThai);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchLayoutAlbum');
	}
	public getLayoutAlbumByMa(ma: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ma, ma);
		return this.thanhVienRaoVatDAL.getList(params, 'SearchLayoutAlbum');
	}



	//#region "dangTinLike"
	public insertDangTinLike(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, 'PostHistoryDangTinLike');
	}

	public addLike(idDangTin: any) {

		let data = {
			IdDangTin: idDangTin,
			IsLike: true,
			IdDevice: Variables.uniqueDeviceID
		}
		return this.insertDangTinLike(data);
	}
	public removeLike(idDangTin: any) {

		let data = {
			IdDangTin: idDangTin,
			IsLike: false,
			IdDevice: Variables.uniqueDeviceID
		}
		return this.insertDangTinLike(data);
	}
	//#endregion

	//#region "dangTinFavorite"
	public insertDangTinFavorite(body?: any) {
		return this.thanhVienRaoVatDAL.insert(body, null, "PostHistoryDangTinFavorite");
	}

	public addFavorite(idDangTin: any) {
		let data = {
			IdDangTin: idDangTin,
			IsFavorite: true,
			IdDevice: Variables.uniqueDeviceID
		}
		return this.insertDangTinFavorite(data);
	}

	public removeFavorite(idDangTin: any) {
		let data = {
			IdDangTin: idDangTin,
			IsFavorite: false,
			IdDevice: Variables.uniqueDeviceID
		}
		return this.insertDangTinFavorite(data);
	}

}

@Injectable()
export class ThanhVienRaoVatBLL_Commonfile extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "Commonfile";
	}
	public createFileChiTietDangTin(id: string, noiDung: string): Observable<any> {
		let body: any = new FileChiTietDangTin(id, noiDung);
		return this.thanhVienRaoVatDAL.insert(body, null!, "CreateFileChiTietDangTin");
	}
	public getChiTietDangTin(id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		return this.thanhVienRaoVatDAL.getList(params, "GetChiTietDangTin");
	}

}
@Injectable()
export class ThanhVienRaoVatBLL_UserPoint extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "UserPoint";
	}
	search(idsThanhVien: any, allIncludingString: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.idsThanhVien, idsThanhVien);
		/**@Trung 20200204_110400  Không dùng ThanhVien_Ext thì nên là false*/
		params = params.set(commonParams.allIncludingString, allIncludingString);
		return this.thanhVienRaoVatDAL.getList(params)
	}
	updatePoint(ids: any, pointTmp: any, note: any, rate: any, type: any, point: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids);
		params = params.set(commonParams.pointTmp, pointTmp);
		params = params.set(commonParams.note, note);
		params = params.set(commonParams.rate, rate);
		params = params.set(commonParams.type, type);
		params = params.set(commonParams.point, point);
		return this.thanhVienRaoVatDAL.update(null, params, "UpdatePoint");
	}
	update(data?) {
		return this.thanhVienRaoVatDAL.update(data, null, "UpdateUserPointByIdThanhVien");
	}

}
@Injectable()
export class ThanhVienRaoVatBLL_HistoryPoint extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "HistoryPoint";
	}
	search(displayItems: any, displayPage: any, idsThanhVien: any, orderString: any = 'NgayTao:desc'): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.idsThanhVien, idsThanhVien);
		params = params.set(commonParams.displayItems, displayItems);
		params = params.set(commonParams.displayPage, displayPage);
		params = params.set(commonParams.orderString, orderString);
		return this.thanhVienRaoVatDAL.getList(params)
	}
}
@Injectable()
export class ThanhVienRaoVatBLL_DaiLyXeData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "DaiLyXeData";
	}
	public updateDaXacThucCmnd(id: any, daXacThucCmnd: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.daXacThucCmnd, daXacThucCmnd);
		return this.thanhVienRaoVatDAL.update(null, params, "UpdateDaXacThucCmnd");
	}
	public updateDaXacThucPassport(id: any, daXacThucPassport: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.daXacThucPassport, daXacThucPassport);
		return this.thanhVienRaoVatDAL.update(null, params, "UpdateDaXacThucPassport");
	}

	public updateListHinhAnhJsonPassport(id: any, listHinhAnhJsonPassport: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.listHinhAnhJsonPassport, listHinhAnhJsonPassport);
		return this.thanhVienRaoVatDAL.update(null, params, "UpdateListHinhAnhJsonPassport");
	}
}
@Injectable()
export class HistoryThanhVienRaoVatBLL_InteractData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "InteractData";
	}
	//#endregion
	//#region "HistoryProfile"
	public insertHistoryProfile(data?: any, paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.changePass, paramsData.changePass);
		params = params.set(commonParams.changePhone, paramsData.changePhone);
		params = params.set(commonParams.changeEmail, paramsData.changeEmail);
		params = params.set(commonParams.removeAccount, paramsData.removeAccount);
		return this.thanhVienRaoVatDAL.insertHistory(data, params, 'PostHistoryProfile');
	}

}
@Injectable()
export class HistoryThanhVienRaoVatBLL_InterfaceData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "InterfaceData";
	}

	// #region PostDangTinView => For Show Description
	public postDangTinView(body?: any) {
		return this.thanhVienRaoVatDAL.insertHistory(body, null, "PostDangTinView");
	}
}

@Injectable()
export class ThanhVienRaoVatBLL_DailyxeData extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "DailyxeData";
	}
	public isExistDienThoaiThanhVienSelfCheck(dienThoai: any, notId?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.notId, notId);
		return this.thanhVienRaoVatDAL.getList(params, 'IsExistDienThoaiThanhVienSelfCheck');
	}
	public isExistEmailThanhVienSelfCheck(email: any, notId?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.email, email);
		params = params.set(commonParams.notId, notId);
		return this.thanhVienRaoVatDAL.getList(params, 'IsExistEmailThanhVienSelfCheck');
	}
}

@Injectable()
export class ThanhVienRaoVatBLL_MoMo extends ThanhVienRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienRaoVatDAL.controllerName = "MoMo";
	}
	public paymentMoMo() {
		let body = {};
		let idThanhVien = this.commonMethodsTs.getData(Variables.infoThanhVien, "Id", -1);
		body[commonParams.idThanhVien] = idThanhVien;
		body[commonParams.returnUrl] = "https://raoxe.page.link?link=${linkUrl}&apn=vn.com.raoxe.admin&ibi=vn.com.raoxe.admin&afl=${linkUrl}&ifl=${linkUrl}&isi=1486119532&efr=1";
		return this.thanhVienRaoVatDAL.insert(body, null);
	}

}