import { commonParams } from './../../../variables';
import { Injectable } from "@angular/core";
import { ThanhVienDaiLyXeDAL } from "./thanhVienDaiLyXeDAL";
import { CoreTemplate } from "src/app/core/core-template";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable()
export class ThanhVienDaiLyXeBLL_Basic {
	protected thanhVienDaiLyXeDAL: ThanhVienDaiLyXeDAL;
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		this.thanhVienDaiLyXeDAL = new ThanhVienDaiLyXeDAL(this.coreTemplate, commonMethodsTs);
	}
	// Use this function in custom-combo.basic1
	public getByParams(params: HttpParams, action: string = ""): Observable<any> {
		if (action.isEmpty()) {
			return this.thanhVienDaiLyXeDAL.getList(params);
		}
		else {
			return this.thanhVienDaiLyXeDAL.getList(params, action);

		}
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.thanhVienDaiLyXeDAL.getList(params);
	}
	public getIndex(Id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, Id!);
		return this.thanhVienDaiLyXeDAL.getList(params);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
			// data.IdThanhVienTao = this.commonMethodsTs.getInfoThanhVien(infoUser.id);
		} catch { };
		return this.thanhVienDaiLyXeDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		// data.IdThanhVienCapNhat = this.commonMethodsTs.getInfoThanhVien(infoUser.id);
		return this.thanhVienDaiLyXeDAL.update(data, params!, undefined);
	}
	public updateTrangThai(ids: string, trangThai: string) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		return this.thanhVienDaiLyXeDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.thanhVienDaiLyXeDAL.delete(null, params!, undefined);
	}
}
@Injectable()
export class ThanhVienDaiLyXeBLL_ThanhVien extends ThanhVienDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienDaiLyXeDAL.controllerName = "ThanhVien";
	}
	public getFromFrontByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.trangThai, paramsData.trangThai);
		params = params.set(commonParams.orderString, paramsData.orderString);
		return this.thanhVienDaiLyXeDAL.getList(params, undefined);
	}
	public getFromFrontById(id: any): Observable<any> {
		let params: any = {};
		params[commonParams.id] = id || undefined;
		return this.getFromFrontByParams(params)
	}
	public getIndex(id: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		return this.thanhVienDaiLyXeDAL.getList(params, undefined);
	}

	public updateDienThoai(id: any, dienThoai: any, daXacNhanDienThoai: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.daXacNhanDienThoai, daXacNhanDienThoai);
		return this.thanhVienDaiLyXeDAL.update(null, params, "UpdateDienThoai");
	}
	public updateEmail(id: any, email: any, daXacNhanEmail: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		params = params.set(commonParams.email, email);
		params = params.set(commonParams.daXacNhanEmail, daXacNhanEmail);
		return this.thanhVienDaiLyXeDAL.update(null, params, "UpdateEmail");
	}

	public updateIdFileCmndTruoc(id: any, idFileCmndTruoc: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		params = params.set(commonParams.idFileCmndTruoc, idFileCmndTruoc);
		return this.thanhVienDaiLyXeDAL.update(null, params, "UpdateIdFileCmndTruoc");
	}
	public updateIdFileCmndSau(id: any, idFileCmndSau: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id || undefined);
		params = params.set(commonParams.idFileCmndSau, idFileCmndSau);
		return this.thanhVienDaiLyXeDAL.update(null, params, "UpdateIdFileCmndSau");
	}
}

@Injectable()
export class ThanhVienDaiLyXeBLL_InterfaceData extends ThanhVienDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.thanhVienDaiLyXeDAL.controllerName = "InterfaceData";
	}
	
	public searchQuangCaoByThanhVien(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.orderString, paramsData.orderString);
		return this.thanhVienDaiLyXeDAL.getList(params, 'searchQuangCaoByThanhVien');
	}

	public searchLienHeByThanhVien(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.orderString, paramsData.orderString);
		return this.thanhVienDaiLyXeDAL.getList(params, 'SearchLienHeByThanhVien');
	}

	public updateLienHeIsReadByThanhVien(paramsData?: any) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.thanhVienIsRead, paramsData.thanhVienIsRead);
		return this.thanhVienDaiLyXeDAL.update(null, params, "UpdateLienHeIsReadByThanhVien");
	}
}

