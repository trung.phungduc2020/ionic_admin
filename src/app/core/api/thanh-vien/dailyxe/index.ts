
import { NgModule } from "@angular/core";
import { ThanhVienDaiLyXeBLL_Basic, ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_InterfaceData } from "./thanhVienDaiLyXeBLL";
import { ThanhVienDaiLyXeDAL } from "./thanhVienDaiLyXeDAL";

const providers: any = [
	ThanhVienDaiLyXeDAL,
	ThanhVienDaiLyXeBLL_Basic,
	ThanhVienDaiLyXeBLL_ThanhVien,
	ThanhVienDaiLyXeBLL_InterfaceData]
@NgModule({
	providers: providers,
})
export class ThanhVienDaiLyXeBLLModule { }