import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Variables, commonParams } from "src/app/core/variables";
import { CommonMethodsTs } from "src/app/core/common-methods";
import { CoreTemplate } from "src/app/core/core-template";
import { optionsHttp } from "src/app/core/entity";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
@Injectable()
export class ThanhVienDaiLyXeDAL {
    public controllerName: string = "";
    private formatHeaders(headers?: HttpHeaders) {
        let token = Variables.tokenThanhVien;
        if (!headers) {
            headers = new HttpHeaders();
        }
        if(token)
        {
            headers = headers.set("Authorization", "bearer " + token);
        }
        return headers
    }
    constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
        
    }
    public getList(params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.getDataDBFromApiDaiLyXe(actionName, options);
    }
    public update(data: any, params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.updateDBFromApiDaiLyXe(actionName, data, options);
    }
    public updateTrangThai(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.updateDBFromApiDaiLyXe(actionName, null, options);
    }
    public insert(data: any, params: HttpParams, actionName?: string)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(null);
        return this.coreTemplate.insertDBFromApiDaiLyXe(actionName, data, options);
    }
    public delete(id: any, params: HttpParams, actionName?: string, headers?: HttpHeaders) {
        if (!actionName || actionName.length < 1) {
            actionName = this.controllerName
        }
        else {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = this.formatHeaders(headers);
        return this.coreTemplate.deleteDBFromApiDaiLyXe(actionName, id, options);
    }
}