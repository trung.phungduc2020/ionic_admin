import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpHeaders } from '@angular/common/http'
import { CoreTemplate } from 'src/app/core/core-template';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { optionsHttp } from 'src/app/core/entity';
@Injectable()
export class AdminRaoVatDAL
{
    public controllerName: string = "";
    constructor(public _coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs)
    {
    }
    public getList(params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
		options.headers = headers;
		options.params = params;
        return this._coreTemplate.getDataDBFromApiHost(actionName, options);
    }
    public updateTrangThai(data: any, params: HttpParams, actionName?: string)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        return this._coreTemplate.updateDBFromApiHost(actionName, null, options);
    }
    public update(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = headers;
        return this._coreTemplate.updateDBFromApiHost(actionName, data, options);
    }
    public insert(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = headers;
        return this._coreTemplate.insertDBFromApiHost(actionName, data, options);
    }
    public delete(id: any, params: HttpParams, actionName?: string)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        return this._coreTemplate.deleteDBFromApiHost(actionName, id, options);
    }
    
}