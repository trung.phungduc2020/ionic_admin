import { NgModule } from "@angular/core";
import { AdminRaoVatBLL_Basic, AdminRaoVatBLL_Commonfile, AdminRaoVatBLL_InterfaceData, AdminRaoVatBLL_SmsPhone } from "./adminRaoVatBLL";
import { AdminRaoVatDAL } from "./adminRaoVatDAL";
const providers: any = [
	AdminRaoVatDAL,
	AdminRaoVatBLL_Basic,
	AdminRaoVatBLL_Commonfile,
	AdminRaoVatBLL_InterfaceData,
	AdminRaoVatBLL_SmsPhone	
]
@NgModule({
	providers: providers,
})
export class AdminRaoVatBLLModule { }
