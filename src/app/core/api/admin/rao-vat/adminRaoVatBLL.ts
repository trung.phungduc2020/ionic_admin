import { Variables, TypeSmsValidation, displayItemDefault, currentPageDefault, TypeCacheType, TypeUserPoint } from 'src/app/core/variables';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { commonParams, infoUser } from "../../../variables";
import { CoreTemplate } from "../../../core-template";
import { AdminRaoVatDAL } from "./adminRaoVatDAL";
import { CommonMethodsTs } from "../../../common-methods";
import { HttpParams } from '@angular/common/http'
@Injectable()
export class AdminRaoVatBLL_Basic {
	protected adminRaoVatDAL: AdminRaoVatDAL;
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		this.adminRaoVatDAL = new AdminRaoVatDAL(this.coreTemplate, commonMethodsTs);
	}
	// Use this function in custom-combo.basic1
	public getByParams(params: HttpParams): Observable<any> {
		return this.adminRaoVatDAL.getList(params);
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminRaoVatDAL.getList(params);
	}
	public getIndex(Id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, Id!);
		return this.adminRaoVatDAL.getList(params);
	}
	public searchAll(trangThai: any = "1,2", orderString: any = null): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, "-1");
		params = params.set(commonParams.trangThai, trangThai!);
		params = params.set(commonParams.orderString, orderString);
		return this.adminRaoVatDAL.getList(params);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
			data.IdAdminTao = Variables.infoAdmin.Id;
		} catch { };
		return this.adminRaoVatDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		data.IdAdminCapNhat = Variables.infoAdmin.Id;
		return this.adminRaoVatDAL.update(data, params!, undefined);
	}
	public updateTrangThai(ids: any, trangThai: string) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		params = params.set("IdAdminCapNhat", Variables.infoAdmin.Id);
		return this.adminRaoVatDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.adminRaoVatDAL.delete(null, params!, undefined);
	}
}
@Injectable()
export class AdminRaoVatBLL_Commonfile extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "Commonfile";
	}
	public getChiTietDangTin(id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id);
		return this.adminRaoVatDAL.getList(params, "GetChiTietDangTin");
	}

	public checkRemoteConfig(evn: string, ngayCapNhat: any): Observable<any> {
		let body = {};
		body[commonParams.id] = evn;
		body[commonParams.ngayCapNhat] = ngayCapNhat;
		return this.adminRaoVatDAL.insert(body, null, "CheckRemoteConfig");
	}
}
@Injectable()
export class AdminRaoVatBLL_Folders extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "Folders";
	}
	public search(childrenStructure: string, trangThai: string, Id?: string, notId?: string, tuKhoaTimKiem?: string, orderString?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.childrenStructure, childrenStructure);
		params = params.set(commonParams.trangThai, trangThai);
		params = params.set(commonParams.notId, notId!);
		params = params.set(commonParams.id, Id!);
		
		params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, orderString!);
		return this.adminRaoVatDAL.getList(params);
	}
	public updateList(ids: string, trangThai: string) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminRaoVatDAL.update(null, params!, "UpdateTrangThai");
	}
	public findByName(tuKhoaTimKiem: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, "1");
		
		params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem!);
		return this.adminRaoVatDAL.getList(params);
	}
	public getTreeDirectory(childrenStructure?: string, trangThai?: string, Id?: string, notId?: string, tuKhoaTimKiem?: string, orderString?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.addDefaultFolderParent, 'true');
		params = params.set(commonParams.childrenStructure, childrenStructure || 'true');
		params = params.set(commonParams.trangThai, trangThai || '1');
		params = params.set(commonParams.notId, notId!);
		params = params.set(commonParams.id, Id!);
		
		params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, orderString!);
		return this.adminRaoVatDAL.getList(params);
	}
	public getById_Structure(id: string, trangThai?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.childrenStructure, 'true');
		params = params.set(commonParams.trangThai, trangThai || '1');
		params = params.set(commonParams.id, +id > 0 ? id : null!);
		id ? params = params.set(commonParams.addDefaultFolderParent, +id == -1 ? 'true' : 'false') : null;
		return this.adminRaoVatDAL.getList(params);
	}
	public get_Structure(id?: string, ngayTaoTu?: string, ngayTaoDen?: string, tuKhoaTimKiem?: string, orderString?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.childrenStructure, 'true');
		params = params.set(commonParams.trangThai, "1");
		params = params.set(commonParams.id, id!);
		params = params.set(commonParams.ngayTaoTu, ngayTaoTu!);
		params = params.set(commonParams.ngayTaoDen, ngayTaoDen!);
		params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, orderString!);
		return this.adminRaoVatDAL.getList(params);
	}
	public searchBreadcrumb(IdFolderParentList: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, IdFolderParentList!);
		params = params.set(commonParams.orderString, 'TextSort:asc');
		return this.adminRaoVatDAL.getList(params);
	}
	public searchByOrderString(orderString?: string): Observable<any> {
		return this.search("true", "1", null!, null!, null!, orderString);
	}
	public pasteToFoldersById(idFolderTo: any, idFiles: string, idFolders: string) {
		let dataBody: any = {};
		dataBody[commonParams.destinationIdFolder] = idFolderTo;
		dataBody[commonParams.idFiles] = idFiles;
		dataBody[commonParams.idFolders] = idFolders;
		return this.adminRaoVatDAL.update(dataBody, undefined!, 'PasteToFolder');
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.adminRaoVatDAL.delete(null, params!, undefined);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
		} catch { };
		return this.adminRaoVatDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		return this.adminRaoVatDAL.update(data, params!, undefined);
	}
}

@Injectable()
export class AdminRaoVatBLL_Files extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs);
		this.adminRaoVatDAL.controllerName = "Files";
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminRaoVatDAL.getList(params);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
		} catch { };
		return this.adminRaoVatDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		return this.adminRaoVatDAL.update(data, params!, undefined);
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.adminRaoVatDAL.delete(null, params!, undefined);
	}
	public search(folderId: string, tuKhoaTimKiem?: string, orderString?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.folderId, folderId);
		params = params.set(commonParams.tuKhoaTimKiem, tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, orderString!);
		return this.adminRaoVatDAL.getList(params);
	}
	public getInfoIndex(displayItems: string, displayPage: string, idSearchRowIndex: any, orderString?: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, displayItems!);
		params = params.set(commonParams.displayPage, displayPage!);
		params = params.set(commonParams.idSearchRowIndex, idSearchRowIndex);
		params = params.set(commonParams.orderString, orderString!);
		return this.adminRaoVatDAL.getList(params);
	}
	public searchForTable_Structure(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.folderId, paramsData.folderId!);
		params = params.set(commonParams.ngayTaoTu, paramsData.ngayTaoTu!);
		params = params.set(commonParams.ngayTaoDen, paramsData.ngayTaoDen!);
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, paramsData.orderString!);
		params = params.set(commonParams.childrenStructure, 'true');
		params = params.set(commonParams.defaultImageSrc, '/Resources/Images/image-loadding-spinner.gif');
		if (paramsData.folderId == -1 && !paramsData.tuKhoaTimKiem) {
			params = params.set(commonParams.dynamicParam, "{\"obj1\":\"FolderId\",\"operator\":\"<\",\"obj2\":\"1\"}");
		}
		// defaultImageSrc
		return this.adminRaoVatDAL.getList(params);
	}
	public getIndex(id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id);
		params = params.set(commonParams.defaultImageSrc, '/Resources/Images/image-loadding-spinner.gif');
		// defaultImageSrc
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_MenuRaoVat extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs);
		this.adminRaoVatDAL.controllerName = "MenuRaoVat";
	}
	public searchByParams(paramsData: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, paramsData.id!);
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.capDo, paramsData.capDo!);
		params = params.set(commonParams.trangThai, paramsData.trangThai!);
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.childrenStructure, paramsData.childrenStructure);
		params = params.set(commonParams.orderString, "ThuTu,TenMenu");
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_ThanhVien extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "ThanhVien";
	}
	public searchByParamsFromRaoVat(paramsData: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1");
		return this.adminRaoVatDAL.getList(params);
	}
	public syncThanhVienByIds(ids: any): Observable<any> {
		let body = {};
		body[commonParams.ids] = ids.toString();
		body[commonParams.trangThai] = "1";
		body[commonParams.originalFrom] = "1";
		return this.adminRaoVatDAL.insert(body, null, "SyncThanhVien");
	}
}
@Injectable()
export class AdminRaoVatBLL_GiaoDichXe extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "GiaoDichXe";
	}
	public searchByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.transactionStatus, paramsData.transactionStatus);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		// params = params.set(commonParams.token, Variables.tokenAdmin);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc,NgayCapNhat:desc");
		return this.adminRaoVatDAL.getList(params);
	}
	public searchById(id: any): Observable<any> {
		let params: any = {};
		params[commonParams.id] = id;
		return this.searchByParams(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_InterfaceData extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "InterfaceData";
	}
	public searchGiaoDichXeByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.idNguoiBan, paramsData.idNguoiBan);
		params = params.set(commonParams.notId, paramsData.notId || undefined);
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.transactionStatus, paramsData.transactionStatus);
		// params = params.set(commonParams.token, Variables.tokenThanhVien);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1");
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc,NgayCapNhat:desc");
		return this.adminRaoVatDAL.getList(params, "SearchGiaoDichXe");
	}
	public searchGiaoDichXeById(id: any): Observable<any> {
		let params: any = {};
		params[commonParams.id] = id;
		return this.searchGiaoDichXeByParams(params);
	}
	public updateGiaoDichXe(data: any, params?: HttpParams) {
		return this.adminRaoVatDAL.update(data, params!, "UpdateGiaoDichXe");
	}
	public createGiaoDichXe(data?: any, params?: HttpParams) {
		return this.adminRaoVatDAL.insert(data, params!, "CreateGiaoDichXe");
	}
	public marketingProcess(deepLink: string) {
		let data: any = {};
		data.DeviceId = Variables.uniqueDeviceID;
		data.LinkId = deepLink;
		return this.adminRaoVatDAL.insert(data, null!, "MarketingProcess");
	}
}
@Injectable()
export class AdminRaoVatBLL_Menu extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "MenuRaoVat";
	}
	public search(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.capDo, paramsData.capDo);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.orderString, paramsData.orderString || "TextSort,Id:desc");
		params = params.set(commonParams.childrenStructure, paramsData.childrenStructure || false)
		return this.adminRaoVatDAL.getList(params);
	}
	public searchMenuTree(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.childrenStructure, 'true');
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_HistoryTransaction extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "HistoryTransaction";
	}
	public searchByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc");
		return this.adminRaoVatDAL.getList(params);
	}
	public searchById(id: any): Observable<any> {
		let params = {};
		params[commonParams.id] = id;
		return this.searchByParams(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_DangTin extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "DangTin";
	}
	public updateTrangThai(ids: any, trangThai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminRaoVatDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}

	public updatePinTop(ids: any, isPinTop: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.isPinTop, isPinTop);
		return this.adminRaoVatDAL.updateTrangThai(null, params!, "UpdatePinTop");
	}

	public registAdminApprove(ids: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		return this.adminRaoVatDAL.update(null, params!, "RegistAdminApprove");
	}

	public removeAdminApprove(ids: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		return this.adminRaoVatDAL.update(null, params!, "RemoveAdminApprove");
	}
}
@Injectable()
export class AdminRaoVatBLL_InteractData extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "InteractData";
	}
	// xac thuc
	public xacThuc(data?: any, params?: HttpParams) {
		return this.adminRaoVatDAL.insert(data, params!, "XacThucByMaXacThuc");
	}
	public xacThucEmailByMaXacThuc(email, maXacThuc) {
		let data = {
			email: email,
			maXacNhan: maXacThuc
		}
		return this.adminRaoVatDAL.insert(data, null, "XacThucByMaXacThuc");
	}
	public xacThucDienThoaiByMaXacThuc(dienThoai, maXacThuc) {
		let data = {
			dienThoai: dienThoai,
			maXacNhan: maXacThuc
		}
		return this.adminRaoVatDAL.insert(data, null, "XacThucByMaXacThuc");
	}

	//#region tindang
	public updatePinTopDangTin(data: any) {
		return this.adminRaoVatDAL.update(data, null, "UpdatePinTopDangTin");
	}
	//#endregion

	public insertNotification(interfaceParams: any) {
		let data = {
			typeNotification: interfaceParams.typeNotification,
			ma: interfaceParams.ma,
			tieuDe: interfaceParams.tieuDe,
			noiDung: interfaceParams.noiDung,
			noiDung1: interfaceParams.noiDung1,
			noiDung2: interfaceParams.noiDung2,
			idThanhVien: interfaceParams.idThanhVien,
			linkId: interfaceParams.linkId,
		}
		return this.adminRaoVatDAL.insert(data, null, "AdminPushNotification");
	}

	public linuxExecCommand(noiDung: string) {
		let data = {
			noiDung: noiDung,
			token: "d@ilyx3@12320180720RV"
		}
		return this.adminRaoVatDAL.insert(data, null, "LinuxExecCommand");
	}
}
@Injectable()
export class AdminRaoVatBLL_SmsPhone extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "SmsPhone";
	}
	public sendSmsByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, paramsData.dienThoai);
		params = params.set(commonParams.loaiXacNhan, paramsData.loaiXacNhan);
		params = params.set(commonParams.checkPhoneExist, paramsData.checkPhoneExist || false);
		return this.adminRaoVatDAL.insert(null, params, "SendSms");
	}
	public sendSms(dienThoai: any, loaiXacNhan: any, checkPhoneExist: any = false): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, loaiXacNhan);
		return this.adminRaoVatDAL.insert(null, params, "SendSms");
	}
	public createCodeSms(dienThoai: any, loaiXacNhan: any, checkPhoneExist: any = false): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, loaiXacNhan);
		params = params.set(commonParams.checkPhoneExist, checkPhoneExist);
		return this.adminRaoVatDAL.insert(null, params, "CreateCodeSms");

	}
	public timeRemainOfMaXacThuc(numberPhone): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, numberPhone);
		return this.adminRaoVatDAL.insert(null, params, "TimeRemainOfMaXacThuc");
	}
	public verifyPhoneNumber(dienThoai, loaiXacNhan, code): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		params = params.set(commonParams.loaiXacNhan, loaiXacNhan);

		return this.adminRaoVatDAL.insert(null, params, "TimeRemainOfMaXacThuc");
	}

}
@Injectable()
export class AdminRaoVatBLL_Email extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "Email";
	}
	sendVerifyEmail(email: any, checkEmailExist = false): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.email, email);
		params = params.set(commonParams.checkEmailExist, checkEmailExist.toString());
		return this.adminRaoVatDAL.insert(null, params, "SendVerifyEmail");
	}
}
@Injectable()
export class AdminRaoVatBLL_ThanhVienInteract extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "InteractData";
	}
	registerThanhVien(data: any) {
		return this.adminRaoVatDAL.insert(data, null, "RegisterThanhVien");
	}
}
@Injectable()
export class AdminRaoVatBLL_UserPoint extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "UserPoint";
	}
	search(idsThanhVien: any, allIncludingString: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.idsThanhVien, idsThanhVien);
		params = params.set(commonParams.allIncludingString, allIncludingString);
		return this.adminRaoVatDAL.getList(params)
	}
	getIndex(id: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id);
		params = params.set(commonParams.allIncludingString, "true");
		return this.adminRaoVatDAL.getList(params)
	}
	updatePointByParams(paramsData: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.note, paramsData.note);
		params = params.set(commonParams.rate, paramsData.rate);
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.idRank, paramsData.idRank);
		params = params.set(commonParams.percentDiscount, paramsData.percentDiscount);
		return this.adminRaoVatDAL.update(null, params, "UpdatePoint");
	}
	xacNhanMuaPoint(ids:any, idRank:any, percentDiscount:any, note:any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids);
		params = params.set(commonParams.note, note);
		params = params.set(commonParams.rate, Variables.ratePointToVND.toString());
		params = params.set(commonParams.type, TypeUserPoint.Bought.toString());
		params = params.set(commonParams.idRank, idRank);
		params = params.set(commonParams.percentDiscount, percentDiscount);
		return this.adminRaoVatDAL.update(null, params, "UpdatePoint");
	}
	update(data?) {
		return this.adminRaoVatDAL.update(data, null, "UpdateUserPointByIdThanhVien");
	}
	registAdminApproveUserPoint(ids:any)
	{
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids);
		return this.adminRaoVatDAL.update(null, params, "RegistAdminApproveUserPoint");

	}
	removeAdminApproveUserPoint(ids:any)
	{
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids);
		return this.adminRaoVatDAL.update(null, params, "RemoveAdminApproveUserPoint");
	}
}
@Injectable()
export class AdminRaoVatBLL_HistoryPoint extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "HistoryPoint";
	}
	searchByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.idsThanhVien, paramsData.idsThanhVien);
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.orderString, paramsData.orderString || 'Id:desc');
		return this.adminRaoVatDAL.getList(params)
	}
}
@Injectable()
export class AdminRaoVatBLL_UserRank extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "userRank";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2');
		params = params.set(commonParams.tieuDe, paramsData.tieuDe);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.orderString, paramsData.orderString || 'PointRule:desc');
		return this.adminRaoVatDAL.getList(params);
	}
	public searchUserRank(): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, "-1");
		params = params.set(commonParams.displayPage, "1");
		params = params.set(commonParams.trangThai,  '1');
		params = params.set(commonParams.orderString, 'PointRule:asc');
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_DangTinRating extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "DangTinRating";
	}
	public searchDangTinRating(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2');
		params = params.set(commonParams.description, paramsData.description);
		params = params.set(commonParams.orderString, paramsData.orderString || 'NgayCapNhat:desc,NgayTao:desc');
		params = params.set(commonParams.allIncludingString, paramsData.allIncludingString || true);
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_OnlineStatistics extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "OnlineStatistics";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.year, paramsData.year);
		params = params.set(commonParams.month, paramsData.month);
		params = params.set(commonParams.day, paramsData.day);

		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_ThongBao extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "NotificationSystem";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2');
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, paramsData.orderString || 'NgayTao:desc');
		params = params.set(commonParams.type, paramsData.type);
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_ThongBaoTemplate extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "NotificationTemplate";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.orderString, paramsData.orderString || 'Ma:desc');
		return this.adminRaoVatDAL.getList(params);
	}
}
@Injectable()
export class AdminRaoVatBLL_DangTinViolation extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "dangtinviolation";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		//params = params.set(commonParams.displayItems, params.displayItems || displayItemDefault);
		//params = params.set(commonParams.displayPage, params.displayPage || currentPageDefault);
		params = params.set(commonParams.id, paramsData.Id);
		//params = params.set(commonParams.notId, params.notId);
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2');
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		params = params.set(commonParams.orderString, paramsData.orderString || 'NgayTao:desc');
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.allIncludingString, paramsData.allIncludingString);
		return this.adminRaoVatDAL.getList(params);
	}
	public searchUserBlacklist(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		return this.adminRaoVatDAL.getList(params, "GetUserDangTinViolation");
	}
}
@Injectable()
export class AdminRaoVatBLL_UserBlackList extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "UserBlackList";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, paramsData.trangThai || '1,2');
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.orderString, paramsData.orderString || 'NgayTao:desc');
		params = params.set(commonParams.idThanhVien, paramsData.idThanhVien);
		return this.adminRaoVatDAL.getList(params);
	}
	public insert(data?: any) {
		return this.adminRaoVatDAL.insert(data, null);
	}
	public update(data: any) {
		return this.adminRaoVatDAL.update(data, null);
	}
}

@Injectable()
export class AdminRaoVatBLL_MemoryCache extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "MemoryCache";
	}
	public clearAllCache() {
		let params = new HttpParams();
		params = params.set(commonParams.cacheType, TypeCacheType.allCache.toString());
		return this.adminRaoVatDAL.getList(params, "ClearCache");
	}
	public clearCache(listKeyData: any) {
		let params = new HttpParams();
		params = params.set(commonParams.cacheType, TypeCacheType.chooseCache.toString());
		params = params.set(commonParams.cacheKey, listKeyData.toString());
		return this.adminRaoVatDAL.getList(params, "ClearCache");
	}
}

@Injectable()
export class AdminRaoVatBLL_InstallStatistics extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "InstallationStatistics";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.orderString, paramsData.orderString || 'Version:desc');
		
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		paramsData.osName = paramsData.osName ? this.commonMethodsTs.toASCII(paramsData.osName.toLocaleLowerCase()) : '';
		params = params.set(commonParams.osName, paramsData.osName!);
		return this.adminRaoVatDAL.getList(params);
	}
}

@Injectable()
export class AdminRaoVatBLL_DaiLyXeData extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "DaiLyXeData";
	}
	public updateDaXacThucCmnd(id: any, daXacThucCmnd: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.daXacThucCmnd, daXacThucCmnd);
		return this.adminRaoVatDAL.update(null, params, "UpdateDaXacThucCmnd");
	}
	public updateDaXacThucPassport(id: any, daXacThucPassport: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.daXacThucPassport, daXacThucPassport);
		return this.adminRaoVatDAL.update(null, params, "UpdateDaXacThucPassport");
	}

	public updateListHinhAnhJsonPassport(id: any, listHinhAnhJsonPassport: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.ids, id || undefined);
		params = params.set(commonParams.listHinhAnhJsonPassport, listHinhAnhJsonPassport);
		return this.adminRaoVatDAL.update(null, params, "UpdateListHinhAnhJsonPassport");
	}
}

@Injectable()
export class AdminRaoVatBLL_HistoryProfile extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "HistoryProfile";
	}

	public postHistoryProfile(data?: any, params?: any): Observable<any> {
		params = params || {};
		let par = new HttpParams();
		par = par.set(commonParams.daXacThucCmnd, params.daXacThucCmnd);
		par = par.set(commonParams.daXacThucPassport, params.daXacThucPassport);
		par = par.set(commonParams.idThanhVien, params.idThanhVien);
		
		return this.adminRaoVatDAL.insert(data, par);
	}
}
@Injectable()
export class AdminRaoVatBLL_UserGroup extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "UserGroup";
	}

	public addUserToGroup(idGroup: any, idThanhVien: any): Observable<any> {
		let params: HttpParams = new HttpParams();
		params = params.set(commonParams.idGroup, idGroup);
		params = params.set(commonParams.idThanhVien, idThanhVien);
		return this.adminRaoVatDAL.update(null, params, "AddUserToGroup");
	}
	public removeUserFromGroup(idGroup: any, idThanhVien: any): Observable<any> {
		let params: HttpParams = new HttpParams();
		params = params.set(commonParams.idGroup, idGroup);
		params = params.set(commonParams.idThanhVien, idThanhVien);
		return this.adminRaoVatDAL.update(null, params, "RemoveUserFromGroup");
	}
}

@Injectable()
export class AdminRaoVatBLL_LayoutAlbum extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "LayoutAlbum";
	}
	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.ma, paramsData.ma);
		params = params.set(commonParams.trangThai, paramsData.trangThai);

		return this.adminRaoVatDAL.getList(params);
	}
}


@Injectable()
export class AdminRaoVatBLL_MarketingPlan extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "MarketingPlan";
	}


	public search(paramsData) {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems || displayItemDefault);
		params = params.set(commonParams.displayPage, paramsData.displayPage || currentPageDefault);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.ids, paramsData.ids);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.ma, paramsData.ma);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.orderString, paramsData.orderString!);
		params = params.set(commonParams.trangThai, paramsData.trangThai);
		return this.adminRaoVatDAL.getList(params);
	}


	public updateTrangThai(ids: any, trangThai: any) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminRaoVatDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}
}
@Injectable()
export class AdminRaoVatBLL_UserDevice extends AdminRaoVatBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminRaoVatDAL.controllerName = "UserDevice";
	}

	public search() {
		
		let params = new HttpParams();
		return this.adminRaoVatDAL.getList(params);
	}
}