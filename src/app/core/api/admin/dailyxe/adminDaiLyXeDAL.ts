import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpHeaders } from '@angular/common/http'
import { optionsHttp } from 'src/app/core/entity';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CoreTemplate } from 'src/app/core/core-template';
import { commonParams } from 'src/app/core/variables';
@Injectable()
export class AdminDaiLyXeDAL
{
    public controllerName: string = "";
    constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs)
    {
    }
    public getText(params?: HttpParams, actionName?: string): Observable<any[]>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options = { params: params, responseType: 'text' }
        return this.coreTemplate.getDataDBFromApiDaiLyXe(actionName, options, null!);
    }
    public getList(params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.headers = headers;
        options.params = params;
        return this.coreTemplate.getDataDBFromApiDaiLyXe(actionName, options);
    }
    
    public updateTrangThai(data: any, params: HttpParams, actionName?: string)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        return this.coreTemplate.updateDBFromApiDaiLyXe(actionName, null, options);
    }
    public update(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = headers;
        return this.coreTemplate.updateDBFromApiDaiLyXe(actionName, data, options);
    }
    public insert(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = headers;
        return this.coreTemplate.insertDBFromApiDaiLyXe(actionName, data, options);
    }
    public delete(id: any, params: HttpParams, actionName?: string)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        return this.coreTemplate.deleteDBFromApiDaiLyXe(actionName, id, options);
    }
    public insertCompute(data: any, params: HttpParams, actionName?: string, headers?: HttpHeaders)
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.params = params;
        options.headers = headers;
        return this.coreTemplate.insertDBFromApiCompute(actionName, data, options);
    }
    public getListHostDailyxe(params?: HttpParams, actionName?: string, headers?: HttpHeaders): Observable<any>
    {
        if (!actionName || actionName.length < 1)
        {
            actionName = this.controllerName
        }
        else
        {
            actionName = this.controllerName + "/" + actionName;
        }
        let options: optionsHttp = new optionsHttp();
        options.headers = headers;
        options.params = params;
        return this.coreTemplate.getDataDBFromApiHostDailyxe(actionName, options);
    }
}