import { NgModule } from "@angular/core";
import { AdminDaiLyXeDAL } from "./adminDaiLyXeDAL";
import { AdminDaiLyXeBLL_Basic, AdminDaiLyXeBLL_ThanhVien, AdminDaiLyXeBLL_InterfaceData, AdminDaiLyXeBLL_CommonFile } from "./adminDaiLyXeBLL";
import { AdminRaoVatBLL_InteractData } from '../rao-vat/adminRaoVatBLL';
@NgModule({
  providers: [
    AdminDaiLyXeDAL,
    AdminDaiLyXeBLL_Basic,
    AdminDaiLyXeBLL_ThanhVien,
    AdminDaiLyXeBLL_InterfaceData,
    AdminRaoVatBLL_InteractData,
    AdminDaiLyXeBLL_CommonFile
  ]
})
export class AdminDaiLyXeBLLModule { }
