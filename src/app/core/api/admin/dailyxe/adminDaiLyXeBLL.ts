import { commonParams, infoUser, Variables } from 'src/app/core/variables';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminDaiLyXeDAL } from "./adminDaiLyXeDAL";
import { HttpParams } from '@angular/common/http'
import { CoreTemplate } from 'src/app/core/core-template';
import { CommonMethodsTs } from 'src/app/core/common-methods';
@Injectable()
export class AdminDaiLyXeBLL_Basic {
	public adminDaiLyXeDAL: AdminDaiLyXeDAL;
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		this.adminDaiLyXeDAL = new AdminDaiLyXeDAL(this.coreTemplate, commonMethodsTs);
	}
	// Use this function in custom-combo.basic1
	public getByParams(params: HttpParams, actionName?:string): Observable<any> {
		return this.adminDaiLyXeDAL.getList(params, actionName);
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminDaiLyXeDAL.getList(params);
	}
	public getIndex(Id: string): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, Id!);
		return this.adminDaiLyXeDAL.getList(params);
	}
	public getAll(actionName?:string): Observable<any> {	
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, "-1");	
		return this.adminDaiLyXeDAL.getList(params, actionName);
	}
	public searchAll(trangThai: any = "1,2", orderString: any = null): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, "-1");
		params = params.set(commonParams.trangThai, trangThai!);
		params = params.set(commonParams.orderString, orderString);
		return this.adminDaiLyXeDAL.getList(params);
	}
	public insert(data?: any, params?: HttpParams) {
		try {
			data.Id = undefined;
			data.IdAdminTao = Variables.infoAdmin.Id;
		} catch { };
		return this.adminDaiLyXeDAL.insert(data, params!, undefined);
	}
	public update(data?: any, params?: HttpParams) {
		data.IdAdminCapNhat = Variables.infoAdmin.Id;
		return this.adminDaiLyXeDAL.update(data, params!, undefined);
	}
	public updateTrangThai(ids: string, trangThai: string) {
		let params = new HttpParams();
		params = params.set(commonParams.ids, ids!);
		params = params.set(commonParams.trangThai, trangThai);
		params = params.set("IdAdminCapNhat", Variables.infoAdmin.Id);
		return this.adminDaiLyXeDAL.updateTrangThai(null, params!, "UpdateTrangThai");
	}
	public delete(ids?: any) {
		let params = new HttpParams();
		params = params.set(commonParams.id, ids!);
		return this.adminDaiLyXeDAL.delete(null, params!, undefined);
	}
}
@Injectable()
export class AdminDaiLyXeBLL_Admin extends AdminDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminDaiLyXeDAL.controllerName = "Admin";
	}
	public searchByParams(paramsData: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.email, paramsData.email);
		params = params.set(commonParams.dienThoai, paramsData.dienThoai);
		params = params.set(commonParams.partnerType, paramsData.partnerType);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1,2");
		params = params.set(commonParams.isMain, paramsData.isMain || false);
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc");
		return this.adminDaiLyXeDAL.getList(params);
	}
	public getIndex(id, isMain = false): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.id, id);
		params = params.set(commonParams.isMain, isMain? "true": "false");
		return this.adminDaiLyXeDAL.getList(params);
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminDaiLyXeDAL.getList(params);
	}
	public getIsMain(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.isMain, "true");
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminDaiLyXeDAL.getList(params);
	}
	public getByParams(params: HttpParams): Observable<any> {
		return this.adminDaiLyXeDAL.getList(params);
	}
}
@Injectable()
export class AdminDaiLyXeBLL_ThanhVien extends AdminDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminDaiLyXeDAL.controllerName = "ThanhVien";
	}
	public getByParams(params: HttpParams): Observable<any> {
		return this.adminDaiLyXeDAL.getList(params);
	}
	public updateMatKhauAnonymous(interfaceParam: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, interfaceParam.dienThoai);
		params = params.set(commonParams.matKhau, interfaceParam.matKhau)
		return this.adminDaiLyXeDAL.update(null, params, "UpdateMatKhauAnonymous");
	}
	public searchByParams(paramsData?: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems!);
		params = params.set(commonParams.displayPage, paramsData.displayPage!);
		params = params.set(commonParams.id, paramsData.id);
		params = params.set(commonParams.notId, paramsData.notId);
		params = params.set(commonParams.email, paramsData.email || undefined);
		params = params.set(commonParams.dienThoai, paramsData.dienThoai || undefined);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem!);
		params = params.set(commonParams.partnerType, paramsData.partnerType);
		params = params.set(commonParams.trangThai, paramsData.trangThai || "1");
		params = params.set(commonParams.orderString, paramsData.orderString || "NgayTao:desc,NgayCapNhat:desc");
		return this.adminDaiLyXeDAL.getList(params);
	}
	public searchByNumberPhone(dienThoai: any): Observable<any> {
		let params: any = {};
		params[commonParams.dienThoai] = dienThoai || undefined;
		return this.searchByParams(params);
	}
	public get(trangThai: string = "1"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.trangThai, trangThai);
		return this.adminDaiLyXeDAL.getList(params);
	}
	
}
@Injectable()
export class AdminDaiLyXeBLL_DongXe extends AdminDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminDaiLyXeDAL.controllerName = "DongXe";
	}
	public get(displayItems: any, trangThai: any = "1,2", orderString: any = "TenDongXe"): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, displayItems);
		params = params.set(commonParams.trangThai, trangThai);
		params = params.set(commonParams.orderString, orderString);
		return this.adminDaiLyXeDAL.getList(params);
	}
}
@Injectable()
export class AdminDaiLyXeBLL_InterfaceData extends AdminDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		this.adminDaiLyXeDAL.controllerName = "InterfaceData";
	}
	searchDinhNghiaSanPham(): Observable<any> {
		let params = new HttpParams();
		return this.adminDaiLyXeDAL.getList(params, "SearchDinhNghiaSanPham")
	}
	// "ThuongHieu"
	public searchThuongHieu(displayItems: any, displayPage: any): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, displayItems);
		params = params.set(commonParams.displayPage, displayPage);
		params = params.set(commonParams.trangThai, "1")
		return this.adminDaiLyXeDAL.getList(params, "SearchThuongHieu");
	}
	// "TinhThanh"
	public searchTinhThanhByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.trangThai, "1");
		return this.adminDaiLyXeDAL.getList(params, "SearchTinhThanh");
	}
	// "QuanHuyen"
	public searchQuanHuyenByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.IdTinhThanh, paramsData.IdTinhThanh);
		return this.adminDaiLyXeDAL.getList(params, "SearchQuanHuyen");
	}
	// "DongXe"
	public searchDongXeByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.IdThuongHieu, paramsData.IdThuongHieu);
		return this.adminDaiLyXeDAL.getList(params, "SearchDongXe");
	}
	// "LoaiXe"
	public searchLoaiXeByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.IdThuongHieu, paramsData.IdThuongHieu);
		return this.adminDaiLyXeDAL.getList(params, "SearchLoaiXe");
	}
	// "DinhNghiaSanPham"
	public searchDinhNghiaSanPhamByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.IdDongXe, paramsData.IdDongXe);
		return this.adminDaiLyXeDAL.getList(params, "SearchDinhNghiaSanPham");
	}
	// "MauSac"
	public searchMauSacByParams(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		return this.adminDaiLyXeDAL.getList(params, "SearchMauSac");
	}
	
	public searchTinhThanhAll(): Observable<any> {
		let params: any = {};
		params[commonParams.displayItems] = -1;
		return this.searchTinhThanhByParams(params);
	}
	// "ThanhVien"
	public isExistPhoneNumberThanhVien(dienThoai): Observable<any> {
		let params = new HttpParams();
		params = params.set(commonParams.dienThoai, dienThoai);
		return this.adminDaiLyXeDAL.getList(params, "IsExistPhoneNumberThanhVien");
	}
	public postThanhVienByAnonymous(data: any): Observable<any> {
		data.Id = undefined;
		return this.adminDaiLyXeDAL.insert(data, undefined, 'PostThanhVienByAnonymous');
	}

	// TranXuanDuc 20190905 Thêm ThanhVienDaiLyXeBLL_TinTuc load data trong menu tin tức.
	// "TinTuc"
	public searchTinTuc(paramsData: any): Observable<any> {
		paramsData = paramsData || {};
		let params = new HttpParams();
		params = params.set(commonParams.displayItems, paramsData.displayItems);
		params = params.set(commonParams.displayPage, paramsData.displayPage);
		params = params.set(commonParams.tuKhoaTimKiem, paramsData.tuKhoaTimKiem);
		params = params.set(commonParams.id, paramsData.Id);
		params = params.set(commonParams.idLoaiTinTuc, "1");
		params = params.set(commonParams.dependOnPublishDate, "true");
		params = params.set(commonParams.trangThai, "1");
		params = params.set(commonParams.orderString, "NgayUp:desc");
		return this.adminDaiLyXeDAL.getList(params, "SearchTinTuc");
	}

	public searchThongKeTruyCapMacDinh(): Observable<any> {
		let params = new HttpParams();
		return this.adminDaiLyXeDAL.getList(params, "SearchThongKeTruyCapMacDinh");
	}
}
@Injectable()
export class AdminDaiLyXeBLL_CommonFile extends AdminDaiLyXeBLL_Basic {
	constructor(public coreTemplate: CoreTemplate, public commonMethodsTs: CommonMethodsTs) {
		super(coreTemplate, commonMethodsTs)
		
		this.adminDaiLyXeDAL.controllerName = "commonfile";
	}
	public underAttackMode(data: any): Observable<any> {
		return this.adminDaiLyXeDAL.insertCompute(data, undefined, 'UnderAttackMode');
	}

	public searchThongKeUserOnline(): Observable<any> {
		return this.adminDaiLyXeDAL.getListHostDailyxe(null, 'GetOnlineUsers');
	}
	
}