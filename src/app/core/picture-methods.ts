import { Injectable } from '@angular/core';
import { CommonMethodsTs } from './common-methods';
import { ImagePicker, ImagePickerOptions, OutputType } from '@ionic-native/image-picker/ngx';
import { defaultValue } from './variables';
import { labelPages } from './labelPages';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { checkAvailability } from '@ionic-native/core';

@Injectable()
export class PictureMethodsTs {
    constructor(
        public imagePicker: ImagePicker,
        public commonMethodsTs: CommonMethodsTs,
        private webview: WebView) {
    }
    public cameraOptions: any = {
        quality: 100,
        allowEdit: false,
        saveToPhotoAlbum: false, // Chụp hình => true || Chọn từ Library: false 
        correctOrientation: true,
    };
    public imagePickerOptions: ImagePickerOptions = {
        quality: 100,
        maximumImagesCount: defaultValue.maxImageDescription,
        outputType: OutputType.FILE_URL
    };
    /**
        *Defined in ImagePicker.OutputType. Default is FILE_URI.
        *      FILE_URI : 0,   Return image file URI,
        *      DATA_URL : 1,   Return image as base64-encoded string
    
        * Defined in Camera.DestinationType. Default is FILE_URI.
        *      DATA_URL : 0,   Return image as base64-encoded string (DATA_URL can be very memory intensive and cause app crashes or out of memory errors. Use FILE_URI or NATIVE_URI if possible),
        *      FILE_URI : 1,   Return image file URI,
        *      NATIVE_URI : 2  Return image native URI
     */
    async choosePictureCamera(options: any) {
        return null;
    }
    // async choosePictureCamera(options: CameraOptions) {
    //     return new Promise((resolve, error) => {
    //         if (!checkAvailability('cordova-plugin-ionic-webview')) {
    //             options.destinationType = DestinationType.DATA_URL;

    //         }
    //         if (this.commonMethodsTs.platform.is("android") && options.destinationType == DestinationType.FILE_URL) {
    //             options.destinationType = DestinationType.NATIVE_URI;
    //         }

    //         this.camera.getPicture(options).then((data) => {
    //             resolve(data);
    //         }, (err) => {
    //             resolve(null);
    //         });
    //     })
    // }
    async choosePictureImagePicker(options: ImagePickerOptions) {

        let res = await new Promise((resolve, error) => {
            this.imagePicker.getPictures(options).then(async (data) => {
                if (data == "OK") {
                    data = await this.choosePictureImagePicker(options)
                }
                resolve(data);

            }, (err) => {
                resolve([]);
            });
        })
        return res;
    }
    private convertResCamera(options: any, dataUrl: any) {

        return null;
    }

    // private convertResCamera(options: CameraOptions, dataUrl: any) {
    //     let res = dataUrl;
    //     if (dataUrl) {
    //         if (!checkAvailability('cordova-plugin-ionic-webview')) {
    //             options.destinationType = DestinationType.DATA_URL;

    //         }
    //         if (options.destinationType == DestinationType.DATA_URL) {
    //             res = `data:image/jpeg;base64,${dataUrl}`;
    //         }
    //         else {
    //             res = this.webview.convertFileSrc(dataUrl);
    //         }
    //     }

    //     return res;
    // }

    private convertResImagePicker(options: ImagePickerOptions, arrDataUrl: any) {
        let res = arrDataUrl;
        if (arrDataUrl) {

            if (options.outputType == OutputType.DATA_URL) {
                if (arrDataUrl instanceof Array) {
                    for (let i = 0; i < arrDataUrl.length; i++) {
                        let dataUrl = arrDataUrl[i];
                        res[i] = `data:image/jpeg;base64,${dataUrl}`;

                    }
                }
            }
            else {
                if (arrDataUrl instanceof Array) {
                    for (let i = 0; i < arrDataUrl.length; i++) {
                        let dataUrl = arrDataUrl[i];
                        res[i] = this.webview.convertFileSrc(dataUrl);

                    }
                }
            }

        }
        return res;
    }
    /**
     * fun - openCamera: Mở camera thiết bị lên
     * params - destinationType: Kiểu dữ liệu trả về
     */
    async openCamera(destinationType = null) {
        
        return null;
    }
    // async openCamera(destinationType = DestinationType.FILE_URL) {
    //     let options = this.commonMethodsTs.cloneObject(this.cameraOptions);
    //     options.sourceType = PictureSourceType.CAMERA;
    //     options.destinationType = destinationType;
    //     options.saveToPhotoAlbum = true;
    //     let dataUrl = await this.choosePictureCamera(options);
    //     return this.convertResCamera(options, dataUrl);
    // }
    /**
     * fun - openPhotoLibrary: Mở thư viện ảnh lên
     * params - destinationType: Kiểu dữ liệu trả về
     */
    // async openPhotoLibrary(destinationType = DestinationType.FILE_URL) {
    //     let options = this.commonMethodsTs.cloneObject(this.cameraOptions);
    //     options.sourceType = PictureSourceType.PHOTOLIBRARY;
    //     options.destinationType = destinationType;
    //     let dataUrl = await this.choosePictureCamera(options);
    //     return this.convertResCamera(options, dataUrl);
    // }
    /**
     * fun - openSavePhotoAlbum: Mở camera thiết bị lên
     * params - destinationType: Kiểu dữ liệu trả về
     */
    // async openSavePhotoAlbum(destinationType = DestinationType.FILE_URL) {
    //     let options = this.commonMethodsTs.cloneObject(this.cameraOptions);
    //     options.sourceType = PictureSourceType.SAVEDPHOTOALBUM;
    //     options.destinationType = destinationType;
    //     let dataUrl = await this.choosePictureCamera(options);

    //     return this.convertResCamera(options, dataUrl);
    // }
    /**
     * fun - openPhotoLibraryMutiple: Mở thư viện ảnh lên cho chọn nhiều
     * params - outputType: Kiểu dữ liệu trả về
     *        - maximumImagesCount: Số lượng tối đa hình được chọn

     */
    async openPhotoLibraryMutiple(maximumImagesCount = defaultValue.maxImageDescription, outputType = OutputType.FILE_URL) {
        let options = this.commonMethodsTs.cloneObject(this.imagePickerOptions);
        options.outputType = outputType;
        options.maximumImagesCount = maximumImagesCount;
        let arrdataUrl = await this.choosePictureImagePicker(options);
        return this.convertResImagePicker(options, arrdataUrl);
    }
    /**
     * actionChoosePictureSingal: chọn ảnh đơn
     */
    actionChoosePictureSingal() {
        return new Promise((resolve: any, error) => {
            try {
                this.commonMethodsTs.actionSheetController.create({
                    header: labelPages.choosePhoto,
                    buttons: [{
                        text: labelPages.takePhoto,
                        icon: 'camera',
                        handler: () => {
                            this.openCamera().then(res => {
                                resolve(res)

                            });
                        }
                    }, {
                        text: labelPages.takeGallery,
                        icon: 'images',
                        handler: () => {
                            this.openPhotoLibraryMutiple(1).then(res => {
                                resolve(res)

                            });

                        }
                    }, {
                        text: labelPages.cancel,
                        icon: 'close',
                        role: 'cancel',
                        handler: () => {
                            resolve();
                        }
                    }]
                }).then(actionsheet => {
                    actionsheet.present();
                });
            } catch (error) {
                resolve(null);
            }

        });
        // return actionSheet;
    }
    /**
     * actionChoosePictureSingal: chọn ảnh nhiều
     */
    actionChoosePictureMutiple(maximumImagesCount = defaultValue.maxImageDescription) {
        return new Promise((resolve, error) => {
            try {
                this.commonMethodsTs.actionSheetController.create({
                    header: labelPages.choosePhoto,
                    buttons: [{
                        text: labelPages.takePhoto,
                        icon: 'camera',
                        handler: () => {
                            this.openCamera().then(res => {
                                if (res) {
                                    resolve([res]);
                                }
                                else {
                                    resolve([])
                                }
                            });

                        }
                    }, {
                        text: labelPages.takeGallery,
                        icon: 'images',
                        handler: async () => {
                            this.openPhotoLibraryMutiple(maximumImagesCount).then(res => {
                                resolve(res);

                            });

                        }
                    }, {
                        text: labelPages.cancel,
                        icon: 'close',
                        role: 'cancel',
                        handler: () => {
                            resolve([]);
                        }
                    }]
                }).then(actionsheet => {
                    actionsheet.present();
                });
            } catch (error) {
                resolve([]);

            }

        });
        // return actionSheet;
    }
}