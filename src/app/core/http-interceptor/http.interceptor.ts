import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpParams,
    HttpHeaders,
    HttpResponse
} from '@angular/common/http';
import { Variables, commonMessages, listUpdateAddVersion, commonParams } from '../variables';
import { CommonMethodsTs } from '../common-methods';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { tap, timeout, switchMap, filter, take } from 'rxjs/operators';
import { CoreVariables } from '../core-variables';
import { ErrorApi } from '../entity';
import { AuthAdminService } from './auth/auth-admin.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {
    public corsAnywhere = "";
    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private isAdmin = true;
    constructor(
        public commonMethodsTs: CommonMethodsTs,
        public authAdminService: AuthAdminService,

    ) { }
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        let bearerToken: string = null!;
        let reg = /[\"]/g;
        let token: string = null!;
        try {
            token = request.headers.get("Authorization").replace('bearer ', ''); // tronghuu 95 get chuỗi bearerToken gắn trong header (thường là bearerToken của thành viên)
            this.isAdmin = !token;

        } catch {
            this.isAdmin = true;

        }
        let url = request.url.toLocaleLowerCase();
        if (!token) {
            Variables.tokenAdmin = this.commonMethodsTs.getTokenAdmin();
            token = Variables.tokenAdmin;
        }
        if (token) {
            bearerToken = 'bearer '.concat(token);
            bearerToken = bearerToken!.replace(reg, "");
        }



        let headers: HttpHeaders = request.headers;
        if (bearerToken) {
            headers = headers.set("Authorization", bearerToken);
        };


        // let url = request.url;
        /*tronghuu95 20190911112500 tạm đóng lại => để test native http*/
        let _URL = new URL(url);
        let domain = _URL.origin;
        if (+_URL.port > 0) {
            domain = _URL.origin.replace(`:${_URL.port}`, "");
        }
        if (!this.commonMethodsTs.checkPluginAvailability("http") && Variables.haveAnywhere) {
            // Dùng để vượt qua cors hợp lệ https://medium.com/netscape/hacking-it-out-when-cors-wont-let-you-be-great-35f6206cc6468
            url = this.corsAnywhere + url;
        }

        let body = request.body;

        if (request.method == "POST" && !(body instanceof File) && !(body instanceof FormData)) {
            body = this.removeAttrNull(body);
        }

        // Mr. Trung Test
        if (_URL.href.indexOf('raovat/interfacedata') > 0 || _URL.href.indexOf('raovat/interactdata') > 0) {
            headers = headers.set("Content-Type", 'application/json;v=1.0');
        }

        request = request.clone({
            headers: headers,
            params: this.removeParamsNull(request.params),
            body: body,
            url: url

        });
        //#endregion
        //#region 2. Thực hiện gọi api
        //2.1. Thực hiện api ngay với đk là những api đặc biệt hoặc phải tồn tại bearerToken cũng như còn thời hạn

        if (this.isNotCheckExpiryTime(request) || !this.commonMethodsTs.isTokenExpired(token)) {
            return this.callApi(request, next);
        }
        else {
            if (!token) {
                this.refreshTokenSubject.next(null);

                throw commonMessages.E401;
            }
            return this.handle401Error(request, next)
            // tronghuu95 20190121 tạm thời cho quay lại trnag login khi bearerToken hết thời hạn
        }
        //#endregion

    }
    //tronghuu95 20181012 loại bỏ các api không cần bearerToken không cần kiểm tra 
    private isNotCheckExpiryTime(request: HttpRequest<any>): boolean {
        let url = request.url.toLocaleLowerCase();
        return (url.endsWith("/token") && request.method == "POST") ||
            url.indexOf("/interfacedata") >= 0 ||
            ((url.endsWith("/commonfile/getencryptmd5") || url.endsWith(".js") || url.endsWith(".json")) && request.method == "GET") ||
            (url.indexOf("/api/raovat/thanhvien") >= 0 && request.method == "POST") ||
            (url.indexOf("/sendsms") >= 0) ||
            (url.indexOf("/smsphone") >= 0) ||
            (url.indexOf("/timeremainofmaxacthuc") >= 0) ||
            (url.indexOf("/updatematkhauanonymous") >= 0 && request.method == "PUT") ||
            (url.indexOf("/interactdata") >= 0) ||
            (url.indexOf("/dailyxedata") >= 0) ||
            (url.indexOf("/commonfile/createfilechitietdangtin") >= 0) ||
            (url.indexOf("/commonfile/getchitietdangtin") >= 0) ||
            (url.indexOf("/commonfile/checkremoteconfig") >= 0) ||
            (url.indexOf("/xacthucbymaxacthuc") >= 0) ||
            (url.indexOf("/uploadfile/stream") >= 0) ||
            (url.indexOf("/email") >= 0) ||
            (url.indexOf("/registerthanhvien") >= 0) ||
            (url.indexOf("/elasticsearch") >= 0) ||
            (url.indexOf("/momo") >= 0);

    }
    //tronghuu95 23/07/2018 dùng để loại bỏ các params null hay underfiend
    private removeParamsNull(params: HttpParams) {
        params.keys().forEach(key => {
            let v = params.get(key);
            if (v == null || v == undefined) {
                params = params.delete(key);
            }
        });

        return params;
    }

    private removeAttrNull(obj: any) {
        for (let key in obj) {
            let v = obj[key];
            if (v == null || v == undefined) {
                delete obj[key];
            }
        }

        return obj;
    }
    /** getAppVersion: Get apiVersion
    */
    private getApiVersion(url: string) {
        try {
            url = url.toLocaleLowerCase();
            let lApiAddAppVersion = Object.keys(listUpdateAddVersion);
            for (let index = 0; index < lApiAddAppVersion.length; index++) {
                const element = lApiAddAppVersion[index];
                let strApi = element.toLocaleLowerCase();
                if ((url.indexOf(strApi) >= 0)) {
                    return listUpdateAddVersion[element];
                }

            }

        } catch (error) {

        }

        return null;
    }
    private addToken(request: HttpRequest<any>, token: string) {
        return request.clone({
            setHeaders: {
                'Authorization': `Bearer ${token}`
            }
        });
    }
    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            let authService: any = this.authAdminService;
            return authService.refreshToken().pipe(
                switchMap((r) => {
                    this.isRefreshing = false;
                    let jwt = authService.getToken();
                    if (!jwt) {
                        this.refreshTokenSubject.next(null);
                        throw commonMessages.E401;
                    }
                    this.refreshTokenSubject.next(jwt);
                    return next.handle(this.addToken(request, jwt));
                }));

        } else {
            return this.refreshTokenSubject.pipe(
                
                switchMap(jwt => {
                    return next.handle(this.addToken(request, jwt));
                }));
        }
    }
    //tronghuu95 20190121 tạm thời đóng lại do chưa có ràng buộc nhiều
    private callApi(request: HttpRequest<any>, next: HttpHandler) {

        let url = request.url.toLocaleLowerCase();
        url = url.replace(this.corsAnywhere, "");
        let domain = new URL(url).host;
        let apiVersion = this.getApiVersion(url);
        if (apiVersion) {
            let _params = request.params.append(commonParams.apiVersion, apiVersion);
            request = request.clone({
                params: _params
            });
        }
        if (request.method != "GET") {
            //tronghuu95 20190916090600 bổ sung thêm danh sách không hiển thị spinner
            if (
                !(url.indexOf("/interfacedata/") >= 0)
                && !(url.indexOf("/interactdata/") >= 0)
                && !(url.indexOf("/files/") >= 0)
                && !(url.indexOf("/elasticsearch") >= 0)
                && !(url.indexOf("/checkremoteconfig") >= 0)


            ) {
                this.commonMethodsTs.startLoader(request);

            }

        }

        return next.handle(request).pipe(timeout(CoreVariables.timeoutApi)).pipe(
            tap(
                (event: any) => {
                    if (event instanceof HttpResponse && request.method != "GET") {
                        this.commonMethodsTs.stopLoader(request);
                    }
                    CoreVariables.lHealthDomainApi[domain] = true; // sức khoe của domain này OK
                },
                (error: any) => {
                    let errorApi = new ErrorApi();
                    errorApi.Message = commonMessages.E001;
                    errorApi.Url = request.url;

                    errorApi.Error = error;
                    if (request.method != "GET") {
                        this.commonMethodsTs.stopLoader(request);
                    }
                    if (error && error.name == "TimeoutError") {
                        CoreVariables.lHealthDomainApi[domain] = false; // sức khoe của domain này không tốt
                    }
                    else {
                        if (+error.status > 400) {
                            CoreVariables.lHealthDomainApi[domain] = false; // sức khoe của domain này không tốt
                        }

                        switch (+error.status) {
                            case 0:
                                errorApi.Message = commonMessages.E001;
                                break;

                            case 400:
                                errorApi.Message = error.error == "Invalid username or password." ? commonMessages.M019 : error.error;
                                throw errorApi.Message;
                                break;

                            case 404:
                                errorApi.Message = commonMessages.E404;
                                break;

                            case 408:
                                errorApi.Message = commonMessages.E001;
                                break;

                            case 401:
                                errorApi.Message = commonMessages.E401;
                                return this.handle401Error(request, next);
                                break;

                            default:
                                errorApi.Message = commonMessages.E001;
                                break;
                        }
                    }
                    
                    throw Variables.isDev ? JSON.stringify(errorApi) : errorApi.Message;
                    // throwError(Variables.isDev ? JSON.stringify(errorApi) : errorApi.Message);

                }
            )
        )



    }
}
