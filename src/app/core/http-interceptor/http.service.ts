import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { InterceptorService } from "./http.interceptor";
import { NativeHttpModule } from '../native-http';


@NgModule({

    imports: [
        NativeHttpModule,
    ],

    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true,
        },

    ]

})
export class HttpServiceModule { }