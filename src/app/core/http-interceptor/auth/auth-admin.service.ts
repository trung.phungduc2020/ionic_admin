import { Injectable } from '@angular/core';
import { commonParams, Variables, commonMessages, localStorageData, projectName, commonAttr } from '../../variables';
import { CoreHttp } from '../../core-http';
import { CommonMethodsTs } from '../../common-methods';
import { ResponseBase, UserDevice } from '../../entity';
import { ThanhVienRaoVatBLL_InteractData } from '../../api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { CommonService } from 'src/app/forms/member/services/common.service';
import { RaoLocalStore } from '../../rao-localstore';
import { AuthService } from './auth.service';
// import { config } from './../../config';
// import { Tokens } from '../models/tokens';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminService extends AuthService {
  constructor(
    public coreHttp: CoreHttp,
    public commonMethodsTs: CommonMethodsTs,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    public commonService: CommonService,
    public raoLocalStore: RaoLocalStore
  ) {
    super(coreHttp, commonMethodsTs, thanhVienRaoVatBLL_InteractData, commonService, raoLocalStore);
    this.nameStore = projectName + "." + commonParams.token_roleAdmin;
    this.token_requireRole = commonParams.token_roleAdmin;
  }
  public doLogin(response: any) {
    try {
      var uk: any = this.commonMethodsTs.decodeRSA(response[commonParams.cuk]);
      var at: any = this.commonMethodsTs.decodeDES(response[commonParams.cat], uk);
      let jsonToken = JSON.parse(at);
      let data = this.convertDataToken(jsonToken);
      this.setLocal(data);
      Variables.infoAdmin = this.getInfo();
      Variables.tokenAdmin = this.getToken();
      this.commonMethodsTs.mainService.fcmService.addTopicAdmin(Variables.infoAdmin.Id);
    } catch {
      throw commonMessages.M019;
    }
  }


  public doLogout() {
    let info = Variables.infoAdmin;
    this.commonMethodsTs.removeAdminFromToken();
    this.commonMethodsTs.mainService.fcmService.removeTopicAdmin(info.Id);
    this.removeLocal();
  }
  public setInfo(data: any) {
    Variables.infoAdmin = data;
    let dataoLcal = this.getLocal();
    dataoLcal[commonParams.common][commonParams.token_admin64] = this.commonMethodsTs.b64EncodeUnicode(JSON.stringify(data));
    return this.setLocal(dataoLcal);
  }
  public removeLocal()
  {
    try {
      localStorage.removeItem(this.nameStore); // mã hóa dữ liệu 
      Variables.infoAdmin = {};
      Variables.tokenAdmin =null;
      return true;
    } catch (error) {
      
    }
    return false;
  }
}
