import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { commonParams, apiDaiLyXe, apiDaiLyXe_Sufix_DB, defaultWxh, Variables } from '../../variables';
import { checkAvailability } from '@ionic-native/core';
import { CoreHttp } from '../../core-http';
import { CommonMethodsTs } from '../../common-methods';
import { optionsHttp, ResponseBase, UserDevice } from '../../entity';
import { CommonService } from 'src/app/forms/member/services/common.service';
import { RaoLocalStore } from '../../rao-localstore';
import { TripleDESService } from 'src/app/shared/services';
import { ThanhVienRaoVatBLL_InteractData } from '../../api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { Variable } from '@angular/compiler/src/render3/r3_ast';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public tripleDes: TripleDESService = new TripleDESService();
  public nameStore;
  public token_requireRole;
  constructor(
    public coreHttp: CoreHttp,
    public commonMethodsTs: CommonMethodsTs,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    public commonService: CommonService,
    public raoLocalStore: RaoLocalStore
  ) { }

  public buildUrlToken(controllerName: string) {

    return apiDaiLyXe + apiDaiLyXe_Sufix_DB + controllerName;
  }
  public async checkExist(username, password) {
    let passwordMd5 = this.commonMethodsTs.convert_toMD5(password);
    this.commonMethodsTs.startLoading();
    let res = false;
    try {
      let body: any = {}
      body[commonParams.username] = username;
      body[commonParams.password] = passwordMd5;
      body[commonParams.token_requireRole] = this.token_requireRole;
      let cat: any = this.commonMethodsTs.encodeDES(JSON.stringify(body));// nội dung body được mã hóa từ des với (*)
      let cuk: any = this.commonMethodsTs.getKeyDesByRsa();// keydes* được mã hóa bằng rsa 
      res = await this._onlogin(cat, cuk).toPromise();
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    this.commonMethodsTs.stopLoading();
    return res != null;

  }
  public login(username: string, passtoMD5: string): Observable<any> {
    let body: any = {}
    body[commonParams.username] = username;
    body[commonParams.password] = passtoMD5;
    body[commonParams.token_requireRole] = this.token_requireRole;
    let cat: any = this.commonMethodsTs.encodeDES(JSON.stringify(body));// nội dung body được mã hóa từ des với (*)
    let cuk: any = this.commonMethodsTs.getKeyDesByRsa();// keydes* được mã hóa bằng rsa 
    return this._onlogin(cat, cuk).pipe(
      tap(response => {
        this.doLogin(response);
      }
      ))
  }
  public _onlogin(cat: string, cuk: string): Observable<any> {
    let fullUrl = this.buildUrlToken('token');
    let options: any = new optionsHttp();
    if (checkAvailability('cordova.plugin.http') === true) {
      options.headers = { "Content-Type": 'application/x-www-form-urlencoded' }
    }
    let data = new FormData();
    data.append(commonParams.cat, cat);
    data.append(commonParams.cuk, cuk);
    try {
      return this.coreHttp.post(fullUrl, data, options)
        ;
    } catch (error) {
      throw error
    }

  }
  public refreshToken() {
    let token_refreshToken = this.getRefreshToken();
    let body: any = {};
    body[commonParams.token_refreshToken] = token_refreshToken;
    body[commonParams.token_requireRole] = this.token_requireRole;
    let cat: any = this.commonMethodsTs.encodeDES(JSON.stringify(body));// nội dung body được mã hóa từ des với (*)
    let cuk: any = this.commonMethodsTs.getKeyDesByRsa();// keydes* được mã hóa bằng rsa 
    return this._onlogin(cat, cuk);
  }
  public doLogin(response: any) { }
  public doLogout() { }

  public logout() {
    try {
      this.doLogout();
      return true;
    } catch {
      return false;
    }
  }
  public setLocal(data: any) {
    try {
      let encryptData = this.tripleDes.encrypt(JSON.stringify(data));
      localStorage.setItem(this.nameStore, encryptData); // mã hóa dữ liệu 
      return true;
    } catch (error) {

    }
    return false;

  }

  public removeLocal() {
    try {
      localStorage.removeItem(this.nameStore); // mã hóa dữ liệu 
      return true;
    } catch (error) {

    }
    return false;

  }
  public getToken() {
    let data = this.getLocal();
    return this.commonMethodsTs.getData(data, `${commonParams.access}.${commonParams.token_accessToken}`, null);
  }
  public getRefreshToken() {
    let data = this.getLocal();
    return this.commonMethodsTs.getData(data, `${commonParams.access}.${commonParams.token_refreshToken}`, null);
  }
  public getInfo() {
    try {
      let dataoLcal = this.getLocal();
      let infoB64 = this.commonMethodsTs.getData(dataoLcal, `${commonParams.common}.${commonParams.token_admin64}`);
      let data = JSON.parse(this.commonMethodsTs.b64DecodeUnicode(infoB64));
      data.UrlHinhDaiDien = this.commonMethodsTs.builLinkImage(data.RewriteUrl, data.IdFileDaiDien, defaultWxh.two);;
      return data;
    } catch (error) {

    }
    return {};

  }
  public setInfo(data: any) {
    let dataoLcal = this.getLocal();
    dataoLcal[commonParams.common][commonParams.token_admin64] = this.commonMethodsTs.b64EncodeUnicode(JSON.stringify(data));
    return this.setLocal(dataoLcal);
  }
  public getLocal() {
    try {
      let strData = localStorage.getItem(this.nameStore);
      return JSON.parse(this.tripleDes.decrypt(strData));
    } catch (error) {

    }
    return null;

  }

  public convertDataToken(d: any): any {
    let data = {};
    let dataAccess = {};
    dataAccess[commonParams.token_accessToken] = d[commonParams.token_accessToken];
    dataAccess[commonParams.token_expiresIn] = d[commonParams.token_expiresIn];
    let token_refreshToken:string = d[commonParams.token_refreshToken];
    if(token_refreshToken==null || token_refreshToken.isEmpty()){
      token_refreshToken = this.getRefreshToken();
    }
    dataAccess[commonParams.token_refreshToken] =token_refreshToken;
    let strAdmin64 = commonParams.token_admin64;
    let dataCommon = {};
    dataCommon[strAdmin64] = d[strAdmin64];
    data[commonParams.access] = dataAccess;
    data[commonParams.common] = dataCommon;
    return data;
  }

}
