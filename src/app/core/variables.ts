import { Validation, ItemRecord, Admin } from "./entity";
import { environment } from "src/environments/environment";
import { labelPages } from './labelPages';
declare var require: any;
export const nameSource = 'raovat'
/** listUpdateAddVersion ds cần bổ sung thêm params api-version */
export const listUpdateAddVersion = { "InterfaceData/SearchDangTin": "2.0" }
export class defaultValue {
	public static idTinhThanh = 31;
	public static idQuanHuyen = 307;
	public static maxImageDescription = 40;
	public static maxImageForLayout = 4;
	public static percentImageForLayout = 0.1; // tương ứng 10%
	public static maxLengthMoTaNganTinDang = 120; // giới hạn khi lấy kí tự trong MoTaChiTiet truyền vào MoTaNgan
	public static minTimeStopPintop = 5; // giới hạn khi lấy kí tự trong MoTaChiTiet truyền vào MoTaNgan
	public static defaultMoTaChiTiet = "Bán/Mua";
	public static typeUnit: any = null; //'million'; // đơn vị chuẩn của tiền
	public static maxDanhGiaDefault = 2;
	public static maxLengthInput = 50;
	public static maxLengthInputAddress = 100;
}
export class lCaches {
	public static tindang = "cache-tin-dang";
}
export class Variables {
	public static packageIdAndroid: string = "vn.com.raoxe.admin";
	public static bundleId: string = "vn.com.raoxe.admin";
	public static domainES: string = environment.domainES || "https://stgesdlx02.dailyxe.com.vn";
	public static domainDynamicLinks: string = environment.domainDynamicLinks;
	public static domainDeepLinks: string = environment.domainDeepLinks;
	public static adminTheme: string = "../themes/default/crisis-list.component.html";
	public static isShownConfirmDialog: boolean = false;
	public static listAllowNavigate: any[] = ["/admin", "/admin/home"];
	public static infoAdmin: any = null!;
	public static infoThanhVien: any = null!;
	public static tokenAdmin: string = null!;
	public static tokenThanhVien: string = null!;
	public static isLoginThanhVien: boolean = false;
	public static isLoginAdmin: boolean = false;
	public static isMobile: boolean = false;
	public static isDev: boolean = !environment.production;
	public static haveAnywhere: boolean = environment.haveAnywhere; // dùng để kt xem coi có dùng nativeHTTP => vì devapp cũng k dùng đc nativeHTTP
	public static autoUpdateLayoutImages: boolean = environment.autoUpdateLayoutImages;
	public static asyceUploadFile: boolean = environment.asyceUploadFile;
	public static rootMarginLazyLoading: string = '1600px';
	public static textLockAccount: string = 'lock';
	public static isOnline: boolean = false;
	public static countNotification: any;
	public static defaultFileName: string = "New-File"; //seconds
	public static ratePointToVND: number = 1;
	public static paramsMarketing: any = "";
	public static cacheVariables: any = {};
	public static nView: any = 1;
	public static colorBanner: any = 1;
	public static isHomePage: boolean = false;
	public static wxhAllowLazyLoading: any = 0;
	public static isViewAdmin: any = false;
	public static isUploadForFireStore: boolean = true;
	public static search: any = {}; // các giá trị search
	public static isReady: boolean = false;
	public static limitByteForImage = environment.limitByteForImage || 1000000;
	public static limitQualityForImage = environment.limitQualityForImage || 85;
	public static compressedForImageX: number = environment.compressedForImageX || 900;
	public static compressedForImageY: number = environment.compressedForImageY || 1200;
	public static actionDeepLink = environment.actionDeepLink || "rao-xe";
	public static appStoreID = environment.appStoreID || "1486119532";
	public static listDomainCheckConfig = environment.listDomainCheckConfig;
	public static objMaCauHinh: any = {};
	public static isIos: boolean = false;
	public static appVersion: string = "";
	
	public static dungSms3GKhiGuiXongLanThu: number = environment.dungSms3GKhiGuiXongLanThu || 2;
	//Số lần gửi lại mã sms tối đa với 1 type
	public static smsLoopDefault = environment.smsLoopDefault || 3;
	//Time gian khi bị lock khi gửi sms 1 type quá smsLoopDefault lần
	public static smsTimeBetaDefault = environment.smsTimeBetaDefault || 15;
	//Time block khi firebase bị khóa khi gửi nhiều
	public static smsTimeBlockForFireBase = environment.smsTimeBlockForFireBase || 60;
	//Thời gian hiệu lực của mã xác thực dùng chung email và sdt
	public static timeVerifyValid = environment.timeVerifyValid || 2; // phút
	public static timeVerifyByNumberPhone: any = {};
	public static timeVerifyByEmail: any = {};
	public static intervalTimeServer: any = 30000; //ms
	public static uniqueDeviceID: any = null;
	public static tuKhoaHot: any = environment.tuKhoaHot || ["mua xe", "bán xe", "xe tải", "xe cũ", "mua xe cũ", "xe gia đình", "xe du lịch", "xe bán tải", "xe giá rẻ"];
	public static setDomainConfigDoamin(config: any) {
		apiHost = config.apiHost || apiHost;
		apiHostDailyXe = config.apiHostDailyXe || apiHostDailyXe;
		apiHost_Sufix_DB = config.apiHost_Sufix_DB || apiHost_Sufix_DB;
		apiHistoryHost_Sufix_DB = config.apiHistoryHost_Sufix_DB || apiHistoryHost_Sufix_DB;
		apiDaiLyXe = config.apiDaiLyXe || apiDaiLyXe;
		apiDaiLyXe_Sufix_DB = config.apiDaiLyXe_Sufix_DB || apiDaiLyXe_Sufix_DB;
		apiCdn = config.apiCdn || apiCdn;
		projectName = config.projectName || projectName;
	}
	public static presentLoadingWithOptions: any;
	// public static timeDateServer: number = CoreVariables.timeDateServer;
	/*tronghuu20190903112200 dùng để xử lý một số biến đặt biệt cho từng loại môi trường */
	public static functionDev() {
		// allowFirebaseAnalytics = false;
	}
	public static functionProd() {
		// allowFirebaseAnalytics = true;
	}
	public static functionGetVersionApp(ver, newVer) {
		versionApp = ver;
		newVersionApp = newVer;
		hasNewVersion =  versionApp.replace(/\./g,'') != newVersionApp.replace(/\./g,'') ? true : false;
	}
	public static listEmailMoiDaXacThuc: any = [];
	public static listNumberPhoneMoiDaXacThuc: any = [];
	public static listUserRank: any = [];


	public static listImageBannerDefault: any[] = [
		"/assets/banner-1.jpg",
		"/assets/banner-2.jpg",
		"/assets/banner-3.jpg",
	];
	
	public static listImageBanner: any[] = [];


}

export const isPhoneTestFireBase = false;
export const nameDBImageFireStore = 'ImageStorage'
export var allowFirebaseAnalytics = false; /*dùng để cho phép chức năng thống kê Analytics bằng firebase */
export var configCK = {
	"height": '300px',
	"width": '100%',
	"resize_enabled": false,
	"toolbar": [
		{
			"name": "document",
			"items": [
				"Source",
				"Undo",
				"Redo",
				"Image",
				"SelectAll",
				"Bold",
				"Italic",
				"Underline"
			]
		}
	],
	// "extraPlugins": 'divarea',
	"readOnly": true,
};
export const storageKeyImage = 'dailyxe__image';
//tronghuu95 20181031 các k dùng để mã hóa cũng nhưng gải mã các gói dữ liệu
export const varks = {
	// k để giải mã các thông tin được đóng gói từ ruk
	rrk: `
    MIICXQIBAAKBgQCAxhFc9tk9SJ8H2dyG9IJf5sfeojG3tpbUE/Jvks1nK+B+8eWm
    SO4c+jXpgW1k+HK42/gc8jf1tl6ay2lWybF2C65Jef7oJztCPrZy0ZU1WNB/mpI8
    617d8AsCLH8NJnDhHEk3f6L9Wx4xHtlDUzyOJWJ3tU9FTISSMaQRVw8+JQIDAQAB
    AoGACLFWKrhfIcvtMFJ8mH+Y7XBevaClSomA1QAjtXRreTN1DBy4K+lwaXRaf1DF
    WbOLyv7OWOXg8S1GZZyll741xHGW1+QtaUsN1KRhY5LrZXdk5hczvwjkqUBlm2pg
    jPQtFjZDXaE/8QEeTCg4ht3Eva9lpcOQ65N8O2GfoqVjNQECQQDHR46FKexzWc2k
    Svy0Ap5OWxDcPbgfHVsSrrLCiR4DCo/wPfYf+TkvRyKbAbX5t4Q30h/w0G7b7ZW0
    kUA65mh5AkEApW0gKcMTSP6jOyuUa2eD5jaepp1M+lXBh8jgxlErgi6gmiFALkUu
    HusezaPBwmVcFwjeaOlcbzZMsw/KzR9wDQJBAKf2khN/IIKhIIjng1MeGdwlOXLI
    upXPImH+yUDaXWdm0adGMlsErsRAitRnfr/5hAGgo7dPlwDboaDOBHsglNECQH98
    8TueHl+z9oJkXJbFiQ5Da9NGCQwoSOTnd+r+pURHwPfnxjmikR+83dlnaazyRp1t
    9VHu/pCqzUN2WTnqD4UCQQCMLe+CRPwc5hpa+bXJBAmy4kukt6dh2OVKUKPGacSX
    ymuh3YjjDNqhzdINl1Rs3K6LvnwUJfX2B4W8gjmNgmw/
    `,
	// k để cung cấp cho các server đóng gói
	ruk: `
    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAxhFc9tk9SJ8H2dyG9IJf5sfe
    ojG3tpbUE/Jvks1nK+B+8eWmSO4c+jXpgW1k+HK42/gc8jf1tl6ay2lWybF2C65J
    ef7oJztCPrZy0ZU1WNB/mpI8617d8AsCLH8NJnDhHEk3f6L9Wx4xHtlDUzyOJWJ3
    tU9FTISSMaQRVw8+JQIDAQAB
    `,
	tdk: `--@Đại lý xe 2018@--`, //`-Đại lý XE @@2018@@-`,
	// k để đóng gói gửi tới server cung cấp k này
	rukForApi: `MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHCIiepJqVZwdN+ERYLWUU0+Vte1fHFJvfzbmonAluZxg3A5ETHh2CshAbKSQxwdcGZ4z81kFpCOPX/3ppATjftHNI1jvB84BbG70psjFR/C1SWrswea8YE4Qn801KqJzEWiu7t0IdsAjsXfXKouWxmrfR5F6rn5BpvterfHAmUxAgMBAAE=`,
}
export const noImage = "/assets/no-image.jpg";
export const noImageAvailabelFolder = "/resources/Images/hinh-anh-khong-ton-tai/";
export const noImageUuDai = "/assets/gift.png";
export const noImageUser = "/assets/avatar.svg";
export const lazyLoadImageDefault = "/assets/lazy-load-image.jpg";
export const checkIconSuccess = "/assets/check-verify.jpg";
export const typeHistoryPoint = {
	0: "Chưa xác định",
	1: "Tặng đăng ký",
	2: "Mua",
	3: "Tặng đăng bài (10 bài)",
	4: "Sử dụng",
	5: "Tặng cài App",
	6: "Tặng nhập mã giới thiệu",
	7: "Tặng chia sẻ mã giới thiệu",
}
export const mucDichUpdatePoint = {
	suDungDoiGioDangTin: 'Sử dụng đổi giờ đăng tin',
}

export const placeHolder = {
	hoTen: "Ví dụ: Nguyễn Thị Hồng Hạnh",
	hoDem: "Ví dụ: Nguyễn Thị Hồng",
	ten: "Ví dụ: Hạnh",
	matKhau: "Mật khẩu cần ít nhất 8 ký tự",
	matKhauCu: "Mật khẩu cũ",
	matKhauMoi: "Mật khẩu mới",
	matKhauXacNhan: "Xác nhận mật khẩu",
	diaChi: "Ví dụ: Tầng trệt toà nhà D-Head 371 Nguyễn Kiệm, trong khuôn viên đại học Mở.",
	dienThoai: "Ví dụ: 08 8080 8080",
}
export const NotificationSystemType = {
	sendToAll: 1,
	sendToUser: 2,
	sendToLogin: 3
}
export const MoneyDiscountType = {
	Point: 1,
	Percent: 2,
	Cash: 3
}
export const NotificationSystemTemplate = {
	F001: "F001", // Đăng ký thành công
	F002: "F002", // Thăng cấp Ranking thành công
	F003: "F003", // Mua point thành công
	F004: "F004", // Duyệt tin thành công
	F005: "F005", // Duyệt tin thất bại
	F006: "F006", // Warning: {0}
	F007: "F007", // MarketingPlan Install App
	F008: "F008", // Gửi đánh giá thành công - {0}
	F009: "F009", // Gửi đánh giá thất bại - {0}
	F010: "F010", // Cập nhật email thành công
	F011: "F011", // Tạo mới email thành công
	F012: "F012", // Giới thiệu thành công.
	E001: "E001", // Send mail - Login from a new device
}
export const maxFilesUpload: number = 30;
export const maxCapacityUpload: number = 10000;// cái này chưa dùng mục đích giới hạn dung lượng của file Upload
export const sitekeyVariable: string = '6LeJjEgUAAAAAAH-gIAIIXelGJR5q-vf1Dwa-XYO'; //mail  hữu
export const keyCacheParamsSplitChar: string = ']{!'; // nguyencuongcs 20180318
export const $ = require('jquery');
export const at2 = "'../themes/default/crisis-list.component.pug'";
export const at3 = "./crisis-list.component.pub";

export const lHostUseDev: string[] = ["192.168.11.96"]; // ==> tronghuu95 20190521 domain này bị block cors
// apiHost
export var apiHostDailyXe = environment.apiHostDailyXe;//"https://docker.dailyxe.com.vn"; // https://dailyxe.com.vn , https://docker.dailyxe.com.vn  
export var apiHostDailyXe_Sufix_DB: string = "/api/";
export var apiHost: string = environment.apiHost; // https://dev16raovatapi.dailyxe.com.vn
export var apiHost_Sufix_DB: string = environment.apiHost_Sufix_DB;
export var apiHistoryHost_Sufix_DB: string = environment.apiHistoryHost_Sufix_DB;
// apiDaiLyXe
export var apiDaiLyXe: string = environment.apiDaiLyXe;//http://office.1onnet.com:5005// "https://dev16dailyxeapi.dailyxe.com.vn"; //==> For DEV
export var apiDaiLyXe_Sufix_DB: string = environment.apiDaiLyXe_Sufix_DB;
// apiCdn
export var apiCdn: string = environment.apiCdn; //"https://dev16raovatcdn.dailyxe.com.vn"; // https://dev16raovatapi.dailyxe.com.vn
//apiCompute
export var apiCompute: string = "https://compute02.dailyxe.com.vn";
export var apiCompute_Sufix_DB: string = "/api/";
// #endregion
export var versionApp: any = "";
export var newVersionApp: any = "";
export var hasNewVersion: any = true;
export const durationForMessage: number = 5000;
export const uploadFromWebUri: string = "webUri";
export const idNhomQuyenAdmin = 1; //tronghuu95 19032017 xác định quyền admin
export const urlFileUpload = "/AppData/FileUploads/";
export const configImageExtension: string[] = ["jpg", "jpeg", "gif", "png", "bmp"];
export const configIconExtension: string[] = ["ico"];
export const configFlashExtension: string[] = ["swf"];
export const configMediaExtension: string[] = ["3gp", "avi", "mp3", "mp4", "mpg3", "mpg4", "flv", "swf", "vob", "wma", "wmv", "wav"];
export const configDocumentExtension: string[] = ["txt", "chm", "html", "htm", "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx"];
export const shortDateForSql = "yyyy/MM/dd";
export const shortDateTimeForSql = "yyyy/MM/dd HH:mm:ss";
// nguyencuongcs 20180331: nên dùng configImageExtension.concat(configIconExtension).concat(configFlashExtension).concat(configMediaExtension).concat(configDocumentExtension)
export const configIsAllowExtension: string[] = ["jpg", "jpeg", "png", "gif", "bmp", //images
	"ico", //icon
	"txt", "chm", "html", "htm", "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx",//documents
	"rar", "zip", "7z", "gzip", //nen
	"mp3", "mp4", "wmv", "flv", "wav", "vob", "3gp", "swf" //media  
];
export const hostNotCheckCaptChas: string[] = ["http://localhost:8100"];
export const imageBackgroundHome: string = "/assets/images/bg-banner.png";
//file manager
export const fileTypeVideoExtension = ["avi", "mp4", "wma", "wmv", "wav"]; // Media collapse
export const fileTypeSoundExtension = ["mp3", "mpg3"]; // Media collapse
export const fileTypeWordExtension = ["doc", "docx"];
export const fileTypeExcelExtension = ["xls", "xlsx"];
export const fileTypePowerpointExtension = ["ppt", "pptx", "pptm"];
export const fileTypePdfExtension = ["pdf"];
export const DefaultNhomThuocTinh1 = "Default1";
export const DefaultClassImageInCkEditor = "figure-img";
export const defaultNoMessage = "Chưa xác định";
// List status
export const listDisplayItem: any[] = [10, 20, 30, 50, 100, 500];
export const listLevel: any[] = [new ItemRecord('Cấp 1', 1), new ItemRecord('Cấp 2', 2)];
export const listGender: any[] = [new ItemRecord('Nam', true), new ItemRecord('Nữ', false)];
export const listStatus: any[] = [new ItemRecord(labelPages.hidden, 2), new ItemRecord(labelPages.show, 1)];
export const listStatusLienHe: any[] = [new ItemRecord(labelPages.read, 2), new ItemRecord(labelPages.unread, 1)];
export const listStatusAdmin: any[] = [new ItemRecord('Đã khóa', 2), new ItemRecord('Đang hoạt động', 1)];
export const listTransactionStatus: any[] = [new ItemRecord('Chưa xác định', 0), new ItemRecord('Treo', 1), new ItemRecord('Hết hàng', 2), new ItemRecord('Chờ xe', 3)
	, new ItemRecord('Kết thúc', 4), new ItemRecord('Đã ký hợp đồng', 5), new ItemRecord('Đã chuyển khoản hoa hồng', 6)];
export const listPartnerType: any[] = [new ItemRecord('Chưa xác định', 0), new ItemRecord('Cộng tác viên', 1), new ItemRecord('Thành viên', 2), new ItemRecord('Vãng lai', 3)];
export const listIsDuyet: any[] = [new ItemRecord('Chưa duyệt', 0), new ItemRecord('Đã duyệt', 1)];
export const listCancel: any[] = [new ItemRecord('Canceled', 1), new ItemRecord('No', 0)];
export const listTinhTrangDangTin: any[] = [new ItemRecord('Mới', 1), new ItemRecord('Đã qua sử dụng', 2)];
export const listPriceArea: any[] = [new ItemRecord('Dưới 500 triệu', 0), new ItemRecord('500 triệu đến 1 tỉ', 1), new ItemRecord('1 tỉ đến 2 tỉ', 2), new ItemRecord('Trên 2 tỉ', 3)];
export const listTinhTrangDangTinChiTiet: any[] = [new ItemRecord('Mới', 1), new ItemRecord('Đã qua sử dụng', 2)];
export const sortByListTinDang: any[] = [new ItemRecord('Tin đăng mới nhất', 0), new ItemRecord('Tin đăng cũ nhất', 1), new ItemRecord('Giá cao đến thấp', 2), new ItemRecord('Giá thấp đến cao', 3)];
export const listApproved: any[] = [new ItemRecord('Approved', 1), new ItemRecord('No', 0)];
export const autoIndexES: boolean = true;
export const urlPrefixDNSP: any = "urlprefix_dnsp";
export const listCacheStartWith: any[] = [new ItemRecord('Chỉ lọc External', true), new ItemRecord('Chỉ lọc Local', false)];
export const listTenNhienLieu: any[] = [new ItemRecord('Xăng', 1), new ItemRecord('Dầu', 2), new ItemRecord('Hybrid', 3), new ItemRecord('Điện', 4)];
export const listTypeThongBao: any[] = [new ItemRecord('Send All', 1), new ItemRecord('Send To User', 2), new ItemRecord('Send To Login', 3), new ItemRecord('Send To Group', 4)];
export const listLinkNotify: any[] = [new ItemRecord('Chọn Link', ''), new ItemRecord('Point', '/raovat/member/thong-tin-point'), 
									new ItemRecord('DangTin', '/raovat/member/tin-dang/-1'), 
									new ItemRecord('Install App', 'https://raoxe.dailyxe.com.vn/rao/?link=https://dailyxe.com.vn/rao-xe?appinstall=E120570E4718A93D08C9807E531BF13F&apn=vn.com.raoxe.admin&ibi=vn.com.raoxe.admin&isi=1486119532&efr=1')];
export const listTrangThai: any[] = [new ItemRecord('Hiện', 1), new ItemRecord('Ẩn', 2)];
export const listTrangThaiDangTinViolation: any[] = [new ItemRecord('Đã xử lý', 1), new ItemRecord('Chưa xử lý', 2)];
export const listIsPin: any[] = [new ItemRecord('ON', true), new ItemRecord('OFF', false)];
export const listTrangThaiUserBlackList: any[] = [new ItemRecord('Đang hoạt động', 1), new ItemRecord('Hủy', 2)];
export const listTrangThaiIsBlock: any[] = [new ItemRecord('Còn hiệu lực', 1), new ItemRecord('Hết hiệu lực', 2)];
export const listThanhVienBlackList: any[] = [new ItemRecord('Tất cả thành viên', 1), new ItemRecord('Thành viên vi phạm', 2)];

export const listTinNoiBatOpenApp: any[] = [];
export const listMarketingPlanType: any[] = [
	{ key: "Chia sẻ liên kết", value: 1 },
	{ key: "Tương tác User", value: 2 },
	{ key: "Hệ thống", value: 3 }
];
export const listMarketingPlanValueType: any[] = [
	{ key: "Tặng point", value: 1 },
	{ key: "Giảm phần trăm (%)", value: 2 },
	{ key: "Giảm tiền trực tiếp", value: 3 }
];
export const listMarketingPlanMaCauHinh: any[] = [
	{
		key: "MGT",
		value: "MaGioiThieu"
	},
	{
		key: "AI",
		value: "AppInstall"
	},
	{
		key: "SD",
		value: "SpecialDay"
	}

];

 

export const listOsName: any[] = [new ItemRecord('IOS', 'ios'), new ItemRecord('Android', 'android')];
export const groupColorDefault: string[] = [];
export const formatPirceDefault = { prefix: '', thousands: ',', decimal: '.', precision: 0, align: 'right' }
export const paginationRangeDefault: number = 6;
export const displayItemDefault: number = 20;
export const currentPageDefault: number = 1;
export const pointRegister: number = 10;
export const pointRateRegister: number = 1;
export const defaultPrice: any = {
	lower: 0,
	upper: 10000000000
}
export const keyPersonalizeLS: string = "datapersonalize";
export const versionStructorPersonalizeLS: string = "20200102";
export const countItemRemoveInArrayPersonalize: number = 20;
export const maxItemInArrayPersonalize: number = 100;
export const defaultPointMemberAdd: any = [10, 20, 50, 100, 200, 500];
export const defaultCheckInitToken: number = 86400;

export const defaultThanhToan: any = {
	momo: 1,
	chuyenKhoan: 2
}
export const typeNoiDung: any = {
	dieuKhoan: "dieukhoan",
	chinhSach: "chinhsach"
};
export const linkNoiDung: any = {
	dieuKhoan: "https://docs.google.com/document/d/e/2PACX-1vSCwdwfhncFilHGBq30Wf4RFoj4eDmFNuFs8yNOsPTl-SANYiITlxYBr7SNLKi7Iw/pub?embedded=true",
	chinhSach: "https://docs.google.com/document/d/e/2PACX-1vS9_o2O2dMj8ZKzm21RiYBbs9_Z94Zl2l6vKUw6uGMCwyg4_AixftAO-lp0rj4fMneuWbJoxcjB9j64/pub?embedded=true",
	feedBack: "https://docs.google.com/forms/d/e/1FAIpQLSdXiEoL4SAs25rhsn-TM_Qa6-aiqTi5EyKxb6wgad4PIaDSJg/viewform?embedded=true&entry.1829567736=$ten$&entry.1507208422=$sdt$",
	gopYBaoLoi: "https://docs.google.com/forms/d/e/1FAIpQLSc_jGYUcXvRieynLykLPjjf-r41L2CT5OAiqUn93OvcmwQzAg/viewform?usp=sf_link&entry.407002442=$ten$&entry.67388375=$sdt$",
	updateVersionIos: "itms-apps://itunes.apple.com/app/id1486119532",
	updateVersionAndroid: "market://details?id=vn.com.raoxe.admin"
};

export const validationType = {
	email: new Validation('Email bắt buộc phải có @ và tên miền! vd: phucuong@gmail.com', /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/),
	username: new Validation('Tên đăng nhập không vượt quá 20 ký tự và không chứa ký tự đặc biệt', /^[a-zA-Z0-9\.-]{1,20}$/),
	password: new Validation('Mật khẩu không hợp lệ 8 - 15 ký tự: vd: @12Huu*567 ', /^([a-zA-Z0-9@*#]{8,15})$/),
	phone: new Validation('Số phone không hợp lệ: vd: 01657644098', /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/),
	number: new Validation('Chỉ cho nhập số', /^[0-9]+$/),
	string: new Validation('Chỉ cho nhập chữ', /^[a-zA-Z]$/),
}
export const showConsoleLog: boolean = false;
export const maxLengthEmail: number = 150;
export const maxValueNumberDefault: number = 100000000000;
export const maxLengthTextDefault: number = 1000;
export const maxLengthTitle: number = 70;
export const maxLengthDescription: number = 160;
export const maxLengthDescriptionOfSeo: number = 3700;
// export const defaultTimeVerifyIsValid: number = 120; //seconds
export const commonMaCauHinh = {
	appVersion: "AppVersion",
	durationVerifyIsValid: "DurationVerifyIsValid",
	giftPointFromAppInstalled: "GiftPointFromAppInstalled",
	giftPointFromAppShared: "GiftPointFromAppShared",
	giftPointRegist: "GiftPointRegist",
	ratePointToTime: "RatePointToTime",
	ratePointToVND: "RatePointToVND",
	paramsMarketing: "ParamsMarketing",
	listImageBanner: "ImageBanner"
}
export const commonPropertyModule = {
	tables: 'tables',
}
export const tagDialogTinDangDetail = [
	'Rất tốt',
	'Uy tín',
	'Không tốt'
]
export const commonPropertyModule_Tables = {
	table1: 'table1',
	table2: 'table2',
	table3: 'table3',
}
export const commonComponentModule = {
	list: 'list',
	detail: 'detail',
}
export const commonNameComponent = {
}
//tronghuu95 20180907 các tên thuộc tính của entity phục vụ cho hàm this.commonMethodsTs.getData*
export const commonAttr = {
	id: "Id",
	data: "data",
	dataId: "data.Id",
	name: "Name",
	dienThoai: "DienThoai",
	isMain: "IsMain",
	urlHinhDaiDien: "UrlHinhDaiDien"
}
export const linkFirebaseToPage = {
	inApp: "InApp",
	inAppContactDailyxe: "InAppContactDailyxe",
	inAppAdminDuyetTin: "InAppAdminDuyetTin",
	inAppTinRao: "InAppTinRao",
	outApp: "outApp",
	// outAppRaoXeInstalled: "OutAppRaoXeInstalled",
}
export var projectName: string = environment.projectName;
export const commonException =
{
	cancel: "!!!!@@@@@" + projectName + 'cancel' + "@@@@@!!!!#",
	error: "!!!!@@@@@" + projectName + 'error' + "@@@@@!!!!#",
}
export const commonParams = {
	dalyxeAccess: projectName + '_Access',
	dalyxeModule: projectName + '_Module',
	dalyxeCommon: projectName + '_Common',
	isViewType: 'isViewType',
	isMain: 'ismain',
	idNhomQuyen: 'idnhomquyen',
	orderString: "orderString",
	isKey: "isKey",
	searchString: "searchString",
	displayItems: "displayItems",
	displayName: "displayName",
	displayPage: "displayPage",
	description: "description",
	trangThai: "trangThai",
	allowElasticSearch: "allowElasticSearch",
	dienThoai: "dienThoai",
	email: "email",
	idLinhVucKinhDoanh: "idLinhVucKinhDoanh",
	idNguonKhachHang: "idNguonKhachHang",
	idMucDoQuanHe: "idMucDoQuanHe",
	idNguoiPhuTrach: "idNguoiPhuTrach",
	idTinhThanh: "idTinhThanh",
	idQuanHuyen: 'idQuanHuyen',
	idCongTy: "idCongTy",
	totalItems: "totalItems",
	idListKhachHangNhomKhachHang: "idListKhachHangNhomKhachHang",
	idCongViec: "idCongViec",
	loaiCongViec: "loaiCongViec",
	idUserLogin: "idUserLogin",
	ids: "ids",
	listiduser: "listiduser",
	idKhachHang: "idKhachHang",
	idUserTao: "idUserTao",
	idUserCapNhat: "idUserCapNhat",
	isDeleted: "isDeleted",
	dynamicParam: "dynamicParam",
	startDate: "startDate",
	endDate: "endDate",
	isDaXem: "isDaXem",
	idUser: "idUser",
	idUsers: "idUsers",
	changePass: "changePass",
	changePhone: "changePhone",
	changeEmail: "changeEmail",
	removeAccount: "removeAccount",
	idTinDangs: "idTinDangs",
	idFolderParent: "IdFolderParent",
	idFile: "idFile",
	childrenStructure: "childrenStructure",
	folderId: 'folderId',
	parentId: 'parentId',
	idFolders: 'idFolders',
	idFiles: 'idFiles',
	destinationIdFolder: 'destinationIdFolder',
	notId: 'notId',
	id: 'id',
	name: 'name',
	wxh: 'wxh',
	extension: 'extension',
	token: 'token',
	//Menu    
	capDo: 'capDo',
	noiDung: 'noiDung',
	idLoaiModule: 'idLoaiModule',
	tuKhoaTimKiem: 'tuKhoaTimKiem',
	maCauHinh: 'maCauHinh',
	giaTriCauHinh: 'giaTriCauHinh',
	tenDangNhap: 'tenDangNhap',
	matKhau: 'matKhau',
	ngayUpFrom: 'ngayUpFrom',
	ngayUpTo: 'ngayUpTo',
	ngayTaoFrom: 'ngayTaoFrom',
	ngayTaoTo: 'ngayTaoTo',
	sourcesNewsId: 'sourcesNewsId',
	idMenu: 'idMenu',
	rewriteUrlPrefix: 'rewriteUrlPrefix',
	exactlySearchLinkCu: 'exactlySearchLinkCu',
	exactlySearchLinkMoi: 'exactlySearchLinkMoi',
	linkCu: 'LinkCu',
	linkMoi: 'LinkMoi',
	tenMenuHeThong: 'TenMenuHeThong',
	pageDisplayName: 'PageDisplayName',
	rawUrlRule: 'RawUrlRule',
	tieuDe: 'tieuDe',
	pageId: 'PageId',
	previousLinkSave: "previousLinkSave",
	groupStyle: 'GroupStyle',
	getNewsId: 'getNewsId',
	addDefaultFolderParent: 'addDefaultFolderParent',
	allIncluding: 'allincluding',
	pageLayoutNameExtend: 'pageLayoutNameExtend',
	username: 'sn',
	password: 'aw',
	access_token: 'ct',
	expires_in: 'xi',
	cat: 'at',
	cuk: 'uk',
	isCreate: 'isCreate',
	cms_ranking_detail: "cms_ranking_detail",
	cms_dangtin_rating_detail: "cms_dangtin_rating_detail",
	notIdComponentData: 'notIdComponentData',
	idComponentData: 'idComponentData',
	idTinTuc: 'idTinTuc',
	htmlControlId: 'htmlControlId',
	pageLayoutId: 'pageLayoutId',
	linkGoc: 'linkGoc',
	PercentDiscount: "PercentDiscount",
	PointRule: "PointRule",
	TieuDe: "TieuDe",
	regStructure: 'regStructure',
	idAdminDangKy: 'idAdminDangKy',
	postDateFrom: 'postDateFrom',
	postDateTo: 'postDateTo',
	searchIdAdminDangKy: 'searchIdAdminDangKy',
	isApproved: 'isApproved',
	isCancel: 'isCancel',
	rewriteUrl: 'rewriteUrl',
	defaultImageSrc: 'defaultImageSrc',
	notIdPageLayout: 'notIdPageLayout',
	listDataComponent: 'listDataComponent',
	idLoaiComponentData: 'idLoaiComponentData',
	idLoaiTinTuc: 'idLoaiTinTuc',
	idLoaiMenu: 'idLoaiMenu',
	tenNhomQuyen: 'tenNhomQuyen',
	idQuocGia: 'idQuocGia',
	idHangXe: 'idHangXe',
	componentDataNameExtend: 'componentDataNameExtend',
	idThuongHieu: 'idThuongHieu',
	notIdThuongHieu: 'notIdThuongHieu',
	idThanhVien: 'idThanhVien',
	notIdThanhVien: 'notIdThanhVien',
	idDaiLy: 'idDaiLy',
	notIdDaiLy: 'notIdDaiLy',
	thuTu: 'thuTu',
	danhSachCauHinh: 'danhSachCauHinh',
	componentId: 'componentId',
	apiIsLoadManually: 'apiIsLoadManually',
	apiIdLoaiComponentData: 'apiIdLoaiComponentData',
	getPropertyNameList: 'getPropertyNameList',
	componentDataId: 'componentDataId',
	buildhtml: 'buildhtml',
	keyCache: 'keyCache',
	idSeo: 'idSeo',
	isReg: 'isReg',
	advancedSeoOnly: 'advancedSeoOnly',
	maComponent: 'maComponent',
	idDinhNghiaSanPham: 'idDinhNghiaSanPham',
	idMauSac: 'idMauSac',
	idNhomThuocTinh: 'idNhomThuocTinh',
	tenKhuyenMai: 'tenKhuyenMai',
	giaTu: 'giaTu',
	giaDen: 'giaDen',
	xemGiaNam: 'xemGiaNam',
	xemGiaThang: 'xemGiaThang',
	xemGiaNgay: 'xemGiaNgay',
	maGroup: 'maGroup',
	idDongXe: 'idDongXe',
	idDongXes: 'idDongXes',
	namSanXuat: 'namSanXuat',
	idLoaiXe: 'idLoaiXe',
	idLoaiXes: 'idLoaiXes',
	nhomMau: 'nhomMau',
	systemName: 'systemName',
	isCreateManually: 'isCreateManually',
	pathLoad: 'pathLoad',
	isExternal: 'isExternal',
	readTB1: 'readTB1',
	idBaiViet: 'idBaiViet',
	idQuangCao: 'idQuangCao',
	isDangKyLaiThu: 'isDangKyLaiThu',
	isDangKyBaoGia: 'isDangKyBaoGia',
	isDangKyNhanBanTin: 'isDangKyNhanBanTin',
	idmenu: 'idmenu',
	idApi: 'idApi',
	data: "at",
	protocol: "it",
	nameIndex: "ni",
	executeUrl: "xu",
	token_data64: "at64",
	token_username: "sn",
	token_password: "aw",
	token_requireRole: "er",
	token_accessToken: "ct",
	token_expiresIn: "xi",
	token_roleAdmin: "dm",
	roleAdmin: `${nameSource}.dm`,
	token_roleThanhVien: "hv",
	roleThanhVien: `${nameSource}.hv`,
	token_bodyEmail: "li",
	token_ngayDangNhapBangMxh: "ds",
	token_admin64: 'dm64',
	token_refreshToken: 'eft',
	token_tenDangNhap_moi: "ednm",
	httpType_Get: "httpget",
	httpType_Post: "httppost",
	httpType_Put: "httpput",
	httpType_Delete: "httpdelete",
	etn: "entityName",
	ngayHienTaiTu: 'ngayHienTaiTu',
	ngayHienTaiDen: 'ngayHienTaiDen',
	ngayTaoTu: 'ngayTaoTu',
	ngayTaoDen: 'ngayTaoDen',
	idNguonGoc: 'idNguonGoc',
	clearCacheActive: 'clearCacheActive',
	idSearchRowIndex: 'idSearchRowIndex',
	uploadFile_FolderId: "f",
	uploadFile_FileUpdateId: "id",
	uploadFile_FileName: "name",
	uploadFile_FileWidth: "w",
	uploadFile_FileHeight: "h",
	uploadFile_FileWidthxHeight: "wxh",
	uploadFile_FileExtension: "ext",
	uploadFile_ParentID: "parentID",
	uploadFile_DestinationIdFolder: "destinationIdFolder",
	uploadFile_IdFolders: "idFolders",
	uploadFile_IdFiles: "idFiles",
	uploadFile_IsFromWebUri: "isFromWebUri",
	fromId: "fromId",
	toId: "toId",
	loaiSiteMap: "loaiSiteMap",
	ma: "Ma",
	ifl: "ifl",
	partnerType: "partnerType",
	transactionStatus: "transactionStatus",
	isDuyet: "isDuyet",
	user: "user",
	idNguoiBan: "IdNguoiBan",
	checkPhoneExist: 'checkPhoneExist',
	loaiXacNhan: 'loaiXacNhan',
	isPinTop: "isPinTop",
	idTinhThanhDT: "IdTinhThanh",
	gia: "gia",
	tinhTrang: "tinhTrang",
	type: 'type',
	checkExpire: "checkExpire",
	idsThanhVien: "idsThanhVien",
	allIncludingString: "allIncludingString",
	isCache: "isCache",
	timeEffective: "timeEffective",
	pointTmp: "pointTmp",
	sdt: 'sdt',
	tableName: "tableName",
	syncDate: "syncDate",
	note: "note",
	rate: "rate",
	point: "point",
	idRank: "idRank",
	percentDiscount: "percentDiscount",
	price: "price",
	daXacNhanEmail: "daXacNhanEmail",
	daXacNhanDienThoai: "daXacNhanDienThoai",
	daXacThucCmnd: "daXacThucCmnd",
	daXacThucPassport: "daXacThucPassport",
	listHinhAnhJsonPassport: "listHinhAnhJsonPassport",
	idFileCmndTruoc: "idFileCmndTruoc",
	idFileCmndSau: "idFileCmndSau",
	isView: "isView",
	// param cua nhut (column name)
	IdThanhVien: "IdThanhVien",
	Point: "Point",
	PointUsed: 'PointUsed',
	PointTmp: 'PointTmp',
	ThanhVien_Ext: 'ThanhVien_Ext',
	HoTen: 'HoTen',
	Rate: 'Rate',
	TimeDangTin_Ext: "TimeDangTin_Ext",
	RatePointToTime_Ext: "RatePointToTime_Ext",
	IdThuongHieu: "IdThuongHieu",
	IdDongXe: "IdDongXe",
	IdTinhThanh: "IdTinhThanh",
	Description: "Description",
	// param NotificationUser
	ngayTao: "NgayTao",
	idNotificationSystem: "IdNotificationSystem",
	TrangThai: "TrangThai",
	IsView: "IsView",
	isFromAdmin: "isFromAdmin",
	year: "year",
	month: "month",
	day: "day",
	cacheType: "cacheType",
	cacheKey: "cacheKey",
	osName: "osName",
	isFromRaoVat: "sfrv",
	deviceId: "deviceId",
	idGroup: "idGroup",
	isPlusChuaDuyet: "isPlusChuaDuyet",
	dependOnPublishDate: "dependOnPublishDate",
	originalFrom: "originalFrom",
	idAdmin: "idAdmin",
	idAdminDangKyDuyet: "idAdminDangKyDuyet",
	tv: "tv",
	tinRao: "r",
	environment: "env",
	ngayCapNhat: "ngayCapNhat",
	checkEmailExist: "checkEmailExist",
	code: "code",
	IdMarketingPlan: "IdMarketingPlan",
	IdAdminDangKyDuyet: "IdAdminDangKyDuyet",
	isUsed: "isUsed",
	idMarketingPlan: "idMarketingPlan",
	Uuid: "uuid",
	amount: "amount",
	returnUrl: "returnUrl",
	extraData: "extraData",
	orderInfo: "orderInfo",
	apiVersion: "api-version",
	thanhVienIsRead: "thanhVienIsRead",
	access:"access",
	common:"common"

}
export const maxLenght = { // chủ yếu là để xác định giá trị tối đa khi nhập vào các control trên giao diện
	gia: 14, // tính luôn dấu phẩy
	dienThoai: 10
}

export const valueDefault = { // chủ yếu là để xác định giá trị tối đa khi nhập vào các control trên giao diện
	priceMax: 100000000000 // tính luôn dấu phẩy
}
export const controlType = {
	GroupDefault: 'GroupDefault',
	ListHours: 'ListHours',
	ListMinute: 'ListMinute',
	ListYear: 'ListYear',
	ListDay: 'ListDay',
	ListMonth: 'ListMonth',
	Menu: 'Menu',
	Level: 'Cấp Độ',
	Gender: 'Giới Tính',
	Status: 'Trạng Thái',
	TransactionStatus: 'Trạng Thái Hợp Đồng',
	StatusAdmin: 'StatusAdmin',
	StatusLienHe: 'StatusLienHe',
	PerGroup: 'NhomQuyen',
	//Dùng cho autocomlete
	UrlPrefix: 'UrlPrefix',
	SourcesNews: 'SourcesNews',
	GetNews: 'GetNews',
	SourcesNewsAuto: 'SourcesNewsAuto',
	GetNewsAuto: 'GetNewsAuto',
	MenuHeThong: 'MenuHeThong',
	PageDisplayName: 'PageDisplayName',
	PageNameExtend: 'PageNameExtend',
	PageLayoutNameExtend: 'PageLayoutNameExtend',
	ComponentNameExtend: 'ComponentNameExtend',
	ComponentDataNameExtend: 'ComponentDataNameExtend',
	ComponentDataNameExtendStartsWith: 'ComponentDataNameExtendStartsWith',
	GroupStyle: 'GroupStyle',
	HtmlControlId: 'HtmlControlId',
	PageLayoutId: 'PageLayoutId',
	ComponentDataType: 'ComponentDataType',
	ComponentDataTuongDuong: 'ComponentDataTuongDuong',
	QuocGia: 'QuocGia',
	HangXe: 'HangXe',
	TinhThanh: 'TinhThanh',
	QuanHuyen: 'QuanHuyen',
	ThuongHieu: 'ThuongHieu',
	LoaiXe: 'LoaiXe',
	DongXe: 'DongXe',
	HopSo: 'HopSo',
	NhienLieu: 'NhienLieu',
	ApiParams: 'ApiParams',
	Api: 'Api',
	Params: 'Params',
	Properties: 'Properties',
	MappingProperties: 'MappingProperties',
	ApiParamsJson: 'ApiParamsJson',
	ListData: 'ListData',
	CommonParams: 'CommonParams',
	NhomThuocTinh: 'NhomThuocTinh',
	ThuocTinh: 'NhomThuocTinh',
	DinhNghiaSanPham: 'DinhNghiaSanPham',
	DaiLy: 'DaiLy',
	Routing: 'Routing',
	CacheStartWith: 'CacheStartWith',
	CacheGroup: 'CacheGroup',
	MemoryCacheHttp: 'MemoryCacheHttp',
	TypeLienHe: 'TypeLienHe',
	NguonGoc: 'NguonGoc',
	CacheRule: 'CacheRule',
	LoaiNhomThuocTinh: 'LoaiNhomThuocTinh',
	MenuRaoVat: 'MenuRaoVat',
	CongTacVien: 'CongTacVien',
	Admin: 'Admin',
	PartnerType: 'PartnerType',
	IsDuyet: 'IsDuyet',
	ShowRoom: 'ShowRoom',
}
// E: Error => Red
// W: Warning => Yellow
// I: Infor => Blue
export const commonMessages = {
	M001: "Không tìm thấy dữ liệu",
	M002: "Vui lòng chọn dữ liệu",
	M003: "Thêm thành công",
	M004: "Thêm không thành công",
	M005: "Cập nhật thành công",
	M006: "Cập nhật không thành công",
	M007: "Xóa thành công",
	M008: "Xóa không thành công",
	M009: "Đăng xuất thành công",
	M010: "Đăng xuất không thành công",
	M011: "Vui lòng nhập dữ liệu.",
	M012: "Cập nhật mật khẩu thành công. Vui lòng đăng nhập lại!",
	M013: "Pin Top thành công!",
	M014: "Đã hủy Pin Top",
	M015: "Xác thực thành công.",
	M016: "Xác thực không thành công",
	M017: "Token không tồn tại hoặc đã hết thời gian vui lòng đăng nhập lại",
	M018: "Dữ liệu không hợp lệ",
	M019: "Tài khoản hoặc mật khẩu không hợp lệ",
	M020: "Không có kết nối mạng",
	M021: "Vui lòng báo cáo với admin để khắc phục lỗi",
	M022: "Mã xác thực không đúng hoặc đã hết hạn",
	M023: "Vui lòng nhập lý do không duyệt",
	M024: "Đã gửi mã xác thực",
	M025: `Đã gửi mã xác thực! Vui lòng thử lại sau ${Variables.timeVerifyValid} phút`,
	M026: "Chưa nhập mã xác thực",
	M027: "Từ ngày không thể lớn hơn đến ngày",
	M028: "Đến ngày không thể nhỏ hơn từ ngày",
	M029: "Tài khoản đang được xác thực bởi người khác",
	M030: `Vui lòng thử lại sau ${Variables.timeVerifyValid} phút`,
	M031: "Tin đã được lưu trước đó",
	M032: "Tin đăng đang đươc duyệt lại hoặc đã bị xoá",
	M033: "Tài khoản không tồn tại hoặc đã bị khóa",
	M034: "Để thực hiện chức năng này, cần có kết nối mạng!",
	M035: "Cắt hình thành công!",
	M036: "Upload hình trước khi cắt!",
	M037: "Bạn chưa bật chức năng cắt hình!",
	M038: "Bạn chưa cắt xong hình!",
	M039: "Đánh giá thành công, đang chờ duyệt",
	M040: "Bạn phải thực hiện hoàn tất các bước trước",
	M041: "Vui lòng nhập đầy đủ dữ liệu!",
	M042: "Báo cáo vi phạm thành công!",
	M043: "Dữ liệu không tồn tại hoặc đã bị xoá",
	M044: "Ngày pin top không hợp lệ!",
	M045: "Bạn phải Pin Top ít nhất 5 phút trước khi muốn tắt!",
	M046: "Thời gian xác thực đã hết",
	M047: "Up tin thành công!",
	M048: "Thời gian hiệu lực đã hết",
	M049: "Thành viên đã vi phạm nội quy: {0}! Lock acc từ {1} - {2}",
	M050: "Mã xác thực không hợp lệ",
	M051: "Tin đang pintop",
	M052: "Bạn chưa chọn thành viên",
	M053: "Từ ngày không thể lớn hơn hoặc bằng đến ngày",
	M054: "Xóa cache thành công",
	M055: "Xóa cache thất bại",
	M056: "Bật đăng nhập vân tay thành công!",
	M057: "Bật đăng nhập vân tay thất bại!",
	M058: "Tắt đăng nhập vân tay thành công!",
	M059: "Tắt đăng nhập vân tay thất bại!",
	M060: "Thiết bị không hỗ trợ!",
	M061: "Có vẻ như bạn vừa cập nhật vân tay trên máy. Vui lòng truy cập vào 'Cài đặt > Ứng dụng > DaiLyXe > Xóa dữ liệu' trên máy của bạn để làm mới lại dữ liệu!",
	M062: "Thao tác thất bại! Đây là dữ liệu mặc đinh",
	M063: "Đã chuyển sang màn hình đăng nhập Admin",
	M064: "Mật khẩu phải có ít nhất 8 ký tự, 1 số, 1 chữ hoa và 1 chữ thường",
	M065: "Tin rao chưa được duyệt!",
	M067: "Đăng ký duyệt thành công",
	M068: "Bạn không có quyền chỉnh sửa dữ liệu này",
	M069: "Thời gian không đủ! Vui lòng mua thêm",
	M070: "Tin đã up. Vui lòng thử lại sau",
	M071: "Từ ngày không hợp lệ",
	M072: "Ngày đến không hợp lệ",
	M073: "Vui lòng kiểm tra đường mạng và thử lại",
	M074: "Tài khoản đã được thay đổi vui lòng xác nhận lại tài khoản",
	M075: "Sao chép liên kết",
	M076: "Vui lòng chờ, cho đến khi tiến trình xử lý được hoàn thành",
	M077: "Server đang bận. Vui lòng thử lại sau",
	M078: "Server ảnh đang bận. Vui lòng thử lại sau",
	M079: "Thao tác lưu tin thất bại",
	M080: "Quá trình xử lý tin có vấn đề! Vui lòng thực hiện lại",
	M081: "Hủy đăng ký duyệt thành công",
	M082: "Thao tác thất bại bạn có muốn thử lại",
	M083: "Không tìm thấy tài khoản",
	M084: "Đã bật TouchId",
	M085: "Đã tắt TouchId",
	M086: "Dữ liệu không có sự thay đổi",
	M087: "Bạn không thể đánh giá tin rao của chính mình",
	M088: "Bạn không thể báo vi phạm tin rao của chính mình",
	M089: "Vân tay đã mất hiệu lực",
	M090: "Bài đăng này đã được duyệt, sau khi thay đổi. Vui lòng chờ duyệt lại.",
	M091: "Sao chép thành công",
	M092: "Sao chép thất bại",
	M093: "Dán thành công",
	M094: "Dán thất bại",
	M095: "Thiết bị này không thể gửi mã xác thực tới số điện thoại trên vì hành vi bất thường.",
	M096: "Đã gửi lại mã xác thực",
	M097: "Gửi lại mã xác thực thất bại",
	M098: "Email đã tồn tại",
	M099: "Thiết bị này không thể gửi mã xác thực vì hành vi bất thường.",
	M100: "Sao chép mã ưu đãi thành công.",
	M101: "Áp dụng mã ưu đãi thành công.",
	M102: "Giao dịch thành công.",
	M103: "Vui lòng đăng nhập để thực hiện cuộc gọi.",
	M104: "Vui lòng đăng nhập để Báo cáo vi phạm.",
 

	P001: "{0} đã tồn tại.",
	P002: "{0} không hợp lệ",
	P003: "Bạn có muốn xóa {0} không?",
	P004: "{0} vượt quá giới hạn",
	P005: "{0} không được bỏ trống",
	P006: "Vui lòng nhập {0}",
	P007: "Vui lòng chọn {0}",
	P008: "Vui lòng lưu {0}",
	P009: "Vui lòng chờ {0} phút để thử lại.",
	P010: "Từ ngày phải lớn hơn khoảng thời gian bị khóa cuối cùng (hoặc ngày hiện tại): {0}",
	P011: "{0} không tồn tại.",
	//N: Notify
	N001: "Thăng hạng thành công",
	N002: "Chúc mừng bạn đã thăng hạng: {0}",
	// Used: CMS-DangTin
	N003: "Tin mới - Chờ xét duyệt",
	N004: "Bạn đã đăng nhập trên thiết bị mới, vui lòng kiểm tra email (nếu đã cập nhật trên hệ thống) để biết thêm thông tin chi tiết.",
	N005: "Tài khoản của bạn đã được đăng nhập ở web",
	N006: "Tài khoản quý khách đã bị " + Variables.textLockAccount + ": {0} - {1}. Lý do: {2}", // {0}: TuNgay || {1}: DenNgay || {2}: LyDo
	N008: "Mua Point thành công", // Title
	N009: "Chúc mừng bạn đã mua [{0}] Point thành công", // {0}: Point
	N010: 'Duyệt thành công - [{0}]', // {0}: Title
	N011: 'Duyệt thất bại - [{0}]', // {0}: Title
	N012: 'Chúc mừng',
	N013: 'Duyệt lại',
	N014: "Cập nhật môi trướng mới",
	N015: "Mua Point thành công",
	N016: "[{0}] mua [{1}] point",
	T001: "Cảnh báo", // Title
	T002: "Cảnh báo: {0}", // {0}: Phone
	E000: "Mất kết nối mạng. Vui lòng thử lại sau",
	E001: "Máy chủ đang bận. Vui lòng thử lại sau",
	E401: "Bạn không có quyền với thao tác này",
	E404: "Thao tác này không tồn tại",

}
export const nameShortEnviroment = {
	development: "ev",
	productionstage: "rs",
	productionlive: "rl",
	current: "ur" // là biến đang sử dụng được gán từ các môi trường khác ev|rs|rl
}
export const loaiThongBao = {
	success: "success",
	error: "error",
	warn: "warn",
	info: "info",
}
export const operators = {
	and: "and",
	or: "or",
	equals: "==",
	greaterThan: ">",
	lessThan: "<",
	notEquals: "!=",
	greaterThanOrEqualTo: ">=",
	lessThanOrEqualTo: "<=",
	like: "like",
	in: "in",
}
export class DynamicParams {
	public static build(object1: any, operators: any, object2: any) {
		return { obj1: object1, operator: operators, obj2: object2 };
	}
}
export const spinnerType = {
	Data_ChasingDots: "sk-chasing-dots",
	Data_CubeGrid: "sk-cube-grid",
	Data_DoubleBounce: "sk-double-bounce",
	Data_FoldingCube: "sk-folding-cube",
	Data_RotatingPlane: "sk-rotating-plane",
	Data_ThreeBounce: "sk-three-bounce",
	Data_Wave: "sk-wave",
}
export const keyControl = {
	manager: 'CtrlManagerComponent',
	root: "RootComponent",
}
export enum defaultPinStatus {
	new = 0,// new
	stop = 1,// stop
	waiting = 2,// waiting
	stopWaiting = 3,// Stop waiting
	updateWaiting = 4,// Update waiting
}
export enum defaultWxh {
	one = '1',
	two = '2',
	three = '3',
	four = '4',
	five = '5',
	six = '6',
	seven = '7',
	eight = '8',
	nine = '9',
	regular = '',
}
//Nguyễn Trọng Hữu 5/12/2017 kết wuar kiểm tra token
export const resultCheckToken = {
	error: 'error', // router tới login
	warn: 'warn', // bật lại hộp thoại check mật khẩu
	success: 'success'// thực hiện thành công
}
export const infoUser = {
	isMain: "IsMain",
	id: "Id",
	hoTen: "HoTen",
	idNhomQuyen: "IdNhomQuyen",
	tenDangNhap: "TenDangNhap"
}
export const commonNavigate = {
	cms_dashboard: "/admin/cms-dashboard",
	cms_login: "/admin/cms-login",
	cms_menu: "/admin/cms-menu",
	cms_giaoDichXeCreate: "/admin/cms-giaodichxe/create",
	cms_giaoDichXe: "/admin/cms-giaodichxe",
	cms_dangTin: "/admin/cms-dangtin",
	cms_thanhVien: "/admin/cms-thanhvien",
	cms_userpoint: "/admin/cms-userpoint",
	cms_groupThanhVien: "/admin/cms-group-thanhvien",
	cms_thongTinCaNhan: "/admin/cms-thong-tin-ca-nhan",
	cms_ranking: "/admin/cms-ranking",
	cms_dangTinRating: "/admin/cms-dang-tin-rating",
	cms_settings: "/admin/cms-settings",
	cms_command_linux: "/admin/cms-command-linux",
	cms_thongBao: "/admin/cms-thong-bao",
	cms_thongBaoTemplate: "/admin/cms-thong-bao-template",
	cms_dangTinViolation: "/admin/cms-dang-tin-violation",
	cms_userBlackList: "/admin/cms-user-blacklist",
	cms_thongKe: "/admin/cms-thong-ke",
	cms_installStatistics: "/admin/cms-install-statistics",
	cms_layoutAlbum: "/admin/cms-layout-album",
	cms_uuDai: "/admin/cms-uu-dai",
	cms_attackMode: "/admin/cms-attackmode",
	cms_userDevice: "/admin/cms-userdevice",
	dangTin: "raovat/dangtin",
	tinTuc: "raovat/tin-tuc",
	homePage: "raovat/home",
	notifications: "raovat/notifications",
	timKiem: "raovat/tim-kiem",
	member: "raovat/member",
	test: "raovat/test",
	homeListTinDang: "/raovat/home/tindang",
	f_homeTinDang: "/raovat/home/tindang/",
	f_dashboard: "raovat/member/dashboard",
	f_login: "raovat/member/login",
	f_register: "raovat/member/register",
	f_verifyRegister: "raovat/member/register/verify-register",
	f_registerDone: "raovat/member/register/register-done",
	f_transactionCarManage: "raovat/member/giao-dich",
	f_transactionCarManageCreate: "raovat/member/giao-dich/create",
	f_infoUser: "raovat/member/thong-tin-ca-nhan",
	f_tinDang: "raovat/member/tin-dang",
	f_dangTinCreate: "raovat/member/tin-dang/-1",
	f_forgotPassword: "raovat/member/forgot-password",
	f_verifyForgotPassword: "raovat/member/forgot-password/verify-forgot-password",
	f_forgotPasswordDone: "raovat/member/forgot-password/forgot-password-done",
	f_thongTinPoint: "raovat/member/thong-tin-point",
	f_tinDaLuu: "raovat/member/tin-da-luu",
	f_tinDangRating: "raovat/member/tin-ban-danh-gia",
	f_pinTopHistory: "raovat/member/tin-dang/pintop-history",
	f_uuDaiCuaBan: "raovat/member/uu-dai-cua-ban",
	f_chiaSeApp: "raovat/member/chia-se-app",
	f_contactDailyxe: "raovat/member/contact-dailyxe",
	f_advertisementDailyxe: "raovat/member/advertisement-dailyxe",
}
//tronghuu95 20190502 Hữu sửa lại cho đúng ý nghĩa
export const backgroundColorForMessage =
{
	error: 'danger',
	success: 'success',
	warning: 'warning',
	info: 'info'
}
export const dateFormat = {
	FullDateTime: "yyyy-mm-dd HH:MM:ss",
	DateTime: "yyyy-mm-dd HH:MM",
	TFullDateTime: "yyyy-mm-dd'T'HH:MM:ss",
	SlashShortDateTime: "yyyy/mm/dd",
}
export enum CommissionStatus {
	Paid = 1,
	NoPaid = 2
}
export enum TransactionMethod {
	Cash = 1,
	Installment = 2
}
export enum PartnerType {
	CTV = 1,
	Member = 2,
	Anonymous = 3
}
export enum TypeSmsValidation {
	Regist = 1, // Regist Member
	ForgetPassword = 2, // Forget password Member
	updateInfoPerson = 3, // Update informative Member
	sendEmail = 4 // informative Member
}
export enum TypeDangTin {
	Mua = '2',
	Ban = '1'
}
export enum TypeDangTinFavorite {
	addFavorite = 1,
	addSeen = 2,
}

export enum TypeCacheType {
	allCache = 1,
	chooseCache = 2,
}
export enum TypeUserPoint {
	GiftRegist = 1, // Tặng khi đăng ký thành viên
	Bought = 2, // Mua point
	UsedPinTop = 4 // Sử dụng đăng tin pin top
}
export enum TypeLayoutAlbum {
	vertical = "v", // kiểu dọc
	horizontal = "h", // kiểu ngang
	square = "s", // kiểu vuoong
}
export enum TypeHistoryDangTinView {
	ViewDangTin = 1,
	ShowContact = 2,
	CallContact = 3,
	ViewDescription = 4,
	HideDescription = 5
}
// this is for caching data on client
export const cacheData = {
	TinMua: 'TinMua',
	TinBan: 'TinBan',
	ThuongHieu: 'ThuongHieu',
	MauSac: 'MauSac',
	TinhThanh: 'TinhThanh',
	Menu: 'Menu',
	LoaiXe: 'LoaiXe',
	QuanHuyen: 'QuanHuyen',
	DongXe: 'DongXe',
	DangTinDetail: 'DangTinDetail',
	DangTinPhoto: 'DangTinPhoto',
	DangTinListPhoto: 'DangTinListPhoto',
	UserPoint: 'UserPoint',
	UserPointProfile: 'UserPointProfile',
	UserPointHistory: 'UserPointHistory',
	DangTinMoiFirebase: 'dangTinMoi/',
	DangTinNoiBatFirebase: 'dangTinNoiBat/',
	MenuRaoVatFirebase: 'menuRaoVat/',
	TinhThanhFirebase: 'tinhThanh/',
	CauHinhHeThongFirebase: 'cauHinhHeThong/',
};
// this is for localStorage data on client
export const localStorageData = {
	tinDangYeuThich: 'tinDangYeuThich',
	tinDangLike: 'tinDangLike',
	phuongThucThanhToan: 'phuongThucThanhToan',
	muaPointDataActiveCode: 'mpac',
	maGioiThieu: 'mgt',
	lUserOnFinger: 'lUserOnFinger',
	detailRao: "detailRao",
	smsCode: "smsCode",
	timeBlock: "timeBlock",
	lNewEmailDaXacThuc: "lNewEmailDaXacThuc",
	historyResend: "historyResend",
	verifyNumberPhone: "@@@@verifyNumberPhone@@@@@",
	verifyEmail: "@@@verifyEmail@@@@@"
};
export const listShowTab =
	["/", "/raovat/member", '/raovat/home', '/raovat/tin-tuc', '/raovat/notifications', '/raovat/member', '/raovat/home/tindang', '/raovat/member/dashboard']
//tronghuu95 cái này dùng để giả lập ảnh cho việc test lưu ảnh trên browers
export const configOTP = {
	allowNumbersOnly: true,
	length: 6,
	isPasswordInput: false,
	disableAutoFocus: false,
	placeholder: 0,
	inputStyles: {
		// 'width': '50px',
		// 'height': '50px'
	}
};