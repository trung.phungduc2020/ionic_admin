import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Variables, commonMaCauHinh, commonMessages, linkFirebaseToPage, commonAttr, defaultCheckInitToken } from "src/app/core/variables";
import { FcmService } from './shared/services/fcm.service';
import { ToastService } from './shared/services/toast.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { enableProdMode } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { CacheService } from "ionic-cache";
import { routeAnimations } from './shared/animations/animations';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { CauHinhHeThongService } from './forms/member/services/cauHinhHeThong.service';
import { CoreVariables } from './core/core-variables';
import { labelPages } from './core/labelPages';
import { AdminRaoVatBLL_InterfaceData } from './core/api/admin/rao-vat/adminRaoVatBLL';
import { CoreProcess } from './core/core-process';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AuthAdminService } from './core/http-interceptor/auth/auth-admin.service';

enableProdMode();
@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	providers: [StatusBar, SplashScreen],
	animations: [routeAnimations]
})
export class AppComponent {
	public isReady: boolean = false; // cái này là để xác định các biến khởi tạo init
	public platformReady: boolean = false; // cái này là để xác định các biến khởi tạo init
	public showSplash: boolean = true;// cái này là để xác định tắt spiner loading
	public lbl = labelPages;
	public classRoute: any = "";
	public variables = Variables;
	public isHomePage = false;
	public timSetInterval: any;
	public isInit: boolean = false;
	constructor(
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private fcm: FcmService,
		private toastr: ToastService,
		private screenOrientation: ScreenOrientation,
		public commonMethodsTs: CommonMethodsTs,
		public cache: CacheService,
		private appVersion: AppVersion,
		public firebase: Firebase,
		private cauHinhHeThongService: CauHinhHeThongService,
		public adminRaoVatBLL_InterfaceData: AdminRaoVatBLL_InterfaceData,
		public coreProcess: CoreProcess,
		private backgroundMode: BackgroundMode,
		public uniqueDeviceID: UniqueDeviceID,
		private keyboard: Keyboard,
		public authAdminService: AuthAdminService
	) {

		this.commonMethodsTs.initTimeEmailVerify();
		this.commonMethodsTs.initTimeNumberPhoneVerify();
		Variables.tokenAdmin = this.authAdminService.getToken();
		let isTokenExpired = this.commonMethodsTs.isTokenExpired(Variables.tokenAdmin, defaultCheckInitToken);
		Variables.infoAdmin = {};
		if (Variables.tokenAdmin && isTokenExpired) {
			this.authAdminService.logout();
			return;
		} else {
			Variables.infoAdmin = this.authAdminService.getInfo();

		}
		this.cache.setDefaultTTL(60 * 60); //set default cache TTL for 1 hour
		this.isHomePage = ["/", "/raovat/home"].indexOf(this.commonMethodsTs.router.url) >= 0;
		this.commonMethodsTs.router.events.pipe(
			filter((event: any) => event instanceof NavigationEnd)
		)
			.subscribe(event => {
				this.coreProcess.getTimeServer();
				let url = event.url;
				this.isHomePage = ["/", "/raovat/home"].indexOf(url) >= 0;
				Variables.isViewAdmin = url.startsWith('/admin');
				Variables.isLoginAdmin = Variables.tokenAdmin != null;
				this.keyboard.hide();
			});
		this.initializeApp();
	}
	ionViewDidEnter() {
	}

	private notificationSetup() {
		/**
		 * Notifycation onapp
		 */

		this.fcm.requestPermission();
		this.fcm.onMessage().subscribe(
			(msg) => {
				this.processNotifycation(msg)
			});
		/**
		 * Notifycation onBackground
		 */
		this.fcm.onBackgroundMessage().subscribe(
			(msg) => {
				this.processLinkToPage(msg);
				this.processNotifycation(msg)
			});
	}

	processLinkToPage(msg: any) {

		var category = this.commonMethodsTs.getData(msg.aps, "category");
		if (category && category != '') {
			var key = category.split("_");

			if (key[0].toLowerCase() == linkFirebaseToPage.inAppAdminDuyetTin.toLowerCase()) {
				this.commonMethodsTs.toPage_AdminDangTin(key[1]);
				return;
			}

			return;
		}
	}

	processNotifycation(msg: any) {
		let message = msg.message || this.commonMethodsTs.getData(msg.gcm, "title")
			|| this.commonMethodsTs.getData(msg.gcm, "body")
			|| this.commonMethodsTs.getData(msg, "body");

		message && this.toastr.presentToast(message);
		if (msg.data) {
			try {
				let object = JSON.parse(msg.data);
				let evn = object.id;
				if (evn) {
					this.coreProcess.getRemoteConfig(evn).then(config => {
						if (config) {
							this.coreProcess.setRemoteCurrentConfig(config);
						}
					});
				}
			} catch (error) {
			}
		}
	}
	/**
	 * Xử lý init app
	 */
	initializeApp() {
		this.commonMethodsTs.spinner.show();
		this.commonMethodsTs.platform.ready().then(async () => {
			try {
				this.keyboard.hideFormAccessoryBar(false);
				/**Khởi tạo biến */
				this.backgroundMode.setDefaults({ hidden: true, silent: true });
				this.backgroundMode.enable(); /** Bật tính năng  backgroundMode => mục đích dù có ẩn app đi code app vẫn hoạt động như await setInterval và setTimeout ...*/
				Variables.isMobile = (!document.URL.startsWith('http') || document.URL.startsWith('http://localhost:8080'));
				Variables.uniqueDeviceID = await this.commonMethodsTs.getUUID();

				/**tronghuu95 Xử lý pushNotication */
				this.notificationSetup();
				/** tronghuu95 xử lý onDynamicLink*/
				this.platformReady = true;
				
				this.initOnline();
				/** tronghuu95 biến width*/
				this.formatWidth();
				this.commonMethodsTs.platform.resize.subscribe(() => {
					this.formatWidth();
				});
				this.statusBar.overlaysWebView(false);
				this.statusBar.backgroundColorByHexString('#b62831');
				this.splashScreen.hide();
				this.commonMethodsTs.removeAllRaoBackgroud();// Khi mở lại app thì xóa tất cả dữ liệu tin rao đang chạy nền! ngừng theo dõi!
				this.commonMethodsTs.storeToken();
				this.isReady = true;
				Variables.isReady = true;
			} catch (error) {
				this.commonMethodsTs.createMessageError(error);
			}
		});
		/**
		 * Mở lại app
		 */
		this.commonMethodsTs.platform.resume.subscribe(async () => {
		});
	}

	//#region =============================== fn Init ================================
	formatWidth() {
		let viewWidth = this.commonMethodsTs.platform.width();
		let nj = 9;
		if (viewWidth <= 100) {
			nj = 6;
		} else if (viewWidth <= 250) {
			nj = 7;
		} else if (viewWidth <= 300) {
			nj = 8;
		}
		Variables.nView = nj;
	}

	async initTimeApp() {
		await this.coreProcess.getTimeServer();

		try {
			clearInterval(this.timSetInterval);
		} catch (error) {
		}
		let intervalTimeServer = Variables.intervalTimeServer;
		intervalTimeServer = +intervalTimeServer > 1000 ? intervalTimeServer : 1000;
		this.timSetInterval = setInterval(() => {
			CoreVariables.timeDateServer += Variables.intervalTimeServer;
		}, Variables.intervalTimeServer);
	}
	async initAppConfigRemote() {
		let config = await this.coreProcess.getRemoteCurrentConfig();
		if (config) {
			this.coreProcess.setRemoteCurrentConfig(config);
		}
	}
	async initOnline() {
		try {
			await this.initAppConfigRemote();
			await this.initTimeApp();
			this.cauHinhHeThongService.pushDataToCache();
			Variables.objMaCauHinh = await this.cauHinhHeThongService.getMaCauHinh();
			Variables.isIos = this.commonMethodsTs.platform.is('ios');
			Variables.ratePointToVND = this.commonMethodsTs.getData(Variables.objMaCauHinh[commonMaCauHinh.ratePointToVND], "GiaTriCauHinh", 1);
			if (Variables.isMobile) {
				// var appNewVersion = Variables.isIos ? await this.cauHinhHeThongService.getDataFromCacheByMaCauHinh('AppAdminVersion')
				// 	: await this.cauHinhHeThongService.getDataFromCacheByMaCauHinh('AppAdminVersionAndroid');
				// // Check compatible version
				// this.appVersion.getVersionNumber().then(res => {
				// 	Variables.functionGetVersionApp(res, appNewVersion);
				// })
				Variables.appVersion = await this.appVersion.getVersionNumber();
				this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY);
			}
			this.isInit = true;
		} catch (error) {
			this.isInit = false;;

		}
	}

	prepareRoute(outlet: any) {
		let res = outlet && outlet.isActivated;
		if (this.showSplash && res) {
			this.commonMethodsTs.spinner.hide();
			if (this.isReady) {
				setTimeout(() => {
					this.showSplash = false;
				}, 0);
			}
		}
		return res && !this.showSplash;
	}
	//#endregion =============================== end fn Init ================================
}
