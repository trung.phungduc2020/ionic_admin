import { NgModule } from '@angular/core';
import { RaoVatDialogUploadImagesPage } from './raovat-dialog-upload-images.page';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { RaoVatDialogUploadImagesModal } from './raovat-dialog-upload-images.modal';
import { RaoVatDialogUploadImagesService } from './raovat-dialog-upload-images.service';


@NgModule({
  declarations: [
    RaoVatDialogUploadImagesPage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule
  ],
  exports: [RaoVatDialogUploadImagesPage],

  entryComponents: [
    RaoVatDialogUploadImagesPage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    RaoVatDialogUploadImagesModal,
    RaoVatDialogUploadImagesService,
  ]
})
export class RaoVatDialogUploadImagesModule { }