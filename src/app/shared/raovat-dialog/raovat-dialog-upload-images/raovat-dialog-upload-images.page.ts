import { Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { RaoVatDialogUploadImagesModal } from './raovat-dialog-upload-images.modal';
import { CoreProcess } from 'src/app/core/core-process';
import { commonMessages } from 'src/app/core/variables';
import { Files } from 'src/app/core/entity';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'raovat-dialog-upload-images-page',
	templateUrl: './raovat-dialog-upload-images.page.html',
	styleUrls: ['./raovat-dialog-upload-images.page.scss'],
	providers: []
})
export class RaoVatDialogUploadImagesPage {
	// #region =============== Variables ====
	public type: any;
	public data: any;
	public dataRoot: any;
	public options: any = {};
	public ids: any;
	public percentageItem: any = {
		index: 0,
		item: null,
	};

	//#endregion ============ End variables ===
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public raoVatDialogVerifyModal: RaoVatDialogUploadImagesModal,
		public coreProcess: CoreProcess,
		public modalController: ModalController
	) {
	}
	ionViewDidEnter() {
		this.finish();
	}
	public async uploadImage() {
		let nameFile = this.options.nameFile || "new-file";
		this.ids = [];
		try {
			this.percentageItem.index = 0;
			this.percentageItem.item = null;
			let idHinhDaiDien = -1;
			//[recheck] tronghuu95 20191007100300 cái này sẽ hướng đối tượng riêng
			// this.commonMethodsTs.startLoading();
			for (let i = 0; i < this.data.length; i++) {
				const element: any = this.data[i];
				this.percentageItem.index++;
				this.percentageItem.item = element;
				let id = this.commonMethodsTs.getIdByUrlDailyXe(element);

				if (id > 0) {
					idHinhDaiDien = this.commonMethodsTs.getIdByUrlDailyXe(element);
				}
				else {
					idHinhDaiDien = await this.coreProcess.insertByUrl(element, nameFile);
				}
				if (+idHinhDaiDien > 0) {
					this.data[i] = this.commonMethodsTs.builLinkImage(nameFile, idHinhDaiDien);
					this.ids.push(idHinhDaiDien);
				}
				else {
					let resAlert = await this.commonMethodsTs.alertConfirm(`Tiến trình upload file bị lỗi! bạn có muốn bỏ qua file này: ${element}`);
					if (!resAlert) {
						return false;
					}
				}
			}

			// this.commonMethodsTs.stopLoading();
			return true;

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error, "Upload xảy ra lỗi");
		}
		return false;
	}

	public async uploadImageForListFiles() {
		this.ids = [];
		try {
			this.percentageItem.index = 0;
			this.percentageItem.item = null;
			//[recheck] tronghuu95 20191007100300 cái này sẽ hướng đối tượng riêng
			for (let i = 0; i < this.data.length; i++) {
				const f: Files = this.data[i];
				this.percentageItem.index++;
				this.percentageItem.item = f.ImageUrl;
				await this.coreProcess.insertByUrl_FireStoreForFile(f);
			}

			return true;

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error, "Upload xảy ra lỗi");
		}
		return false;
	}

	// trả về mảng ids đã được upload
	public async finish() {
		let res = this.options.isTypeFiles ? await this.uploadImageForListFiles() : await this.uploadImage();
		if (res) {
			this.dismiss(this.ids);
		}
	}

	cancel() {
		if (this.percentageItem.index == 0 || (this.percentageItem.index / this.data.length) == 1) {
			this.dismiss(null);

		}
		else {
			this.commonMethodsTs.createMessageWarning(commonMessages.M076);
		}
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {

	}
}