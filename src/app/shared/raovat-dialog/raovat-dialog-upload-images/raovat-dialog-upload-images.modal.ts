import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CoreProcess } from 'src/app/core/core-process';

@Injectable()
export class RaoVatDialogUploadImagesModal {
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public coreProcess: CoreProcess) { }
  
}
