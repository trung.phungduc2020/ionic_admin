import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { RaoVatDialogUploadImagesPage } from './raovat-dialog-upload-images.page';
import { Files } from 'src/app/core/entity';
import { ModalController } from '@ionic/angular';

@Injectable()
export class RaoVatDialogUploadImagesService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController ) { }

  private async show(componentProps: any) {
    return new Promise(async (resolve, reject) => {
			const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: RaoVatDialogUploadImagesPage,
        componentProps: componentProps
      });
    

      modal.onDidDismiss().then(async (res: any) => {
        resolve(res.data);
      });
      await modal.present();

		});
    
  }
  //Upload image list url hình ảnh
  async showProcessUploadImages(lUrl: string[]) {
    let d = this.commonMethodsTs.cloneArray(lUrl);
    let componentProps = {
      data: d,
      options: {
        title: "Upload Images",
      }
    }
    return await this.show(componentProps);
  }
  //Upload image list file hình ảnh
  async showProcessUploadImagesByListFiles(data: Files[]) {
    let componentProps = {
      data: data,
      options: {
        title: "Upload Images",
        isTypeFiles: true,
      }
    }
    return await this.show(componentProps);
  }
}
