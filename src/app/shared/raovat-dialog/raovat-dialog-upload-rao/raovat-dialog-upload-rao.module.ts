import { NgModule } from '@angular/core';
import { RaoVatDialogUploadRaoPage } from './raovat-dialog-upload-rao.page';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { RaoVatDialogUploadRaoModal } from './raovat-dialog-upload-rao.modal';
import { RaoVatDialogUploadRaoService } from './raovat-dialog-upload-rao.service';
import { NgPipesModule } from '../../pipes';

@NgModule({
  declarations: [
    RaoVatDialogUploadRaoPage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule,
    NgPipesModule
  ],
  exports: [RaoVatDialogUploadRaoPage],

  entryComponents: [
    RaoVatDialogUploadRaoPage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    RaoVatDialogUploadRaoModal,
    RaoVatDialogUploadRaoService
  ]
})
export class RaoVatDialogUploadRaoModule { }