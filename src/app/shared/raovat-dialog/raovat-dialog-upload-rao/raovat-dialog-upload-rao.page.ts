import { Component } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CoreProcess } from 'src/app/core/core-process';
import { commonMessages, Variables, commonAttr } from 'src/app/core/variables';
import { RaoVatDialogUploadRaoModal } from './raovat-dialog-upload-rao.modal';
import { Files } from 'src/app/core/entity';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';


@Component({
	selector: 'raovat-dialog-upload-rao-page',
	templateUrl: './raovat-dialog-upload-rao.page.html',
	styleUrls: ['./raovat-dialog-upload-rao.page.scss'],
	providers: []
})
export class RaoVatDialogUploadRaoPage {
	// #region =============== Variables ====
	public type: any;
	public data: any;
	public dataRoot: any;
	public options: any = {};
	public ids: any;
	public percentageItem: any = {
		index: 0,
		item: null,
	};
	// public fileUploads: any[] = [];
	public stepped: number = 0;
	public maxStep: number = 2;
	public listFileUploaded: any[] = [];
	public numFileUploaded: number = 0;
	public isfinish = false;
	public lbl = labelPages;
	public isProcessUpdate: any = true;
	//#endregion ============ End variables ===
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public raoVatDialogUploadRaoModal: RaoVatDialogUploadRaoModal,
		public coreProcess: CoreProcess,
		public modalController: ModalController
	) {
	}
	async ionViewDidEnter() {
		this.isProcessUpdate = this.data.Id > 0;
		let detailRao: any = this.commonMethodsTs.getRaoBackgroud(this.data.Id);
		this.removeProcess();
		if (detailRao) {
			this.data = detailRao.data;
			this.stepped = detailRao.stepped;
			this.listFileUploaded = detailRao.listFileUploaded;
		}
		this.dataRoot = this.commonMethodsTs.cloneObject(this.data);
		await this.processUpload();

	}


	removeListHinhAnh(data: any) {
		data.ListHinhAnh = [];
		data.ListHinhAnhJson = "[]";
		data.ListFilesUploaded = "0/0";
		data.MaLayoutAlbum = null;
		return data;
	}
	public async processUpload() {
		try {
			//xử lý dữ liệu

			if (this.stepped == 0) {
				let resB1ProcessUpload = await this.b1_ProcessUpload();
				if (resB1ProcessUpload) {
					this.stepped = 1;
				}
				else {
					return;
				}
			}
			if (this.stepped == 1) {

				await this.b2_ProcessUpload();

			}
		} catch (error) {

			this.removeProcess();
		}



	}
	//tiến trình đang chạy ngầm
	public isRunning(isRunning: boolean = true) {
		try {
			this.commonMethodsTs.setRaoBackgroud(this.data.Id, { data: this.data, stepped: this.stepped, listFileUploaded: this.listFileUploaded, isRunning: isRunning })

		} catch (error) {
			this.commonMethodsTs.createMessageErrorByTryCatch(error)
		}
	}
	public removeProcess() {
		this.commonMethodsTs.removeRaoBackgroud(this.data.Id)
	}
	/** tronghuu95 201910170935 OK
	 * b1_ProcessUpload
	 */
	public async b1_ProcessUpload() {
		//B1 Tạo tin
		//đang chạy tiến trình
		this.isRunning();
		let data = this.commonMethodsTs.cloneObject(this.data);
		if (!(data.Id > 0)) {
			data = this.removeListHinhAnh(data);
		}
		let dataUploadRao = await this.raoVatDialogUploadRaoModal.saveTinRao(this.data);
		this.removeProcess();
		if (dataUploadRao == null) {
			this.cancel();
			return false;
		}
		if (dataUploadRao == -1) {
			//xử lý dữ liệu khi lưu k đc
			let alertAlert = await this.commonMethodsTs.alertConfirm(commonMessages.M077);
			if (alertAlert) {
				return await this.b1_ProcessUpload();
			}
			else {
				this.cancel();
				return false;
			}

		}

		this.data.Id = dataUploadRao.Id; // lúc này sẽ là dạng cập nhật tin nên phải gán id mới vào data
		// Keep in client -> chờ xét duyệt, gửi cho admin, ko lưu notification
		let dataNotification = {
			Type: 5,
			TieuDe: commonMessages.N003,
			NoiDung: '[' + Variables.infoThanhVien.HoTen + '] - ' + data.TieuDe,
			FromIdThanhVien: this.commonMethodsTs.getInfoThanhVien(commonAttr.id),
			FromTenThanhVien: Variables.infoThanhVien.HoTen,
			Id: this.data.Id
		}
		this.commonMethodsTs.storeMessageSendNotification(dataNotification);
		return true;
	}

	public async b2_ProcessUpload() {
		this.isfinish = false;
		if (!(this.listFileUploaded && this.listFileUploaded.length > 0)) {
			let listFileUploaded = [];
			this.listFileUploaded = [];
			this.data.ListHinhAnh.forEach((element) => {
				this.listFileUploaded.push({ id: -1, base64Image: element, isUploaded: false })
			});
			//B1 Tạo tin
			if (this.data.ListHinhAnh && this.data.ListHinhAnh.length > 0) {
				let resInsertListImage = await this.coreProcess.insertListImageToServerQuick(this.data.ListHinhAnh, this.data.TieuDe);
				if (!resInsertListImage) {
					let resAlert = await this.commonMethodsTs.alertConfirmV1(`${commonMessages.M078} hoặc bỏ qua để cập nhật cập nhật ảnh sau`);
					if (resAlert == 0) {
						this.data = this.removeListHinhAnh(this.data);
						listFileUploaded = [];

					}
					else {
						if (resAlert == -1) {
							this.cancel();
							return;
						}
						else {
							this.b2_ProcessUpload();
							return;
						}
					}

				}
				else {
					this.data.ListHinhAnhJson = JSON.stringify(resInsertListImage.ids);
					this.data.ListHinhAnh.forEach((element, index) => {
						let id = resInsertListImage.ids[index];
						let indexFileUploads = resInsertListImage.fileUploads.findIndex(res => res.Id == id);
						let name = indexFileUploads >= 0 ? resInsertListImage.fileUploads[indexFileUploads].Name : "";
						listFileUploaded.push({ id: id, name: name, base64Image: element, isUploaded: this.commonMethodsTs.getIdByUrlDailyXe(element) > 0 })
					});
				}

			}
			this.listFileUploaded = listFileUploaded;
		}

		if (this.checkErrorUploadListImage()) {
			//B2 Upload files....
			this.isRunning();
			this.numFileUploaded = 0;
			let fileUploads: any[] = [];
			if (this.listFileUploaded && this.listFileUploaded.length > 0) {
				this.listFileUploaded.forEach((element, index) => {
					if (!element.isUploaded) {
						let f = new Files();
						f.Id = element.id;
						f.ImageUrl = element.base64Image;
						let name: string = element.name
						f.Name = name.convertrUrlPrefix();
						fileUploads.push(f);
					}
					else {
						this.numFileUploaded++;
					}

				});
			}
			else {
				this.data = this.removeListHinhAnh(this.data);
				this.listFileUploaded = [];
			}

			if (fileUploads && fileUploads.length > 0) {
				// let l = this.commonMethodsTs.cloneArray(this.listFileUploaded);
				for (let index = 0; index < fileUploads.length; index++) {
					const element = fileUploads[index];
					let findIndex = this.listFileUploaded.findIndex(i => i.id == element.Id);
					try {
						let imageUrl = await this.commonMethodsTs.convertImageResizer(element.ImageUrl);
						element.ImageUrl = imageUrl;
						let task: any = this.raoVatDialogUploadRaoModal.updateUrl(element);
						if (task) {
							this.listFileUploaded[findIndex].percentage = task.percentageChanges();
							task.finish.toPromise().then(res => {
								this.listFileUploaded[findIndex].isUploaded = res;
								this.isRunning();
								this.numFileUploaded++;
								if (!this.checkErrorUploadListImage()) {
									this.finish();
								}
							}
							);

						}
					} catch (error) {
						this.listFileUploaded[findIndex].isUploaded = false;
						this.isRunning();
					}

				}
			}
			else {
				this.finish();

			}

		}
		else {
			this.finish();
		}



	}
	//Loại bỏ hình ảnh không cần upload
	removeImage(item) {
		this.listFileUploaded = this.listFileUploaded.filter(res => res.id != item.id);
	}

	ignoreImage() {
		this.listFileUploaded = this.listFileUploaded.filter(res => res.isUploaded);
		this.b2_ProcessUpload();
	}

	async finish() {
		let data = this.commonMethodsTs.cloneObject(this.data);
		this.isRunning(false);
		let lUploaded = this.listFileUploaded.filter(res => res.isUploaded);
		if (this.listFileUploaded.length == lUploaded.length) {
			this.commonMethodsTs.createMessageSuccess(this.isProcessUpdate ? commonMessages.M005 : commonMessages.M003);
			this.stepped = 2;
			await this.raoVatDialogUploadRaoModal.updateTinDangAfter(data);
			this.removeProcess();
			this.dismiss(this.data);
		}
		this.isfinish = true;
	}
	checkErrorUploadListImage() {
		let lUploaded = this.listFileUploaded.filter(res => res.isUploaded);
		this.numFileUploaded = lUploaded.length;
		return (this.listFileUploaded.length - lUploaded.length) > 0;
	}

	cancel() {
		this.removeProcess();
		this.dismiss(this.stepped == 1 ? 1 : -1);
	}

	back() {
		this.dismiss(null);
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {

	}
}