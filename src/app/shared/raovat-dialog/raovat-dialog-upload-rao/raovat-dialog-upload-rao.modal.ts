import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { CoreProcess } from 'src/app/core/core-process';
import { Variables, commonMessages } from 'src/app/core/variables';
import { ResponseBase, Files } from 'src/app/core/entity';
import { ThanhVienRaoVatBLL_Commonfile, ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { RoundPipe, PluckPipe } from '../../pipes';
import { Observable } from 'rxjs';

@Injectable()
export class RaoVatDialogUploadRaoModal {
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public coreProcess: CoreProcess,
    public thanhVienRaoVatBLL_Commonfile: ThanhVienRaoVatBLL_Commonfile,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData) { }
  //Hàm tạo chitietBaiViet
  public async createFileChiTietBaiViet(id: any, content: any) {
    if (+id <= 0) {
      this.commonMethodsTs.createMessageSuccess("Tin đăng không tồn tại");
      return false;
    }
    try {
      let res: ResponseBase = await this.thanhVienRaoVatBLL_Commonfile.createFileChiTietDangTin(id, content).toPromise();
        res = new ResponseBase(res);
        if (res.isSuccess()) {
          return true;
        }
        else {
          this.commonMethodsTs.createMessageSuccess(res.getMessage());
          return false;
        }
    }
    catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
      return false;
    }
  }

  //if(local.idTinDang) => hiển thị dialog này
  //(1): vd local.idTinDang: {data: data, stepping: 1,2, listHinhAnhUploaded} => steped: 1 là chưa hoàn thành bước 1 -  2 chưa hoàn thành bước 2
  //B1 save tin rao
  public async saveTinRao(data: any, isCreateFile: boolean = true) {
    try {
      let res = await this.processSaveRao(data, isCreateFile);
      return res;
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
      return -1;
    }
    // TC:  => B2
    // TB:  => lưu vào localstore (1) => local.idTinDang.stepping =1 (chưa hoàn thành bước 1)
  }
  async updateHinhAnhDangTin(data: any) {
    let api = this.thanhVienRaoVatBLL_InteractData.updateHinhAnhDangTin(data);
    let res: ResponseBase = await api.toPromise();
    res = new ResponseBase(res);
    return res.isSuccess();


  }
  //saveDangTin
  private async processSaveRao(d: any, isCreateFile: boolean = true) {
    //xử lý dữ liệu tin mua/bán
    // 1. Tin bán
    // 2. Tin mua
    // Call api theo biến isUpdate? apiUpdate: apiCreate
    // Call tao file chi tiet
    let data = this.commonMethodsTs.cloneObject(d);
    let isUpdate = +data.Id > 0;
    try {
      let content = data.MoTaChiTiet;
      let objInfo = this.commonMethodsTs.getInfoThanhVien();
        // tin mua

        if (data.Type != 1) {
          delete data.IdThuongHieu;
          delete data.TenThuongHieu;
          delete data.DongCo;
          delete data.HopSo;
          delete data.SoChoNgoi;
          delete data.SoCua;
          delete data.NamSanXuat;
          delete data.NhienLieu;
          delete data.MucTieuHaoNhienLieu100;
          delete data.SoKmDaDi;
          delete data.NamDangKyXe;
          delete data.IdMauNgoaiThat;
          delete data.TenMauNgoaiThat;
          delete data.IdMauNoiThat;
          delete data.TenMauNoiThat;
          if (data.Gia >= 0) {
            data.Gia = 0;
          }
          else {
            delete data.Gia;
          }

        }
        else {
          //TranXuanDuc 2020/02/11
          //delete data.IdLoaiXe;
          //delete data.TenLoaiXe;
          //tin bán         
          if (data.TinhTrang == 1)  //xe mới
          {
            delete data.SoKmDaDi;
            delete data.NamDangKyXe;
          }
        }
        data.TuNgay = this.commonMethodsTs.convertDate(data.TuNgay);
        data.DenNgay = this.commonMethodsTs.convertDate(data.DenNgay);
        data.IdThanhVien = objInfo.Id;
        data.TenThanhVien = objInfo.HoTen;
        data.GhiChu = "Đang chờ xét duyệt!";
        data.MucTieuHaoNhienLieu100 = new RoundPipe().transform(data.MucTieuHaoNhienLieu100, 1);
        data.MaLayoutAlbum = await this.commonMethodsTs.getMaLayoutAlbum(data.ListHinhAnh || []);
        data.TongImage = data.ListHinhAnh.length;
        data.RewriteUrl = "";
        // xóa đối tượng dư thừa trước khi lưu
        delete data.UrlHinhDaiDien;
        delete data.ListHinhAnh;
        delete data.MoTaChiTiet;
        delete data.ListObjectHinhAnh;

        let api = isUpdate ? this.thanhVienRaoVatBLL_InteractData.updateDangTinXeInteract(data) : this.thanhVienRaoVatBLL_InteractData.insertDangTinInteract(data);
        let res: ResponseBase = await api.toPromise();
        res = new ResponseBase(res);

        if (res.isSuccess()) {
          let id = data.Id;
          id = +id > 0 ? id : res.getFirstDataId();
          if (isCreateFile && +id > 0) {
            this.createFileChiTietBaiViet(id, content); // Lưu nội dung CK Editor           
          }
          return res.getFirstData();
        }
        else {
          this.commonMethodsTs.createMessageError(res.getMessage());
        }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return null;

  }
  //B2 upload image
  public updateUrlListImage(listImage: any[]) {
    try {
      return new Observable(observer => {
        for (let i = 0; i < listImage.length; i++) {
          const f: Files = listImage[i];
          this.coreProcess.insertByUrl_FireStoreForFile(f).then(res => {
            observer.next({ id: f.Id, base64Image: f.ImageUrl, isUploaded: res });
          });
        }
      })


    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error, "Upload xảy ra lỗi");
    }
  }

  //B2 upload asyce image
  public asyceUpdateUrlListImage(listImage: any[]) {
    try {
      let listApiInsertByUrl: any[] = [];
      return new Observable(observer => {
        for (let i = 0; i < listImage.length; i++) {
          const f: Files = listImage[i];
          listApiInsertByUrl.push(this.coreProcess.insertByUrl_FireStoreForFile(f));
        }
        Promise.all(listApiInsertByUrl).then(lfile => {
          lfile.forEach(element => {
            observer.next({ id: element.Id, base64Image: element.ImageUrl, isUploaded: element != null });
          });

        }
        )

      })

    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error, "Upload xảy ra lỗi");
    }
  }

  //B2 upload image
  public updateUrl(f: Files) {
    return this.coreProcess.insertByUrl_FireStoreForFileWithTask(f);

  }

  //B2.2. xử lý event
  public async updateTinDangAfter(data: any): Promise<boolean> {
    try {
      let detailRao: any = this.commonMethodsTs.getRaoBackgroud(data.Id);

      if (detailRao) {
        let listImageUpload = detailRao.listFileUploaded.filter(res => res.isUploaded);// get localstore
        data.ListHinhAnh = new PluckPipe().transform(listImageUpload, "base64Image");
        data.ListHinhAnhJson = JSON.stringify(new PluckPipe().transform(listImageUpload, "id") || []);
        data.ListFilesUploaded = `${data.TongImage}/${data.TongImage}`;
        let dataUploadRao = await this.updateHinhAnhDangTin(data);
        if (!dataUploadRao) {
          //xử lý dữ liệu khi lưu k đc
          let alertAlert = await this.commonMethodsTs.alertConfirm(commonMessages.M077);
          if (alertAlert) {
            return await this.updateTinDangAfter(data);
          }
          else {
            return false;
          }

        }
        return true;

      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error, "Thao tác lưu thất bại");
    }
    return false;


  }

  public async processError(data: any, stepped: number, listFileUploaded: any[]) {

    this.commonMethodsTs.setRaoBackgroud(data.Id, { data: data, stepped: stepped, listFileUploaded: listFileUploaded })

  }

}
