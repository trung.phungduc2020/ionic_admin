import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { RaoVatDialogUploadRaoPage } from './raovat-dialog-upload-rao.page';
import { ModalController } from '@ionic/angular';

@Injectable()
export class RaoVatDialogUploadRaoService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController
     ) { }

  private async show(componentProps: any) {
  
    return new Promise(async (resolve, reject) => {
			const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: RaoVatDialogUploadRaoPage,
        componentProps: componentProps
      });
    

      modal.onDidDismiss().then(async (res: any) => {
        resolve(res.data);
      });
      await modal.present();

		});
    
  }
  //Upload image list url hình ảnh
  async showUploadRao(data:any) {
    let componentProps = {
      data: data,
      options: {
        title: "Upload rao",
      }
    }
    return await this.show(componentProps);
  }
 
}
