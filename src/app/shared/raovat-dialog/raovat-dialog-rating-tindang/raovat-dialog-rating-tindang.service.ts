import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { RaoVatDialogRatingTinDangComponent } from './raovat-dialog-rating-tindang.component';
import { ModalController } from '@ionic/angular';

@Injectable()
export class RaoVatDialogRatingTinDangService {
  public modal: any;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController) { }

  public show(dataTinDang) {
    return new Promise(async (resolve, error) => {
      this.modal = await this.modalController.create({
        component: RaoVatDialogRatingTinDangComponent,
        componentProps: { dataTinDang: dataTinDang }
      });
      this.modal.onDidDismiss().then((res: any) => {
        resolve(res.data);
      });
      await this.modal.present();

    })

  }



}
