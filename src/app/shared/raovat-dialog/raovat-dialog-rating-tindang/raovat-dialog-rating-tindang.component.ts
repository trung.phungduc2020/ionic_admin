import { noImage, defaultWxh, commonAttr } from 'src/app/core/variables';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Input, Component } from '@angular/core';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';
import { ViewDanhGiaModal } from 'src/app/components/views/view-danhgia/view-danhgia.modal';

@Component({
  selector: 'raovat-dialog-rating-tindang',
  templateUrl: './raovat-dialog-rating-tindang.component.html',
})
export class RaoVatDialogRatingTinDangComponent {
  //#region ================================== Variable =========================
  @Input() options: any = {};
  @Input() dataTinDang: any;
  public dataRating: any;
  public lbl = labelPages
  public urlHinhDaiDien: string = "";
  public labelBanDaDanhGia : string = "";
  public idThanhVien: any;
  data: any = {
    rate: 0,
    content: "",
    tieuDe: ""
  };

  public configCkDanhGia = {
    "height": 'auto',
    "resize_enabled": false,
    "toolbar": [],
    "toolbarStartupExpanded": false,
    "removePlugins": 'elementspath',
    // "extraPlugins": 'divarea',
  };

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController,
    public viewDanhGiaModal: ViewDanhGiaModal){

  }

  async ngOnInit() {
    
    this.idThanhVien = await this.commonMethodsTs.getInfoThanhVien(commonAttr.id);
    let listHinhAnhJson = this.commonMethodsTs.stringJsonToArray(this.dataTinDang.ListHinhAnhJson);
    if (listHinhAnhJson && listHinhAnhJson.length > 0) {
      this.urlHinhDaiDien = this.commonMethodsTs.builLinkImage(this.dataTinDang.RewriteUrl, listHinhAnhJson[0], defaultWxh.two);
    }
    else {
      this.urlHinhDaiDien = noImage;
    }
    if(!this.dataRating)
    {
      this.loadData();
    }
    
    
  }


  async loadData(){
    let params = {
			idTinDangs: this.dataTinDang.Id,
			idUsers: this.idThanhVien,
			displayItems: 1,
			currentPage: 1
		}
		this.dataRating = await this.viewDanhGiaModal.getData(params);
		if (this.dataRating && this.dataRating.length > 0) {
      let item = this.dataRating && this.dataRating[0]?this.dataRating[0]:null;
			this.data.content = item && item.Description ? item.Description : "";
      this.data.rate = item && item.Rating ? item.Rating : "";
      this.labelBanDaDanhGia = " (Bạn đã đánh giá)"; 
		}
  }

  closeModal() {
    // this.selectedItem = item;
    this.dismiss()
  }

  startOnClick(e) {

    this.data.rate = e.rating;
  }


  sendRating() {
    this.data.tieuDe = this.dataTinDang && this.dataTinDang.TieuDe ? this.dataTinDang.TieuDe : "";
    this.dismiss(this.data);
  }

  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }
}
