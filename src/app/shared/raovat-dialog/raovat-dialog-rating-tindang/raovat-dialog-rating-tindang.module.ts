import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { IonicRatingModule } from 'ionic-rating';
import { RaoVatDialogRatingTinDangComponent } from "./raovat-dialog-rating-tindang.component";
import { RaoVatDialogRatingTinDangService } from "./raovat-dialog-rating-tindang.service";
import { CKEditorModule } from 'ngx-ckeditor';
import { NgObjectPipesModule } from '../../pipes';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingModule,
    CKEditorModule,
    NgObjectPipesModule
  ],
  declarations: [RaoVatDialogRatingTinDangComponent],
  entryComponents: [
    RaoVatDialogRatingTinDangComponent
  ],
  providers:[RaoVatDialogRatingTinDangService]
})
export class RaoVatDialogRatingTinDangModule {
  constructor() {
  }


}
