
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { RaoVatDialogCongratulationsPage } from './raovat-dialog-congratulations.page';
// The modal's component of the previous chapter
@NgModule({
  declarations: [
    RaoVatDialogCongratulationsPage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule
  ],
  exports: [RaoVatDialogCongratulationsPage],

  entryComponents: [
    RaoVatDialogCongratulationsPage
  ],
  providers: [

  ]
})
export class RaoVatDialogCongratulationsModule { }