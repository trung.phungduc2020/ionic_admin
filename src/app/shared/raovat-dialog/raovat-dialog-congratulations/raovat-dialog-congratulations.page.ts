import { CommonMethodsTs } from "src/app/core/common-methods";
import { Component  } from "@angular/core";
import { ModalController } from '@ionic/angular';
@Component({
	selector: 'raovat-dialog-congratulations',
	templateUrl: './raovat-dialog-congratulations.page.html',
})
export class RaoVatDialogCongratulationsPage {
	soPoint: any = 0;
	//#endregion ============ End variables ===
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public modalController: ModalController
	) {
	}

	xemPoint(){
		this.commonMethodsTs.toPage_MemberThongTinPoint();
		this.dismiss(null);
	}
 
	cancel() {
		this.dismiss(null)
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
}