import { NumberPhoneVerifyCodeModule } from './number-phone-verify-code/number-phone-verify-code.module';
import { NumberPhoneSendCodeModule } from './number-phone-send-code/number-phone-send-code.module';
import { NgModule } from '@angular/core';
import { NumberPhoneModal } from './number-phone.modal';


@NgModule({
  declarations: [

  ],
  imports: [
    NumberPhoneSendCodeModule,
    NumberPhoneVerifyCodeModule
  ],
  providers:[NumberPhoneModal]
 
})
export class NumberPhoneModule { }