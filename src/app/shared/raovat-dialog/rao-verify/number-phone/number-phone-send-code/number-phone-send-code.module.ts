
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData, AdminRaoVatBLL_SmsPhone } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { NumberPhoneSendCodePage } from './number-phone-send-code.page';
import { NumberPhoneSendCodeModal } from './number-phone-send-code.modal';
import { NumberPhoneSendCodeService } from './number-phone-send-code.service';

@NgModule({
  declarations: [
    NumberPhoneSendCodePage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule
  ],
  exports: [NumberPhoneSendCodePage],

  entryComponents: [
    NumberPhoneSendCodePage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    AdminRaoVatBLL_SmsPhone,
    NumberPhoneSendCodeModal,
    ThanhVienDaiLyXeBLL_ThanhVien,
    NumberPhoneSendCodeService
  ]
})
export class NumberPhoneSendCodeModule { }