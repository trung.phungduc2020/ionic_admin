import { CommonMethodsTs } from "src/app/core/common-methods";
import { Component } from "@angular/core";
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { NumberPhoneSendCodeModal } from './number-phone-send-code.modal';
import { placeHolder, commonMessages } from 'src/app/core/variables';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';
// import { AuthFirebaseService } from 'src/app/shared/services/auth-firebase.service';
@Component({
	selector: 'number-phone-send-code-page',
	templateUrl: './number-phone-send-code.page.html',
	styleUrls: ['./number-phone-send-code.page.scss'],
	providers: [ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_ThanhVien]
})
export class NumberPhoneSendCodePage {

	//#endregion ============ End variables ===
	numberPhone: string = "";
	typeSmsValidation: any;
	options: any = {};
	isCheckedRecaptcha: boolean = false;
	placeHolder: any = placeHolder;
	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public numberPhoneSendCodeModal: NumberPhoneSendCodeModal,
		public modalController: ModalController

	) {
	}
	async ionViewDidEnter() {
		if (!this.numberPhone.isEmpty()) {
			let res = await this.checkCodeSend(this.numberPhone, this.typeSmsValidation)
			if (res) {
				this.dismiss(this.numberPhone);
				return;
			}

		}
		//cai nay la su kien sau khi da checkbox recaptcha
		this.numberPhoneSendCodeModal.initRecaptchaVerifier().verify()
			.then(result => {
				this.commonMethodsTs.ngZone.run(() => {
					this.isCheckedRecaptcha = true;
				})
			})
			.catch(error => {
				// this.commonMethodsTs.createMessageErrorByTryCatch(error);
			});
	}
	public async checkCodeSend(numberPhone: string, typeSmsValidation: string) {
		let timeVerifyValid = await this.numberPhoneSendCodeModal.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
		return +timeVerifyValid > 0
	}

	public async sendCode() {

		if (this.numberPhone.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.phone));
			return;
		}
		let numberPhone = this.commonMethodsTs.formatNumberPhoneSantandize(this.numberPhone.replace(/[^0-9]*/g, ''));
		if (numberPhone.length != 10) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P002.format(labelPages.numberPhone)+". Số điện thoại phải đảm bảo 10 chữ số");
			return;
		}
		let res = await this.checkCodeSend(numberPhone, this.typeSmsValidation)
		if (res) {
			this.dismiss(numberPhone);
			return;
		}
		if (!this.isCheckedRecaptcha) {
			this.commonMethodsTs.createMessageWarning("Vui lòng check recaptcha");
			return;
		}
		this.commonMethodsTs.startLoading();
		res = await this.numberPhoneSendCodeModal.sendCodeByNumberPhone(numberPhone, this.typeSmsValidation, this.options.isCheckExist);
		this.commonMethodsTs.stopLoading();

		if (res) {
			this.dismiss(numberPhone);
		}
	}

	startTimeOut() {

	}
	cancel() {

		this.dismiss(null)
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {
	}
}