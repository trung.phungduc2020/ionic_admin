import { Injectable } from '@angular/core';

import { NumberPhoneModal } from '../number-phone.modal';

@Injectable()
export class NumberPhoneSendCodeModal {
  constructor(
    public numberPhoneModal: NumberPhoneModal) { }
  /**
   * sendCodeByNumberPhone: fun gửi code
   * @param numberPhone numberPhone sẽ gửi code
   */
  public initRecaptchaVerifier() {
    return this.numberPhoneModal.initRecaptchaVerifier();
  }
  
  public async sendCodeByNumberPhone(numberPhone: string, typeSmsValidation:string, checkPhoneExist?: boolean) {
    return await this.numberPhoneModal.sendCodeByNumberPhone(numberPhone, typeSmsValidation, checkPhoneExist);
  }
  public async getTimeCodeByNumberPhone(numberPhone: string, typeSmsValidation:string)
  {
    return await this.numberPhoneModal.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
  }
  public async removeTimeCodeByNumberPhone(numberPhone: string, typeSmsValidation:string)
  {
    return await this.numberPhoneModal.removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation);

  }

  public getTimeBlockNumberPhone(numberPhone: string)
  {
    return this.numberPhoneModal.getTimeBlockNumberPhone(numberPhone);
  }
  
}
