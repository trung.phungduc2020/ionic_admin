import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { NumberPhoneSendCodePage } from './number-phone-send-code.page';
import { ModalController } from '@ionic/angular';

@Injectable()
export class NumberPhoneSendCodeService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController ) { }

  private show(componentProps: any) {
    return new Promise(async (resolve, error) => {
      let modal = await this.modalController.create({
        component: NumberPhoneSendCodePage,
        componentProps: componentProps
      });
      modal.onDidDismiss().then(async (res: any) => {
        resolve(res.data);

      });
      await modal.present();
    })

  }
  async showSendCode(numberPhone: string, typeSmsValidation: string, options: any) {
    options = options || {};
    options.title = options.title || "Bước 1/2. Số điện thoại của bạn";
    let componentProps = {
      numberPhone: numberPhone,
      typeSmsValidation: typeSmsValidation,
      options: options
    }
    return await this.show(componentProps) as string;
  }

  async showSendCodeByNumberPhone(numberPhone: string, typeSmsValidation: string, options?: any) {
    options = options || {};
    let componentProps = {
      numberPhone: numberPhone,
      typeSmsValidation: typeSmsValidation,
      options: {
        title: options.title,
      }
    }
    return await this.show(componentProps) as string;
  }

  async showSendCodeByNewNumberPhone(typeSmsValidation: string, options?: any) {
    options = options || {};
    options.allowChange = true;
    options.isCheckExist = true;
    return await this.showSendCode("", typeSmsValidation, options);
  }

  async showSendCodeByForgotPassword(typeSmsValidation: string, options?: any) {
    options = options || {};
    options.allowChange = true;
    options.isCheckExist = false;

    return await this.showSendCode("", typeSmsValidation, options);
  }
}
