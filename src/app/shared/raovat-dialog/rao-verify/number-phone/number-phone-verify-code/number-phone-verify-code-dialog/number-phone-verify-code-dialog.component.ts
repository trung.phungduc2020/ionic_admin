import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Component} from '@angular/core';
import { Variables, commonMessages } from 'src/app/core/variables';
import { NumberPhoneVerifyCodeModal } from '../number-phone-verify-code.modal';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'number-phone-verify-code-dialog',
  templateUrl: './number-phone-verify-code-dialog.component.html',
})
export class NumberPhoneVerifyCodeDialogComponent {
  //#endregion ============ End variables ===
  numberPhone: string = "";
  options: any = {};
  public code: string;
  public type: any;
  public variables = Variables;
  public history: any = [];
  public isCheckedRecaptcha: boolean = false;
  public lbl = labelPages;
  public notRecaptcha: boolean = false;;
  public modal: any;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public numberPhoneVerifyCodeModal: NumberPhoneVerifyCodeModal,
    public modalController: ModalController
  ) {
  }

  async ionViewDidEnter() {
    //cai nay la su kien sau khi da checkbox recaptcha

    if (this.numberPhone.isEmpty()) {
      this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.phone));
      this.dismiss(false)
    }
    this.initRecapcha();

  }
  async initRecapcha() {
    this.history = await this.numberPhoneVerifyCodeModal.getHistory(this.numberPhone);
    if (this.history.length > Variables.dungSms3GKhiGuiXongLanThu) {
      this.notRecaptcha = true;
      this.reSendMaXacThuc();
    }
    else {
      this.numberPhoneVerifyCodeModal.initRecaptchaVerifier().verify()
        .then(result => {
          this.commonMethodsTs.ngZone.run(() => {
            this.reSendMaXacThuc()
          })
        })
        .catch(error => {
          this.commonMethodsTs.createMessageErrorByTryCatch(error);
        });
    }
  }

  async reSendMaXacThuc() {
    //B1 xóa time codeByEmail
    let timeBlock = this.numberPhoneVerifyCodeModal.getTimeBlockNumberPhone(this.numberPhone);
    if (+timeBlock > 0) {
      let numMinutesAwait = (Math.ceil(timeBlock / 60)).toString();
      this.commonMethodsTs.createMessageWarning(commonMessages.M099 + " " + commonMessages.P009.format(numMinutesAwait));
      return;
    }

    this.commonMethodsTs.startLoading();
    try {
      let res = await this.numberPhoneVerifyCodeModal.removeTimeCodeByNumberPhone(this.numberPhone, this.type);
      if (res) {
        let sendCodeByNumberPhone = await this.numberPhoneVerifyCodeModal.sendCodeByNumberPhone(this.numberPhone, this.type, this.options.checkExistNumberPhone);
        if (sendCodeByNumberPhone) {
          //Thông báo đã gửi mã sendcode
          this.commonMethodsTs.createMessageSuccess(commonMessages.M096);
          this.dismiss(this.numberPhone);
        }
      }
      else {
        this.commonMethodsTs.createMessageError(commonMessages.M097);
      }
    } catch (error) {
      this.commonMethodsTs.createMessageError(error);

    }

    this.commonMethodsTs.stopLoading()
  }

  cancel() {
    this.dismiss(null);
  }
  async dismiss(item?: any) {
    await this.modalController.dismiss(item);
  }
  ionViewDidLeave() {
  }
}
