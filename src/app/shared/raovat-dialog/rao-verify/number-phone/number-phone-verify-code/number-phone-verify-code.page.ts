import { CommonMethodsTs } from "src/app/core/common-methods";
import { Component } from "@angular/core";
import { NumberPhoneVerifyCodeModal } from './number-phone-verify-code.modal';
import { commonMessages, Variables, configOTP } from 'src/app/core/variables';
import { labelPages } from 'src/app/core/labelPages';
import { NumberPhoneVerifyCodeDialogComponent } from './number-phone-verify-code-dialog/number-phone-verify-code-dialog.component';
import { ModalController } from '@ionic/angular';
@Component({
	selector: 'number-phone-verify-code-page',
	templateUrl: './number-phone-verify-code.page.html',
	styleUrls: ['./number-phone-verify-code.page.scss'],
	providers: []
})
export class NumberPhoneVerifyCodePage {

	//#endregion ============ End variables ===
	numberPhone: string = "";
	options: any = {};
	public code: string;
	public timeVerifyValid: any;
	public refreshInterval: any;
	public typeSmsValidation: any;
	public variables = Variables;
	public history: any = [];
	public isCheckedRecaptcha: boolean = false;
	public lbl = labelPages;
	public notRecaptcha: boolean = false;;
	public numberPhoneDaXacThuc: boolean = false;
	public modal: any;
	public config = configOTP

	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public numberPhoneVerifyCodeModal: NumberPhoneVerifyCodeModal,
		public modalController: ModalController
	) {
	}

	async ionViewDidEnter() {
		//cai nay la su kien sau khi da checkbox recaptcha

		if (this.numberPhone.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.phone));
			this.dismiss(false)
		}
		if (Variables.listNumberPhoneMoiDaXacThuc.indexOf(this.numberPhone) >= 0) {
			this.numberPhoneDaXacThuc = true;
			return;
		}
		this.timeVerifyValid = this.variables.timeVerifyByNumberPhone[this.numberPhone][this.typeSmsValidation] || {};
		if (!(+this.timeVerifyValid.value > 0)) {

			this.commonMethodsTs.createMessageWarning(commonMessages.M048);
			await this.numberPhoneVerifyCodeModal.removeTimeCodeByNumberPhone(this.numberPhone, this.typeSmsValidation);
			this.cancel();
			return;
		}
		this.startTimeOut();

	}
	async initRecapcha() {
		this.history = await this.numberPhoneVerifyCodeModal.getHistory(this.numberPhone);
		if (this.history.length > Variables.dungSms3GKhiGuiXongLanThu) {
			this.isCheckedRecaptcha = true;
			this.notRecaptcha = true;
		}
		else {
			this.numberPhoneVerifyCodeModal.initRecaptchaVerifier().verify()
				.then(result => {
					this.commonMethodsTs.ngZone.run(() => {
						this.isCheckedRecaptcha = true;
					})
				})
				.catch(error => {
					this.commonMethodsTs.createMessageErrorByTryCatch(error);
				});
		}
	}
	next() {
		this.dismiss(this.numberPhone);

	}
	onOtpChange(otp) {
		this.code = otp;
	}
	public async verifyCode() {
		if (!this.commonMethodsTs.checkMaXacThuc(this.code)) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M050);
			return;
		}

		this.commonMethodsTs.startLoading();
		await this.onVerifyCode();
		this.commonMethodsTs.stopLoading();
	}

	private async onVerifyCode() {
		if (this.numberPhone.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.phone));
			return;
		}
		if (!(+this.timeVerifyValid.value > 0)) {
			await this.numberPhoneVerifyCodeModal.removeTimeCodeByNumberPhone(this.numberPhone, this.typeSmsValidation);
			this.commonMethodsTs.createMessageWarning(commonMessages.M048);
			return;
		}
		if (!this.code || this.code.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.verifyCode));
			return;
		}
		this.code = this.code.toString();
		let res = await this.numberPhoneVerifyCodeModal.verifyCodeByNumberPhone(this.numberPhone, this.code, this.typeSmsValidation);
		if (res) {
			this.dismiss(this.numberPhone);
			return;

		}
	}
	async reSendMaXacThuc() {
		let timeBlock = this.numberPhoneVerifyCodeModal.getTimeBlockNumberPhone(this.numberPhone);
		if (+timeBlock > 0) {
			let numMinutesAwait = (Math.ceil(timeBlock / 60)).toString();
			this.commonMethodsTs.createMessageWarning(commonMessages.M099 + " " + commonMessages.P009.format(numMinutesAwait));
			return;
		}
		this.history = await this.numberPhoneVerifyCodeModal.getHistory(this.numberPhone);
		if (this.history.length > Variables.dungSms3GKhiGuiXongLanThu) {
			this.commonMethodsTs.startLoading();
			try {
				let res = await this.numberPhoneVerifyCodeModal.removeTimeCodeByNumberPhone(this.numberPhone, this.typeSmsValidation);
				if (res) {
					// //Gọi lại hàm gửi sendcode
					let sendCodeByNumberPhone = await this.numberPhoneVerifyCodeModal.sendCodeByNumberPhone(this.numberPhone, this.typeSmsValidation, this.options.checkExistNumberPhone);
					if (sendCodeByNumberPhone) {
						//Thông báo đã gửi mã sendcode
						this.commonMethodsTs.createMessageSuccess(commonMessages.M096);
						this.startTimeOut();
					}
				}
				else {
					this.commonMethodsTs.createMessageError(commonMessages.M097);
				}
			} catch (error) {
				this.commonMethodsTs.createMessageError(error);

			}

			this.commonMethodsTs.stopLoading()
		}
		else {
			this.modal = await this.modalController.create({
				component: NumberPhoneVerifyCodeDialogComponent,
				componentProps: { numberPhone: this.numberPhone, type: this.typeSmsValidation }
			});
			this.modal.onDidDismiss().then((res: any) => {
				this.startTimeOut();
			});
			await this.modal.present();
		}

		// //B1 xóa time codeByEmail




	}
	async startTimeOut() {
		this.timeVerifyValid = this.variables.timeVerifyByNumberPhone[this.numberPhone][this.typeSmsValidation] || {};
		await this.initRecapcha();

	}
	stopTimeOut() {
		try {
			clearInterval(this.refreshInterval);
		} catch (error) {

		}
	}
	cancel() {
		this.dismiss(null)
	}
	async  dismiss(item?: any) {
		this.stopTimeOut();
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {
		this.stopTimeOut();
	}
}