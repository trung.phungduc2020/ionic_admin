import { Injectable } from '@angular/core';
import { NumberPhoneModal } from '../number-phone.modal';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';

@Injectable()
export class NumberPhoneVerifyCodeModal {
  constructor(
    public numberPhoneModal: NumberPhoneModal, public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData) { }
  /**
   * sendCodeByNumberPhone: fun gửi code
   * @param numberPhone numberPhone sẽ gửi code
   */
  public initRecaptchaVerifier() {
    return this.numberPhoneModal.initRecaptchaVerifier();
  }
  public async verifyCodeByNumberPhone(numberPhone: string, code: string, typeSmsValidation: string    ) {
    let res =  await this.numberPhoneModal.verifyCodeByNumberPhone(numberPhone, code, typeSmsValidation);
    return res;
  }
  public async getTimeCodeByNumberPhone(numberPhone: string, typeSmsValidation: string)
  {
    return await this.numberPhoneModal.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
  }
  public async removeTimeCodeByNumberPhone(numberPhone: string, typeSmsValidation)
  {
    return await this.numberPhoneModal.removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation);

  }

  public async getHistory(numberPhone: string)
  {
    return await this.numberPhoneModal.getHistory(numberPhone);

  }

  public getTimeBlockNumberPhone(numberPhone: string)
  {
    return  this.numberPhoneModal.getTimeBlockNumberPhone(numberPhone);

  }
  public async sendCodeByNumberPhone(numberPhone: string, typeSmsValidation:string, checkPhoneExist?: boolean) {
    return await this.numberPhoneModal.sendCodeByNumberPhone(numberPhone, typeSmsValidation, checkPhoneExist);
  }
}
