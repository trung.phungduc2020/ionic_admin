
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData, AdminRaoVatBLL_SmsPhone } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { NumberPhoneVerifyCodePage } from './number-phone-verify-code.page';
import { NumberPhoneVerifyCodeModal } from './number-phone-verify-code.modal';
import { NumberPhoneVerifyCodeService } from './number-phone-verify-code.service';
import { NgPipesModule } from 'src/app/shared/pipes';
import { NumberPhoneVerifyCodeDialogModule } from './number-phone-verify-code-dialog/number-phone-verify-code-dialog.module';
import { NgOtpInputModule } from 'src/lib/input-otp/ng-otp-input.module';

@NgModule({
  declarations: [
    NumberPhoneVerifyCodePage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule,
    NgPipesModule,
    NumberPhoneVerifyCodeDialogModule,
    NgOtpInputModule
    ],
  entryComponents: [
    NumberPhoneVerifyCodePage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    AdminRaoVatBLL_SmsPhone,
    NumberPhoneVerifyCodeModal,
    ThanhVienDaiLyXeBLL_ThanhVien,
    NumberPhoneVerifyCodeService
    
  ]
})
export class NumberPhoneVerifyCodeModule { }