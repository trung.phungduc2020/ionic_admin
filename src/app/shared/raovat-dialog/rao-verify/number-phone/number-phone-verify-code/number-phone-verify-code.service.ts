import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { NumberPhoneVerifyCodePage } from './number-phone-verify-code.page';
import { OverlayEventDetail } from '@ionic/core';
import { ModalController } from '@ionic/angular';

@Injectable()
export class NumberPhoneVerifyCodeService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController ) { }

  private show(componentProps: any) {

    return new Promise(async (resolve, error) => {
      const modal: HTMLIonModalElement = await this.modalController.create({
        component: NumberPhoneVerifyCodePage,
        componentProps: componentProps
      });
      modal.onDidDismiss().then((res: OverlayEventDetail) => {
        resolve(res.data);

      });
      await modal.present();


    })

  }

  async showVerifyByNumberPhone(numberPhone, typeSmsValidation: string, options?: any) {
    options = options || {};

    let componentProps = {
      numberPhone: numberPhone,
      typeSmsValidation: typeSmsValidation,
      options: {
        title: options.title || "Bước 2/2. Xác thực số điện thoại là của bạn",
        checkExistNumberPhone: options.checkExistNumberPhone
      }
    }
    return await this.show(componentProps) as string;
  }
}
