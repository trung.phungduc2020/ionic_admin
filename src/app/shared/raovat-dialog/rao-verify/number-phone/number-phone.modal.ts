import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { AdminRaoVatBLL_InteractData, AdminRaoVatBLL_SmsPhone } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
import { Variables, commonMessages, localStorageData, TypeSmsValidation } from 'src/app/core/variables';
import { ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_InterfaceData } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { CoreVariables } from 'src/app/core/core-variables';
import { ThanhVienRaoVatBLL_InteractData, ThanhVienRaoVatBLL_DailyxeData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { FirebaseAuthenticationService } from 'src/app/shared/services/authentication.service';
import { DatePipe } from '@angular/common';
import { labelPages } from 'src/app/core/labelPages';
import { CoreProcess } from 'src/app/core/core-process';

@Injectable()
export class NumberPhoneModal {
  private keyStorage = localStorageData.verifyNumberPhone;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public thanhVienDaiLyXeBLL_ThanhVien: ThanhVienDaiLyXeBLL_ThanhVien,
    public thanhVienDaiLyXeBLL_InterfaceData: ThanhVienDaiLyXeBLL_InterfaceData,
    public adminRaoVatBLL_InteractData: AdminRaoVatBLL_InteractData,
    public adminRaoVatBLL_SmsPhone: AdminRaoVatBLL_SmsPhone,
    public firebaseAuthenticationService: FirebaseAuthenticationService,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    public thanhVienRaoVatBLL_DailyxeData: ThanhVienRaoVatBLL_DailyxeData,
    public coreProcess: CoreProcess) { }
  initRecaptchaVerifier() {
    return this.firebaseAuthenticationService.initRecaptchaVerifier();
  }

  /**
   * sendCodeByNumberPhone: fun gửi code
   * @param numberPhone numberPhone sẽ gửi code
   */
  public async sendCodeByNumberPhone(numberPhone: string, typeSmsValidation: string, checkPhoneExist: boolean = false) {

    numberPhone = this.commonMethodsTs.formatNumberPhoneSantandize(numberPhone);
    try {
      //1Kt time block của sdt này
      let timeBlock = await this.getTimeBlockNumberPhone(numberPhone);
      if (!(timeBlock > 0)) {
        let history = await this.getHistory(numberPhone);
        if (history.length > Variables.smsLoopDefault) {
          timeBlock = Variables.smsTimeBetaDefault * 60;
          await this.setTimeBlockNumberPhone(numberPhone)
        }
      }
      if (+timeBlock > 0) {
        let numMinutesAwait = (Math.ceil(timeBlock / 60)).toString();
        this.commonMethodsTs.createMessageWarning(commonMessages.M095 + " " + commonMessages.P009.format(numMinutesAwait));
        return false;
      }
      //Kt time đã gửi code còn không?
      let time = await this.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation)
      if (time > 0) {
        return true;
      }
      //Kt exist email nếu có checkEmailExist
      //Với trường hợp là quên mật khẩu thì phải kt số điện thoại có tồn tại hay không
      if (checkPhoneExist || typeSmsValidation == TypeSmsValidation.ForgetPassword.toString()) {
        let checkExistByNumberPhone = await this.checkExistByDienThoai(numberPhone);
        //Quên maattj khẩu thì bắt buột số điện thoại phải tồn tại
        if (typeSmsValidation == TypeSmsValidation.ForgetPassword.toString()) {
          if (!checkExistByNumberPhone) {
            this.commonMethodsTs.createMessageWarning(commonMessages.P011.format(labelPages.numberPhone));
            return false;

          }
        }
        else {
          if (checkExistByNumberPhone) {
            this.commonMethodsTs.createMessageWarning(commonMessages.P001.format(labelPages.numberPhone));
            return false;

          }
        }

      }
      //đã xác thực trước đó
      if (Variables.listNumberPhoneMoiDaXacThuc.indexOf(numberPhone) >= 0) {
        return true;
      }
      let history = await this.getHistory(numberPhone);
      if (history.length > Variables.dungSms3GKhiGuiXongLanThu) {
        return await this.sendByServer(numberPhone, typeSmsValidation);
      }
      else {
        return await this.sendByFireBase(numberPhone, typeSmsValidation);
      }
    } catch (error) {
      if (error.code == "auth/too-many-requests") {
        await this.setTimeBlockNumberPhone(numberPhone, Variables.smsTimeBlockForFireBase); // khóa tài khoản lại trong 15'
        await this.removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation);// xóa luôn ở local
        let numMinutesAwait = Variables.smsTimeBlockForFireBase.toString();
        this.commonMethodsTs.createMessageError(commonMessages.M095 + " " + commonMessages.P009.format(numMinutesAwait));

      }
      else {
        this.commonMethodsTs.createMessageError(error);
      }

    }
    return false;
  }
  async sendByServer(numberPhone, typeSmsValidation) {


    let result: ResponseBase = await this.adminRaoVatBLL_SmsPhone.sendSms(numberPhone, typeSmsValidation).toPromise();
    result = new ResponseBase(result);
    if (result.isSuccess()) {
      await this.setTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
      await this.addHistory(numberPhone);
      return true;
    }
    else {

      if (result.IntResult == -4) {
        let time = await this.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation)
        if (time > 0) {
          this.commonMethodsTs.createMessageWarning(result.getMessage());
          return true;

        }
        else {
          this.commonMethodsTs.createMessageWarning(commonMessages.M030);
          return false;
        }

      }
      else {
        this.commonMethodsTs.createMessageWarning(result.getMessage());

      }
    }
  }
  async sendByFireBase(numberPhone, typeSmsValidation) {
    let result: ResponseBase = await this.adminRaoVatBLL_SmsPhone.createCodeSms(numberPhone, typeSmsValidation).toPromise();
    result = new ResponseBase(result);

    if (result.isSuccess()) {

      var sendSmsProfile = await this.firebaseAuthenticationService.sendCodePhoneNumber(numberPhone, typeSmsValidation);
      if (!sendSmsProfile) {

        await this.removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
        this.commonMethodsTs.createMessageWarning(sendSmsProfile);

        return false;
      }
      //create time code
      await this.setTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
      await this.addHistory(numberPhone);
      return true;
    }
    else {

      if (result.IntResult == -4) {
        let time = await this.getTimeCodeByNumberPhone(numberPhone, typeSmsValidation)
        if (time > 0) {
          this.commonMethodsTs.createMessageWarning(result.getMessage());
          return true;

        }
        else {
          this.commonMethodsTs.createMessageWarning(commonMessages.M030);
          return false;
        }

      }
      else {
        this.commonMethodsTs.createMessageWarning(result.getMessage());

      }
    }
  }
  /**
  * verifyCodeByNumberPhone: fun gửi code
  * @param numberPhone numberPhone sẽ gửi code
  * @param code code đã gửi

  */
  public async verifyCodeByNumberPhone(numberPhone: string, code: any, typeSmsValidation: string) {
    try {
      let history = await this.getHistory(numberPhone);
      let verifyPhoneNumber;
      if (history.length > Variables.dungSms3GKhiGuiXongLanThu) {
        verifyPhoneNumber = await this.verifyCodeByServer(numberPhone, code);
      }
      else {
        verifyPhoneNumber = await this.verifyCodeByFirebase(numberPhone, code, typeSmsValidation);
      }
      if (verifyPhoneNumber) {
        numberPhone && Variables.listNumberPhoneMoiDaXacThuc.push(numberPhone);
        await this.removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation);
        await this.removeTimeBlockNumberPhone(numberPhone);
        await this.removeHistory(numberPhone);
        return true;
      }
      else {
        return false
      }
    } catch (error) {
    }
    this.commonMethodsTs.createMessageWarning(commonMessages.M022);

    return false;
  }
  public async verifyCodeByFirebase(numberPhone, code, typeSmsValidation) {
    let verifyPhoneNumber = await this.firebaseAuthenticationService.verifyPhoneNumber(numberPhone, code, typeSmsValidation);
    return verifyPhoneNumber
  }

  public async verifyCodeByServer(numberPhone, code) {
    try {

      let res: ResponseBase = await this.adminRaoVatBLL_InteractData.xacThucDienThoaiByMaXacThuc(numberPhone, code).toPromise();
      res = new ResponseBase(res);
      return res.isSuccess();
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return false;
  }

  async checkExistByDienThoai(dienThoai: string) {
    let res: ResponseBase = await this.thanhVienRaoVatBLL_DailyxeData.isExistDienThoaiThanhVienSelfCheck(dienThoai).toPromise();
    res = new ResponseBase(res);
    return res.StrResult == "True"
  }


  //#region =================================== Time code ====================================
  /**
   * 
   * @param numberPhone: time send code by numberPhone
   */
  async getTimeCodeByNumberPhone(numberPhone, typeSmsValidation: string) {
    try {
      let value = Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].value;
      if (value <= 0) {
        return 0
      }
      return value;
    } catch (error) {

    }
    return 0;
  }
  async setTimeCodeByNumberPhone(numberPhone, typeSmsValidation: string) {
    try {
      // let key = this.keyStorage + numberPhone + typeSmsValidation;
      await this.coreProcess.getTimeServer();
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(numberPhone);
      objTime[typeSmsValidation] = CoreVariables.timeDateServer;
      await this.commonMethodsTs.setObjTimeByNumberPhone(numberPhone, objTime);
      /**Xử lý biến static */
      Variables.timeVerifyByNumberPhone[numberPhone] = Variables.timeVerifyByNumberPhone[numberPhone] || {};
      Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation] = Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation] || {};
      if (Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval) {
        clearInterval(Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval);
      }
      Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].value = Variables.timeVerifyValid * 60;
      Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval = setInterval(() => {
        Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].value -= 1;
        if (Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval && Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].value <= 0) {
          clearInterval(Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval);
        }
      }, 1000);

      return true;
    } catch (error) {

    }
    return false;

  }
  async removeTimeCodeByNumberPhone(numberPhone, typeSmsValidation: string) {
    try {
      let res: ResponseBase = await this.thanhVienRaoVatBLL_InteractData.deleteMaXacThucByPhone(numberPhone, typeSmsValidation).toPromise();
      res = new ResponseBase(res);
      if (!res.isSuccess()) {
        this.commonMethodsTs.createMessageWarning(res.getMessage());
        return false;
      }
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(numberPhone)
      objTime[typeSmsValidation] = undefined;
      if (Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval) {
        clearInterval(Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].setInterval);
        Variables.timeVerifyByNumberPhone[numberPhone][typeSmsValidation].value = 0;
      }
      return await this.commonMethodsTs.setObjTimeByNumberPhone(numberPhone, objTime);
    } catch (error) {

    }
    return false;
  }
  //#endregion =================================== end Time code ====================================

  //#region =================================== Time block ====================================
  //GET
  getTimeBlockNumberPhone(numberPhone) {

    try {
      let value = Variables.timeVerifyByNumberPhone[numberPhone][localStorageData.timeBlock].value;
      if (value <= 0) {
        return 0
      }
      return value;
    } catch (error) {

    }
    return 0;

  }
  //SET
  async setTimeBlockNumberPhone(numberPhone, longTimeBlock?: any) {
    longTimeBlock = Variables.smsTimeBetaDefault;
    try {
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(numberPhone);
      let data = { timestart: CoreVariables.timeDateServer, longTime: longTimeBlock * 60 };
      objTime[localStorageData.timeBlock] = data;
      await this.commonMethodsTs.setObjTimeByNumberPhone(numberPhone, objTime);
      await this.commonMethodsTs.initTimeBlockByNumberPhone(numberPhone);
      return true;
    } catch (error) {

    }
    return false;

  }
  //DELETE
  async removeTimeBlockNumberPhone(numberPhone) {

    try {
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(numberPhone)
      objTime[localStorageData.timeBlock] = undefined;
      if (Variables.timeVerifyByNumberPhone[numberPhone][localStorageData.timeBlock].setInterval) {
        clearInterval(Variables.timeVerifyByNumberPhone[numberPhone][localStorageData.timeBlock].setInterval);
        Variables.timeVerifyByNumberPhone[numberPhone][localStorageData.timeBlock].value = 0;
      }
      return await this.commonMethodsTs.setObjTimeByNumberPhone(numberPhone, objTime);
    } catch (error) {

    }
    return false;

  }
  //#endregion =================================== edn Time block ====================================

  //#region =================================== History =======================================
  //GET
  async getHistory(dienThoai) {
    try {
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(dienThoai);
      return objTime[localStorageData.historyResend] || [];
    } catch (error) {

    }
    return [];


  }
  //SET
  /**
   * addHistory thêm lịch sử gửi mã xác thực 
   * @param dienThoai : dienThoai nhận mã xác thực
   * @param time : time gửi mã xác thực
   */
  async addHistory(dienThoai, time?: any) {
    try {
      time = time || CoreVariables.timeDateServer;
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(dienThoai);
      let historyResend: any[] = objTime[localStorageData.historyResend] || [];
      let length = historyResend.length;
      let strItem = "";
      if (length == 0) {
        strItem = `Mã đã được gửi: ${new DatePipe('en-US').transform(time, "dd/MM/yyyy HH:mm:ss")}`
      }
      else {
        strItem = `Mã đã được gửi lại lần ${length}: ${new DatePipe('en-US').transform(time, "dd/MM/yyyy HH:mm:ss")}.`
        if (length == Variables.smsLoopDefault) {
          await this.setTimeBlockNumberPhone(dienThoai);
        }
      }

      historyResend.push(strItem);
      objTime[localStorageData.historyResend] = historyResend;
      await this.commonMethodsTs.setObjTimeByNumberPhone(dienThoai, objTime);
    } catch (error) {

    }
    return false;


  }
  //DElETE
  /**
   * removeHistory xóa lịch sử gửi mã xác thực khi verify thành công
   * @param dienThoai : dienThoai nhận mã xác thực
   */
  async removeHistory(dienThoai) {

    try {
      let objTime = await this.commonMethodsTs.getObjTimeByNumberPhone(dienThoai)
      objTime[localStorageData.historyResend] = undefined;
      return await this.commonMethodsTs.setObjTimeByNumberPhone(dienThoai, objTime);
    } catch (error) {

    }

  }
  //#endregion ======================================== end History ==============================================
}
