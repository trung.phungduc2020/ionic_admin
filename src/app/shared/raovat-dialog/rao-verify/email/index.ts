import { EmailVerifyCodeModule } from './email-verify-code/email-verify-code.module';
import { EmailSendCodeModule } from './email-send-code/email-send-code.module';
import { NgModule } from '@angular/core';
import { EmailModal } from './email.modal';

@NgModule({
  declarations: [

  ],
  imports: [
    EmailSendCodeModule,
    EmailVerifyCodeModule
  ],
  providers:[EmailModal]
 
})
export class EmailModule { }