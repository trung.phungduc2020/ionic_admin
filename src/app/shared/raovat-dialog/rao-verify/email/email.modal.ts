import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import { AdminRaoVatBLL_Email, AdminRaoVatBLL_InteractData } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { ResponseBase } from 'src/app/core/entity';
import { Variables, commonMessages, localStorageData, TypeSmsValidation } from 'src/app/core/variables';
import { ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_InterfaceData } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { CoreVariables } from 'src/app/core/core-variables';
import { ThanhVienRaoVatBLL_InteractData, ThanhVienRaoVatBLL_DailyxeData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { DatePipe } from '@angular/common';
import { CoreProcess } from 'src/app/core/core-process';

@Injectable()
export class EmailModal {
  private keyStorage = localStorageData.verifyEmail;
  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public adminRaoVatBLL_Email: AdminRaoVatBLL_Email,
    public thanhVienDaiLyXeBLL_ThanhVien: ThanhVienDaiLyXeBLL_ThanhVien,
    public thanhVienDaiLyXeBLL_InterfaceData: ThanhVienDaiLyXeBLL_InterfaceData,
    public adminRaoVatBLL_InteractData: AdminRaoVatBLL_InteractData,
    public thanhVienRaoVatBLL_InteractData: ThanhVienRaoVatBLL_InteractData,
    public thanhVienRaoVatBLL_DailyxeData: ThanhVienRaoVatBLL_DailyxeData,
    public coreProcess: CoreProcess) { }
  /**
   * sendCodeByEmail: fun gửi code
   * @param email email sẽ gửi code
   */
  public async sendCodeByEmail(email: string, checkEmailExist = false) {
    try {

      //1Kt time block của sdt này
      let timeBlock = await this.getTimeBlockEmail(email);
      if (!(timeBlock > 0)) {
        let history = await this.getHistory(email);
        if (history.length > Variables.smsLoopDefault) {
          timeBlock = Variables.smsTimeBetaDefault * 60;
          await this.setTimeBlockEmail(email, Variables.smsTimeBetaDefault)
        }
      }
      if (+timeBlock > 0) {
        let numMinutesAwait = (Math.ceil(timeBlock / 60)).toString();
        this.commonMethodsTs.createMessageWarning(commonMessages.M099 + " " + commonMessages.P009.format(numMinutesAwait));
        return false;
      }
      //Kt time đã gửi code còn không?
      let time = await this.getTimeCodeByEmail(email)
      if (time > 0) {
        return true;
      }
      //Kt exist email nếu có checkEmailExist
      if (checkEmailExist) {
        let checkExistByEmail = await this.checkExistByEmail(email);
        if (checkExistByEmail) {
          this.commonMethodsTs.createMessageWarning(commonMessages.M098);
          return false;
        }
      }
      //Đã từng xác thực trước đó
      if (Variables.listEmailMoiDaXacThuc.indexOf(email) >= 0) {
        return true;
      }
      //gọi api
      let res: ResponseBase = await this.adminRaoVatBLL_Email.sendVerifyEmail(email).toPromise();
      res = new ResponseBase(res);
      if (res.isSuccess()) {
        await this.addHistory(email);
        await this.setTimeCodeByEmail(email);
        return true;
      }
      else {
        if (res.IntResult == -3) {
          let time = await this.getTimeCodeByEmail(email)
          if (time > 0) {
            this.commonMethodsTs.createMessageWarning(res.getMessage());
            return true;

          }
          else {
            this.commonMethodsTs.createMessageWarning(commonMessages.M030);
            return false;
          }

        }
        this.commonMethodsTs.createMessageWarning(res.getMessage());

      }
      return false;
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return false;
  }
  /**
  * verifyCodeByEmail: fun gửi code
  * @param email email sẽ gửi code
  * @param code code đã gửi

  */
  public async verifyCodeByEmail(email: string, code: string) {
    try {

      let res: ResponseBase = await this.adminRaoVatBLL_InteractData.xacThucEmailByMaXacThuc(email, code).toPromise();
      res = new ResponseBase(res);
      if (res.isSuccess()) {
        email && Variables.listEmailMoiDaXacThuc.push(email);
        await this.removeTimeCodeByEmail(email);
        await this.removeTimeBlockEmail(email);
        await this.removeHistory(email);
        return true;
      } else {
        this.commonMethodsTs.createMessageError(res.getMessage());
        return false;
      }
    } catch (error) {
      this.commonMethodsTs.createMessageErrorByTryCatch(error);
    }
    return false;
  }

  async checkExistByEmail(email: string) {
    let res: ResponseBase = await this.thanhVienRaoVatBLL_DailyxeData.isExistEmailThanhVienSelfCheck(email).toPromise();
    res = new ResponseBase(res);
    return res.StrResult == "True"
  }

  //#region =================================== Time code ====================================
  //GET
  async getTimeCodeByEmail(email) {
    try {
      let value = Variables.timeVerifyByEmail[email].value;
      if (value <= 0) {
        return 0
      }
      return value;
    } catch (error) {

    }
    return 0;
  }
  //SET
  async setTimeCodeByEmail(email) {
    try {
      await this.coreProcess.getTimeServer();
      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email);
      objTime[TypeSmsValidation.sendEmail] = { timeStart: CoreVariables.timeDateServer, longTime: Variables.timeVerifyValid * 60 };
      await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
      /**Xử lý biến static */
      await this.commonMethodsTs.initTimeVerifyByEmail(email);
      return true;
    } catch (error) {

    }
    return false;
  }
  //DELETE
  async removeTimeCodeByEmail(email) {
    try {
      let res: ResponseBase = await this.thanhVienRaoVatBLL_InteractData.deleteMaXacThucByEmail(email).toPromise();
      res = new ResponseBase(res);
      if (res.isSuccess()) {
        let objTime = await this.commonMethodsTs.getObjTimeByEmail(email)
        objTime[TypeSmsValidation.sendEmail] = undefined;
        if (Variables.timeVerifyByEmail[email].setInterval) {
          clearInterval(Variables.timeVerifyByEmail[email].setInterval);
          Variables.timeVerifyByEmail[email].value = 0;
        }
        return await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
      }
    } catch (error) {

    }
    return false;

  }
  //#endregion =================================== End time code =============================

  //#region =================================== Time block ===================================
  //GET
  async setTimeBlockEmail(email, longTimeBlock?: any) {
    longTimeBlock = Variables.smsTimeBetaDefault;

    try {
      // let key = this.keyStorage + email + typeSmsValidation;
      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email);
      let data = { timestart: CoreVariables.timeDateServer, longTime: longTimeBlock * 60 };
      objTime[localStorageData.timeBlock] = data;
      await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
      await this.commonMethodsTs.initTimeBlockByEmail(email);
      //đã block thì xóa 2 cái còn lại

      return true;
    } catch (error) {

    }
    return false;

  }

  async getTimeBlockEmail(email) {

    try {
      let value = Variables.timeVerifyByEmail[email][localStorageData.timeBlock].value;
      if (value <= 0) {
        return 0
      }
      return value;
    } catch (error) {

    }
    return 0;

  }

  async removeTimeBlockEmail(email) {

    try {
      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email);
      objTime[localStorageData.timeBlock] = undefined;
      if (Variables.timeVerifyByEmail[email][localStorageData.timeBlock].setInterval) {
        clearInterval(Variables.timeVerifyByEmail[email][localStorageData.timeBlock].setInterval);
        Variables.timeVerifyByEmail[email][localStorageData.timeBlock].value = 0;
      }
      return await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
    } catch (error) {

    }
    return false;

  }
  //#endregion =================================== Time block ==================================

  //#region =================================== History ======================================
  async getHistory(email) {
    try {

      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email);
      return objTime[localStorageData.historyResend] || [];
    } catch (error) {

    }
    return [];


  }
  /**
   * addHistory thêm lịch sử gửi mã xác thực 
   * @param email : email nhận mã xác thực
   * @param time : time gửi mã xác thực
   */
  async addHistory(email, time?: any) {
    try {
      time = time || CoreVariables.timeDateServer;
      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email);
      let historyResend: any[] = objTime[localStorageData.historyResend] || [];
      let length = historyResend.length;
      let strItem = "";
      if (length == 0) {
        strItem = `Mã đã được gửi: ${new DatePipe('en-US').transform(time, "dd/MM/yyyy HH:mm:ss")}`
      }
      else {
        strItem = `Mã đã được gửi lại lần ${length}: ${new DatePipe('en-US').transform(time, "dd/MM/yyyy HH:mm:ss")}.`
        if (length == Variables.smsLoopDefault) {
          // strItem +=  `\nNếu bạn không hoàn thành việc xác thực thì hệ thống tạm khóa chức năng gửi mã xác thực trong khoảng ${Variables.smsTimeBetaDefault} phút`
          await this.setTimeBlockEmail(email, Variables.smsTimeBetaDefault);
        }
      }

      historyResend.push(strItem);
      objTime[localStorageData.historyResend] = historyResend;
      return await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
    } catch (error) {

    }
    return false;


  }
  /**
   * removeHistory xóa lịch sử gửi mã xác thực khi verify thành công
   * @param email : email nhận mã xác thực
   */
  async removeHistory(email) {

    try {
      let objTime = await this.commonMethodsTs.getObjTimeByEmail(email)
      objTime[localStorageData.historyResend] = undefined;
      return await this.commonMethodsTs.setObjTimeByEmail(email, objTime);
    } catch (error) {

    }

  }



  //#endregion ===================================== End History ===========================================
}
