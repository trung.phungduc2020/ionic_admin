
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData, AdminRaoVatBLL_SmsPhone, AdminRaoVatBLL_Email } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { EmailSendCodePage } from './email-send-code.page';
import { EmailSendCodeModal } from './email-send-code.modal';
import { EmailSendCodeService } from './email-send-code.service';

@NgModule({
  declarations: [
    EmailSendCodePage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule
  ],
  exports: [EmailSendCodePage],

  entryComponents: [
    EmailSendCodePage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    AdminRaoVatBLL_SmsPhone,
    AdminRaoVatBLL_Email,
    EmailSendCodeModal,
    ThanhVienDaiLyXeBLL_ThanhVien,
    EmailSendCodeService
  ]
})
export class EmailSendCodeModule { }