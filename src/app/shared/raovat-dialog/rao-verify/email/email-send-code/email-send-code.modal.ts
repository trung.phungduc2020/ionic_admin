import { Injectable } from '@angular/core';

import { EmailModal } from '../email.modal';

@Injectable()
export class EmailSendCodeModal {
  constructor(
    public emailModal: EmailModal) { }
  /**
   * sendCodeByEmail: fun gửi code
   * @param email email sẽ gửi code
   */
  public async sendCodeByEmail(email: string, checkEmailExist = false) {
    return await this.emailModal.sendCodeByEmail(email, checkEmailExist);
  }
  public async getTimeCodeByEmail(email: string)
  {
    return await this.emailModal.getTimeCodeByEmail(email);
  }
  public removeTimeCodeByEmail(email: string)
  {
    return this.emailModal.removeTimeCodeByEmail(email);

  }
  public async checkExistByEmail(email: string) {
    return await this.emailModal.checkExistByEmail(email);
  }

  public async getTimeBlockEmail(email: string)
  {
    return await this.emailModal.getTimeBlockEmail(email);
  }
  
}
