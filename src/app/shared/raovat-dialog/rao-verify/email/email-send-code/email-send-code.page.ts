import { CommonMethodsTs } from "src/app/core/common-methods";
import { Component } from "@angular/core";
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { EmailSendCodeModal } from './email-send-code.modal';
import { labelPages } from 'src/app/core/labelPages';
import { commonMessages } from 'src/app/core/variables';
import { ModalController } from '@ionic/angular';
@Component({
	selector: 'email-send-code-page',
	templateUrl: './email-send-code.page.html',
	styleUrls: ['./email-send-code.page.scss'],
	providers: [ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_ThanhVien]
})
export class EmailSendCodePage {

	//#endregion ============ End variables ===
	email: string = "";
	options: any = {};
	labelPages = labelPages;

	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public emailSendCodeModal: EmailSendCodeModal,
		public modalController: ModalController

	) {
	}
	async ionViewDidEnter() {
		if (!this.email.isEmpty()) {
			let res = await this.checkCodeSend(this.email)
			if (res) {
				this.dismiss(this.email);
				return;
			}

		}
		
	}
	public async checkCodeSend(email: string) {
		let timeVerifyValid = await this.emailSendCodeModal.getTimeCodeByEmail(email);
		return +timeVerifyValid > 0
	}
	
	public async sendCode() {
		
		if (this.email.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.email));
			return;
		}
		if (!this.email.isEmail()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P002.format(labelPages.email));
			return;
		}
		
		let res = await this.checkCodeSend(this.email)
		if (res) {
			this.dismiss(this.email);
			return;
		}
		
		this.commonMethodsTs.startLoading();
		res = await this.emailSendCodeModal.sendCodeByEmail(this.email, this.options.isCheckExist);
		this.commonMethodsTs.stopLoading();
		if (res) {
			this.dismiss(this.email);
		}
	}

	startTimeOut() {

	}
	cancel() {
		this.dismiss(null)
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {

	}
}