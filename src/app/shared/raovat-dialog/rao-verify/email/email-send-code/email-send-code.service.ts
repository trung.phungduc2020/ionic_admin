import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { EmailSendCodePage } from './email-send-code.page';
import { ModalController } from '@ionic/angular';

@Injectable()
export class EmailSendCodeService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController) { }

  private show(componentProps: any) {
    return new Promise(async (resolve, error) => {
      let modal = await this.modalController.create({
        component: EmailSendCodePage,
        componentProps: componentProps
      });
      modal.onDidDismiss().then(async (res: any) => {
        resolve(res.data);
      });
      await modal.present();
    })

  }

  async showSendCodeByEmail(email, options?:any) {
    options = options || {};
    let componentProps = {
      email: email,
      options: {
        title: options.title || "Bước 1/2. Email của bạn",
      }
    }
    return await this.show(componentProps) as string;
  }

  async showSendCodeByNewEmail(options?:any) {
    options = options || {};
    let componentProps = {
      options: {
        title: options.title || "Bước 1/2. Nhập email của bạn",
        allowChange: true,
        isCheckExist: true
      }
    }
    return await this.show(componentProps) as string;
  }
}
