import { Injectable } from '@angular/core';
import { EmailModal } from '../email.modal';

@Injectable()
export class EmailVerifyCodeModal {
  constructor(
    public emailModal: EmailModal) { }
  /**
   * sendCodeByEmail: fun gửi code
   * @param email email sẽ gửi code
   */
  public async verifyCodeByEmail(email: string, code: string) {
    return await this.emailModal.verifyCodeByEmail(email, code);
  }
  public async getTimeCodeByEmail(email: string)
  {
    return  await this.emailModal.getTimeCodeByEmail(email);
  }
  public async removeTimeCodeByEmail(email: string)
  {
    return await this.emailModal.removeTimeCodeByEmail(email);

  }
  public async sendCodeByEmail(email: string, checkEmailExist = false)
  {
    return await this.emailModal.sendCodeByEmail(email, checkEmailExist);

  }
  public async getHistory(email: string)
  {
    return await this.emailModal.getHistory(email);

  }

  public async getTimeBlockEmail(email: string)
  {
    return await this.emailModal.getTimeBlockEmail(email);

  }
  
}
