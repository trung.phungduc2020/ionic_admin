
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThanhVienRaoVatBLL_InteractData } from 'src/app/core/api/thanh-vien/rao-vat/thanhVienRaoVatBLL';
import { AdminRaoVatBLL_InteractData, AdminRaoVatBLL_SmsPhone, AdminRaoVatBLL_Email } from 'src/app/core/api/admin/rao-vat/adminRaoVatBLL';
import { FormsModule } from '@angular/forms';
import { ControllersModule } from 'src/app/components/controllers';
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { EmailVerifyCodePage } from './email-verify-code.page';
import { EmailVerifyCodeModal } from './email-verify-code.modal';
import { EmailVerifyCodeService } from './email-verify-code.service';
import { NgPipesModule } from 'src/app/shared/pipes';
import { NgOtpInputModule } from 'src/lib/input-otp/ng-otp-input.module';

@NgModule({
  declarations: [
    EmailVerifyCodePage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ControllersModule,
    NgPipesModule,
    NgOtpInputModule
  ],
  exports: [EmailVerifyCodePage],

  entryComponents: [
    EmailVerifyCodePage
  ],
  providers: [
    ThanhVienRaoVatBLL_InteractData,
    AdminRaoVatBLL_InteractData,
    AdminRaoVatBLL_SmsPhone,
    AdminRaoVatBLL_Email,
    EmailVerifyCodeModal,
    ThanhVienDaiLyXeBLL_ThanhVien,
    EmailVerifyCodeService
  ]
})
export class EmailVerifyCodeModule { }