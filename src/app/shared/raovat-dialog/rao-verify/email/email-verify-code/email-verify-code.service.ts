import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Injectable, EventEmitter } from '@angular/core';
import { EmailVerifyCodePage } from './email-verify-code.page';
import { ModalController } from '@ionic/angular';

@Injectable()
export class EmailVerifyCodeService {
  public onDidDismiss: EventEmitter<any> = new EventEmitter();

  constructor(
    public commonMethodsTs: CommonMethodsTs,
    public modalController: ModalController ) { }

  private show(componentProps: any) {
    
    return new Promise(async (resolve, error) => {
      let modal = await this.modalController.create({
        component: EmailVerifyCodePage,
        componentProps: componentProps
      });
      modal.onDidDismiss().then((res: any) => {
        resolve(res.data);
      });
      await modal.present();
    })

  }

  async showVerifyByEmail(email, options?:any) {
    options = options || {};
    let componentProps = {
      email: email,
      options: {
        title: options.title|| "Bước 2/2. Xác thực email là của bạn",
        checkExistEmail: options.checkExistEmail
      }
    }
    return await this.show(componentProps) as string; 
  }
}
