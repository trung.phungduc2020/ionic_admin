import { CommonMethodsTs } from "src/app/core/common-methods";
import { Component } from "@angular/core";
import { ThanhVienDaiLyXeBLL_ThanhVien } from 'src/app/core/api/thanh-vien/dailyxe/thanhVienDaiLyXeBLL';
import { EmailVerifyCodeModal } from './email-verify-code.modal';
import { commonMessages, Variables, configOTP } from 'src/app/core/variables';
import { labelPages } from 'src/app/core/labelPages';
import { ModalController } from '@ionic/angular';
@Component({
	selector: 'email-verify-code-page',
	templateUrl: './email-verify-code.page.html',
	styleUrls: ['./email-verify-code.page.scss'],
	providers: [ThanhVienDaiLyXeBLL_ThanhVien, ThanhVienDaiLyXeBLL_ThanhVien]
})
export class EmailVerifyCodePage {

	//#endregion ============ End variables ===
	email: string = "";
	options: any = {};
	public code: string;
	public timeVerifyValid: any = {};
	public refreshInterval: any;
	public variables = Variables;
	public lbl = labelPages;
	public history: any = [];
	public emailDaXacThuc: boolean = false;
	public config = configOTP;

	constructor(
		public commonMethodsTs: CommonMethodsTs,
		public emailVerifyCodeModal: EmailVerifyCodeModal,
		public modalController: ModalController

	) {
	}
	async ionViewDidEnter() {
		if (this.email.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.email));
			this.dismiss(false)
		}
		if (Variables.listEmailMoiDaXacThuc.indexOf(this.email) >= 0) {
			this.emailDaXacThuc = true;
			return;
		}
		let timeVerify = await this.emailVerifyCodeModal.getTimeCodeByEmail(this.email);
		if (!(+timeVerify > 0)) {
			this.commonMethodsTs.createMessageWarning(commonMessages.M046);
			await this.emailVerifyCodeModal.removeTimeCodeByEmail(this.email);
			this.cancel();
			return;
		}
		this.startTimeOut();
	}
	public async next() {
		this.dismiss(this.email);
	}
	onOtpChange(otp) {
		this.code = otp;
	  }
	
	public async verifyCode() {
		if(!this.commonMethodsTs.checkMaXacThuc(this.code))
		{
			this.commonMethodsTs.createMessageWarning(commonMessages.M050);
			return;
		}
		this.commonMethodsTs.startLoading();
		await this.onVerifyCode();
		this.commonMethodsTs.stopLoading();
	}
	private async onVerifyCode() {
		if (this.email.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.email));
			return;
		}
		if (!(+this.timeVerifyValid.value > 0)) {
			await this.emailVerifyCodeModal.removeTimeCodeByEmail(this.email);
			this.commonMethodsTs.createMessageWarning(commonMessages.M046);
			return;
		}
		if (!this.code || this.code.isEmpty()) {
			this.commonMethodsTs.createMessageWarning(commonMessages.P005.format(labelPages.verifyCode));
			return;
		}
		this.code = this.code.toString();
		let res = await this.emailVerifyCodeModal.verifyCodeByEmail(this.email, this.code);
		if (res) {
			this.dismiss(this.email);
		}
	}

	async startTimeOut() {
		this.history = await this.emailVerifyCodeModal.getHistory(this.email);
		this.timeVerifyValid = this.variables.timeVerifyByEmail[this.email];
	}
	stopTimeOut() {
		try {
			clearInterval(this.refreshInterval);
		} catch (error) {

		}
	}
	async reSendMaXacThuc() {
		//B1 xóa time codeByEmail

		let timeBlock = await this.emailVerifyCodeModal.getTimeBlockEmail(this.email);
		if (+timeBlock > 0) {
			let numMinutesAwait = (Math.ceil(timeBlock / 60)).toString();
			this.commonMethodsTs.createMessageWarning(commonMessages.M099 + " " + commonMessages.P009.format(numMinutesAwait));
			return;
		}
		this.commonMethodsTs.startLoading();
		try {
			let res = await this.emailVerifyCodeModal.removeTimeCodeByEmail(this.email);
			if (res) {
				//Gọi lại hàm gửi sendcode
				let sendCodeByEmail = await this.emailVerifyCodeModal.sendCodeByEmail(this.email, this.options.checkExistEmail);
				if (sendCodeByEmail) {
					//Thông báo đã gửi mã sendcode
					this.commonMethodsTs.createMessageSuccess(commonMessages.M096);
					this.startTimeOut();
				}
			}
			else {
				this.commonMethodsTs.createMessageError(commonMessages.M097);
			}
		} catch (error) {
			this.commonMethodsTs.createMessageError(error);

		}

		this.commonMethodsTs.stopLoading()
	}

	cancel() {
		this.dismiss(null)
	}
	async dismiss(item?: any) {
		await this.modalController.dismiss(item);
	}
	ionViewDidLeave() {
		this.stopTimeOut();
	}
}