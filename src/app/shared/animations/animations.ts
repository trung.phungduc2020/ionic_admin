
import {
    trigger, animateChild, group,
    transition, animate, style, query, stagger
} from '@angular/animations';

//animate ListDangTinMoi
export const animationList =
    trigger('animationList', [
        transition('* => *', [ // each time the binding value changes
            query(':leave', [
                stagger(100, [
                    animate('0.5s', style({ opacity: 0 }))
                ])
            ], { optional: true }),
            query(':enter', [
                style({ opacity: 0 }),
                stagger(100, [
                    animate('0.5s', style({ opacity: 1 }))
                ])
            ], { optional: true })
        ])
    ])
//animate ListTinNoiBat
export const listTinNoiBat =
    trigger('listTinNoiBat', [
        transition('* => *', [ // each time the binding value changes
            query(':leave', [
                stagger(100, [
                    animate('0.5s', style({ opacity: 0 }))
                ])
            ], { optional: true }),
            query(':enter', [
                style({ opacity: 0 }),
                stagger(100, [
                    animate('0.5s', style({ opacity: 1 }))
                ])
            ], { optional: true })
        ])
    ])

// Routable animations
export const routeAnimations =
    trigger('routeAnimations', [
        transition('* <=> *', [
            query(':enter, :leave', style({ position: 'fixed', width: '100%' })
                , { optional: true }),
            group([  // block executes in parallel
                query(':enter', [
                    style({ transform: 'translateX(100%)' }),
                    animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
                ], { optional: true }),
                query(':leave',
                    [
                        style({ transform: 'translateX(0%)' }),
                        animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
                    ], { optional: true }),
            ])
        ])
    ])
