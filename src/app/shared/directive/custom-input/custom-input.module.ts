import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomInputDirective } from './custom-input.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CustomInputDirective],
  exports: [CustomInputDirective],
})
export class CustomInputDirectiveModule { 
}
