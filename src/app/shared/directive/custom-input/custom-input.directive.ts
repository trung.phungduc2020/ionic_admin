import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'ion-input'
})
export class CustomInputDirective {
  constructor(private _el: ElementRef) { }

  @HostListener('keypress', ['$event']) onKeyPress(event) {

    let  maxlength = this._el.nativeElement.maxlength;
    const initalValue = this._el.nativeElement.value;
    if(+maxlength > 0)
    {
      if (initalValue.length > maxlength) {
        event.stopPropagation();
      }
    }  
  }

}