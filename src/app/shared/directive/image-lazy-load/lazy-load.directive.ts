import { Variables, lazyLoadImageDefault } from 'src/app/core/variables';
import { Directive, ElementRef, HostBinding, Input, Renderer2, OnDestroy, AfterContentInit, HostListener } from '@angular/core';
import { CommonMethodsTs } from "src/app/core/common-methods";
@Directive({
  selector: '[lazy-loading-image]',//img[lazyLoadImage]
})
export class LazyLoadDirective implements AfterContentInit, OnDestroy {
  @HostBinding('attr.src') attrSrc;
  @HostListener('error')
  public errorHandler() {
    // this.renderer.setStyle(this.el.nativeElement, 'visibility', "hidden");
  }

  @Input() src: string;
  private obs: any;
  public srcClone: any;
  private config = {
    // If the image gets within 50px in the Y axis, start the download.
    rootMargin: Variables.rootMarginLazyLoading,
    threshold: 0.01
  };
  constructor(private renderer: Renderer2, private el: ElementRef, public commonMethods: CommonMethodsTs) {

    // nguyencuongcs 20190830
    // if (el.nativeElement.classList.contains('loaded')) {
    //   return;
    // }

    // this.renderer.setStyle(this.el.nativeElement, 'visibility', "hidden");
    // this.renderer.addClass(this.el.nativeElement, 'loading');
  }

  async ngOnChanges() {
    this.srcClone = this.src || this.attrSrc;
    if (!this.srcClone) {
      return;
    }
    let wxh = this.commonMethods.getWxhByUrl(this.srcClone);
    
    if (!wxh || +wxh > Variables.wxhAllowLazyLoading) {
      this.srcClone = +wxh > 0 ? this.srcClone : this.commonMethods.replaceSizeImagesByViewPort(this.srcClone);
      wxh = this.commonMethods.getWxhByUrl(this.srcClone);
      if (!wxh || +wxh > Variables.wxhAllowLazyLoading) {
        //tronghuu95 2019091046 tạm đóng lại hình q1 => hạn cheess gọi api lên server
        // let urlq1 = this.commonMethods.setParamsUrl(this.srcClone, "q", 1);
        this.loadImage(lazyLoadImageDefault);
      }
    }

    this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
  }
  ngAfterContentInit() {
  }
  private canLazyLoad() {
    return window && 'IntersectionObserver' in window;
  }
  private lazyLoadImage() {
    setTimeout(() => {
      this.obs = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          this.imageLoader(entry);
        });
      }, this.config);
      this.obs.observe(this.el.nativeElement);
    }, 0);
  }
  async imageLoader(entry: IntersectionObserverEntry) {
    const { isIntersecting, target } = entry;
    try {
      if (!this.srcClone) {
        console.error('Lazy image element returned empty image source', target);
        return;
      }
      if (entry && entry.isIntersecting) {
        await this.loadImage()
      }
    } catch (exception) {
      console.error(`Image loader failed with ${target}`, exception);
    }
  }
  async loadImage(src?: any) {
    try {
      src = src || this.srcClone;
      if (src) {
        const image = new Image();
        await this.commonMethods.fetchImageMeta(image, src)
        if (image.src) {
          this.src = this.attrSrc = image.src;
          this.renderer.addClass(this.el.nativeElement, 'loaded');
          this.renderer.setStyle(this.el.nativeElement, 'visibility', "unset");
        }

      }
    } catch (error) {
    }

  }

  ngOnDestroy() {
    try {
      this.obs.observe();
    } catch (error) {
    }
  }
}
