import { CommonMethodsTs } from 'src/app/core/common-methods';
import { Pipe, PipeTransform } from '@angular/core';
import { CoreVariables } from 'src/app/core/core-variables';
import { Variables } from 'src/app/core/variables';

@Pipe({ name: 'dailyxeTimeAgo' })
export class DaiLyXeTimeAgoPipe implements PipeTransform {
  public static YEAR_MS: number = 1000 * 60 * 60 * 24 * 7 * 4 * 12;
  public static MAPPER: any = [
    { single: 'Năm trước', many: 'năm', div: 1 },
    { single: 'Tháng trước', many: 'tháng', div: 12 },
    { single: 'Tuần trước', many: 'tuần', div: 4 },
    { single: 'Hôm qua', many: 'ngày', div: 7 },
    { single: 'Một giờ trước', many: 'giờ', div: 24 },
    { single: 'Mới đây', many: 'phút', div: 60 },
  ];
  constructor() {

  }
  /**
   * @param inputDate: Date | Moment - not included as TypeScript interface,
   * in order to keep `ngx-pipes` "pure" from dependencies!
   */
  public transform(inputDate: any) {

    if (!(inputDate instanceof Date)) {
      inputDate = this.newDate(inputDate);
    }
    if (!inputDate || (!inputDate.getTime && !inputDate.toDate)) {
      return 'Ngày không hợp lệ';
    }

    const past = inputDate.toDate ? inputDate.toDate() : inputDate.getTime();
    const now = CoreVariables.timeDateServer;

    if (past > now) {
      return 'Trong tương lai';
    }

    for (let i = 0, l = DaiLyXeTimeAgoPipe.MAPPER.length, ms = now - past, div = DaiLyXeTimeAgoPipe.YEAR_MS; i < l; ++i) {
      const elm = DaiLyXeTimeAgoPipe.MAPPER[i];
      const unit = Math.floor(ms / (div /= elm.div));

      if (unit >= 1) {
        return unit === 1 ? elm.single : `${unit} ${elm.many} trước`;
      }
    }

    return 'Mới đây';
  }
  newDate(date?: any) {

    date = (date || new Date(CoreVariables.timeDateServer)).toString();
    let a = date.split(/[^0-9]/);
    return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);

  }
}
