import { Pipe, PipeTransform } from '@angular/core';
export enum typeTime {
	miliseconds = 'miliseconds',
	seconds = 'seconds',
	minutes = 'minutes',
	hours = 'hours'
}
@Pipe({ name: 'workAgo' })
export class WorkAgoPipe implements PipeTransform {
	// type = miliseconds, seconds, minutes, hours

	public transform(time: number, type: string = typeTime.miliseconds): string {
		if (!time) {
			return "";
		}
		switch (type) {
			case typeTime.hours:
				time = time * 60 * 60 * 1000;
				break;
			case typeTime.minutes:
				time = time * 60 * 1000;
				break;
			case typeTime.seconds:
				time = time * 1000;
				break;
			default:
				break;
		}
		let seconds = time / 1000;
		let days = Math.floor(seconds / 86400);
		seconds -= days * 86400;
		let hours = Math.floor(seconds / 3600) % 24;
		seconds -= hours * 3600;
		let minutes = Math.floor(seconds / 60) % 60;
		if (days && hours && minutes && seconds) {
			return days + ' ngày ' + hours + ' giờ ' + minutes + ' phút ';
		}
		if (days && hours && minutes) {
			return days + ' ngày ' + hours + ' giờ ' + minutes + ' phút ';
		}
		if (!days && hours && minutes) {
			return hours + ' giờ ' + minutes + ' phút ';
		}
		if (days && !hours && minutes) {
			return days + ' ngày ' + minutes + ' phút ';
		}
		if (days && hours && !minutes) {
			return days + ' ngày ' + hours + ' giờ ';
		}
		if (!days && hours && !minutes) {
			return hours + ' giờ ';
		}
		if (days && !hours && !minutes) {
			return days + ' ngày ';
		}
		if (!days && !hours && minutes) {
			return minutes + ' phút ';
		}
		else {
			return days + ' ngày ';
		}

	}
}
