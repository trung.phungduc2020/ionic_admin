import { NgModule } from '@angular/core';
import { TimeAgoPipe } from './time-ago';
import { DaiLyXeTimeAgoPipe } from './dailyxe-time-ago';
import { WorkAgoPipe } from './work-ago';

export const DATE_PIPES = [TimeAgoPipe, DaiLyXeTimeAgoPipe, WorkAgoPipe];

@NgModule({
  declarations: DATE_PIPES,
  imports: [],
  exports: DATE_PIPES,
})
export class NgDatePipesModule {}
export { TimeAgoPipe } from './time-ago';
export { DaiLyXeTimeAgoPipe } from './dailyxe-time-ago';
export { WorkAgoPipe } from './work-ago';
