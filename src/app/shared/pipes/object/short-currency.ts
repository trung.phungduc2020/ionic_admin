import { Pipe, PipeTransform } from '@angular/core';
import { isObject } from '../helpers/helpers';
import { DecimalPipe } from '@angular/common';
import { defaultValue } from 'src/app/core/variables';
@Pipe({ name: 'shortcurrency' })
export class ShortCurrencyPipe implements PipeTransform {
    private unit = {
        'copper': {
            'value': 1,
            'label': 'đồng'
        },
        'thousand': {
            'value': 1000,
            'label': 'nghìn'
        },
        'million': {
            'value': 1000000,
            'label': 'triệu'
        },
        'billion': {
            'value': 1000000000,
            'label': 'tỷ'
        }
    };
    //Hàm get Unit bằng value nếu typeUnit == null
    private getUnitByValue(value) {
        let i = Math.log(value) / Math.log(1000);
        let iValue = Math.pow(1000, i);
        if (iValue >= 1000000000) {
            return this.unit["billion"];
        }
        if (iValue >= 1000000) {
            return this.unit["million"];
        }
        if (iValue >= 1000) {
            return this.unit["thousand"];
        }
        return this.unit["copper"];
    }
    //Hàm định dạng curreny loại bỏ 0 không ý nghĩa
    private formatCurrency(currenry) {
        let lCurreny = currenry.split(".");
        let numberPart = lCurreny[0];
        let decimalPart = lCurreny[1];
        while (decimalPart && decimalPart.endsWith(0)) {
            decimalPart = decimalPart.substring(0, decimalPart.length - 1);
        }
        if (+decimalPart > 0) {
            return numberPart + "." + decimalPart;
        }
        else {
            return numberPart
        }
    }
    
    transform(amount: any, onlyString = false, typeUnit:any = defaultValue.typeUnit, numRound = 3): Object {
        try {
            let iCurrency = this.unit[typeUnit];

            if (!iCurrency) {
                iCurrency = this.getUnitByValue(amount)
            }

            amount = amount || 0; // không để giá trị bị lỗi
            let valueCurrency: any = amount / iCurrency['value'];
            valueCurrency = new DecimalPipe('en-US').transform(amount / iCurrency['value'], '1.2-' + numRound);  // giá trị
            valueCurrency = this.formatCurrency(valueCurrency)
            let typeCurrency = iCurrency['label']; // đơn vị
            if (onlyString) {
                return `${valueCurrency || 0} ${typeCurrency}`;
            }
            return {
                currency: valueCurrency,
                type: typeCurrency
            }
        } catch (error) {
            return amount;
        }

    }
}
