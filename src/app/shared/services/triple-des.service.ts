import { varks } from "src/app/core/variables";

declare var require: any;

const CryptoJS: any = require("crypto-js");

export class TripleDESService {
    // private key: any;
    private iv: any;
    private enabled: boolean;
    private options: any = {};
    public b64EncodeUnicode(str: string): string {
        if (window
            && "btoa" in window
            && "encodeURIComponent" in window) {
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => {
                return String.fromCharCode(("0x" + p1) as any);
            }));
        } else {
            console.warn("b64EncodeUnicode requirements: window.btoa and window.encodeURIComponent functions");
            return "";
        }
    }
    private keyDefault = varks.tdk;

    constructor() {
        this.iv = CryptoJS.lib.WordArray.create(64 / 8) // CryptoJS.enc.Utf8.parse('7061737323313233');//CryptoJS.lib.WordArray.create(64/8);
        this.options = {
            mode: CryptoJS.mode.ECB,
            // padding: CryptoJS.pad.ZeroPadding,
            // iv: this.iv
        };
        this.enabled = true;
    }

    convertKeyBase(keybase: string = this.keyDefault): string {
        if (!keybase) return null!;
        var key64 = this.b64EncodeUnicode(keybase);
        return CryptoJS.enc.Base64.parse(key64);

    }

    isEnabled(): boolean {
        return this.enabled;
    }

    // mã hóa động theo key
    encrypt(plaintext: string, keyBase?: string): string {
        if (!this.enabled)
            return plaintext;
        var encrypted = "";
        try {
            encrypted = CryptoJS.TripleDES.encrypt(plaintext, this.convertKeyBase(keyBase), this.options).toString();
        } catch (error) {
            return "";
        }
        return encrypted;
    }

    // dịch mã động theo key
    decrypt(encrypted: string, keyBase?: string): string {
        if (!this.enabled)
            return encrypted;

        var ct = {
            ciphertext: CryptoJS.enc.Base64.parse(encrypted)
        };
        var encrypted = "";
        try {

            encrypted = CryptoJS.TripleDES.decrypt(ct, this.convertKeyBase(keyBase), this.options).toString(CryptoJS.enc.Utf8);
        } catch {

            return "";
        }
        return encrypted;
    }
}