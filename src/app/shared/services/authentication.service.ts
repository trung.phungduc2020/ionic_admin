import { Injectable } from '@angular/core';
import { CommonMethodsTs } from 'src/app/core/common-methods';
import * as firebase from 'firebase';
import { localStorageData, Variables, isPhoneTestFireBase } from 'src/app/core/variables';

@Injectable()
export class FirebaseAuthenticationService {
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;

  constructor(private commonMethodsTs: CommonMethodsTs) {
    if (isPhoneTestFireBase) {
      firebase.auth().settings.appVerificationDisabledForTesting = Variables.isDev;
    }
  }
  // cực kì quan trọng
  initRecaptchaVerifier(idRecaptcha = "recaptcha-container") {
    try {
      try {
        this.recaptchaVerifier.clear();
      } catch { }
      this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(idRecaptcha, {
        'lang': 'vi',
      });
      this.recaptchaVerifier.render();
      return this.recaptchaVerifier;
    } catch (error) {

    }
    return this.recaptchaVerifier;
  }

  private async sendCode(phoneNumber: string) {
    if (isPhoneTestFireBase) {
      phoneNumber = "+1 650-555-3434";
    }
    else {
      phoneNumber = this.commonMethodsTs.formatNumberPhone(phoneNumber);
    }
    let provider = new firebase.auth.PhoneAuthProvider();
    let verificationId = await provider.verifyPhoneNumber(phoneNumber, this.recaptchaVerifier);
    return verificationId;
  }

  private async verify(phoneNumber: string, verificationId: any, smsCode: any) {
    phoneNumber = this.commonMethodsTs.formatNumberPhone(phoneNumber);//"+1 650-555-3434"
    let signalInCredential = firebase.auth.PhoneAuthProvider.credential(verificationId, smsCode)
    let info = await firebase.auth().signInWithCredential(signalInCredential);
    return info;

  }

  async sendCodePhoneNumber(phoneNumber: string, typeAuthFirebaseService: string) {

    let res = await this.sendCode(phoneNumber);
    await this.commonMethodsTs.storage.set(localStorageData.smsCode + typeAuthFirebaseService, res);
    return res;
  }
  async verifyPhoneNumber(phoneNumber: string, smsCode: any, typeAuthFirebaseService: string) {

    let verificationId = await this.commonMethodsTs.storage.get(localStorageData.smsCode + typeAuthFirebaseService);
    let res = await this.verify(phoneNumber, verificationId, smsCode);
    if (res) {
      await this.commonMethodsTs.storage.remove(typeAuthFirebaseService);

    }
    return res;
  }
}
