import { varks } from "src/app/core/variables";

declare var require: any;
// const crypto:any = require('crypto-browserify');
const JsEncryptModule: any = require('jsencrypt');

// const crypto:any = require('crypto-browserify');

export class RsaService {
  private privateKey: string;
  private publicKey: string;
  private publicKeyForApi: string;

  private enabled: boolean;

  constructor() {

    this.privateKey = varks.rrk;
    this.publicKey = varks.ruk;
    this.publicKeyForApi = varks.rukForApi;
    this.enabled = true;
  }

  isEnabled(): boolean {
    return this.enabled;
  }
  // mã hóa động theo key
  encrypt(plaintext: string, publicKey: string = this.publicKey): string {

    if (!this.enabled)
      return plaintext;
    var encrypt = new JsEncryptModule.JSEncrypt();
    encrypt.setPublicKey(publicKey);

    let encrypted: any = "";
    try {
      encrypted = encrypt.encrypt(plaintext);
    } catch {}
    return encrypted;
  }
  // dịch mã động theo key
  decrypt(encrypted: string, privateKey: string = this.privateKey): string {
    if (!this.enabled)
      return encrypted;
    var decrypt = new JsEncryptModule.JSEncrypt();
    decrypt.setPrivateKey(privateKey);
    let plaintext: any;
    try {
      plaintext = decrypt.decrypt(encrypted);

    } catch {}

    return plaintext
  }
  // mã hóa dữ liệu từ k được cung cấp bởi server khác, mặc định lấy key từ biến publicKeyForApi
  encryptForApi(plaintext: string, publicKeyForApi: string = this.publicKeyForApi): string {
    if (!publicKeyForApi) {
      return "";
    }
    return this.encrypt(plaintext, publicKeyForApi)
  }
  getKeyDes(): string {
    let keyDesBase = varks.tdk;

    if (!keyDesBase) {
      return "";
    }
    return this.encryptForApi(keyDesBase);
  }
}