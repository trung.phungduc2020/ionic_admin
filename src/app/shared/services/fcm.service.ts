import { Injectable } from '@angular/core';
import { FirebaseMessaging } from '@ionic-native/firebase-messaging/ngx';

@Injectable()
export class FcmService {

  constructor(private fcm: FirebaseMessaging) {
  }

  async requestPermission() {
    try {
      let res: any = await this.fcm.requestPermission();
      return res == true;
    } catch (error) {

    }
    return false;

  }
  addTopic(namTopic: string) {
    if (!namTopic) {
      return;
    }
    this.fcm.subscribe(namTopic);

  }

  addTopicAdmin(idAdmin: string) {
    if (!idAdmin) {
      return;
    }
    this.addTopic("Admin");
    this.addTopic("Admin-" + idAdmin);
  }

  removeTopic(namTopic: string) {
    if (!namTopic) {
      return;
    }
    this.fcm.unsubscribe(namTopic);

  }

  removeTopicAdmin(idAdmin: string) {
    if (!idAdmin) {
      return;
    }
    this.removeTopic("Admin");
    this.removeTopic("Admin-" + idAdmin);
  }

  async getToken() {
    let strToken = await this.fcm.getToken();
    return strToken;
  }

  onMessage() {

    return this.fcm.onMessage();
  }

  onBackgroundMessage() {

    return this.fcm.onBackgroundMessage();
  }

}
