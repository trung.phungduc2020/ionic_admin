import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class RaoService {

  constructor() { }
  public onReloadHome: EventEmitter<boolean> = new EventEmitter(); // theo dõi sự kiện load lại trang chủ
  public onReloadListHome: EventEmitter<boolean> = new EventEmitter(); // theo dõi sự kiện load lại trang chủ
  public onReloadNotification: EventEmitter<boolean> = new EventEmitter(); // theo dõi sự kiện load lại trang chủ
  public onPayMoMo: EventEmitter<any> = new EventEmitter(); // theo dõi sự kiện load lại trang chủ

}
