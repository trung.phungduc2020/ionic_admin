/**
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2017.
 *
 * ESTA WebJS: Message Servcice
 *
 * @author u218609 (Kevin Kreuzer)
 * @version: 2.0.0
 * @since 28.04.2017, 2017.
 */

import { Injectable } from '@angular/core';
import { RsaService } from './rsa.service';
import { TripleDESService } from './triple-des.service';
import { FcmService } from './fcm.service';
import { ToastService } from './toast.service';
import { WindowService } from './window.service';
import { RaoService } from './rao.service';
import { RaoLocalStore } from 'src/app/core/rao-localstore';

@Injectable()
export class MainServices {
    constructor(
        public rsaService: RsaService,
        public tripleDESService: TripleDESService,
        public fcmService: FcmService,
        public toastService: ToastService,
        public windowService: WindowService,
        public raoService: RaoService,
        public raoLocalStore: RaoLocalStore,
       ) {
    }
}

export { RsaService } from './rsa.service';
export { TripleDESService } from './triple-des.service';
export { FcmService } from './fcm.service';
export { ToastService } from './toast.service';
export { RaoService } from './rao.service';
export { RaoLocalStore } from 'src/app/core/rao-localstore';
