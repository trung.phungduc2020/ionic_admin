import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RsaService } from "./services/rsa.service";
import { TripleDESService } from "./services/triple-des.service";
import { MainServices, RaoService, RaoLocalStore } from "./services";
import { FcmService } from "./services/fcm.service";
import { ToastService } from "./services/toast.service";
import { WindowService } from "./services/window.service";
import { FirebaseMessaging } from '@ionic-native/firebase-messaging/ngx';
import {NgxImageCompressService} from 'ngx-image-compress';
import { FirebaseAuthenticationService } from './services/authentication.service';

@NgModule({
    imports: [CommonModule],
    declarations: [
    ],
    exports: [
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                FirebaseMessaging,
                NgxImageCompressService,
                MainServices,
                RsaService,
                TripleDESService,
                FcmService,
                ToastService,
                WindowService,
                RaoService,
                RaoLocalStore,
                FirebaseAuthenticationService
            ]
        };
    }
}
