import { IonicRatingModule } from 'ionic-rating';
import { CoreModule } from './core/index';
import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy, NavController, AlertController } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { SharedModule } from './shared/shared.module';
import { Events } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { CurrencyPipe } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { RecaptchaModule } from 'ng-recaptcha';
import { Platform } from '@ionic/angular';
import { ScreenOrientation } from '../../node_modules/@ionic-native/screen-orientation/ngx';
import { CoreProcess } from './core/core-process';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import * as firebase from 'firebase';
import { CommonService } from './forms/member/services/common.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CacheModule } from "ionic-cache";
import { RouterExtService } from './shared/services/router-ext.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Device } from '@ionic-native/device/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { environment } from "src/environments/environment";
import { NativeHttpFallback, NativeHttpBackend, NativeHttpModule } from './core/native-http';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ViewBaiVietModule } from './components/views/view-baiviet/view-baiviet.module';
import { PictureMethodsTs } from './core/picture-methods';
import './core/extension-methods.ts';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { IonicGestureConfig } from './shared/ionic-gesture-config';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { CustomInputDirectiveModule } from './shared/directive/custom-input/custom-input.module';
import { Keyboard } from '@ionic-native/keyboard/ngx';

firebase.initializeApp(environment.firebaseConfig);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NativeHttpModule,
    NgxSpinnerModule,
    CoreModule,
    AppRoutingModule,
    IonicModule.forRoot({
      mode: 'md',
      //rippleEffect: false,
      scrollAssist: false,
      scrollPadding: false,

    }),
    IonicStorageModule.forRoot(),
    SharedModule.forRoot(),
    RecaptchaModule.forRoot(),
    IonicRatingModule,
    ImageCropperModule,
    CacheModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirePerformanceModule,
    ViewBaiVietModule,
    CustomInputDirectiveModule

  ],

  providers: [
    Keyboard,
    UniqueDeviceID,
    BackgroundMode,
    WebView,
    Firebase,
    AngularFirestore,
    Geolocation,
    CoreProcess,
    NavController,
    ImagePicker,
    Events,
    CurrencyPipe,
    FormBuilder,
    Platform,
    AlertController,
    CommonService,
    AppVersion,
    Market,
    InAppBrowser,
    Device,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ScreenOrientation,
    AndroidPermissions,
    Geolocation,
    OpenNativeSettings,
    RouterExtService,
    { provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend] },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
    Clipboard,
    CallNumber,
    PictureMethodsTs,
  ],
  bootstrap: [AppComponent],


})
export class AppModule { }