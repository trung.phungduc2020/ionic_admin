export const environment:any = {
  production: false,
  hello: 'hello dev',
  projectName: "raoxe",
  firebaseConfig: {
    apiKey: "AIzaSyCDH7sZaDUhaDW4SxHS55gf6ZhNPbh9wj0",
    authDomain: "dailyxe-ionic-raovat.firebaseapp.com",
    databaseURL: "https://dailyxe-ionic-raovat.firebaseio.com",
    projectId: "dailyxe-ionic-raovat",
    storageBucket: "dailyxe-ionic-raovat.appspot.com",
    messagingSenderId: "3702342930",
    appId: "1:3702342930:web:5cfb439346534589"
  },
  //Hình ảnh upload khi vượt qua byte này sẽ bị nén lại
  limitByteForImage: 300000,
  limitQualityForImage: 85,
  compressedForImageX: 900,
  compressedForImageY: 1200,

  actionDeepLink: "rao-xe",
  asyceUploadFile: false,
  autoUpdateLayoutImages: true,
  // Stage Config DailyXe
  // apiHost
  apiHost: "https://stgapirv01.dailyxe.com.vn",
  apiHost_Sufix_DB: `/api/raovat/`,
  apiHistoryHost_Sufix_DB: `/api/raovat_history/`,
  // apiDaiLyXe
  apiDaiLyXe: "https://stgapidlxad02.dailyxe.com.vn",

  apiDaiLyXe_Sufix_DB: "/api/dailyxe/",
  // apiCdn
  apiCdn: "https://stgcdndlxad02.dailyxe.com.vn",
  //DynamicLinks
  domainDynamicLinks: "https://raoxe.page.link",
  domainDeepLinks: "https://dailyxe.com.vn",
  appStoreID: "1486119532",
  //Web tin tức
  apiHostDailyXe: "https://docker.dailyxe.com.vn",
  //time timeout cho api
  timeoutApi: 2,
  listDomainCheckConfig: ["https://stgapirv01.dailyxe.com.vn", "https://stgapirv01.dailyxe.com.vn"],
  //Số lần gửi lại mã sms tối đa với 1 type
  smsLoopDefault: 3,
  //Time gian khi bị lock khi gửi sms 1 type quá smsLoopDefault lần
  smsTimeBetaDefault: 15,
  //Time block khi firebase bị khóa khi gửi nhiều
  smsTimeBlockForFireBase: 60,
  //Thời gian hiệu lực của mã xác thực dùng chung email và sdt
  timeVerifyValid: 2, // phút
  //Dùng sms3G KhiGuiLanThu;
  dungSms3GKhiGuiXongLanThu: 4, //dungSms3GKhiGuiXongLanThu > smsLoopDefault sẽ không dùng sms của serve
  //Domain cho ES
  domainES:"https://stgesdlx02.dailyxe.com.vn",
  tuKhoaHot: ["mua xe", "bán xe", "xe tải", "xe cũ", "mua xe cũ", "xe gia đình", "xe du lịch", "xe bán tải", "xe giá rẻ"],
  //Có muốn dùng Anywhere để vượt qua cors ở môi trường test
  haveAnywhere: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
