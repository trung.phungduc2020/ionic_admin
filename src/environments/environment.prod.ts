export const environment: any = {
  production: true,
  projectName: "raoxe",
  firebaseConfig: {
      apiKey: "AIzaSyDt4yxLKNWxMeU1lf-rwqGtVjBiZiOAgXk",
      authDomain: "raovat-257509.firebaseapp.com",
      databaseURL: "https://raovat-257509.firebaseio.com",
      projectId: "raovat-257509",
      storageBucket: "raovat-257509.appspot.com",
      messagingSenderId: "170518340284",
      appId: "1:170518340284:web:f73764e4bb8c05a49b66b9",
      measurementId: "G-QGRTD6E9ZF"
  },
  //Hình ảnh upload khi vượt qua byte này sẽ bị nén lại
  limitByteForImage: 300000,
  limitQualityForImage: 85,
  compressedForImageX: 900,
  compressedForImageY: 1200,

  asyceUploadFile: false,
  actionDeepLink: "rao-xe",

  autoUpdateLayoutImages: false,
  
  // Stage Config DailyXe
  // apiHost
  apiHost: "https://prodapirv01.dailyxe.com.vn",
  apiHost_Sufix_DB: `/api/raovat/`,
  apiHistoryHost_Sufix_DB: `/api/raovat_history/`,
  // apiDaiLyXe
  apiDaiLyXe: "https://api.dailyxe.com.vn",

  apiDaiLyXe_Sufix_DB: "/api/dailyxe/",
  // apiCdn
  apiCdn: "https://cdn.dailyxe.com.vn",
  //DynamicLinks
  domainDynamicLinks: "https://raoxe.dailyxe.com.vn/rao",
  domainDeepLinks: "https://dailyxe.com.vn",
  appStoreID: "1486119532",
  //Web tin tức
  apiHostDailyXe: "https://dailyxe.com.vn",
  //time timeout cho api
  timeoutApi: 2,

  listDomainCheckConfig: ["https://apidlxad02.dailyxe.com.vn", "https://apidlxad01.dailyxe.com.vn"],

  //Số lần gửi lại mã sms tối đa với 1 type
  smsLoopDefault: 3,
  //Time gian khi bị lock khi gửi sms 1 type quá smsLoopDefault lần
  smsTimeBetaDefault: 15,
  //Time block khi firebase bị khóa khi gửi nhiều
  smsTimeBlockForFireBase: 60,
  //Thời gian hiệu lực của mã xác thực dùng chung email và sdt
  timeVerifyValid: 2, // phút
  //Dùng sms3G KhiGuiLanThu;
  dungSms3GKhiGuiXongLanThu: 4, // dungSms3GKhiGuiXongLanThu > smsLoopDefault sẽ không dùng sms của serve
  //Domain cho ES
  domainES: "https://es01.dailyxe.com.vn",
  tuKhoaHot: ["mua xe", "bán xe", "xe tải", "xe cũ", "mua xe cũ", "xe gia đình", "xe du lịch", "xe bán tải", "xe giá rẻ"],
  haveAnywhere: false,
};
