import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Variables, lHostUseDev } from './app/core/variables';
declare var window: any;
import "hammerjs"; // HAMMER TIME

/* tronghuu95 20190903110400 xử lý biến cho từng loại môi trường */
if (environment.production) {
  enableProdMode(); // mặc định khi môi trương là Prod
  let host = window.location.host;
  /*tronghuu95 kiểm tra domain có phải giành cho Prod*/
  if (lHostUseDev.indexOf(host)>=0) {
    Variables.functionDev();
  }
  else {
    Variables.functionProd();
  }
}
else {
  //môi trường Dev
  Variables.functionDev();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch();
